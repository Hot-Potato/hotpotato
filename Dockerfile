FROM node:11

RUN mkdir /theme
WORKDIR /theme
COPY ["theme/package.json", "theme/package-lock.json", "/theme/"]
RUN rm -rf node_modules
RUN npm ci --dev
COPY ["theme/", "/theme/"]
RUN npm run build


FROM python:3.6

ARG HOTPOTATO_BUILD_DEV
ARG pip_version="19.2.1"
ARG pipenv_version="2018.11.26"

ENV HOTPOTATO_LOG_DIR="${HOTPOTATO_LOG_DIR:-/var/log/hotpotato}"
ENV HOTPOTATO_HOTPOTATO_LOG_LEVEL="${HOTPOTATO_HOTPOTATO_LOG_LEVEL:-WARNING}"
ENV FLASK_APP="hotpotato.app.main:app"

ENV FLASK_DEBUG="${FLASK_DEBUG:-false}"

RUN mkdir -p /code
WORKDIR /code
RUN mkdir -p "${HOTPOTATO_LOG_DIR}"
RUN mkdir -p /etc/hotpotato/conf.d
RUN python3 -m venv /venv

ENV PATH="/venv/bin:${PATH}"
ENV VIRTUAL_ENV="/venv"
ENV PYTHONUNBUFFERED=1

RUN pip install --upgrade setuptools wheel "pip==${pip_version}" "pipenv==${pipenv_version}" "dumb-init~=1.2"

COPY ["docker/install", "/install"]
COPY ["docker/start", "/start"]

RUN chmod +x /install /start

COPY [".", "/code"]

RUN /install

COPY --from=0 ["/theme/dist", "/code/theme/dist"]

RUN pipenv run flask digest compile

ENTRYPOINT ["dumb-init", "/start"]

CMD ["gunicorn", "--log-level", "${HOTPOTATO_HOTPOTATO_LOG_LEVEL}", "--bind=0.0.0.0:8000", "hotpotato.app.main:app"]

EXPOSE 8000
