"""
Heartbeat functions.
"""


from datetime import datetime, timedelta

import sqlalchemy

from hotpotato.models import Heartbeat as Model, db


def create(server_id):
    """
    Create a new heartbeat for the server with the given ID.
    """

    heartbeat = Model(server_id=server_id, received_dt=datetime.utcnow())

    db.session.add(heartbeat)
    db.session.commit()

    return heartbeat


def get_last_query():
    """
    Return an SQL query object for finding the last heartbeat for each server.
    If used as a subquery, the last heartbeat can be accessed under the "last_hbeat" field,
    like so:

        subquery.c.last_hbeat

    """

    # TODO: optimise this query so it doesn't take forever
    # e.g. SELECT received_dt FROM heartbeats ORDER BY received_dt DESC LIMIT 1
    return db.session.query(
        Model.server_id, sqlalchemy.func.max(Model.received_dt).label("last_hbeat")
    ).group_by(Model.server_id)


def get_last(server_id=None):
    """
    Get the last heartbeat for each server, or a given server ID.
    """

    if server_id:
        # Slightly optimised query for selecting an individual server ID.
        return (
            db.session.query(Model.received_dt)
            .filter(Model.server_id == server_id)
            .order_by(sqlalchemy.desc(Model.received_dt))
            .first()
        )
    else:
        return dict(get_last_query().all())


def delete_old():
    """
    Delete all rows in the heartbeats table older than 1 day.
    """

    today = datetime.now()
    delta = timedelta(days=1)

    cuttoff_dt = today - delta

    db.session.query(Model).filter(Model.received_dt < cuttoff_dt).delete(
        synchronize_session=False
    )
    db.session.commit()
