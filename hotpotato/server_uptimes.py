"""
Functions for the server_uptimes tables.
"""


import calendar
import decimal
from datetime import datetime, timedelta, timezone as dt_timezone

import sqlalchemy
from flask_babelex import gettext as _

from hotpotato.models import Heartbeat, ServerUptime, db

LENGTH_1D = timedelta(days=1)
LENGTH_7D = timedelta(days=7)


#
# Helper classes.
#


class GetArgConflictError(Exception):
    """
    Raised when get() gets more than one argument when it only accepts one.
    """

    def __init__(self, *args):
        """"""

        params = args[0]
        args = args[1:]

        super().__init__(
            _("{} defined, but only one of {} can be defined at once").format(
                ", ".join(params[:-1])
                + (_(" and {}").format(params[-1]) if len(params) > 1 else ""),
                ", ".join(args[:-1])
                + (_(" and {}").format(args[-1]) if len(args) > 1 else ""),
            )
        )


# pylint: disable=too-many-ancestors
class age(sqlalchemy.sql.functions.GenericFunction):  # noqa: N801
    """
    SQLAlchemy definition for the CockroachDB 'age' function.
    Accessible using 'sqlalchemy.func.age' or 'sqlalchemy.func.timestamp.age'.

    Usage (this gets confusing due to poorly named arguments in the CockroachDB documentation):
    * sqlalchemy.func.timestamp.age(DateTime val) -> Interval
    * sqlalchemy.func.timestamp.age(DateTime time, DateTime val) -> Interval

    NOTE: age() can ONLY be used with a DateTime(timezone=True), or a TIMESTAMPTZ in CockroachDB,
    so either use a column defined using that or use sqlalchemy.sql.expressions.cast to cast to
    that.
    """

    type = sqlalchemy.types.Interval
    package = "timestamp"


# pylint: disable=too-many-ancestors
class extract(sqlalchemy.sql.functions.GenericFunction):  # noqa: N801
    """
    SQLAlchemy definition for the CockroachDB 'extract' function.
    Accessible using 'sqlalchemy.func.extract' or 'sqlalchemy.func.timestamp.extract'.

    Usage:
    * sqlalchemy.func.timestamp.extract(String element, DateTime input) -> Integer
    """

    type = sqlalchemy.types.DateTime
    package = "timestamp"


#
# Helper functions.
#


def get(server_uptime_id=None, server_id=None):
    """
    Return server_uptime objects.
    If no parameters specified, get all server_uptime objects.
    Otherwise, filter by the desired fields.
    """

    if server_uptime_id and server_id:
        raise GetArgConflictError(
            ["server_uptime_id", "server_id"], "server_uptime_id", "server_id"
        )
    elif server_uptime_id:
        # Uses one() for error-reporting purposes. If using a server_uptimes ID,
        # that information likely was received earlier from the database.
        return ServerUptime.query.filter_by(id=server_uptime_id).one()
    elif server_id:
        # Uses one_or_none() for error-reporting purposes. server_id should be unique
        # in server_uptimes but it is not enforced due to it not being a primary key.
        # TODO: Enforce using SQL constraints.
        return ServerUptime.query.filter_by(server_id=server_id).one_or_none()
    else:
        return {suptime.server_id: suptime for suptime in ServerUptime.query.all()}


def subtract_months_get_year(date, months):
    """
    Get the resulting year from subtracting the given months from the given date.
    """

    years = months // 12
    remain_months = months - (years * 12)

    year = date.year - years
    if (date.month - remain_months) < 1:
        year -= 1

    return year


def subtract_months_get_month(date, months):
    """
    Get the resulting month from subtracting the given months from the given date.
    """

    years = months // 12
    remain_months = months - (years * 12)

    res = (date.month - remain_months) % 12
    return 12 if res == 0 else res


#
# Command line interface (CLI) functions.
#


def update_avail_last_1d_7d():
    """
    Update the server_uptimes table with the availability percentages
    for the past day and 7 days for each server.
    """

    now = datetime.utcnow()

    # Heartbeat frequency, in seconds.
    # Ideally should be configurable, and the function is designed around this.
    # TODO: Make hbeat_freq configurable.
    hbeat_freq = 60
    # Number of heartbeats in one day and one week.
    hbeat_1d_num = int(LENGTH_1D.total_seconds()) // hbeat_freq
    hbeat_7d_num = int(LENGTH_7D.total_seconds()) // hbeat_freq

    # Query the database for all the server_uptime row objects.
    suptimes = get()

    # Monsterous query which generates the number of heartbeats from the last 1 day and 7 day
    # for each server.
    subquery_1d = (
        db.session.query(
            Heartbeat.server_id,
            sqlalchemy.func.count(Heartbeat.server_id).label("count"),
        )
        .filter(
            sqlalchemy.func.age(
                # Converted now and received_dt to timezone-aware timestamps,
                # as that is the only format age() supports.
                now.replace(tzinfo=dt_timezone.utc),
                sqlalchemy.sql.expression.cast(
                    Heartbeat.received_dt, sqlalchemy.types.DateTime(timezone=True)
                ),
            )
            < timedelta(days=1)
        )
        .group_by(Heartbeat.server_id)
        .subquery()
    )

    subquery_7d = (
        db.session.query(
            Heartbeat.server_id,
            sqlalchemy.func.count(Heartbeat.server_id).label("count"),
        )
        .filter(
            sqlalchemy.func.age(
                # Converted now and received_dt to timezone-aware timestamps,
                # as that is the only format age() supports.
                now.replace(tzinfo=dt_timezone.utc),
                sqlalchemy.sql.expression.cast(
                    Heartbeat.received_dt, sqlalchemy.types.DateTime(timezone=True)
                ),
            )
            < timedelta(days=7)
        )
        .group_by(Heartbeat.server_id)
        .subquery()
    )

    server_id_num_hbeats = (
        db.session.query(Heartbeat.server_id, subquery_1d.c.count, subquery_7d.c.count)
        .distinct()
        .outerjoin(subquery_1d, subquery_1d.c.server_id == Heartbeat.server_id)
        .outerjoin(subquery_7d, subquery_7d.c.server_id == Heartbeat.server_id)
        .group_by(Heartbeat.server_id, subquery_1d.c.count, subquery_7d.c.count)
        .all()
    )

    # Determine the availability percentages for the last 1 day and 7 days for each server,
    # and update the server_uptimes table accordingly.
    for server_id, num_hbeats_1d, num_hbeats_7d in server_id_num_hbeats:
        avail_1d = (
            decimal.Decimal((num_hbeats_1d / hbeat_1d_num) * 100)
            if num_hbeats_1d
            else 0.0
        )
        avail_7d = (
            decimal.Decimal((num_hbeats_7d / hbeat_7d_num) * 100)
            if num_hbeats_7d
            else 0.0
        )
        if server_id in suptimes:
            suptimes[server_id].updated_dt = now
            suptimes[server_id].avail_last_1d = avail_1d
            suptimes[server_id].avail_last_7d = avail_7d
        else:
            db.session.add(
                ServerUptime(
                    server_id=server_id,
                    updated_dt=now,
                    avail_last_1d=avail_1d,
                    avail_last_7d=avail_7d,
                )
            )
    db.session.commit()


def update_avail_last_month():
    """
    Update the server_uptimes table with the availability percentages
    for the previous month for each server.
    """

    now = datetime.utcnow()
    today = now.date()

    # Work out the specific year/month to query for, as well as the number of
    # days in that month.
    year = subtract_months_get_year(today, 1)
    month = subtract_months_get_month(today, 1)
    month_length = calendar.monthrange(year, month)[1]

    # Heartbeat frequency, in seconds.
    # Ideally should be configurable, and the function is designed around this.
    # TODO: Make hbeat_freq configurable.
    hbeat_freq = 60
    # Number of heartbeats for the given month length.
    hbeat_month_num = (int(LENGTH_1D.total_seconds()) // hbeat_freq) * month_length

    # Query the database for all the server_uptime row objects.
    suptimes = get()

    # Query the database for a filtered list of heartbeats which show the
    # number of heartbeats for the given month for each server.
    server_id_num_hbeats = (
        db.session.query(
            Heartbeat.server_id, sqlalchemy.func.count(Heartbeat.server_id)
        )
        .filter(
            sqlalchemy.func.extract("year", Heartbeat.received_dt) == year,
            sqlalchemy.func.extract("month", Heartbeat.received_dt) == month,
        )
        .group_by(Heartbeat.server_id)
        .all()
    )

    # Determine the availability percentage for the last month for each server,
    # and update the server_uptimes table accordingly.
    for server_id, num_hbeats in server_id_num_hbeats:
        avail = (
            decimal.Decimal((num_hbeats / hbeat_month_num) * 100) if num_hbeats else 0.0
        )
        if server_id in suptimes:
            suptimes[server_id].updated_dt = now
            suptimes[server_id].avail_last_month = avail
        else:
            db.session.add(
                ServerUptime(
                    server_id=server_id, updated_dt=now, avail_last_month=avail
                )
            )

    db.session.commit()


def get_avail_between(start_date, end_date, server=None, server_id=None):
    """
    Get availability statistics between a start and end date,
    for all servers, or optionally a given server.
    """

    if start_date >= end_date:
        raise ValueError(_("start_date needs to be earlier than end_date"))

    # TODO: Make hbeat_freq configurable.
    hbeat_freq = 60
    interval = (end_date - start_date).total_seconds() / hbeat_freq

    if server:
        server_id = server.id
    if server_id:
        return (
            db.session.query(
                (sqlalchemy.func.count(Heartbeat.server_id) / interval) * 100
            )
            .filter(
                Heartbeat.server_id == server_id,
                Heartbeat.received_dt > start_date,
                Heartbeat.received_dt < end_date,
            )
            .first()[0]
        )
    else:
        return (
            db.session.query(
                Heartbeat.server_id,
                (sqlalchemy.func.count(Heartbeat.server_id) / interval) * 100,
            )
            .filter(
                Heartbeat.received_dt > start_date, Heartbeat.received_dt < end_date
            )
            .group_by(Heartbeat.server_id)
            .all()
        )
