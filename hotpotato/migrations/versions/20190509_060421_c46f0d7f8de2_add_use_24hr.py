"""
add use_24hr

Revision ID: c46f0d7f8de2
Revises: ada46006eedf
Create Date: 2019-05-09 06:04:21.399279
"""


import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "c46f0d7f8de2"
down_revision = "02da5481b9d3"
branch_labels = None
depends_on = None


def upgrade():
    """
    Upgrade the database schema.
    """

    print("Performing schema upgrade...")

    op.add_column(
        "user",
        sa.Column("use_24hr", sa.Boolean(), nullable=False, server_default="false"),
    )

    print("Schema upgrade complete.")


def downgrade():
    """
    Downgrade the database schema to the previous version.
    """

    print("Performing schema downgrade...")

    op.drop_column("user", "use_24hr")

    print("Schema downgrade complete.")
