"""
Remove heartbeats enabled

Revision ID: fe2ba1605649
Revises: 09e1b250e671
Create Date: 2019-07-02 02:34:35.364040
"""


import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "fe2ba1605649"
down_revision = "907b276b7110"
branch_labels = None
depends_on = None


def upgrade():
    """
    Upgrade the database.
    """
    op.execute(
        "UPDATE servers SET disable_missed_heartbeat_notif = true WHERE enable_heartbeat = false"
    )
    op.drop_column("servers", "enable_heartbeat")


def downgrade():
    """
    Downgrade the database to the previous version.
    """
    op.add_column(
        "servers", sa.Column("enable_heartbeat", sa.Boolean(), server_default="true")
    )
    op.alter_column("servers", "enable_heartbeat", server_default=None)
