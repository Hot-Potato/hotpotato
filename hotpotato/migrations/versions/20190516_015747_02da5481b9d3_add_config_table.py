"""
Add config table

Revision ID: 02da5481b9d3
Revises: ada46006eedf
Create Date: 2019-05-16 01:57:47.315211
"""


import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "02da5481b9d3"
down_revision = "ada46006eedf"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "config",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("key", sa.Text(), nullable=False, unique=True),
        sa.Column("value", sa.Text(), nullable=True),
        sa.PrimaryKeyConstraint("id"),
    )

    op.execute(
        "INSERT INTO config (key, value) VALUES ('hotpotato_team_id', (SELECT id FROM team WHERE name = 'Hot Potato')::STRING)"
    )


def downgrade():
    op.drop_table("config")
