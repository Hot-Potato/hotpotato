"""
Migrate message body

Revision ID: d86032547328
Revises: 7fe1a193b6ce
Create Date: 2018-12-20 22:05:45.136990
"""


import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "d86032547328"
down_revision = "7fe1a193b6ce"
branch_labels = None
depends_on = None


def row_select(session, table, *columns, **search_params):
    """
    Return the results of a SELECT query.

    Returns a generator of single elements if there was only one column specified,
    otherwise returns a generator of arrays of elements.
    """

    cols = [table.columns[col] for col in columns]

    query = session.query(*cols)

    if search_params:
        for key, value in search_params.items():
            query = query.filter(table.columns[key] == value)

    return (data[0] for data in query) if len(columns) == 1 else query


def row_update(table, row_id, **columns):
    """
    Update a row on the given table with the given column values.
    """

    op.execute(table.update().where(table.c.id == row_id).values(**columns))


def upgrade():
    """
    Upgrade the database.
    """

    print("Performing upgrade migration...")

    data_upgrade()

    print("Upgrade migration complete.")


def downgrade():
    """
    Downgrade the database to the previous version.
    """

    print("Performing downgrade migration...")

    data_downgrade()

    print("Downgrade migration complete.")


def data_upgrade():
    """
    Upgrade the database data.
    """

    bind = op.get_bind()
    session = sa.orm.Session(bind=bind)

    user = sa.Table(
        "user",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.Text),
    )

    notif_types = frozenset(("alert", "handover", "message"))

    notification_log = sa.Table(
        "notification_log",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("tenant_id", sa.Integer),
        sa.Column("user_id", sa.Integer, sa.ForeignKey("User.id", ondelete="SET NULL")),
        sa.Column("received_dt", sa.DateTime, nullable=False),
        sa.Column("notif_type", sa.Enum(*notif_types), nullable=False),
        sa.Column("body", sa.Text, nullable=False),
        sa.Column(
            "json", sa.ext.mutable.MutableDict.as_mutable(sa.JSON), nullable=False
        ),
    )

    messages = row_select(
        session, notification_log, "id", "body", "json", notif_type="message"
    )

    for message in messages:
        message_body = message[1]
        json = message[2]
        json["message"] = message_body
        body = (
            "Message from {} via Hot Potato: {}".format(
                row_select(session, user, "name", id=json["from_user_id"]), message_body
            )
            if json["from_user_id"]
            else "Message from Hot Potato: {}".format(message_body),
        )

        row_update(notification_log, message[0], body=body, json=json)

    session.commit()


def data_downgrade():
    """
    Downgrade the database data to the previous version.
    """

    bind = op.get_bind()
    session = sa.orm.Session(bind=bind)

    notif_types = frozenset(("alert", "handover", "message"))

    notification_log = sa.Table(
        "notification_log",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("tenant_id", sa.Integer),
        sa.Column("user_id", sa.Integer, sa.ForeignKey("User.id", ondelete="SET NULL")),
        sa.Column("received_dt", sa.DateTime, nullable=False),
        sa.Column("notif_type", sa.Enum(*notif_types), nullable=False),
        sa.Column("body", sa.Text, nullable=False),
        sa.Column(
            "json", sa.ext.mutable.MutableDict.as_mutable(sa.JSON), nullable=False
        ),
    )

    messages = row_select(session, notification_log, "id", "json", notif_type="message")

    for message in messages:
        json = message[1]
        body = json["message"]
        del json["message"]

        row_update(notification_log, message[0], body=body, json=json)

    session.commit()
