"""
Add granular permissions

Revision ID: 907b276b7110
Revises: e079b8ae2b1e
Create Date: 2019-03-28 22:17:37.254527
"""


import sqlalchemy as sa
from alembic import context, op
from werkzeug.datastructures import ImmutableDict

# revision identifiers, used by Alembic.
revision = "907b276b7110"
down_revision = "09e1b250e671"
branch_labels = None
depends_on = None


def upgrade():
    """
    Upgrade the database.
    """

    print("Performing upgrade migration...")

    with context.begin_transaction():
        schema_upgrade()
    data_upgrade()

    print("Upgrade migration complete.")


def downgrade():
    """
    Downgrade the database to the previous version.
    """

    print("Performing downgrade migration...")

    data_downgrade()
    with context.begin_transaction():
        schema_downgrade()

    print("Downgrade migration complete.")


def schema_upgrade():
    """
    Upgrade the database schema.
    """

    print("Performing schema upgrade...")

    print("Creating table 'permission'...")
    op.create_table(
        "permission",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.Text(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("name"),
    )
    print("Creating table 'role_permission'...")
    op.create_table(
        "role_permission",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("permission_id", sa.Integer(), nullable=False),
        sa.Column("role_id", sa.Integer(), nullable=False),
        sa.Column("access", sa.Boolean(), nullable=False),
        sa.ForeignKeyConstraint(
            ["permission_id"], ["permission.id"], ondelete="CASCADE"
        ),
        sa.ForeignKeyConstraint(["role_id"], ["role.id"], ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("id"),
    )
    op.add_column(
        "role", sa.Column("priority", sa.Integer(), nullable=False, server_default="0")
    )
    # ### end Alembic commands ###

    print("Schema upgrade complete.")


def schema_downgrade():
    """
    Downgrade the database schema to the previous version.
    """

    print("Performing schema downgrade...")

    print("Dropping table 'role_permission'...")
    op.drop_table("role_permission")
    print("Dropping table 'permission'...")
    op.drop_table("permission")
    op.execute("DROP INDEX IF EXISTS role@role_team_id_priority_key CASCADE")
    op.drop_column("role", "priority")

    print("Schema downgrade complete.")


permissions = ImmutableDict(
    {
        "handover": ImmutableDict(
            {"do": ImmutableDict({"description": "Perform a handover"})}
        ),
        "messages": ImmutableDict(
            {"send": {"description": "Send a message to the on-call persion"}}
        ),
        "notifications": ImmutableDict(
            {
                "get": ImmutableDict(
                    {"description": "Get and search for notifications"}
                ),
                "acknowledge": ImmutableDict(
                    {"description": "Acknowledge a notification that failed to send"}
                ),
            }
        ),
        "servers": ImmutableDict(
            {
                "get": ImmutableDict({"description": "Get the status of servers"}),
                "add": ImmutableDict({"description": "Add a new server"}),
                "heartbeats_toggle": ImmutableDict(
                    {"description": "Turn on or off heartbeats for a server"}
                ),
            }
        ),
        "teams": ImmutableDict(
            {
                "create": ImmutableDict({"description": "Create a new team"}),
                "edit": ImmutableDict({"description": "Edit an existing team"}),
                "user_add": ImmutableDict({"description": "Add users to a team"}),
            }
        ),
        "users": ImmutableDict(
            {
                "create": ImmutableDict({"description": "Create a new user"}),
                "get": ImmutableDict({"description": "View the details of a user"}),
                "toggle_active": ImmutableDict(
                    {"description": "Activate or deactivate a user account"}
                ),
            }
        ),
    }
)


def permission_name_get(module_name, permission_key):
    """
    Generate a permission name from its module name and permission key.
    """

    return f"{module_name}.{permission_key}"


PERMISSIONS_BY_NAME = {}
for module_name, module_data in permissions.items():
    for permission_key, permission_data in module_data.items():
        PERMISSIONS_BY_NAME[
            permission_name_get(module_name, permission_key)
        ] = permission_data
PERMISSIONS_BY_NAME = ImmutableDict(PERMISSIONS_BY_NAME)

roles_permissions = {
    "admin": {
        "description": "Administrator",
        "priority": 0,
        "permissions": [
            "handover.do",
            "messages.send",
            "notifications.get",
            "notifications.acknowledge",
            "servers.get",
            "servers.add",
            "servers.heartbeats_toggle",
            "teams.create",
            "teams.edit",
            "teams.user_add",
            "users.create",
            "users.get",
            "users.toggle_active",
        ],
    },
    "oncall": {
        "description": "On-call User",
        "priority": 1,
        "permissions": [
            "handover.do",
            "messages.send",
            "notifications.get",
            "notifications.acknowledge",
        ],
    },
    "guest": {
        "description": "Guest",
        "priority": 2,
        "permissions": ["notifications.get"],
    },
}


def data_upgrade():
    """
    Upgrade the database data.
    """

    print("Performing data upgrade...")

    bind = op.get_bind()
    role = table_role(bind)

    print("Renaming all pagerpeeps roles to 'oncall'...")
    op.execute(
        role.update()
        .where(role.c.name == "pagerpeeps")
        .values(name="oncall", description="On-call User")
    )
    print("Renaming all 'user' roles to 'guest'...")
    op.execute(
        role.update()
        .where(role.c.name == "user")
        .values(name="guest", description="Guest")
    )

    print("Add oncall roll for teams that don't have it")
    op.execute(
        "INSERT INTO role (name, description, team_id) SELECT 'oncall', 'On-call User', id FROM team ON CONFLICT DO NOTHING"
    )

    op.execute(role.update().where(role.c.name == "admin").values(priority=0))
    op.execute(role.update().where(role.c.name == "oncall").values(priority=1))
    op.execute(role.update().where(role.c.name == "guest").values(priority=2))

    for permission_name in PERMISSIONS_BY_NAME.keys():
        op.execute(f"INSERT INTO permission (name) VALUES ('{permission_name}')")

    for name, data in roles_permissions.items():
        for permission in data["permissions"]:
            op.execute(
                f"INSERT INTO role_permission (permission_id, role_id, access) SELECT (SELECT id FROM permission WHERE name = '{permission}'), id, True FROM role WHERE name = '{name}';"
            )

    print("Data upgrade complete.")
    op.alter_column("role", "priority", server_default=None)
    op.create_unique_constraint(
        "role_team_id_priority_key", "role", ["team_id", "priority"]
    )


def data_downgrade():
    """
    Downgrade the database data to the previous version.
    """

    print("Performing data downgrade...")

    bind = op.get_bind()

    role = table_role(bind)

    # No need to remove permission table entries here.

    for name, data in roles_permissions.items():
        op.execute(
            f"INSERT INTO role (name, description, team_id, priority) SELECT '{name}', '{data['description']}', id, {data['priority']} FROM team ON CONFLICT DO NOTHING"
        )

    print("Renaming all 'oncall' roles to 'pagerpeeps'...")
    op.execute(
        role.update()
        .where(role.c.name == "oncall")
        .values(name="pagerpeeps", description="pagerpeeps")
    )

    print("Renaming all 'guest' roles to 'user'...")
    op.execute(
        role.update()
        .where(role.c.name == "guest")
        .values(name="user", description="User")
    )

    print("Data downgrade complete.")


def table_role(bind):
    """
    Generate a Table object for the 'role' table, using the given connection.
    """

    return sa.Table(
        "role",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer, nullable=False),
        sa.Column("name", sa.Text, nullable=False),
        sa.Column("description", sa.Text, nullable=False),
        sa.Column("team_id", sa.Integer, nullable=True),
        sa.Column("priority", sa.Integer(), nullable=False, server_default="0"),
        sa.ForeignKeyConstraint(["team_id"], ["team.id"], ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("name", "team_id"),
    )
