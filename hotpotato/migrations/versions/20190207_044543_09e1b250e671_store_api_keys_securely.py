"""
Store API keys securely

Revision ID: 09e1b250e671
Revises: e217361bac0c
Create Date: 2019-02-07 04:45:43.122612
"""


import secrets
import string

import flask_security
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "09e1b250e671"
down_revision = "c46f0d7f8de2"
branch_labels = None
depends_on = None


def row_select(session, table, *columns, **search_params):
    """
    Return the results of a SELECT query.

    Returns a generator of single elements if there was only one column specified,
    otherwise returns a generator of arrays of elements.
    """

    cols = [table.columns[col] for col in columns]

    query = session.query(*cols)

    if search_params:
        for key, value in search_params.items():
            query = query.filter(table.columns[key] == value)

    return (data[0] for data in query) if len(columns) == 1 else query


def row_update(table, row_id, **columns):
    """
    Update a row on the given table with the given column values.
    """

    op.execute(table.update().where(table.c.id == row_id).values(**columns))


def upgrade():
    """
    Upgrade the database.
    """

    print("Performing upgrade migration...")

    op.add_column(
        "servers",
        sa.Column("apikey_id", sa.Text(), nullable=False, server_default="TMP"),
    )
    op.add_column(
        "servers",
        sa.Column("apikey_secret", sa.Text(), nullable=False, server_default="TMP"),
    )
    op.alter_column("servers", "apikey_id", server_default=None)
    op.alter_column("servers", "apikey_secret", server_default=None)

    bind = op.get_bind()
    session = sa.orm.Session(bind=bind)

    servers_table = sa.Table(
        "servers",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("hostname", sa.Text, nullable=True),
        sa.Column("apikey", sa.Text, nullable=True),
        sa.Column("apikey_id", sa.Text, nullable=False),
        sa.Column("apikey_secret", sa.Text, nullable=False),
        sa.Column("enable_heartbeat", sa.Boolean),
        sa.Column("disable_missed_heartbeat_notif", sa.Boolean),
        sa.Column("missed_heartbeat_limit", sa.Integer),
        sa.Column("timezone", sa.Text, nullable=True),
        sa.Column("link", sa.Text, nullable=True),
    )
    servers = row_select(session, servers_table, "id", "apikey")
    for server in servers:
        apikey = server[1]
        apikey_id = apikey[:20]
        apikey_secret = flask_security.utils.hash_password(apikey[20:])

        row_update(
            servers_table, server[0], apikey_id=apikey_id, apikey_secret=apikey_secret
        )

    session.commit()

    print("Upgrade migration complete.")


def downgrade():
    """
    Downgrade the database to the previous version.
    """

    print("Performing downgrade migration...")

    op.drop_column("servers", "apikey_secret")
    op.drop_column("servers", "apikey_id")

    # Keys are not recoverable here if downgraded so give all servers a new key
    bind = op.get_bind()
    session = sa.orm.Session(bind=bind)
    servers_table = sa.Table(
        "servers",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("hostname", sa.Text, nullable=True),
        sa.Column("apikey", sa.Text, nullable=True),
        sa.Column("enable_heartbeat", sa.Boolean),
        sa.Column("disable_missed_heartbeat_notif", sa.Boolean),
        sa.Column("missed_heartbeat_limit", sa.Integer),
        sa.Column("timezone", sa.Text, nullable=True),
        sa.Column("link", sa.Text, nullable=True),
    )
    servers = row_select(session, servers_table, "id", "apikey")
    for server in servers:
        apikey = "".join(
            secrets.choice(
                string.ascii_uppercase + string.ascii_lowercase + string.digits
            )
            for _ in range(100)
        )
        row_update(servers_table, server[0], apikey=apikey)

    session.commit()

    print("Downgrade migration complete.")
