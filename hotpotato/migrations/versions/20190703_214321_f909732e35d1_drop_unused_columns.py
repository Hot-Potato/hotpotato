"""
Drop unused columns

Revision ID: f909732e35d1
Revises: fe2ba1605649
Create Date: 2019-07-03 21:43:21.338725
"""


from datetime import datetime

import dateutil
import pytz
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "f909732e35d1"
down_revision = "fe2ba1605649"
branch_labels = None
depends_on = None


def row_select(session, table, *columns, **search_params):
    """
    Return the results of a SELECT query.

    Returns a generator of single elements if there was only one column specified,
    otherwise returns a generator of arrays of elements.
    """

    cols = [table.columns[col] for col in columns]

    query = session.query(*cols)

    if search_params:
        for key, value in search_params.items():
            query = query.filter(table.columns[key] == value)

    return (data[0] for data in query) if len(columns) == 1 else query


def row_update(table, row_id, **columns):
    """
    Update a row on the given table with the given column values.
    """

    op.execute(table.update().where(table.c.id == row_id).values(**columns))


def datetime_process(datetime_dt, to_tz):
    datetime_dt = pytz.utc.localize(datetime_dt)

    if datetime_dt.tzinfo is None:
        raise ValueError(
            "datetime_dt needs to be timezone-aware for timezone conversions"
        )
    datetime_dt = datetime_dt.astimezone(tz=to_tz)

    return datetime_dt


def datetime_get_from_string(timestamp, tz_space_sep=False):
    """
    Convert an ISO 8601-compliant timestamp to a DateTime object.

    The tz_space_sep parameter is used for handling not fully ISO 8601-compliant timestamps
    from older versions of the Hot Potato monitoring system alert handler.
    """

    if tz_space_sep:
        try:
            datetime_dt = datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S %z")
        except ValueError:
            datetime_dt = dateutil.parser.isoparse(timestamp)
    else:
        datetime_dt = dateutil.parser.isoparse(timestamp)

    return datetime_dt


def upgrade():
    """
    Upgrade the database.
    """
    op.drop_column("notification_log", "body")

    op.drop_constraint("fk_user_id_ref_user", "notification_log", type_="foreignkey")
    op.drop_column("notification_log", "user_id")

    op.drop_column("servers", "apikey")


def downgrade():
    """
    Downgrade the database to the previous version.
    """
    op.add_column("notification_log", sa.Column("user_id", sa.INTEGER(), nullable=True))
    op.create_index(
        "notification_log_auto_index_fk_user_id_ref_user",
        "notification_log",
        ["user_id"],
        unique=False,
    )
    op.create_foreign_key(
        "fk_user_id_ref_user",
        "notification_log",
        "user",
        ["user_id"],
        ["id"],
        ondelete="SET NULL",
    )

    op.add_column(
        "notification_log",
        sa.Column("body", sa.Text(), server_default="", nullable=False),
    )

    # --- BEGIN DATA ---
    # Generates the notification body from information stored on the notification

    bind = op.get_bind()
    session = sa.orm.Session(bind=bind)
    notif_types = frozenset(("alert", "handover", "message"))
    notifications_table = sa.Table(
        "notification_log",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("user_id", sa.Integer, sa.ForeignKey("User.id", ondelete="SET NULL")),
        sa.Column("received_dt", sa.DateTime, nullable=False),
        sa.Column("notif_type", sa.Enum(*notif_types), nullable=False),
        sa.Column("body", sa.Text, server_default=""),
        sa.Column(
            "json", sa.ext.mutable.MutableDict.as_mutable(sa.JSON), nullable=False
        ),
        sa.Column("team_id", sa.Integer, sa.ForeignKey("Team.id", ondelete="RESTRICT")),
    )
    users_table = sa.Table(
        "user",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.Text, nullable=False),
        sa.Column("timezone", sa.Text),
    )
    servers_table = sa.Table(
        "servers",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("hostname", sa.Text, nullable=False),
    )
    notifications = row_select(
        session, notifications_table, "id", "notif_type", "json", "user_id"
    ).all()

    def get_user(id):
        return row_select(session, users_table, "name", "timezone", id=id).first() or [
            "No one",
            "UTC",
        ]

    for notification in notifications:
        json = notification[2]
        type_ = notification[1]

        if type_ == "message":
            user = get_user(json["from_user_id"])
            body = (
                "Message from {} via Hot Potato: {}".format(user[0], json["message"])
                if json["from_user_id"]
                else "Message from Hot Potato: {}".format(json["message"])
            )

        elif type_ == "handover":
            if json["from_user_id"] and json["message"]:
                from_user = get_user(json["from_user_id"])
                to_user = get_user(json["to_user_id"])
                body = "{} is now on call, taking over from {} with the following message: {}".format(
                    to_user[0], from_user[0], json["message"]
                )
            elif json["from_user_id"]:
                from_user = get_user(json["from_user_id"])
                to_user = get_user(json["to_user_id"])
                body = "{} is now on call, taking over from {}.".format(
                    to_user[0], from_user[0]
                )
            elif json["message"]:
                to_user = get_user(json["to_user_id"])
                body = "{} is now on call with the following message: {}".format(
                    to_user[0], json["message"]
                )
            else:
                to_user = get_user(json["to_user_id"])
                body = "{} is now on call.".format(to_user[0])

        elif type_ == "alert":
            hostname = next(
                row_select(session, servers_table, "hostname", id=json["server_id"])
            )
            user = get_user(notification[3])
            if user:
                try:
                    pytz_timezone = pytz.timezone(user[1] or "UTC")
                except pytz.UnknownTimeZoneError:
                    pytz_timezone = pytz.timezone("UTC")

            timestamp = datetime_get_from_string(json["timestamp"])
            user_ts_str = datetime.strftime(
                datetime_process(timestamp, to_tz=pytz_timezone) if user else timestamp,
                "%Y-%m-%d %H:%M:%S",
            )

            if json["alert_type"] == "service":
                body = (
                    "{serv_hostname}: {trouble_code} {hostname} {service_name} state is "
                    "{state} ({output}) "
                    "{user_timestamp}"
                ).format(**json, serv_hostname=hostname, user_timestamp=user_ts_str)
            elif json["alert_type"] == "host":
                body = (
                    "{serv_hostname}: {trouble_code} {hostname} state is "
                    "{state} ({output}) "
                    "{user_timestamp}"
                ).format(**json, serv_hostname=hostname, user_timestamp=user_ts_str)
            else:
                body = "GET RID OF YOUR STUPID INVALID DATA"

        row_update(notifications_table, notification[0], body=body)
        session.commit()

    # --- END DATA ---

    op.alter_column("notification_log", "body", server_default=None)

    op.add_column("servers", sa.Column("apikey", sa.VARCHAR(), nullable=True))
