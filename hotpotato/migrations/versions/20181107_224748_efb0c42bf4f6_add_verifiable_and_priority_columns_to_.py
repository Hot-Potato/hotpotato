"""
Add verifiable and priority columns to oncall_contacts

Revision ID: efb0c42bf4f6
Revises:
Create Date: 2018-11-07 22:47:48.927691
"""


import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "efb0c42bf4f6"
down_revision = None
branch_labels = None
depends_on = None


def row_select(session, table, *columns, **search_params):
    """
    Return the results of a SELECT query.

    Returns a generator of single elements if there was only one column specified,
    otherwise returns a generator of arrays of elements.
    """

    cols = [table.columns[col] for col in columns]

    query = session.query(*cols)

    if search_params:
        for key, value in search_params.items():
            query = query.filter(table.columns[key] == value)

    return (data[0] for data in query) if len(columns) == 1 else query


def row_update(table, row_id, **columns):
    """
    Update a row on the given table with the given column values.
    """

    op.execute(table.update().where(table.c.id == row_id).values(**columns))


def upgrade():
    """
    Upgrade the database.
    """

    print("Performing upgrade migration...")

    schema_upgrade()
    data_upgrade()

    print("Upgrade migration complete.")


def downgrade():
    """
    Downgrade the database to the previous version.
    """

    print("Performing downgrade migration...")

    schema_downgrade()

    print("Downgrade migration complete.")


def schema_upgrade():
    """
    Upgrade the database schema.
    """

    print("Performing schema upgrade...")

    print("Adding column 'priority'...")
    op.add_column("oncall_contacts", sa.Column("priority", sa.Integer()))
    print("Adding column 'verifiable'...")
    op.add_column("oncall_contacts", sa.Column("verifiable", sa.Boolean()))

    print("Schema upgrade complete.")


def schema_downgrade():
    """
    Downgrade the database schema to the previous version.
    """

    print("Performing schema downgrade...")

    print("Dropping column 'verifiable'...")
    op.drop_column("oncall_contacts", "verifiable")
    print("Dropping column 'priority'...")
    op.drop_column("oncall_contacts", "priority")

    print("Schema downgrade complete.")


def data_upgrade():
    """
    Upgrade the database data.
    """

    print("Performing data upgrade...")

    bind = op.get_bind()
    session = sa.orm.Session(bind=bind)

    oncall_contacts = sa.Table(
        "oncall_contacts",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("user_id", sa.Integer, sa.ForeignKey("User.id"), nullable=False),
        sa.Column("method", sa.Enum("app", "pushover", "pager", "sms"), nullable=False),
        sa.Column("verifiable", sa.Boolean),
        sa.Column("priority", sa.Integer),
        sa.Column("contact", sa.Text, nullable=False),
        sa.Column("send_pages", sa.Boolean, nullable=False),
        sa.Column("send_failures", sa.Boolean, nullable=False),
    )

    for data in session.query(oncall_contacts.c.user_id):
        user_id = data[0]

        print("Migrating on-call contacts for user with ID {}...".format(user_id))

        verifiable_priority = 0
        unverifiable_priority = 0

        # Do this in a specific order to give appropriate priorities to
        # the different sending methods.

        print("Migrating app contacts...")
        for contact_id in row_select(
            session, oncall_contacts, "id", user_id=user_id, method="app"
        ):
            print("Migrating app contact with ID {}...".format(contact_id))
            row_update(
                oncall_contacts,
                contact_id,
                verifiable=True,
                priority=verifiable_priority,
            )
            verifiable_priority += 1
        print("Migrating app contacts complete.")

        print("Migrating Pushover contacts...")
        for contact_id in row_select(
            session, oncall_contacts, "id", user_id=user_id, method="pushover"
        ):
            print("Migrating Pushover contact with ID {}...".format(contact_id))
            row_update(
                oncall_contacts,
                contact_id,
                verifiable=True,
                priority=verifiable_priority,
            )
            verifiable_priority += 1
        print("Migrating Pushover contacts complete.")

        print("Migrating pager contacts...")
        for contact_id in row_select(
            session, oncall_contacts, "id", user_id=user_id, method="pager"
        ):
            print("Migrating pager contact with ID {}...".format(contact_id))
            row_update(
                oncall_contacts,
                contact_id,
                verifiable=False,
                priority=unverifiable_priority,
            )
            unverifiable_priority += 1
        print("Migrating pager contacts complete.")

        print("Migrating SMS contacts...")
        for contact_id in row_select(
            session, oncall_contacts, "id", user_id=user_id, method="sms"
        ):
            print("Migrating SMS contact with ID {}...".format(contact_id))
            row_update(
                oncall_contacts,
                contact_id,
                verifiable=False,
                priority=unverifiable_priority,
            )
            unverifiable_priority += 1
        print("Migrating SMS contacts complete.")

    print("Data upgrade complete.")
