"""
Multiple recipients

Revision ID: b238d5353954
Revises: b3f148c2c52c
Create Date: 2019-01-31 01:09:49.608042
"""


import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "b238d5353954"
down_revision = "b3f148c2c52c"
branch_labels = None
depends_on = None


def row_select(session, table, *columns, **search_params):
    """
    Return the results of a SELECT query.

    Returns a generator of single elements if there was only one column specified,
    otherwise returns a generator of arrays of elements.
    """

    cols = [table.columns[col] for col in columns]

    query = session.query(*cols)

    if search_params:
        for key, value in search_params.items():
            query = query.filter(table.columns[key] == value)

    return (data[0] for data in query) if len(columns) == 1 else query


def row_update(table, row_id, **columns):
    """
    Update a row on the given table with the given column values.
    """

    op.execute(table.update().where(table.c.id == row_id).values(**columns))


def upgrade():
    """
    Upgrade the database.
    """

    print("Performing upgrade migration...")

    schema_upgrade()

    print("Upgrade migration complete.")


def downgrade():
    """
    Downgrade the database to the previous version.
    """

    print("Performing downgrade migration...")

    schema_downgrade()

    print("Downgrade migration complete.")


def schema_upgrade():
    """
    Upgrade the database schema.
    """

    print("Performing schema upgrade...")

    # NOTE: Add print statements for each statement.
    # print("Adding column 'column_name'...")
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "notification_attempt",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("notification_id", sa.Integer(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=True),
        sa.Column(
            "status",
            sa.Enum(
                "SEND_FAILED_ACKNOWLEDGED",
                "NEW",
                "SEND_FAILED",
                "SENT_TO_CLIENT",
                "RECEIVED_BY_CLIENT",
                "UNSENT",
                "SENT_TO_PROVIDER",
                "RECEIVED_BY_PROVIDER",
                "SENDING",
                "READ_BY_CLIENT",
            ),
            nullable=False,
        ),
        sa.Column("warnings", postgresql.ARRAY(sa.Text()), nullable=False),
        sa.Column("errors", postgresql.ARRAY(sa.Text()), nullable=False),
        sa.Column("method", sa.Enum("pushover", "sms", "app", "pager"), nullable=True),
        sa.Column("provider", sa.Enum("modica", "pushover", "twilio"), nullable=True),
        sa.Column("provider_notif_id", sa.Text(), nullable=True),
        sa.ForeignKeyConstraint(["user_id"], ["user.id"], ondelete="SET NULL"),
        sa.ForeignKeyConstraint(
            ["notification_id"], ["notification_log.id"], ondelete="CASCADE"
        ),
        sa.PrimaryKeyConstraint("id"),
    )

    op.alter_column("notification_log", "body", nullable=True)

    bind = op.get_bind()

    offset = 0

    while True:
        result = bind.execute(
            f"INSERT INTO notification_attempt (notification_id, user_id, warnings, errors, method, status, provider, provider_notif_id) SELECT id, user_id, ARRAY[(json ->> 'warnings')], ARRAY[(json ->> 'errors')], (json ->> 'method'), (json ->> 'status'), (json ->> 'provider'), (json ->> 'provider_notif_id') FROM notification_log ORDER BY id LIMIT 1000 OFFSET {offset};"
        )
        if result.rowcount == 0:
            break
        offset += 1000

    # ### end Alembic commands ###

    print("Schema upgrade complete.")


def schema_downgrade():
    """
    Downgrade the database schema to the previous version.
    """

    print("Performing schema downgrade...")

    # NOTE: Add print statements for each statement.
    # print("Dropping column 'column_name'...")
    # ### commands auto generated by Alembic - please adjust! ###

    # --- BEGIN DATA ---
    # Migrates information stored on NotificationAttempt back into the json
    # column of the notification object
    bind = op.get_bind()
    session = sa.orm.Session(bind=bind)
    notif_types = frozenset(("alert", "handover", "message"))
    notifications_table = sa.Table(
        "notification_log",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("user_id", sa.Integer, sa.ForeignKey("User.id", ondelete="SET NULL")),
        sa.Column("received_dt", sa.DateTime, nullable=False),
        sa.Column("notif_type", sa.Enum(*notif_types), nullable=False),
        sa.Column("body", sa.Text, nullable=False),
        sa.Column(
            "json", sa.ext.mutable.MutableDict.as_mutable(sa.JSON), nullable=False
        ),
        sa.Column("team_id", sa.Integer, sa.ForeignKey("Team.id", ondelete="RESTRICT")),
    )
    attempts_table = sa.Table(
        "notification_attempt",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer(), nullable=False, primary_key=True),
        sa.Column("notification_id", sa.Integer(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=True),
        sa.Column(
            "status",
            sa.Enum(
                "SEND_FAILED_ACKNOWLEDGED",
                "NEW",
                "SEND_FAILED",
                "SENT_TO_CLIENT",
                "RECEIVED_BY_CLIENT",
                "UNSENT",
                "SENT_TO_PROVIDER",
                "RECEIVED_BY_PROVIDER",
                "SENDING",
                "READ_BY_CLIENT",
            ),
            nullable=False,
        ),
        sa.Column("warnings", postgresql.ARRAY(sa.Text()), nullable=False),
        sa.Column("errors", postgresql.ARRAY(sa.Text()), nullable=False),
        sa.Column("method", sa.Enum("pushover", "sms", "app", "pager"), nullable=True),
        sa.Column("provider", sa.Enum("modica", "pushover", "twilio"), nullable=True),
        sa.Column("provider_notif_id", sa.Text(), nullable=True),
        sa.ForeignKeyConstraint(["user_id"], ["user.id"], ondelete="CASCADE"),
        sa.ForeignKeyConstraint(
            ["notification_id"], ["notification_log.id"], ondelete="CASCADE"
        ),
    )
    attempts = row_select(
        session,
        attempts_table,
        "notification_id",
        "user_id",
        "status",
        "warnings",
        "errors",
        "method",
        "provider",
        "provider_notif_id",
    )
    for attempt in attempts:
        # This is intentionally imperfect, the attempt info that gets assigned to
        # a notification if it was sent to multiple users is essentially random.
        notif = row_select(
            session, notifications_table, "id", "json", id=attempt[0]
        ).one()
        json = notif[1]
        json["status"] = attempt[3]
        json["warnings"] = attempt[4]
        json["errors"] = attempt[5]
        json["method"] = attempt[6]
        json["provider_notif_id"] = attempt[7]
        row_update(notifications_table, notif[0], user_id=attempt[1], json=json)
        session.commit()
    # --- END DATA ---

    op.drop_table("notification_attempt")
    # ### end Alembic commands ###

    print("Schema downgrade complete.")
