"""
Server functions.
"""


import secrets
import string
from datetime import datetime

import flask_security
import pytz
import sqlalchemy
from flask_babelex import gettext

from hotpotato import heartbeats
from hotpotato.models import Server as Model, db

# Heartbeat frequency, in seconds.
# Ideally should be configurable.
HBEAT_FREQ = 60


class ServerError(Exception):
    """
    Server exception base class.
    """

    pass


class ServerCreateError(Exception):
    """
    Server create exception class.
    """

    pass


class ServerIDError(ServerError):
    """
    Exception for invalid server ID.
    """

    def __init__(self, server_id):
        self.server_id = server_id
        super().__init__(
            gettext("Invalid server ID (or unable to find server with ID): {}").format(
                server_id
            )
        )


class ServerAPIKeyError(ServerError):
    """
    Exception for invalid server API key.
    """

    def __init__(self, api_key):
        self.api_key = api_key
        super().__init__(
            gettext(
                "Invalid server API key (or unable to find server with API key): {}"
            ).format(api_key)
        )


class ServerTimezoneError(ServerError):
    """
    Exception for invalid server timezone.
    """

    def __init__(self, server_id, timezone):
        self.server_id = server_id
        self.timezone = timezone
        super().__init__(
            gettext("Invalid timezone for server with ID {}: {}").format(
                self.server_id, self.timezone
            )
        )


def get(server_id):
    """
    Get a server object using the given ID and return it.
    """

    try:
        return Model.query.filter_by(id=server_id).one()
    except (sqlalchemy.orm.exc.NoResultFound, sqlalchemy.exc.DataError):
        raise ServerIDError(server_id)


def get_by(**kwargs):
    """
    Return a tuple containing all server object matching the given search parameters.
    """

    return Model.query.filter_by(**kwargs).all()


def get_by_api_key(apikey):
    """
    Get a server object using its API key and return it.
    """
    # Newer API keys have a . to seperate the identifier and secret part of the key
    # To maintain backwards compatiability with old keys if they don't have the .
    # we split after 20 characters to get the id and secret.
    if "." in apikey:
        apikey_parts = apikey.split(".")
        apikey_id = apikey_parts[0]
        apikey_secret = apikey_parts[1]
    else:
        apikey_id = apikey[:20]
        apikey_secret = apikey[20:]
    try:
        server = Model.query.filter_by(apikey_id=apikey_id).one()
    except (sqlalchemy.orm.exc.NoResultFound, sqlalchemy.exc.DataError):
        raise ServerAPIKeyError(apikey_id)
    if flask_security.utils.verify_password(apikey_secret, server.apikey_secret):
        return server
    raise ServerAPIKeyError(apikey_id)


def missed_hbeats_query(now, hbeat_freq):
    """
    Return an SQL query object for finding servers have not sent heartbeats
    within their threshold time, or have never sent a heartbeat.
    """
    subquery_last_hbeats = heartbeats.get_last_query().subquery()
    return (
        db.session.query(
            Model.hostname,
            Model.timezone,
            Model.disable_missed_heartbeat_notif,
            subquery_last_hbeats.c.last_hbeat,
            Model.id,
            Model.link,
            Model.missed_heartbeat_limit,
        )
        .outerjoin(subquery_last_hbeats, subquery_last_hbeats.c.server_id == Model.id)
        .filter(
            Model.disable_missed_heartbeat_notif == sqlalchemy.sql.expression.false(),
            sqlalchemy.or_(
                sqlalchemy.sql.expression.cast(
                    now - subquery_last_hbeats.c.last_hbeat, sqlalchemy.types.Integer
                )
                >= Model.missed_heartbeat_limit * hbeat_freq,
                subquery_last_hbeats.c.server_id.is_(None),
            ),
        )
    )


def get_num_missed_hbeats():
    """
    Return the number of servers which have not sent heartbeats within their threshold time,
    or have never sent a heartbeat.
    """

    now = datetime.utcnow()

    return missed_hbeats_query(now, HBEAT_FREQ).count()


def get_missed_hbeats():
    """
    Return a list of servers which have not sent heartbeats within their threshold time,
    or have never sent a heartbeat.
    """

    now = datetime.utcnow()

    missed_servs_res = missed_hbeats_query(now, HBEAT_FREQ).all()

    return missed_servs_res


def get_monitored():
    """
    Get a list of all the servers that are currently being actively monitored.
    Does not list servers whose notifications have temporarily been disabled, therefore
    it does not constitute a "list of servers that we care about on an on-going basis".
    """
    return Model.query.filter_by(disable_missed_heartbeat_notif=False).all()


def generate_api_key():
    """
    Generates a random key for servers to use with the servers api
    """
    apikey_id = "".join(
        secrets.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits)
        for _ in range(20)
    )
    apikey_secret = "".join(
        secrets.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits)
        for _ in range(80)
    )
    return (apikey_id, apikey_secret)


# pylint: disable=too-many-arguments
def create(
    hostname,
    timezone,
    link,
    disable_missed_heartbeat_notif=False,
    missed_heartbeat_limit=5,
    apikey_tuple=None,
):
    """
    Create a new server,  and add it to the database, and return it
    """

    if get_by(hostname=hostname):
        raise ServerCreateError(
            gettext('Server with hostname "{}" already exists').format(hostname)
        )

    apikey_id, apikey_secret = generate_api_key() if not apikey_tuple else apikey_tuple

    server = Model(
        hostname=hostname,
        apikey_id=apikey_id,
        apikey_secret=flask_security.utils.hash_password(apikey_secret),
        disable_missed_heartbeat_notif=disable_missed_heartbeat_notif,
        missed_heartbeat_limit=missed_heartbeat_limit,
        timezone=timezone,
        link=link,
    )

    db.session.add(server)
    db.session.commit()

    return (server, "{}.{}".format(apikey_id, apikey_secret))


def timezone_get_as_timezone(tz_str):
    """
    Return the given timezone string as a timezone object.
    """

    return pytz.timezone(tz_str)
