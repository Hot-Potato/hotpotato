"""
Flask app helper functions.
"""

import os
import pprint

# Normal package imports.
import flask
import flask_mail

from hotpotato import (
    __version__,
    api,
    cli,
    config,
    headers,
    locale,
    logging,
    models,
    teams,
    util,
    views,
)
from hotpotato.notifications import queue as notifications_queue


def create(
    init_cli=True,
    init_config=True,
    config_preset=None,
    config_filepaths=None,
    config_use_env=True,
    config_use_default_paths=True,
    init_logging=True,
    init_models=True,
    init_notifications_queue=True,
    init_views=True,
    init_api=True,
    setup_server_name=False,
    unsafe_crypto=False,
):
    """
    Create and return a Flask app.
    """

    app = flask.Flask(__name__)

    if init_cli:
        cli.init_app(app)

    if init_config:
        config.init_app(
            app,
            preset=config_preset,
            filepaths=config_filepaths,
            use_env=config_use_env,
            use_default_paths=config_use_default_paths,
            setup_server_name=setup_server_name,
        )

    if unsafe_crypto:
        app.config["SECURITY_PASSWORD_HASH"] = "plaintext"
        app.config["SECURITY_HASHING_SCHEMES"] = ["plaintext"]
        app.config["SECURITY_DEPRECATED_HASHING_SCHEMES"] = []
        if not app.config["TESTING"]:
            raise RuntimeError(
                "unsafe_crypto requested outside of a testing environment"
            )

    locale.init_app(app)
    if init_logging:
        logging.init_app(app)

    if init_models:
        models.init_app(app)

    if init_notifications_queue:
        notifications_queue.init_app(app)

    if init_views and not os.environ.get("HOTPOTATO_END_TO_END", False):
        views.init_app(app)

    if init_api:
        api.init_app(app)

    if app.debug:
        import flask_debugtoolbar

        # This is annoying, messes with tests, and generally useless
        app.config["DEBUG_TB_INTERCEPT_REDIRECTS"] = False

        flask_debugtoolbar.DebugToolbarExtension().init_app(app)
        views.csrf.exempt(flask_debugtoolbar.module)

    teams.init_app(app)

    with app.app_context():
        flask.g.mail = flask_mail.Mail(app)
        flask.g.server_name = util.node_name
        flask.g.version = "Version {}".format(__version__)

    with app.app_context():
        if app.config["TESTING"]:
            app.logger.debug("")
            app.logger.debug("Hot Potato")
            app.logger.debug("Version: {}".format(__version__))
            app.logger.debug("")
            app.logger.debug(
                "Configuration:\n{}".format(pprint.pformat(app.config, indent=2))
            )
            app.logger.debug("")

    headers.init_app(app)

    return app
