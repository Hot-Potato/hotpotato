Hot Potato uses flask-babelex to help translate the website into other languages.
All translating is done by people. Original strings are saved in all_strings.pot
Translations are kept in translations/language/LC_MESSAGES/messages.po
Messages.mo is a compiled version of messages.po

To mark a string to be translated
Add gettext around string: gettext("my string")
In your terminal from the hotpotato dir run: `pipenv run gen-language`

To compile Translations
In your terminal from the hotpotato dir run: `pipenv run compile-language`

To add Translation language files
In your terminal run: `pipenv run pybabel init -i hotpotato/language-support/all_strings.pot -d hotpotato/app/translations -l [language code]`
Replace [language code] with a language code e.g. en

Update translation language files
In your terminal run: `pipenv run update-language`
This will update .po files from all_strings.pot
Then compile translations (see above)

Translations are currently stored in app/translations because babelex doesn't have a location configuration option
