import pytest

from hotpotato import servers as servers_module
from hotpotato.tests import servers


def test_apikey_just_provide_id(session):
    """
    Test that providing an apikey id without the key fails
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    session.commit()

    with pytest.raises(servers_module.ServerAPIKeyError):
        servers_module.get_by_api_key(server.apikey_id)


def test_apikey_id_correct_wrong_key(session):
    """
    Test that providing an apikey id without the wrong key fails
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    session.commit()

    with pytest.raises(servers_module.ServerAPIKeyError):
        servers_module.get_by_api_key("{}badkey".format(server.apikey_id))


def test_apikey_id_correct_key(session):
    """
    Test that providing an apikey id with the correct key succeeds
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    session.commit()

    result = servers_module.get_by_api_key("{}testingapi".format(server.apikey_id))
    assert server.id == result.id


def test_apikey_to_short(session):
    """
    Test that providing an apikey less than 20 characters raises the right error
    """
    servers.ServerFactory(api_key_secret_unhashed="testingapi")
    session.commit()

    with pytest.raises(servers_module.ServerAPIKeyError):
        servers_module.get_by_api_key("testapikey")


def test_apikey_with_dot(session):
    """
    Test that providing an apikey with . seperation succeeds
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    session.commit()

    result = servers_module.get_by_api_key("{}.testingapi".format(server.apikey_id))
    assert server.id == result.id


def test_apikey_witn_dot_no_secret(session):
    """
    Test that providing an apikey with a . and no secret fails
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    session.commit()

    with pytest.raises(servers_module.ServerAPIKeyError):
        servers_module.get_by_api_key("{}.".format(server.apikey_id))


def test_apikey_witn_dot_no_id(session):
    """
    Test that providing an apikey with a . and no id fails
    """
    servers.ServerFactory(api_key_secret_unhashed="testingapi")
    session.commit()

    with pytest.raises(servers_module.ServerAPIKeyError):
        servers_module.get_by_api_key(".testingapi")
