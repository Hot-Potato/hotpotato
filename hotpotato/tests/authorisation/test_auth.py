"""
Test API authentication
"""
from hotpotato.tests import servers


def test_invalid_format_auth_header(client):
    """
    Test that auth headers without a space returns 401.
    """
    r = client.post(
        "/api/server/v2/heartbeats/create", headers={"Authorization": "dasdasgehjd"}
    )
    assert r.status_code == 401


def test_invalid_format_auth_header2(client):
    """
    Test that auth headers without a space returns 401.
    """
    r = client.post(
        "/api/server/v2/heartbeats/create", headers={"Authorization": "made dasjkhwdk"}
    )
    assert r.status_code == 401


def test_missing_auth_header(client):
    """
    Test that leaving out the auth header returns 401.
    """
    r = client.post("/api/server/v2/heartbeats/create")
    assert r.status_code == 401


def test_invalid_auth_header(client, session):
    """
    Test that an invalid auth header returns 401.
    """
    r = client.post(
        "/api/server/v2/heartbeats/create",
        headers={"Authorization": "apikey dasdasgehjd"},
    )
    assert r.status_code == 401


def test_valid_auth_header(client, session):
    """
    Test that an valid auth header returns 201.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    session.commit()
    apikey = "{}testingapi".format(server.apikey_id)
    r = client.post(
        "/api/server/v2/heartbeats/create",
        headers={"Authorization": "apikey {}".format(apikey)},
    )
    assert r.status_code == 201
