import pkg_resources

from hotpotato import app as hotpotato_app


def _app_create(filepath):
    """
    Create a Hot Potato Flask app with the required settings.
    """

    return hotpotato_app.create(
        config_filepaths=(filepath,),
        config_use_env=False,
        config_use_default_paths=False,
        init_cli=False,
        init_logging=False,
        init_models=False,
        init_notifications_queue=False,
        init_views=False,
        init_api=False,
    )


def test_correct_config_ini():
    """
    Test loading a correctly formatted configuration file.
    """

    filepath = pkg_resources.resource_filename(
        "hotpotato", "tests/config/correct_config.ini"
    )

    app = _app_create(filepath)

    assert app.config["TESTING"] is True
    assert app.config["SECRET_KEY"] == "correct-config-ini-secret-key"

    assert app.config["HOTPOTATO_WEBUI_URL"] == "correct-config-ini.com"
    assert (
        app.config["HOTPOTATO_TROUBLECODE_URL"]
        == "http://wiki.correct-config-ini.com/wiki/{trouble_code}"
    )

    assert app.config["SECURITY_PASSWORD_SALT"] == "correct-config-ini-password-salt"

    assert app.config["MAIL_SUPPRESS_SEND"] is True
    assert app.config["MAIL_DEFAULT_SENDER"] == "correct_config_ini@example.com"

    assert app.config["COCKROACH_SERVER"] == "cockroach.correct-config-ini.com"
    assert app.config["COCKROACH_DATABASE"] == "correct-config-ini"

    assert app.config["RABBITMQ_SERVER"] == "rabbitmq.correct-config-ini.com"
    assert app.config["RABBITMQ_USERNAME"] == "correct-config-ini"
    assert app.config["RABBITMQ_PASSWORD"] == "correct-config-ini"


def test_mixed_case_config_ini():
    """
    Test loading a configuration file with lower and mixed case section names and keys.
    This should load correctly, as configuration loading should be case insensitive.
    """

    filepath = pkg_resources.resource_filename(
        "hotpotato", "tests/config/mixed_case_config.ini"
    )

    app = _app_create(filepath)

    assert app.config["TESTING"] is True
    assert app.config["SECRET_KEY"] == "mixed-case-config-ini-secret-key"

    assert app.config["HOTPOTATO_WEBUI_URL"] == "mixed-case-config-ini.com"
    assert (
        app.config["HOTPOTATO_TROUBLECODE_URL"]
        == "http://wiki.mixed-case-config-ini.com/wiki/{trouble_code}"
    )

    assert app.config["SECURITY_PASSWORD_SALT"] == "mixed-case-config-ini-password-salt"

    assert app.config["MAIL_SUPPRESS_SEND"] is True
    assert app.config["MAIL_DEFAULT_SENDER"] == "mixed_case_config_ini@example.com"

    assert app.config["COCKROACH_SERVER"] == "cockroach.mixed-case-config-ini.com"
    assert app.config["COCKROACH_DATABASE"] == "mixed-case-config-ini"

    assert app.config["RABBITMQ_SERVER"] == "rabbitmq.mixed-case-config-ini.com"
    assert app.config["RABBITMQ_USERNAME"] == "mixed-case-config-ini"
    assert app.config["RABBITMQ_PASSWORD"] == "mixed-case-config-ini"
