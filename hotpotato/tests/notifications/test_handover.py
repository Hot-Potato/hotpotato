import pytest

from hotpotato import roles, teams
from hotpotato.notifications import handovers
from hotpotato.tests import oncall_contacts, users


def test_handover_no_contact_method(session):
    """
    Test that trying to take the tater with no contact methods fails.
    """
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    session.commit()

    with pytest.raises(ValueError):
        handovers.handover(teams.hotpotato_team(), user)

    team = teams.get(teams.hotpotato_team().id)
    assert team.on_call is None


def test_handover_with_contact_methood(session):
    """
    Test that trying to take the tater with a contact method puts you on call.
    """
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    oncall_contacts.OncallContactsFactory(user=user)
    session.commit()
    handovers.handover(teams.hotpotato_team(), user)

    team = teams.get(teams.hotpotato_team().id)
    assert team.on_call_id == user.id
