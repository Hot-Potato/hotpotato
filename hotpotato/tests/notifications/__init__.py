"""
Notification unit test functions.
"""


import string

import factory
import faker

from hotpotato import models, users, util as hotpotato_util
from hotpotato.models import Team, User, db
from hotpotato.notifications import alerts, notifications
from hotpotato.tests import attempts, servers, teams, util


class NotificationFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Notification
        sqlalchemy_session = db.session

    team = factory.SubFactory(teams.TeamFactory)
    received_dt = factory.Faker("date_time_this_year")
    notif_type = factory.Faker("random_element", elements=notifications.TYPE)

    @factory.lazy_attribute
    def json(self):
        return generate_json(faker.Faker(), self.notif_type, self.received_dt)[0]


class AlertFactory(NotificationFactory):
    class Meta:
        model = models.Alert
        sqlalchemy_session = db.session

    notif_type = "alert"


def generate_json(fake, notif_type, received_dt):
    method = fake.random_element([None, "sms", "pager"])

    json_obj = {
        "version": notifications.JSON_VERSION,
        "node_name": hotpotato_util.node_name,
        "event_id": None,  # TODO: events
        "ticket_id": None,  # TODO: tickets
        "method": method,
    }
    if notif_type == "message":
        json_obj, body = generate_message_json(fake, json_obj)
    elif notif_type == "handover":
        json_obj, body = generate_handover_json(fake, json_obj)
    elif notif_type == "alert":
        json_obj, body = generate_alert_json(fake, json_obj, received_dt)
    return (json_obj, body)


def generate_message_json(fake, json_obj):
    from_user = fake.random_element(
        [None, util.get_random_object(models.db.session, models.User)]
    )
    if from_user:
        reply = util.get_random_object(models.db.session, models.Notification)
        reply_to_notif_id = fake.random_element([None, reply.id if reply else None])
    else:
        reply_to_notif_id = None

    message = fake.text(max_nb_chars=160)
    if from_user:
        body = "Message from {} via Hot Potato: {}".format(from_user.name, message)
    else:
        body = "Message from Hot Potato: {}".format(message)

    json_obj.update(
        {
            "from_user_id": from_user.id if from_user else None,
            "reply_to_notif_id": reply_to_notif_id,
            "message": message,
        }
    )
    return (json_obj, body)


def generate_handover_json(fake, json_obj):
    to_user = fake.random_element(
        [None, util.get_random_object(models.db.session, models.User)]
    )
    from_user = fake.random_element(
        [None, util.get_random_object(models.db.session, models.User)]
    )
    message = fake.random_element([None, fake.text(max_nb_chars=160)])

    to_user_name = to_user.name if to_user else "No one"

    if from_user and message:
        body = "{} is now on call, taking over from {} with the following message: {}".format(
            to_user_name, from_user.name, message
        )
    elif from_user:
        body = "{} is now on call, taking over from {}.".format(
            to_user_name, from_user.name
        )
    elif message:
        body = "{} is now on call with the following message: {}".format(
            to_user_name, message
        )
    else:
        body = "{} is now on call.".format(to_user_name)

    json_obj.update(
        {
            "to_user_id": to_user.id if to_user else None,
            "from_user_id": from_user.id if from_user else None,
            "message": message,
        }
    )
    return (json_obj, body)


def generate_alert_json(fake, json_obj, received_dt):
    alert_type = fake.random_element(alerts.TYPE)
    server = util.get_random_object(models.db.session, models.Server)
    if not server:
        server = servers.ServerFactory()
        db.session.commit()
    trouble_code = fake.word().upper() + "".join(
        fake.random_elements(tuple(string.digits), length=4)
    )
    hostname = "{}-prod-{}{}".format(
        fake.word().lower(), fake.word().lower(), fake.random_element([1, 2, 3])
    )
    output = fake.text(max_nb_chars=160)
    timestamp = hotpotato_util.datetime_get_as_string(received_dt)

    if alert_type == "service":
        service_name = fake.word()
        state = fake.random_element(alerts.SERVICE_STATE)
        body = (
            "{server_name}: {trouble_code} {hostname} {service_name} state is "
            "{state} ({output}) "
            "{timestamp}"
        ).format(
            server_name=server.hostname,
            trouble_code=trouble_code,
            hostname=hostname,
            service_name=service_name,
            state=state,
            output=output,
            timestamp=timestamp,
        )

    elif alert_type == "host":
        service_name = None
        state = fake.random_element(alerts.HOST_STATE)
        body = (
            "{server_name}: {trouble_code} {hostname} state is "
            "{state} ({output}) "
            "{timestamp}"
        ).format(
            server_name=server.hostname,
            trouble_code=trouble_code,
            hostname=hostname,
            state=state,
            output=output,
            timestamp=timestamp,
        )

    json_obj.update(
        {
            "alert_type": alert_type,
            "server_id": server.id,
            "trouble_code": trouble_code,
            "hostname": hostname,
            "display_name": hostname,
            "service_name": service_name,
            "state": state,
            "output": output,
            "timestamp": timestamp,
        }
    )
    return (json_obj, body)


def create_fake(fake, session):
    """
    Generate a fake message, with randomly set data.
    """

    received_dt = fake.date_time_this_year()
    notif_type = fake.random_element(notifications.TYPE)

    json_obj, body = generate_json(fake, notif_type, received_dt)

    # Create the notification_log database object, and return it.
    notif = models.Notification(
        team_id=util.get_random_object(session, Team).id,
        received_dt=received_dt,
        notif_type=notif_type,
        body=body,
        json=json_obj,
    )

    if notif_type == "message":
        user = util.get_random_object(session, User)
        if user:
            # Keep generating attempts until the last one didn't fail
            while (
                len(notif.send_attempts) == 0
                or notif.send_attempts[-1].failed_to_send()
            ):
                notif.send_attempts.append(attempts.create_fake(fake, notif, user))

    if notif_type == "handover":
        if notif.json["from_user_id"]:
            if notif.json["to_user_id"]:
                while (
                    len(notif.send_attempts) == 0
                    or notif.send_attempts[-1].failed_to_send()
                ):
                    notif.send_attempts.append(
                        attempts.create_fake(
                            fake, notif, users.get(notif.json["to_user_id"])
                        )
                    )
            n = len(notif.send_attempts)
            while (
                len(notif.send_attempts) == n
                or notif.send_attempts[-1].failed_to_send()
            ):
                notif.send_attempts.append(
                    attempts.create_fake(
                        fake, notif, users.get(notif.json["from_user_id"])
                    )
                )
        else:
            if notif.json["to_user_id"]:
                while (
                    len(notif.send_attempts) == 0
                    or notif.send_attempts[-1].failed_to_send()
                ):
                    notif.send_attempts.append(
                        attempts.create_fake(
                            fake, notif, users.get(notif.json["to_user_id"])
                        )
                    )

    if notif_type == "alert":
        user = util.get_random_object(session, User)
        if user:
            while (
                len(notif.send_attempts) == 0
                or notif.send_attempts[-1].failed_to_send()
            ):
                notif.send_attempts.append(attempts.create_fake(fake, notif, user))

    return notif
