from unittest.mock import Mock

import requests
import twilio.rest
from requests.models import Response

from hotpotato.notifications import exceptions, messages, send, senders
from hotpotato.notifications.senders.base import BaseSender
from hotpotato.tests import oncall_contacts, users


def modica_api_mock(url, data=None, json=None, **kwargs):
    """
    Return a successful modica response to outgoing requests
    """
    response = Mock(spec=Response)
    response.status_code = 201
    response.text = "[50]"
    return response


def test_send_modica_sms(monkeypatch, session):
    """
    Create and send a text message to an NZ user and check that
    the request is sent to the modica sender
    """
    oncall_user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(
        user=oncall_user, contact="+646458888555", method="sms"
    )
    session.commit()

    monkeypatch.setattr(requests, "post", modica_api_mock)

    notif = messages.create(team_id=None, body="This is a test message")
    send.using_sender(notif, senders.sms_modica.ModicaSMSSender(), contact)

    assert notif.send_attempts[0].has_been_sent()
    assert notif.send_attempts[0].provider == "modica"


def test_send_twilio_sms(monkeypatch, session):
    """
    Create and send a text message to a non NZ user and check that
    the request is sent to the twilio sender
    """
    oncall_user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(
        user=oncall_user, contact="+15888855555", method="sms"
    )
    session.commit()

    class TwilioMessageMock:
        sid = 50

    create = Mock(return_value=TwilioMessageMock())

    class TwilioMessagesMock:
        def __init__(self):
            self.create = create

    class TwilioClientMock:
        def __init__(self, sid, token):
            self.sid = sid
            self.token = token
            self.messages = TwilioMessagesMock()

    monkeypatch.setattr(twilio.rest, "Client", TwilioClientMock)

    notif = messages.create(team_id=None, body="This is a test message")
    send.using_sender(notif, senders.sms_twilio.TwilioSMSSender(), contact)

    assert create.call_count == 1

    assert notif.send_attempts[0].has_been_sent()
    assert notif.send_attempts[0].provider == "twilio"


def test_send_modica_pager(monkeypatch, session):
    """
    Create and send a text message to an NZ pager user and check that
    the request isin't sent to the modica sender
    """
    oncall_user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(
        user=oncall_user, contact="+646458888555", method="pager"
    )
    session.commit()

    # TODO check that the message is actually sent to the API

    monkeypatch.setattr(requests, "post", modica_api_mock)

    notif = messages.create(team_id=None, body="This is a test message")
    send.using_sender(notif, senders.pager_modica.ModicaPagerSender(), contact)

    assert notif.send_attempts[0].provider == "modica"
    assert notif.send_attempts[0].has_been_sent()


class PushoverSessionMock(requests.Session):
    def get(self, url, **kwargs):
        response = Mock(spec=Response)
        if "limits.json" in url:
            response.status_code = 200
            response.json = lambda: {
                "limit": 7500,
                "remaining": 7494,
                "reset": 1551420000,
                "status": 1,
                "request": "5b848f3c-e7f5-4ff1-9cf4-a70b73227d73",
            }
        return response

    def post(self, url, **kwargs):
        response = Mock(spec=Response)
        if "messages.json" in url:
            response.status_code = 200
            response.json = lambda: {
                "status": 1,
                "request": "5b848f3c-e7f5-4ff1-9cf4-a70b73227d73",
                "receipt": "TEST_RECEIPT",
            }
        return response


def test_send_pushover(app, monkeypatch, session):
    oncall_user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(
        user=oncall_user, contact="a" * 31, method="pushover"
    )
    session.commit()

    monkeypatch.setattr(requests, "Session", PushoverSessionMock)

    notif = messages.create(team_id=None, body="This is a test message")
    with app.app_context():
        send.using_sender(notif, senders.pushover.PushoverSender(), contact)

    session.add(notif)
    assert notif.send_attempts[0].provider == "pushover"
    assert notif.send_attempts[0].has_been_sent()


class MockSender(BaseSender):
    method = "pushover"
    provider = "pushover"

    def __init__(self):
        self.can_send = Mock()
        self.send = Mock(
            return_value={
                "success": True,
                "warnings": [],
                "errors": [],
                "provider_notif_id": "3",
            }
        )


def test_sent_once_for_working_contact_method(session):
    """
    Test that calling send with one contact method creates a send_attempt and sends the notification
    """
    oncall_user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(
        user=oncall_user, contact="a" * 31, method="pushover"
    )
    session.commit()
    notif = messages.create(team_id=None, body="This is a test message")
    mock_sender = MockSender()
    mock_sender.can_send.return_value = True
    send._send(notif, [contact], senders_for_method=lambda m: tuple((mock_sender,)))
    # assert mock_sender.can_send.call_count == 1
    assert mock_sender.send.call_count == 1
    assert len(notif.send_attempts) == 1
    assert notif.send_attempts[0].status == "SENT_TO_PROVIDER"
    assert notif.send_attempts[0].method == "pushover"
    assert notif.send_attempts[0].provider == "pushover"
    assert notif.send_attempts[0].provider_notif_id == "3"


def test_send_to_backup_method_if_first_fails(session):
    """
    Tests that if sending to the first contact throws an error it tries
    sending to the second
    """
    oncall_user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(
        user=oncall_user, contact="a" * 31, method="pushover"
    )
    contact2 = oncall_contacts.OncallContactsFactory(
        user=oncall_user, contact="b" * 31, method="pushover"
    )
    session.commit()
    notif = messages.create(team_id=None, body="This is a test message")
    mock_sender = MockSender()
    mock_sender.send.side_effect = [
        exceptions.NotificationSendError(),
        {"success": True, "warnings": [], "errors": [], "provider_notif_id": "3"},
    ]
    send._send(
        notif, [contact, contact2], senders_for_method=lambda m: tuple((mock_sender,))
    )
    # assert mock_sender.can_send.call_count == 2
    assert mock_sender.send.call_count == 2
    session.refresh(notif)
    assert len(notif.send_attempts) == 2
    assert notif.send_attempts[0].status == "SEND_FAILED"
    assert notif.send_attempts[1].status == "SENT_TO_PROVIDER"
