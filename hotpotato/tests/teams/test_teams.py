import pytest

from hotpotato import teams


def test_create(session):
    """
    Test that a team can be created.
    """

    team = teams.create("Test Team!")

    assert team.name == "Test Team!"

    assert len(list(team.roles)) == 3


def test_create_duplicate_name(session):
    """
    Test that creating a team with a duplicate name throws an error.
    """

    teams.create("Test Team!")
    with pytest.raises(teams.DuplicateTeamName):
        teams.create("Test Team!")
