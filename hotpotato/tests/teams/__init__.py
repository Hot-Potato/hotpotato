"""
Team unit test functions.
"""

import factory

from hotpotato import models, roles
from hotpotato.models import db


class TeamFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Team
        sqlalchemy_session = db.session

    name = factory.Faker("name")
    escalates_to = None

    @factory.post_generation
    def roles(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return
        from hotpotato.tests import roles as roles_test

        for name, data in roles.DEFAULT.items():
            role = roles_test.RoleFactory(
                team=self,
                name=name,
                description=data["description"],
                permissions=data["permissions"],
                priority=data["priority"],
            )
            self.roles.append(role)

        if extracted:
            for role in extracted:
                self.roles.append(role)
