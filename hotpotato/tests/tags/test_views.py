"""
Test server related views
"""

from flask import url_for

from hotpotato import roles
from hotpotato.models import Tag
from hotpotato.tests import users
from hotpotato.tests.conftest import login


def test_creating_tag(client, session, app):
    """
    Test that creating a tag from the create tag form creates the tag.
    """
    app.config["FEATURE_TAGS"] = True
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.add_tag",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
            ),
            data={"name": "Planned work"},
            follow_redirects=True,
        )
        assert resp.status_code == 200

    tag = Tag.query.filter_by(name="Planned work").first()
    assert tag


def test_creating_duplicate_tag(client, session, app):
    """
    Test that creating a tag with a name that already exists returns an error.
    """
    app.config["FEATURE_TAGS"] = True
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    tag = Tag(name="Planned work", team=user.primary_team)
    session.add(tag)
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.add_tag",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
            ),
            data={"name": "Planned work"},
            follow_redirects=True,
        )
        assert resp.status_code == 200

    tags = Tag.query.filter_by(name="Planned work").all()
    assert len(tags) == 1


def test_creating_empty_name(client, session, app):
    """
    Test that if you try to add a tag with an empty name it returns an error.
    """
    app.config["FEATURE_TAGS"] = True
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.add_tag",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
            ),
            data={"name": ""},
            follow_redirects=True,
        )
        assert resp.status_code == 200

    tags = Tag.query.all()
    assert not tags


def test_creating_empty_no_name(client, session, app):
    """
    Test that if you don't submit a name it returns an error.
    """
    app.config["FEATURE_TAGS"] = True
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.add_tag",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
            ),
            data={},
            follow_redirects=True,
        )
        assert resp.status_code == 200

    tags = Tag.query.all()
    assert not tags


def test_get_missing_tag(client, session, app):
    """
    Test getting the page for a tag that doesn't exist returns 404.
    """
    app.config["FEATURE_TAGS"] = True
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.get(
            url_for(
                "views_admin.edit_tag",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
                tag_id=54321,
            )
        )
        assert resp.status_code == 404


def test_get_real_tag(client, session, app):
    """
    Test that getting the page for a tag returns successfully.
    """
    app.config["FEATURE_TAGS"] = True
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    tag = Tag(name="Planned work", team=user.primary_team)
    session.add(tag)
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.get(
            url_for(
                "views_admin.edit_tag",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
                tag_id=tag.id,
            ),
            follow_redirects=True,
        )
        assert resp.status_code == 200


def test_edit_duplicate_tag(client, session, app):
    """
    Test that changing a tag to a name that already exists returns an error.
    """
    app.config["FEATURE_TAGS"] = True
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    tag = Tag(name="Planned work", team=user.primary_team)
    tag2 = Tag(name="Self-resolved", team=user.primary_team)
    session.add(tag)
    session.add(tag2)
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.edit_tag",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
                tag_id=tag.id,
            ),
            data={"name": "Self-resolved"},
        )
        assert resp.status_code == 200

    tags = Tag.query.filter_by(name="Self-resolved").all()
    assert len(tags) == 1


def test_edit_duplicate_tag_to_self(client, session, app):
    """
    Test that changing a tag name to its self succeeds.
    """
    app.config["FEATURE_TAGS"] = True
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    tag = Tag(name="Planned work", team=user.primary_team)
    session.add(tag)
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.edit_tag",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
                tag_id=tag.id,
            ),
            data={"name": "Planned work"},
        )

        assert resp.status_code == 302

    tags = Tag.query.filter_by(name="Planned work").all()
    assert len(tags) == 1


def test_edit_missing_tag(client, session, app):
    """
    Test that posting the tag edit form for a missing tag returns 404.
    """
    app.config["FEATURE_TAGS"] = True
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.edit_tag",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
                tag_id=54321,
            ),
            data={"name": "Self-resolved"},
        )
        assert resp.status_code == 404

    tags = Tag.query.all()
    assert not tags


def test_edit_tag(client, session, app):
    """
    Test that successfully posting to the tag edit form changes the tag name.
    """
    app.config["FEATURE_TAGS"] = True
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    tag = Tag(name="Planned work", team=user.primary_team)
    session.add(tag)
    session.commit()
    tag_id = tag.id

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.edit_tag",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
                tag_id=tag_id,
            ),
            data={"name": "Self-resolved"},
            follow_redirects=True,
        )
        assert resp.status_code == 200

    tag = Tag.query.get(tag_id)
    assert tag.name == "Self-resolved"


def test_edit_tag_no_name(client, session, app):
    """
    Test that leaving the name field out on the edit form returns an error.
    """
    app.config["FEATURE_TAGS"] = True
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    tag = Tag(name="Planned work", team=user.primary_team)
    session.add(tag)
    session.commit()
    tag_id = tag.id

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.edit_tag",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
                tag_id=tag_id,
            ),
            data={},
            follow_redirects=True,
        )
        assert resp.status_code == 200

    tag = Tag.query.get(tag_id)
    assert tag.name == "Planned work"


def test_edit_tag_empty_name(client, session, app):
    """
    Test that leaving the name field blank on the edit form returns an error.
    """
    app.config["FEATURE_TAGS"] = True
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    tag = Tag(name="Planned work", team=user.primary_team)
    session.add(tag)
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.edit_tag",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
                tag_id=tag.id,
            ),
            data={"name": ""},
        )
        assert resp.status_code == 200

    tag = Tag.query.get(tag.id)
    assert tag.name == "Planned work"
