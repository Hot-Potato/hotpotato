"""
Test account related views
"""


from flask import url_for

from hotpotato.models import Team, TeamsUsers
from hotpotato.tests import teams, users
from hotpotato.tests.conftest import login


def test_set_primary(app, client, session):
    """
    Test changing primary team
    """
    team = teams.TeamFactory()
    user = users.UserFactory()
    session.add(TeamsUsers(primary=False, user=user, team=team))
    session.commit()
    team_id = team.id
    with app.app_context(), login(client, user):
        r = client.post(
            url_for("views_account.account_team", team_id=user.primary_team.id),
            data=dict(action="set_primary", team_id=team_id),
            follow_redirects=True,
        )

        assert r.status_code == 200

    user = session.merge(user)
    session.refresh(user)
    assert user.primary_team.id == team_id


def test_set_primary_invalid_team(app, client, session):
    """
    Test changing primary team
    """
    user = users.UserFactory()
    session.commit()
    primary_id = user.primary_team.id
    with app.app_context(), login(client, user):
        r = client.post(
            url_for("views_account.account_team", team_id=user.primary_team.id),
            data=dict(action="set_primary", team_id=12345),
            follow_redirects=True,
        )

        assert r.status_code == 200

    user = session.merge(user)
    session.refresh(user)
    assert user.primary_team.id == primary_id


def test_set_primary_team_not_in(app, client, session):
    """
    Test changing primary team
    """
    team = teams.TeamFactory()
    user = users.UserFactory()
    session.commit()
    primary_id = user.primary_team.id
    team_id = team.id
    with app.app_context(), login(client, user):
        r = client.post(
            url_for("views_account.account_team", team_id=user.primary_team.id),
            data=dict(action="set_primary", team_id=team_id),
            follow_redirects=True,
        )

        assert r.status_code == 200

    user = session.merge(user)
    session.refresh(user)
    assert user.primary_team.id == primary_id


def test_leave_team(app, client, session):
    """
    Test changing primary team
    """
    team = teams.TeamFactory()
    user = users.UserFactory()
    session.add(TeamsUsers(primary=False, user=user, team=team))
    session.commit()
    team_id = team.id
    with app.app_context(), login(client, user):
        r = client.post(
            url_for("views_account.account_team", team_id=user.primary_team.id),
            data=dict(action="leave", team_id=team_id),
            follow_redirects=True,
        )

        assert r.status_code == 200

    user = session.merge(user)
    team = session.merge(team)
    session.refresh(user)
    session.refresh(team)
    assert team not in user.teams


def test_leave_team_doesnt_exist(app, client, session):
    """
    Test changing primary team
    """
    user = users.UserFactory()
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post(
            url_for("views_account.account_team", team_id=user.primary_team.id),
            data=dict(action="leave", team_id=12345),
            follow_redirects=True,
        )

        assert r.status_code == 200


def test_leave_primary_team(app, client, session):
    """
    Test changing primary team
    """
    user = users.UserFactory()
    session.commit()
    primary_id = user.primary_team.id
    with app.app_context(), login(client, user):
        r = client.post(
            url_for("views_account.account_team", team_id=user.primary_team.id),
            data=dict(action="leave", team_id=primary_id),
            follow_redirects=True,
        )

        assert r.status_code == 200

    user = session.merge(user)
    primary_team = Team.query.get(primary_id)
    session.refresh(user)
    assert primary_team in user.teams


def test_no_action(app, client, session):
    """
    Test changing primary team
    """
    user = users.UserFactory()
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post(
            url_for("views_account.account_team", team_id=user.primary_team.id),
            follow_redirects=True,
        )

        assert r.status_code == 200


def test_unknown_action(app, client, session):
    """
    Test changing primary team
    """
    user = users.UserFactory()
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post(
            url_for("views_account.account_team", team_id=user.primary_team.id),
            data=dict(action="unknown-action", team_id=12345),
            follow_redirects=True,
        )

        assert r.status_code == 200


def test_changing_timezone(app, client, session):
    """
    Test changing primary team
    """
    team = teams.TeamFactory()
    user = users.UserFactory(timezone="UTC")
    session.add(TeamsUsers(primary=False, user=user, team=team))
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post(
            url_for("views_account.change_timezone", team_id=user.primary_team.id),
            data=dict(timezone="Pacific/Auckland"),
            follow_redirects=True,
        )

        assert r.status_code == 200

    user = session.merge(user)
    session.refresh(user)
    assert user.timezone == "Pacific/Auckland"


def test_changing_language(app, client, session):
    """
    Test changing primary team
    """
    app.config["FEATURE_TRANSLATIONS"] = True
    team = teams.TeamFactory()
    user = users.UserFactory(language="de")
    session.add(TeamsUsers(primary=False, user=user, team=team))
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post(
            url_for("views_account.change_language", team_id=user.primary_team.id),
            data=dict(language="en"),
            follow_redirects=True,
        )

        assert r.status_code == 200

    user = session.merge(user)
    session.refresh(user)
    assert user.language == "en"
