import factory
from sqlalchemy import Text
from sqlalchemy.dialects.postgresql import array

from hotpotato import models, notifications
from hotpotato.tests import oncall_contacts


class AttemptFactory(factory.alchemy.SQLAlchemyModelFactory):
    notification = None
    status = factory.Faker("random_element", elements=notifications.STATUS)
    warnings = []
    errors = []
    method = factory.Faker("random_element", elements=notifications.METHOD)
    provider = factory.Faker("random_element", elements=notifications.PROVIDER)
    provider_notif_id = factory.Faker("pyint")

    @factory.post_generation
    def contact(self, create, user, **kwargs):
        if not user.contacts:
            user.contacts.append(oncall_contacts.OncallContactsFactory())
        self.user = factory.Faker("random_element", elements=user)


def create_fake(fake, notification, user):
    return models.NotificationAttempt(
        notification=notification,
        user=user,
        status=fake.random_element(notifications.STATUS),
        warnings=array([], type_=Text),
        errors=array([], type_=Text),
        method=fake.random_element(notifications.METHOD),
        provider=fake.random_element(notifications.PROVIDER),
        provider_notif_id=str(fake.pyint()),
    )
