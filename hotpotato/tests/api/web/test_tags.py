from http import HTTPStatus

from hotpotato import roles
from hotpotato.models import Tag
from hotpotato.notifications import notifications
from hotpotato.tests import notifications as test_notifications, users
from hotpotato.tests.conftest import login


def test_adding_tag(client, session, app):
    """
    Test adding a tag to an alert via the API.
    """
    user = users.UserFactory()
    session.commit()
    tag = Tag(name="Planned work", team=user.primary_team)
    notif = test_notifications.AlertFactory(team=user.primary_team)
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    session.add(tag)
    session.commit()
    notif_id = notif.id
    tag_id = tag.id

    with app.app_context(), login(client, user):
        resp = client.post(
            "/api/web/v1/notifications/alerts/{}/add_tag?team_id={}".format(
                notif.id, user.primary_team.id
            ),
            json={"id": tag.id},
        )

        assert resp.status_code == HTTPStatus.OK

    notif = notifications.get(notif_id)
    assert tag_id == notif.tags[0].id


def test_adding_tag_to_handover(client, session, app):
    """
    Test adding a tag to a handover returns 404
    """
    user = users.UserFactory()
    session.commit()
    tag = Tag(name="Planned work", team=user.primary_team)
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    notif = test_notifications.NotificationFactory(
        team=user.primary_team, notif_type="handover"
    )
    session.add(tag)
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            "/api/web/v1/notifications/alerts/{}/add_tag?team_id={}".format(
                notif.id, user.primary_team.id
            ),
            json={"id": tag.id},
        )
        print(resp.json)

        assert resp.status_code == HTTPStatus.NOT_FOUND


def test_adding_duplicate_tag(client, session, app):
    """
    Test adding a tag that already exists on an alert returns an error.
    """
    user = users.UserFactory()
    session.commit()
    tag = Tag(name="Planned work", team=user.primary_team)
    notif = test_notifications.AlertFactory(team=user.primary_team)
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    session.add(tag)
    session.commit()
    notif_id = notif.id
    team_id = user.primary_team.id
    tag_id = tag.id

    with app.app_context(), login(client, user):
        resp = client.post(
            f"/api/web/v1/notifications/alerts/{notif_id}/add_tag?team_id={team_id}",
            json={"id": tag_id},
        )

        assert resp.status_code == HTTPStatus.OK

    session.add(user)

    with app.app_context(), login(client, user):
        resp = client.post(
            f"/api/web/v1/notifications/alerts/{notif_id}/add_tag?team_id={team_id}",
            json={"id": tag_id},
        )

        print(resp.json)
        assert resp.status_code == HTTPStatus.UNPROCESSABLE_ENTITY

    notif = notifications.get(notif_id)
    assert len(notif.tags) == 1


def test_adding_missing_alert(client, session, app):
    """
    Test that adding a tag to an alert that doesn't exist returns 404.
    """
    user = users.UserFactory()
    session.commit()
    tag = Tag(name="Planned work", team=user.primary_team)
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    session.add(tag)
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            "/api/web/v1/notifications/alerts/5/add_tag?team_id={}".format(
                user.primary_team.id
            ),
            json={"id": tag.id},
        )

        assert resp.status_code == HTTPStatus.NOT_FOUND


def test_adding_missing_tag(client, session, app):
    """
    Test that trying to add a tag that doesn't exists returns an error.
    """
    user = users.UserFactory()
    session.commit()
    notif = test_notifications.AlertFactory(team=user.primary_team)
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            "/api/web/v1/notifications/alerts/{}/add_tag?team_id={}".format(
                notif.id, user.primary_team.id
            ),
            json={"id": 5},
        )
        print(resp.data)
        assert resp.status_code == HTTPStatus.UNPROCESSABLE_ENTITY

    notif = notifications.get(notif.id)
    assert not notif.tags


def test_removing_tag(client, session, app):
    """
    Test that removing a tag from an alert via the API works.
    """
    user = users.UserFactory()
    session.commit()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    tag = Tag(name="Planned work", team=user.primary_team)
    notif = test_notifications.AlertFactory(team=user.primary_team)
    notif.tags.append(tag)
    session.add(tag)
    session.commit()
    notif_id = notif.id

    with app.app_context(), login(client, user):
        resp = client.post(
            "/api/web/v1/notifications/alerts/{}/remove_tag?team_id={}".format(
                notif.id, user.primary_team.id
            ),
            json={"id": tag.id},
        )

        assert resp.status_code == HTTPStatus.OK

    notif = notifications.get(notif_id)
    assert not notif.tags


def test_removing_tag_from_handover(client, session, app):
    """
    Test that removing a tag from an alert via the API works.
    """
    user = users.UserFactory()
    session.commit()
    tag = Tag(name="Planned work", team=user.primary_team)
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    notif = test_notifications.NotificationFactory(notif_type="handover")
    session.add(tag)
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            "/api/web/v1/notifications/alerts/{}/remove_tag?team_id={}".format(
                notif.id, user.primary_team.id
            ),
            json={"id": tag.id},
        )

        assert resp.status_code == HTTPStatus.NOT_FOUND


def test_removing_tag_dosnt_exist(client, session, app):
    """
    Test that removing a tag that doen't exist throws an error.
    """
    user = users.UserFactory()
    session.commit()
    tag = Tag(name="Planned work", team=user.primary_team)
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    notif = test_notifications.AlertFactory(team=user.primary_team)
    notif.tags.append(tag)
    session.add(tag)
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            "/api/web/v1/notifications/alerts/{}/remove_tag?team_id={}".format(
                notif.id, user.primary_team.id
            ),
            json={"id": 5},
        )

        assert resp.status_code == HTTPStatus.UNPROCESSABLE_ENTITY

    notif = notifications.get(notif.id)
    assert len(notif.tags) == 1


def test_removing_tag_not_on_alert(client, session, app):
    """
    Test that removing a tag that doen't exist throws an error.
    """
    user = users.UserFactory()
    session.commit()
    tag = Tag(name="Planned work", team=user.primary_team)
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    notif = test_notifications.AlertFactory(team=user.primary_team)
    session.add(tag)
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            "/api/web/v1/notifications/alerts/{}/remove_tag?team_id={}".format(
                notif.id, user.primary_team.id
            ),
            json={"id": tag.id},
        )

        assert resp.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_removing_missing_alert(client, session, app):
    """
    Test that trying to remove a tag from an alert that doesn't exist returns 404.
    """
    user = users.UserFactory()
    session.commit()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    tag = Tag(name="Planned work", team=user.primary_team)
    session.add(tag)
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            "/api/web/v1/notifications/alerts/{}/remove_tag?team_id={}".format(
                5, user.primary_team.id
            ),
            json={"id": 5},
        )

        print(resp.data)

        assert resp.status_code == HTTPStatus.NOT_FOUND
