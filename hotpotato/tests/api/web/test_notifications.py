import csv
from datetime import datetime
from http import HTTPStatus

import pytest

from hotpotato import roles, teams
from hotpotato.models import TeamsUsers
from hotpotato.tests import notifications, users
from hotpotato.tests.conftest import login


def test_get_notifications_no_notifications(app, client, session):
    """
    Test that a request for a period with no notifications returns 200.
    """
    user = users.UserFactory()
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    session.commit()
    with app.app_context(), login(client, user):
        r = client.get(
            "/api/web/v1/notifications/get?end_date=2018-11-27T00:00Z&start_date=2018-11-26T00:00Z&team_id={}".format(
                team.id
            )
        )

        assert r.status_code == HTTPStatus.OK
        assert r.json["success"] is True


def test_get_notifications_one_notification(app, client, session):
    """
    Test that a request for a period with one notification returns that one notification.
    """
    user = users.UserFactory(timezone="UTC")
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    session.commit()
    with app.app_context(), login(client, user):
        alert = notifications.AlertFactory(
            notif_type="alert", received_dt=datetime(2018, 11, 26, hour=11), team=team
        )
        notifications.AlertFactory(
            notif_type="alert", received_dt=datetime(2018, 11, 25, hour=11), team=team
        )
        session.commit()
        print(alert.received_dt)
        r = client.get(
            "/api/web/v1/notifications/get?end_date=2018-11-27T00:00Z&start_date=2018-11-26T00:00Z&team_id={}".format(
                team.id
            )
        )

        assert r.status_code == HTTPStatus.OK
        assert r.json["success"] is True
        assert r.json["data"][str(alert.id)]["id"] == alert.id
        assert len(r.json["data"]) == 1


def test_get_notification_by_id(app, client, session):
    """
    Test that getting a single notification succeeds.
    """
    user = users.UserFactory()
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    session.commit()
    with app.app_context(), login(client, user):
        alert = notifications.AlertFactory(
            notif_type="alert", received_dt=datetime(2018, 11, 26, hour=11), team=team
        )
        session.commit()
        r = client.get(f"/api/web/v1/notifications/get/{alert.id}?team_id={team.id}")

        print(r.json)

        assert r.status_code == HTTPStatus.OK
        assert r.json["success"] is True
        assert r.json["data"]["id"] == alert.id


def test_get_notification_by_id_dosnt_exist(app, client, session):
    """
    Test that getting a single notification that doesn't exists 404s.
    """
    user = users.UserFactory()
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    session.commit()
    with app.app_context(), login(client, user):
        r = client.get(f"/api/web/v1/notifications/get/124235236?team_id={team.id}")

        print(r.json)

        assert r.status_code == HTTPStatus.NOT_FOUND


def test_get_notifications_as_csv_by_id(app, client, session):
    """
    Test that getting a notification as a CSV returns 200.
    """
    user = users.UserFactory(timezone="Pacific/Auckland")
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    session.commit()
    with app.app_context(), login(client, user):
        alert = notifications.AlertFactory(
            notif_type="alert", received_dt=datetime(2018, 11, 26, hour=11), team=team
        )
        session.commit()
        r = client.get(
            f"/api/web/v1/notifications/get/{alert.id}/as_csv?team_id={team.id}"
        )

        assert r.status_code == HTTPStatus.OK
        reader = csv.DictReader(r.get_data(as_text=True).splitlines())

        row = next(reader)
        assert row["id"] == str(alert.id)
        assert row["received_dt"] == "2018-11-27T00:00:00+13:00"


def test_get_notifications_as_csv_by_id_dosnt_exist(app, client, session):
    """
    Test that getting a notification as a CSV that dosn't exist returns 404.
    """
    user = users.UserFactory()
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    session.commit()
    with app.app_context(), login(client, user):
        r = client.get(
            f"/api/web/v1/notifications/get/4235235423/as_csv?team_id={team.id}"
        )

        assert r.status_code == HTTPStatus.NOT_FOUND


def test_get_notifications_as_csv_no_notifications(app, client, session):
    """
    Test that getting the notifications as a CSV returns 200.
    """
    user = users.UserFactory()
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    session.commit()
    with app.app_context(), login(client, user):
        r = client.get(
            "/api/web/v1/notifications/get/as_csv?end_date=2018-11-27T00:00Z&start_date=2018-11-26T00:00Z&team_id={}".format(
                team.id
            )
        )

        assert r.status_code == HTTPStatus.OK
        reader = csv.reader(r.get_data(as_text=True).splitlines())
        row = next(reader)

        assert row == [
            "id",
            "team_id",
            "received_dt",
            "notif_type",
            "body",
            "version",
            "node_name",
            "event_id",
            "ticket_id",
            "warnings",
            "errors",
            "status",
            "method",
            "provider",
            "provider_notif_id",
        ]

        with pytest.raises(StopIteration):
            next(reader)


def test_get_notifications_as_csv_one_notification(app, client, session):
    """
    Test that getting a notification as a CSV returns 200.
    """
    user = users.UserFactory(timezone="UTC")
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    session.commit()
    with app.app_context(), login(client, user):
        alert = notifications.AlertFactory(
            notif_type="alert", received_dt=datetime(2018, 11, 26, hour=11), team=team
        )
        notifications.AlertFactory(
            notif_type="alert", received_dt=datetime(2018, 11, 25, hour=11), team=team
        )
        session.commit()
        r = client.get(
            "/api/web/v1/notifications/get/as_csv?end_date=2018-11-27T00:00Z&start_date=2018-11-26T00:00Z&team_id={}".format(
                team.id
            )
        )

        assert r.status_code == HTTPStatus.OK
        reader = csv.DictReader(r.get_data(as_text=True).splitlines())

        row = next(reader)
        assert row["id"] == str(alert.id)
        assert row["received_dt"] == "2018-11-26T11:00:00+00:00"


def test_get_handover_no_handover(app, client, session):
    """
    Test that a request for a period with no handovers returns 200.
    """
    user = users.UserFactory()
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    session.commit()
    with app.app_context(), login(client, user):
        r = client.get(
            "/api/web/v1/notifications/handovers/get?end_date=2018-11-27T00:00Z&start_date=2018-11-26T00:00Z&team_id={}".format(
                team.id
            )
        )

        assert r.status_code == HTTPStatus.OK
        assert r.json["success"] is True


def test_get_handover_as_csv_no_handovers(app, client, session):
    """
    Test that getting the notifications as a CSV returns 200.
    """
    user = users.UserFactory()
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    session.commit()
    with app.app_context(), login(client, user):
        r = client.get(
            "/api/web/v1/notifications/handovers/get/as_csv?end_date=2018-11-27T00:00Z&start_date=2018-11-26T00:00Z&team_id={}".format(
                team.id
            )
        )

        assert r.status_code == HTTPStatus.OK
        reader = csv.reader(r.get_data(as_text=True).splitlines())
        row = next(reader)

        assert row == [
            "id",
            "team_id",
            "received_dt",
            "notif_type",
            "body",
            "version",
            "node_name",
            "event_id",
            "ticket_id",
            "warnings",
            "errors",
            "status",
            "method",
            "provider",
            "provider_notif_id",
            "to_user_id",
            "from_user_id",
            "message",
        ]

        with pytest.raises(StopIteration):
            next(reader)


def test_get_alert_as_csv_no_alerts(app, client, session):
    """
    Test that getting the notifications as a CSV returns 200.
    """
    user = users.UserFactory()
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    session.commit()
    with app.app_context(), login(client, user):
        r = client.get(
            "/api/web/v1/notifications/alerts/get/as_csv?end_date=2018-11-27T00:00Z&start_date=2018-11-26T00:00Z&team_id={}".format(
                team.id
            )
        )

        assert r.status_code == HTTPStatus.OK
        reader = csv.reader(r.get_data(as_text=True).splitlines())
        row = next(reader)

        assert row == [
            "id",
            "team_id",
            "received_dt",
            "notif_type",
            "body",
            "version",
            "node_name",
            "event_id",
            "ticket_id",
            "warnings",
            "errors",
            "status",
            "method",
            "provider",
            "provider_notif_id",
            "alert_type",
            "server_id",
            "trouble_code",
            "hostname",
            "display_name",
            "service_name",
            "state",
            "output",
            "timestamp",
        ]

        with pytest.raises(StopIteration):
            next(reader)


def test_get_message_as_csv_no_messages(app, client, session):
    """
    Test that getting the notifications as a CSV returns 200.
    """
    user = users.UserFactory()
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    session.commit()
    with app.app_context(), login(client, user):
        r = client.get(
            "/api/web/v1/notifications/messages/get/as_csv?end_date=2018-11-27T00:00Z&start_date=2018-11-26T00:00Z&team_id={}".format(
                team.id
            )
        )

        assert r.status_code == HTTPStatus.OK
        reader = csv.reader(r.get_data(as_text=True).splitlines())
        row = next(reader)

        assert row == [
            "id",
            "team_id",
            "received_dt",
            "notif_type",
            "body",
            "version",
            "node_name",
            "event_id",
            "ticket_id",
            "warnings",
            "errors",
            "status",
            "method",
            "provider",
            "provider_notif_id",
            "from_user_id",
            "reply_to_notif_id",
            "message",
        ]

        with pytest.raises(StopIteration):
            next(reader)


def test_get_unknown_handler_404(app, client, session):
    """
    Test that a request for a period with no handovers returns 200.
    """
    user = users.UserFactory()
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    session.commit()
    with app.app_context(), login(client, user):
        r = client.get(
            "/api/web/v1/notifications/madeup/get?end_date=2018-11-27T00:00Z&start_date=2018-11-26T00:00Z&team_id={}".format(
                team.id
            )
        )

        assert r.status_code == HTTPStatus.NOT_FOUND


def test_get_alert_by_id(app, client, session):
    """
    Test that getting a single alert by id succeeds.
    """
    user = users.UserFactory()
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    session.commit()
    with app.app_context(), login(client, user):
        alert = notifications.AlertFactory(
            notif_type="alert", received_dt=datetime(2018, 11, 26, hour=11), team=team
        )
        session.commit()
        r = client.get(
            f"/api/web/v1/notifications/alerts/get/{alert.id}?team_id={team.id}"
        )

        print(r.json)

        assert r.status_code == HTTPStatus.OK
        assert r.json["success"] is True
        assert r.json["data"]["id"] == alert.id


def test_get_alert_as_csv_by_id(app, client, session):
    """
    Test that getting a alert by id as a CSV succeeds.
    """
    user = users.UserFactory(timezone="Pacific/Auckland")
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    session.commit()
    with app.app_context(), login(client, user):
        alert = notifications.AlertFactory(
            notif_type="alert", received_dt=datetime(2018, 11, 26, hour=11), team=team
        )
        session.commit()
        r = client.get(
            f"/api/web/v1/notifications/alerts/get/{alert.id}/as_csv?team_id={team.id}"
        )

        assert r.status_code == HTTPStatus.OK
        reader = csv.DictReader(r.get_data(as_text=True).splitlines())

        row = next(reader)
        assert row["id"] == str(alert.id)
        assert row["received_dt"] == "2018-11-27T00:00:00+13:00"
