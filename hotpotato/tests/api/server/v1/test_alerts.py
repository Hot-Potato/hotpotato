"""
Test the V1 add notification endpoint
"""

from http import HTTPStatus

from hotpotato.notifications import alerts
from hotpotato.tests import notifications, servers, teams, users


def test_add_notification_empty_request(client, session):
    """
    Test that you get a 400 if you don't put anything in the request.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    session.commit()

    r = client.post(
        "/api/server/v1/add/notification",
        data=dict(apikey="{}.testingapi".format(server.apikey_id)),
    )

    assert r.status_code == HTTPStatus.BAD_REQUEST


def test_add_notification_bad_apikey(client, session):
    """
    Test that you get a 403 if you use a bad API key.
    """
    r = client.post(
        "/api/server/v1/add/notification", data=dict(apikey="not-a-valid-apikey")
    )

    assert r.status_code == HTTPStatus.FORBIDDEN


def test_add_notification_success(client, session):
    """
    Test that if you submit a well formed request the notification is created and the request
    returns 201.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    session.commit()

    r = client.post(
        "/api/server/v1/add/notification",
        data=dict(
            apikey="{}.testingapi".format(server.apikey_id),
            troublecode="EXAMPLE123",
            hostname="criticalinfra.example.com",
            displayname="Critical Infrastructure",
            icingadatetime="2018-11-22 11:18:22 +0000",
            servicename="nginx",
            state="Anything right?",
            output="CRITICAL everything is broken",
        ),
    )

    assert r.status_code == HTTPStatus.CREATED

    alert = alerts.get(int(r.data))

    assert alert.json["output"] == "CRITICAL everything is broken"
    assert alert.json["timestamp"] == "2018-11-22 11:18:22"


def test_add_notification_invalid_timestamp(client, session):
    """
    Test that if you submit an invalid timestamp it returns 400.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    session.commit()

    r = client.post(
        "/api/server/v1/add/notification",
        data=dict(
            apikey="{}.testingapi".format(server.apikey_id),
            troublecode="EXAMPLE123",
            hostname="criticalinfra.example.com",
            displayname="Critical Infrastructure",
            icingadatetime="notatimestamp",
            servicename="nginx",
            state="Anything right?",
            output="CRITICAL everything is broken",
        ),
    )

    assert r.status_code == HTTPStatus.BAD_REQUEST


def test_add_notification_success_with_team(client, session):
    """
    Test that if you submit a well formed request with a team the notification is created, the request
    returns 201 and the alert is on the correct team.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    user = users.UserFactory()
    team = teams.TeamFactory(on_call=user)
    session.commit()

    r = client.post(
        "/api/server/v1/add/notification",
        data=dict(
            apikey="{}.testingapi".format(server.apikey_id),
            troublecode="EXAMPLE123",
            hostname="criticalinfra.example.com",
            displayname="Critical Infrastructure",
            icingadatetime="2018-11-22 11:18:22 +0000",
            servicename="nginx",
            state="Anything right?",
            output="CRITICAL everything is broken",
            team_id=team.id,
        ),
    )

    assert r.status_code == HTTPStatus.CREATED

    alert = alerts.get(int(r.data))

    assert alert.json["output"] == "CRITICAL everything is broken"
    assert alert.json["timestamp"] == "2018-11-22 11:18:22"
    assert alert.team == team


def test_add_notification_no_timezone(client, session):
    """
    Test that when you submit a notification without timezone info in the timestamp, the
    servers timezone will be fetched and the time will be processed as if it's in that timezone.
    """
    server = servers.ServerFactory(
        timezone="Pacific/Auckland", api_key_secret_unhashed="testingapi"
    )
    session.commit()

    r = client.post(
        "/api/server/v1/add/notification",
        data=dict(
            apikey="{}.testingapi".format(server.apikey_id),
            troublecode="EXAMPLE123",
            hostname="criticalinfra.example.com",
            displayname="Critical Infrastructure",
            icingadatetime="2018-11-22 11:18:22",
            servicename="nginx",
            state="Anything right?",
            output="CRITICAL everything is broken",
        ),
    )

    assert r.status_code == HTTPStatus.CREATED

    alert = alerts.get(int(r.data))

    assert alert.json["output"] == "CRITICAL everything is broken"
    assert alert.json["timestamp"] == "2018-11-21 22:18:22"


def test_add_notification_iso8601_timestamp(client, session):
    """
    Test that when you submit a notification with an iso8601 timestamp it is created correctly.
    """
    server = servers.ServerFactory(
        timezone="Pacific/Auckland", api_key_secret_unhashed="testingapi"
    )
    session.commit()

    r = client.post(
        "/api/server/v1/add/notification",
        data=dict(
            apikey="{}.testingapi".format(server.apikey_id),
            troublecode="EXAMPLE123",
            hostname="criticalinfra.example.com",
            displayname="Critical Infrastructure",
            icingadatetime="2018-11-22T11:18:22+0000",
            servicename="nginx",
            state="Anything right?",
            output="CRITICAL everything is broken",
        ),
    )

    assert r.status_code == HTTPStatus.CREATED

    alert = alerts.get(int(r.data))

    assert alert.json["output"] == "CRITICAL everything is broken"
    assert alert.json["timestamp"] == "2018-11-22 11:18:22"


def test_add_notification_no_servicename(client, session):
    """
    Test that if you leave out the servicename a host type notification is
    created and it returns 201.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    session.commit()

    r = client.post(
        "/api/server/v1/add/notification",
        data=dict(
            apikey="{}.testingapi".format(server.apikey_id),
            troublecode="EXAMPLE123",
            hostname="criticalinfra.example.com",
            displayname="Critical Infrastructure",
            icingadatetime="2018-11-22 11:18:22 +0000",
            state="Anything right?",
            output="CRITICAL everything is broken",
        ),
    )

    assert r.status_code == HTTPStatus.CREATED

    alert = alerts.get(int(r.data))

    assert alert.json["alert_type"] == "host"
    assert alert.json["output"] == "CRITICAL everything is broken"
    assert alert.json["timestamp"] == "2018-11-22 11:18:22"


def test_check_notification_bad_apikey(client, session):
    """
    Test that when you check the status of a notification with a bad apikey you get 403
    """
    r = client.post("/api/server/v1/check/notification", data=dict(apikey="made-upkey"))

    assert r.status_code == HTTPStatus.FORBIDDEN


def test_check_notification_invalid_id(client, session):
    """
    Test that when you check the status of a notification that doesn't exist
    you get a 404.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    session.commit()

    r = client.post(
        "/api/server/v1/check/notification",
        data=dict(apikey="{}.testingapi".format(server.apikey_id), id=4),
    )

    assert r.status_code == HTTPStatus.NOT_FOUND


def test_check_notification_no_id(client, session):
    """
    Test that if you don't include an id you get a 400.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    session.commit()

    r = client.post(
        "/api/server/v1/check/notification",
        data=dict(apikey="{}.testingapi".format(server.apikey_id)),
    )

    print(r.status_code)
    assert r.status_code == HTTPStatus.BAD_REQUEST


def test_check_notification_success(client, session):
    """
    Tests that if you check for an alert that exists you get a 200.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    users.UserFactory()
    alert = notifications.NotificationFactory(notif_type="alert")
    session.commit()

    r = client.post(
        "/api/server/v1/check/notification",
        data=dict(apikey="{}.testingapi".format(server.apikey_id), id=alert.id),
    )

    assert r.status_code == HTTPStatus.OK
    assert r.get_data(as_text=True) == "true"
