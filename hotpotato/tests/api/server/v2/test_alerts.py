"""
Test the V2 alert endpoints
"""

from http import HTTPStatus

import pytest

from hotpotato.api.server.v2 import exceptions, handlers
from hotpotato.notifications import alerts
from hotpotato.tests import notifications, servers, teams, users


def test_get_invalid_alert(client, session):
    """
    Test getting an alert that doesn't exist returns 404.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    session.commit()
    r = client.get(
        "/api/server/v2/alerts/get/5",
        headers={"Authorization": "apikey {}testingapi".format(server.apikey_id)},
    )

    assert r.status_code == HTTPStatus.NOT_FOUND


def test_get_valid_alert(client, session):
    """
    Test getting an alert that does exist returns 200 and the alert.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    users.UserFactory()
    session.commit()
    alert = notifications.AlertFactory(notif_type="alert")
    session.commit()

    r = client.get(
        "/api/server/v2/alerts/get/{}".format(alert.id),
        headers={"Authorization": "apikey {}testingapi".format(server.apikey_id)},
    )

    assert r.status_code == HTTPStatus.OK
    assert r.json["data"]["id"] == alert.id
    assert r.json["data"]["output"] == alert.json["output"]


def test_add_notification_empty_request(client, session):
    """
    Test adding a notification with an empty request returns 422.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    session.commit()

    r = client.post(
        "/api/server/v2/alerts/send",
        json={},
        headers={"Authorization": "apikey {}testingapi".format(server.apikey_id)},
    )

    assert r.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_add_notification_success(client, session):
    """
    Test adding a valid notification creates the notification and returns 201.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    user = users.UserFactory()
    team = teams.TeamFactory()
    team.on_call = user
    session.commit()

    r = client.post(
        "/api/server/v2/alerts/send",
        json={
            "team_id": team.id,
            "alert_type": "service",
            "trouble_code": "bob",
            "hostname": "bob",
            "display_name": "bob",
            "service_name": "bob",
            "state": "bob",
            "output": "bob",
            "timestamp": "2018-11-22T11:18:22+0000",
        },
        headers={"Authorization": "apikey {}testingapi".format(server.apikey_id)},
    )

    assert r.status_code == HTTPStatus.CREATED

    alert = alerts.get(int(r.json["data"]["id"]))

    assert alert.json["output"] == "bob"
    assert alert.json["timestamp"] == "2018-11-22 11:18:22"


def test_add_notification_service_without_service_name(client, session):
    """
    Test adding a service alert without a service_name returns 422.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    user = users.UserFactory()
    team = teams.TeamFactory()
    team.on_call = user
    session.commit()

    r = client.post(
        "/api/server/v2/alerts/send",
        json={
            "team_id": team.id,
            "alert_type": "service",
            "trouble_code": "bob",
            "hostname": "bob",
            "display_name": "bob",
            "state": "bob",
            "output": "bob",
            "timestamp": "2018-11-22T11:18:22+0000",
        },
        headers={"Authorization": "apikey {}testingapi".format(server.apikey_id)},
    )

    assert r.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_add_notification_service_empty_service_name(client, session):
    """
    Test adding a service alert with an empty service_name returns 422.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    user = users.UserFactory()
    team = teams.TeamFactory()
    team.on_call = user
    session.commit()

    r = client.post(
        "/api/server/v2/alerts/send",
        json={
            "team_id": team.id,
            "alert_type": "service",
            "trouble_code": "bob",
            "hostname": "bob",
            "display_name": "bob",
            "service_name": "",
            "state": "bob",
            "output": "bob",
            "timestamp": "2018-11-22T11:18:22+0000",
        },
        headers={"Authorization": "apikey {}testingapi".format(server.apikey_id)},
    )

    assert r.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_add_notification_invalid_team_id(client, session):
    """
    Test adding a alert with an invalid team id returns 422.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    user = users.UserFactory()
    team = teams.TeamFactory()
    team.on_call = user
    session.commit()

    r = client.post(
        "/api/server/v2/alerts/send",
        json={
            "team_id": 12345,
            "alert_type": "service",
            "trouble_code": "bob",
            "hostname": "bob",
            "display_name": "bob",
            "service_name": "bob",
            "state": "bob",
            "output": "bob",
            "timestamp": "2018-11-22T11:18:22+0000",
        },
        headers={"Authorization": "apikey {}testingapi".format(server.apikey_id)},
    )

    assert r.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_add_notification_host_success(client, session):
    """
    Test adding a valid host notification creates the notification and
    returns 201.
    """
    server = servers.ServerFactory(api_key_secret_unhashed="testingapi")
    user = users.UserFactory()
    team = teams.TeamFactory()
    team.on_call = user
    session.commit()

    r = client.post(
        "/api/server/v2/alerts/send",
        json={
            "team_id": team.id,
            "alert_type": "host",
            "trouble_code": "bob",
            "hostname": "bob",
            "display_name": "bob",
            "state": "bob",
            "output": "bob",
            "timestamp": "2018-11-22T11:18:22+0000",
        },
        headers={"Authorization": "apikey {}testingapi".format(server.apikey_id)},
    )

    assert r.status_code == HTTPStatus.CREATED

    alert = alerts.get(int(r.json["data"]["id"]))

    assert alert.json["output"] == "bob"
    assert alert.json["timestamp"] == "2018-11-22 11:18:22"


@pytest.mark.parametrize(
    "test_input,expected",
    [
        ("2018-11-22 11:18:22+0000", "2018-11-22 11:18:22"),  # With space instead of T
        ("2018-11-22T11:18:22+0000", "2018-11-22 11:18:22"),  # Regular iso8601
        (
            "2018-11-22 11:18:22",
            "2018-11-21 22:18:22",
        ),  # No timezone form servers timezone
    ],
)
def test_add_notification_dateformating(client, session, test_input, expected):
    """
    Test adding a valid notification with various timestamp formats to check
    that it is added correctly and returns 201.
    """
    server = servers.ServerFactory(
        timezone="Pacific/Auckland", api_key_secret_unhashed="testingapi"
    )
    user = users.UserFactory()
    team = teams.TeamFactory()
    team.on_call = user
    session.commit()

    r = client.post(
        "/api/server/v2/alerts/send",
        json={
            "team_id": team.id,
            "alert_type": "service",
            "trouble_code": "bob",
            "hostname": "bob",
            "display_name": "bob",
            "service_name": "bob",
            "state": "bob",
            "output": "bob",
            "timestamp": test_input,
        },
        headers={"Authorization": "apikey {}testingapi".format(server.apikey_id)},
    )

    assert r.status_code == HTTPStatus.CREATED

    alert = alerts.get(int(r.json["data"]["id"]))

    assert alert.json["output"] == "bob"
    assert alert.json["timestamp"] == expected


def test_get_unknown_handler():
    with pytest.raises(exceptions.APIServerV2HandlerError):
        handlers.get("unknown")
