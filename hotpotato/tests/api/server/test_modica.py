"""
Test the modica callback endpoints.
"""

from http import HTTPStatus

import pytest
from sqlalchemy.dialects.postgresql import array

from hotpotato.models import NotificationAttempt, db
from hotpotato.notifications import notifications
from hotpotato.tests import notifications as notifications_test, oncall_contacts, users


def test_modica_incoming_message_no_body(client, session):
    """
    Test that a incoming message with no body returns 422.
    """
    r = client.post("/api/server/v2/senders/modica/mo-message")
    assert r.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_modica_incoming_message_invalid_id(client, session):
    """
    Test that an incoming message response to a mesage id that doesn't exist retuns 400.
    """
    r = client.post(
        "/api/server/v2/senders/modica/mo-message",
        json={
            "id": 7,
            "source": "+6464588885555",
            "destination": "202",
            "content": "Test Message",
            "operator": "Test Operator",
            "reply-to": "5",
        },
    )
    assert r.status_code == HTTPStatus.BAD_REQUEST


def test_modica_incoming_message_success(client, session):
    """
    Test that a correctly formatted incoming message returns 204.
    """
    orig_user = users.UserFactory()
    oncall_contacts.OncallContactsFactory(user=orig_user)

    users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(
        contact="+646458888555", method="sms"
    )
    session.commit()
    alert = notifications_test.NotificationFactory(notif_type="alert")
    alert.send_attempts.append(
        NotificationAttempt(
            provider="modica",
            provider_notif_id="5",
            status="SENT_TO_PROVIDER",
            user=orig_user,
            warnings=array([], type_=db.Text()),
            errors=array([], type_=db.Text()),
        )
    )
    session.add(alert)
    session.commit()

    r = client.post(
        "/api/server/v2/senders/modica/mo-message",
        json={
            "id": 7,
            "source": contact.contact,
            "destination": "202",
            "content": "Test Message",
            "operator": "Test Operator",
            "reply-to": "5",
        },
    )

    assert r.status_code == HTTPStatus.NO_CONTENT

    next(notifications.get_by(message="Test Message"))


# TODO This should throw 400 since there is on valid JSON
def test_modica_notification_status_no_body(client, session):
    """
    Test that an empty body on incoming notification status returns 422.
    """
    r = client.post("/api/server/v2/senders/modica/dlr-status")
    assert r.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_modica_notification_status_invalid_id(client, session):
    """
    Test that notification status for an id that does not exist returns 400.
    """
    r = client.post(
        "/api/server/v2/senders/modica/dlr-status",
        json={"message_id": "5", "status": "sent"},
    )
    assert r.status_code == HTTPStatus.BAD_REQUEST


def test_modica_notification_status_success(client, session):
    """
    Test that a correctly formatted notification status update returns 204.
    """
    user = users.UserFactory()

    users.UserFactory()
    session.commit()
    alert = notifications_test.NotificationFactory(notif_type="alert")
    alert.send_attempts.append(
        NotificationAttempt(
            provider="modica",
            provider_notif_id="5",
            status="SENT_TO_PROVIDER",
            user=user,
            warnings=array([], type_=db.Text()),
            errors=array([], type_=db.Text()),
        )
    )
    session.add(alert)
    session.commit()

    r = client.post(
        "/api/server/v2/senders/modica/dlr-status",
        json={"message_id": "5", "status": "sent"},
    )
    assert r.status_code == HTTPStatus.NO_CONTENT


@pytest.mark.parametrize(
    "test_input,status",
    [
        ("submitted", "RECEIVED_BY_PROVIDER"),
        ("sent", "SENT_TO_CLIENT"),
        ("received", "RECEIVED_BY_CLIENT"),
        ("frozen", "SEND_FAILED"),
        ("rejected", "SEND_FAILED"),
        ("failed", "SEND_FAILED"),
        ("dead", "SEND_FAILED"),
        ("expired", "SEND_FAILED"),
        ("made-up-status", "SEND_FAILED"),
    ],
)
def test_modica_status_to_alert_status(client, session, test_input, status):
    user = users.UserFactory()

    users.UserFactory()
    session.commit()
    alert = notifications_test.NotificationFactory(notif_type="alert")
    alert.send_attempts.append(
        NotificationAttempt(
            provider="modica",
            provider_notif_id="5",
            status="SENT_TO_PROVIDER",
            user=user,
            warnings=array([], type_=db.Text()),
            errors=array([], type_=db.Text()),
        )
    )
    session.add(alert)
    session.commit()

    r = client.post(
        "/api/server/v2/senders/modica/dlr-status",
        json={"message_id": "5", "status": test_input},
    )
    assert r.status_code == HTTPStatus.NO_CONTENT

    alert = session.merge(alert)
    session.refresh(alert)
    assert alert.send_attempts[0].status == status
