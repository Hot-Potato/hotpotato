"""
Test the twilio callback endpoint.
"""

from http import HTTPStatus

from sqlalchemy.dialects.postgresql import array

from hotpotato.models import NotificationAttempt, db
from hotpotato.tests import notifications, users


# TODO This should throw 400 since there is on valid JSON
def test_twilio_notification_status_no_body(client, session):
    """
    Test that a notification update with no body returns 422.
    """
    r = client.post("/api/server/v2/senders/twilio/messagestatus")
    assert r.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_twilio_notification_status_invalid_id(client, session):
    """
    Test that a notification update for a notification that does not exist returns 400.
    """
    r = client.post(
        "/api/server/v2/senders/twilio/messagestatus",
        data={"MessageSid": "5", "MessageStatus": "delivered"},
    )
    assert r.status_code == HTTPStatus.BAD_REQUEST


def test_twilio_notification_status_success(client, session):
    """
    Test that a valid notification update, updates the notification status successully and returns 204.
    """
    user = users.UserFactory()

    users.UserFactory()
    alert = notifications.NotificationFactory(notif_type="alert")
    alert.send_attempts.append(
        NotificationAttempt(
            provider="twilio",
            provider_notif_id="5",
            status="SENT_TO_PROVIDER",
            user=user,
            warnings=array([], type_=db.Text()),
            errors=array([], type_=db.Text()),
        )
    )
    session.commit()

    session.add(alert)
    session.commit()
    r = client.post(
        "/api/server/v2/senders/twilio/messagestatus",
        data={"MessageSid": "5", "MessageStatus": "delivered"},
    )
    assert r.status_code == HTTPStatus.NO_CONTENT
