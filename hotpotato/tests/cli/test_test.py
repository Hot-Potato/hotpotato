from unittest.mock import Mock

from hotpotato import tests
from hotpotato.cli.test import test as cli_test_group


def test_test_create_data(runner, monkeypatch, app):
    """
    Test create-data runs without any errors.
    """
    mock = Mock()
    monkeypatch.setattr(tests, "create_data", mock)
    result = runner.invoke(cli=cli_test_group, args=["create-data", "--number", "1"])
    assert mock.called
    assert result.exit_code == 0
