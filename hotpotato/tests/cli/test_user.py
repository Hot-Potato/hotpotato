import faker
import flask_security.utils

from hotpotato import roles, teams, users as hotpotato_users
from hotpotato.tests import oncall_contacts, users


def test_user_create_admin(runner, session):
    """
    Test creating an admin user.
    """
    hp_team_id = teams.hotpotato_team().id

    fake = faker.Faker()
    email = fake.email()
    while hotpotato_users.get_by(email=email):
        email = fake.email()
    name = fake.name()
    password = "password"

    result = runner.invoke(
        args=[
            "user",
            "create",
            str(hp_team_id),
            name,
            email,
            "--password",
            password,
            "--admin",
        ]
    )
    assert result.exit_code == 0

    user = hotpotato_users.get_by_email(email)
    assert user.name == name
    assert user.active is True
    assert roles.get_by_name(hp_team_id, "admin") in user.roles
    assert flask_security.utils.verify_password(password, user.password)


def test_user_create_oncall(runner, session):
    """
    Test creating an oncall user.
    """
    hp_team_id = teams.hotpotato_team().id

    fake = faker.Faker()
    email = fake.email()
    while hotpotato_users.get_by(email=email):
        email = fake.email()
    name = fake.name()
    password = "password"

    result = runner.invoke(
        args=[
            "user",
            "create",
            str(hp_team_id),
            name,
            email,
            "--password",
            password,
            "--oncall",
        ]
    )
    assert result.exit_code == 0

    user = hotpotato_users.get_by_email(email)
    assert user.name == name
    assert user.active is True
    assert roles.get_by_name(hp_team_id, "oncall") in user.roles
    assert flask_security.utils.verify_password(password, user.password)


def test_user_create_not_active(runner, session):
    """
    Test creating an inactive user.
    """

    fake = faker.Faker()
    email = fake.email()
    while hotpotato_users.get_by(email=email):
        email = fake.email()
    name = fake.name()
    password = "password"

    result = runner.invoke(
        args=[
            "user",
            "create",
            str(teams.hotpotato_team().id),
            name,
            email,
            "--password",
            password,
            "--not-active",
        ],
        catch_exceptions=False,
    )
    assert result.exit_code == 0

    user = hotpotato_users.get_by_email(email)
    assert user.name == name
    assert user.active is False
    assert flask_security.utils.verify_password(password, user.password)


def test_user_create_andmin_and_oncall(runner, session):
    """
    Test creating an admin and oncall user fails.
    """

    fake = faker.Faker()
    email = fake.email()
    name = fake.name()
    password = "password"

    result = runner.invoke(
        args=[
            "user",
            "create",
            str(teams.hotpotato_team().id),
            name,
            email,
            "--password",
            password,
            "--admin",
            "--oncall",
        ],
        catch_exceptions=False,
    )
    assert result.exit_code == 2


def test_user_create_invalid_timezone(runner, session):
    """
    Test creating a user with an invalid timezone fails.
    """

    fake = faker.Faker()
    email = fake.email()
    name = fake.name()
    password = "password"

    result = runner.invoke(
        args=[
            "user",
            "create",
            str(teams.hotpotato_team().id),
            name,
            email,
            "--password",
            password,
            "-t",
            "made_up_timezone",
        ],
        catch_exceptions=False,
    )
    assert result.exit_code == 2


def test_user_create_invalid_language(runner, session):
    """
    Test creating a user with an invalid language fails.
    """

    fake = faker.Faker()
    email = fake.email()
    name = fake.name()
    password = "password"

    result = runner.invoke(
        args=[
            "user",
            "create",
            str(teams.hotpotato_team().id),
            name,
            email,
            "--password",
            password,
            "-l",
            "made_up_language",
        ],
        catch_exceptions=False,
    )
    assert result.exit_code == 2


def test_user_create_invalid_team_id(runner, session):
    """
    Test creating a user with an invalid team id fails.
    """

    fake = faker.Faker()
    email = fake.email()
    name = fake.name()
    password = "password"

    result = runner.invoke(
        args=[
            "user",
            "create",
            str(1111111111111111111),
            name,
            email,
            "--password",
            password,
            "--not-active",
        ],
        catch_exceptions=False,
    )
    assert result.exit_code == 2


def test_user_create_duplicate_email(runner, session):
    """
    Test creating a suer with a duplicate email fails.
    """

    user = users.UserFactory()

    session.flush()

    fake = faker.Faker()
    email = user.email
    name = fake.name()
    password = "password"

    result = runner.invoke(
        args=[
            "user",
            "create",
            str(teams.hotpotato_team().id),
            name,
            email,
            "--password",
            password,
            "--not-active",
        ],
        catch_exceptions=False,
    )
    assert result.exit_code == 2


def test_user_activate(runner, session):
    """
    Test activating a user.
    """

    user = users.UserFactory(active=False)
    email = user.email

    session.commit()

    result = runner.invoke(args=["user", "activate", user.email])
    assert result.exit_code == 0

    user = hotpotato_users.get_by_email(email)
    assert user.active is True


def test_user_activate_already_active(runner, session):
    """
    Test activating a user where they are already active.
    """

    user = users.UserFactory(active=True)
    email = user.email

    session.commit()

    result = runner.invoke(args=["user", "activate", user.email])
    assert result.exit_code == 0

    user = hotpotato_users.get_by_email(email)
    assert user.active is True


def test_user_activate_no_user(runner, session):
    """
    Test activating a user that doesn't exist fails.
    """
    result = runner.invoke(args=["user", "activate", "dontexist@example.com"])
    assert result.exit_code == 2


def test_user_deactivate(runner, session):
    """
    Test deactivating a user.
    """

    user = users.UserFactory(active=True)
    email = user.email

    session.commit()

    result = runner.invoke(args=["user", "deactivate", user.email])
    assert result.exit_code == 0

    user = hotpotato_users.get_by_email(email)
    assert user.active is False


def test_user_deactivate_already_deactivated(runner, session):
    """
    Test deactivating a user that's already deactivated.
    """

    user = users.UserFactory(active=False)
    email = user.email

    session.commit()

    result = runner.invoke(args=["user", "deactivate", user.email])
    assert result.exit_code == 0

    user = hotpotato_users.get_by_email(email)
    assert user.active is False


def test_user_deactivate_no_user(runner, session):
    """
    Test deactivating a user that doesn't exist fails.
    """
    result = runner.invoke(args=["user", "deactivate", "dontexist@example.com"])
    assert result.exit_code == 2


def test_take_pager(runner, session):
    """
    Test that the pager can be taken.
    """

    user = users.UserFactory(active=False)
    email = user.email
    oncall_contacts.OncallContactsFactory(user=user)

    team = teams.hotpotato_team()
    result = runner.invoke(args=["user", "take-pager", email, team.name])
    session.add(team)
    assert result.exit_code == 0
    assert team.on_call.email == user.email
