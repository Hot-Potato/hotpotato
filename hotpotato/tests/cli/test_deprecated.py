from unittest.mock import Mock

from hotpotato.cli import deprecated


def test_check_failed(runner, monkeypatch):
    """
    test check-failed runs without any errors.
    """
    mock = Mock()
    monkeypatch.setattr(deprecated, "check_failed_alerts_internal", mock)
    result = runner.invoke(args=["alert", "check-failed"])
    assert mock.called
    assert result.exit_code == 0


def test_delete_old(runner, monkeypatch):
    """
    test delete-old runs without any errors.
    """
    mock = Mock()
    monkeypatch.setattr(deprecated, "_delete_old", mock)
    result = runner.invoke(args=["heartbeat", "delete-old"])
    assert mock.called
    assert result.exit_code == 0


def test_check_missed_hbeats(runner, monkeypatch):
    """
    test check-missed-hbeats runs without any errors.
    """
    mock = Mock()
    monkeypatch.setattr(deprecated, "check_missed_hbeats_internal", mock)
    result = runner.invoke(args=["server", "check-missed-hbeats"])
    assert mock.called
    assert result.exit_code == 0
