import datetime

from hotpotato.notifications import alerts, attempts, messages
from hotpotato.tests import oncall_contacts, servers, teams, users


def test_get(runner, session):
    """
    Test get runs without any errors.
    """
    users.UserFactory()
    session.commit()

    message = messages.create(team_id=None, body="test")
    result = runner.invoke(args=["notification", "get", str(message.id)])
    assert result.exit_code == 0


def test_alert(runner, session):
    """
    Test alert sends an alert.
    """
    user = users.UserFactory()
    team = teams.TeamFactory()
    oncall_contacts.OncallContactsFactory(user=user)
    server = servers.ServerFactory()
    session.commit()

    timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    count = len(alerts.get_by(output="Something is probably broken"))
    result = runner.invoke(
        args=[
            "notification",
            "send",
            "alert",
            str(team.id),
            str(user.id),
            "host",
            str(server.id),
            "TEST123",
            "example.com",
            "Example Wesbite",
            "Example Service",
            "CRITICAL",
            "Something is probably broken",
            timestamp,
        ]
    )
    assert result.exit_code == 0
    assert len(alerts.get_by(output="Something is probably broken")) > count


def test_message(runner, session, logger):
    """
    Test message sends a message.
    """
    user = users.UserFactory()
    oncall_contacts.OncallContactsFactory(user=user)
    session.commit()
    user_id = user.id

    count = len(attempts.get_by(user_id=user_id))
    result = runner.invoke(
        args=["notification", "send", "message", "Test", str(user_id)],
        catch_exceptions=False,
    )
    assert result.exit_code == 0
    assert len(attempts.get_by(user_id=user_id)) > count
