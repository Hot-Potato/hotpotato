from hotpotato import __version__
from hotpotato.cli.main import cli


def test_version(runner):
    """
    Test version runs without any errors.
    """
    result = runner.invoke(cli=cli, args=["--version"])
    assert __version__ in result.output
    assert result.exit_code == 0
