from hotpotato import cli


def test_init_without_test(no_cli_app):
    """
    Test initing cli without debug mode leaves out testing commands
    """
    no_cli_app.config["DEBUG"] = False
    cli.init_app(no_cli_app)

    assert "test" not in no_cli_app.cli.commands.keys()


def test_init_with_test(no_cli_app):
    """
    Test initing cli with debug mode includes testing commands
    """
    no_cli_app.config["DEBUG"] = True
    cli.init_app(no_cli_app)
    assert "test" in no_cli_app.cli.commands.keys()
