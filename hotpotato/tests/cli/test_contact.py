from hotpotato.tests import users


def test_create_contact(runner, session):
    """
    Test a contact method can be created.
    """

    user = users.UserFactory()

    result = runner.invoke(
        args=["contact", "create", user.email, "pushover", "a" * 30, "--send-pages"]
    )

    session.add(user)

    assert result.exit_code == 0
    assert len(user.contacts) == 1
