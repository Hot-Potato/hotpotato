from hotpotato import servers, tests


def test_get_monitored_no_servers(runner, session):
    """
    Test get-monitored runs without any errors with no servers.
    """
    result = runner.invoke(args=["server", "get-monitored"])
    assert result.exit_code == 0


def test_get_monitored_one_server(runner, session):
    """
    Test get-monitored prints out monitored server.
    """
    hostname = tests.servers.ServerFactory(
        disable_missed_heartbeat_notif=False
    ).hostname
    session.commit()

    result = runner.invoke(args=["server", "get-monitored"])
    assert result.exit_code == 0
    assert hostname in result.output


def test_get_monitored_one_server_unmonitored(runner, session):
    """
    Test get-monitored doesn't print out unmonitored server.
    """
    hostname = tests.servers.ServerFactory(disable_missed_heartbeat_notif=True).hostname
    session.commit()

    result = runner.invoke(args=["server", "get-monitored"])
    assert result.exit_code == 0
    assert hostname not in result.output


def test_create_server(runner, session):
    """
    Test a server can be created
    """

    result = runner.invoke(
        args=["server", "create", "test_create_server-host", "http://myhost", "UTC"]
    )
    assert result.exit_code == 0
    servers.get_by_api_key(result.output.strip())


def test_create_server_with_apikey(runner, session):
    """
    Test a server can be created with a provided API key
    """

    result = runner.invoke(
        args=[
            "server",
            "create",
            "test_create_server-host",
            "http://myhost",
            "UTC",
            "--api-key=ND8VQ7nzWGBXNracR09P.b7QS3ZGrqYFX8DNxRn8epiN2WKbTWy1GTFNFPEjHzzAA4q3RhFdaqOUgcaJEtxmNhOaUPMA0nxRVVDS1",
        ]
    )
    assert result.exit_code == 0
    assert (
        result.output.strip()
        == "ND8VQ7nzWGBXNracR09P.b7QS3ZGrqYFX8DNxRn8epiN2WKbTWy1GTFNFPEjHzzAA4q3RhFdaqOUgcaJEtxmNhOaUPMA0nxRVVDS1"
    )
    servers.get_by_api_key(result.output.strip())
