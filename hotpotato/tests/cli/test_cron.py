from datetime import datetime, timedelta

from hotpotato import teams
from hotpotato.models import Heartbeat, Message, TeamsUsers
from hotpotato.notifications.exceptions import NotificationSendError
from hotpotato.tests import notifications, oncall_contacts, servers, users


def test_delete_old(runner, session):
    """
    Test delete-old runs without any errors.
    """
    result = runner.invoke(args=["cron", "delete-old-hbeats"])
    assert result.exit_code == 0


def test_check_missed_hbeats_no_servers(runner, session):
    """
    Test check-missed-hbeats runs without any errors.
    """
    result = runner.invoke(args=["cron", "check-missed-hbeats"])
    assert result.exit_code == 0


def test_check_missed_hbeats_one_server_never_sent(runner, session):
    """
    Test check-missed-hbeats runs and sends a message when one server
    exists and has never sent a heartbeat.
    """
    server = servers.ServerFactory(disable_missed_heartbeat_notif=False)
    session.flush()
    heartbeat = Heartbeat(
        server_id=server.id, received_dt=datetime.now() - timedelta(days=5)
    )
    session.add(heartbeat)
    session.flush()

    assert Message.query.count() == 0

    result = runner.invoke(args=["cron", "check-missed-hbeats"])
    assert result.exit_code == 0
    assert Message.query.count() == 1


def test_check_missed_hbeats_one_server_missed(runner, session):
    """
    Test check-missed-hbeats runs and sends a message when one server
    hasn't sent a heartbeat since the threshold.
    """
    servers.ServerFactory(disable_missed_heartbeat_notif=False)
    session.commit()

    assert Message.query.count() == 0

    result = runner.invoke(args=["cron", "check-missed-hbeats"])
    assert result.exit_code == 0
    assert Message.query.count() == 1


def test_check_missed_hbeats_one_server_someone_oncall(runner, session):
    """
    Test check-missed-hbeats runs and sends a message when one server
    exists and has never sent a heartbeat.
    """
    user = users.UserFactory()
    team = teams.hotpotato_team()
    team.on_call = user
    servers.ServerFactory(disable_missed_heartbeat_notif=False)
    session.commit()

    assert Message.query.count() == 0

    result = runner.invoke(args=["cron", "check-missed-hbeats"])
    assert result.exit_code == 0
    assert Message.query.count() == 1


def test_check_missed_hbeats_two_servers_missed(runner, session):
    """
    Test check-missed-hbeats runs and sends two messages when two servers
    exist and have never sent a heartbeat.
    """
    servers.ServerFactory(disable_missed_heartbeat_notif=False)
    servers.ServerFactory(disable_missed_heartbeat_notif=False)
    session.commit()

    assert Message.query.count() == 0

    result = runner.invoke(args=["cron", "check-missed-hbeats"])
    assert result.exit_code == 0

    assert Message.query.count() == 2


def test_check_failed_no_alerts(runner, session):
    """
    Run check-failed on an empty database and check that it exits without errors.
    """
    result = runner.invoke(args=["cron", "check-failed-alerts"])
    assert " 0 failed alerts" in result.output
    assert result.exit_code == 0


def test_check_failed(runner, session, app):
    """
    Run check-failed on a database with a failed alert and check that it sends a message.
    """
    oncall_user = users.UserFactory()
    servers.ServerFactory()
    team = teams.hotpotato_team()
    team.on_call = oncall_user
    oncall_contacts.OncallContactsFactory(user=oncall_user)
    alert = notifications.NotificationFactory(notif_type="alert")
    alert.json["status"] = "SEND_FAILED"
    session.commit()

    assert Message.query.count() == 0
    result = runner.invoke(args=["cron", "check-failed-alerts"])
    assert " 1 failed alerts" in result.output
    assert result.exit_code == 0
    assert Message.query.count() == 1


def test_check_failed_error_sending(runner, session, app, monkeypatch):
    """
    Run check-failed on a database with a failed alert and check that it sends a message.
    """
    oncall_user = users.UserFactory()
    servers.ServerFactory()
    team = teams.hotpotato_team()
    team.on_call = oncall_user
    alert = notifications.NotificationFactory(notif_type="alert")
    alert.json["status"] = "SEND_FAILED"
    session.commit()

    def mock_send(self, user_id, contact=None, run_async=False):
        raise NotificationSendError

    monkeypatch.setattr(Message, "send", mock_send)

    assert Message.query.count() == 0
    result = runner.invoke(args=["cron", "check-failed-alerts"])
    assert " 1 failed alerts" in result.output
    assert "Failed to send" in result.output
    assert result.exit_code == 0
    assert Message.query.count() == 1


def test_check_failed_send_failures(runner, session, app):
    """
    Test messages are sent to contacts with send_failures enabled.
    """
    oncall_user = users.UserFactory()
    servers.ServerFactory()
    team = teams.hotpotato_team()
    team.on_call = oncall_user
    oncall_contacts.OncallContactsFactory(user=oncall_user)
    alert = notifications.NotificationFactory(notif_type="alert")
    alert.json["status"] = "SEND_FAILED"

    other_user = users.UserFactory()
    other_user.teams_users.append(TeamsUsers(team=team, primary=False))
    oncall_contacts.OncallContactsFactory(user=other_user, send_failures=True)
    session.commit()

    assert Message.query.count() == 0
    result = runner.invoke(args=["cron", "check-failed-alerts"], catch_exceptions=False)
    assert result.exit_code == 0
    assert "Notifying" in result.output
    assert Message.query.count() == 1


def test_check_failed_send_failures_error_sending(runner, session, app, monkeypatch):
    """
    Test an error is printed when sending fails to contacts with send_failures enabled.
    """
    oncall_user = users.UserFactory()
    servers.ServerFactory()
    team = teams.hotpotato_team()
    team.on_call = oncall_user
    alert = notifications.NotificationFactory(notif_type="alert")
    alert.json["status"] = "SEND_FAILED"

    other_user = users.UserFactory()
    other_user.teams_users.append(TeamsUsers(team=team, primary=False))
    oncall_contacts.OncallContactsFactory(user=other_user, send_failures=True)
    session.commit()

    assert Message.query.count() == 0

    def mock_send(self, user_id, contact=None, run_async=False):
        raise NotificationSendError

    monkeypatch.setattr(Message, "send", mock_send)
    result = runner.invoke(args=["cron", "check-failed-alerts"], catch_exceptions=False)
    assert result.exit_code == 0
    assert "Notifying" in result.output
    assert "Failed to notify" in result.output
    assert Message.query.count() == 1
