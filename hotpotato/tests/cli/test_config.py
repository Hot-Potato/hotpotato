def test_config(runner):
    """
    Test config runs without any errors.
    """
    result = runner.invoke(args=["config"])
    assert result.exit_code == 0
