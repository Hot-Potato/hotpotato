import pytest

from hotpotato import roles
from hotpotato.tests import roles as test_roles, teams


def test_get(app, session):
    """
    test that get returns the role
    """
    role = test_roles.RoleFactory(name="test_role")
    session.commit()
    role_id = role.id
    role = roles.get(role_id)
    assert role
    assert role.name == "test_role"


def test_get_by_name_dosnt_exist(session):
    "Test gettinga role with a name that doesn't exist errors"
    team = teams.TeamFactory()
    session.commit()
    with pytest.raises(roles.RoleNameError):
        roles.get_by_name(team.id, "idontexist")


def test_get_by(session):
    """
    check that the get_by function returns something
    """
    roles_list = roles.get_by(name="*")
    assert roles_list is not None


def test_create(session):
    team = teams.TeamFactory()
    session.commit()
    role = roles.create(
        team_id=team.id, name="examp", description="example role", priority=6
    )

    assert role == roles.get_by_name(team.id, "examp")

    with pytest.raises(roles.RoleNameError):
        roles.get_by_name(team.id, "wrong_name")


def test_create_no_priority(session):
    """
    Test that creating a role without a priority increments to the next avilable one
    """
    team = teams.TeamFactory()
    session.commit()
    role = roles.create(team_id=team.id, description="example role")

    assert role.priority == 3
