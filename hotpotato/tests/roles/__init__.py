"""
Role unit test functions.
"""


import factory

from hotpotato import models, permissions
from hotpotato.tests import teams


class RoleFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Role
        sqlalchemy_session = models.db.session

    name = factory.Faker("word")
    description = factory.Faker("text", max_nb_chars=160)
    team = factory.SubFactory(teams.TeamFactory)
    priority = factory.Faker("random_int", min=3, max=1000000)

    @factory.post_generation
    def permissions(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for permission in extracted:
                permission_model = permissions.get_by_name(permission)
                perm = models.RolePermission(
                    permission_id=permission_model.id, access=True
                )
                self.permissions.append(perm)
