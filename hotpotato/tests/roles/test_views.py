"""
Test role related views
"""

import pytest
import sqlalchemy
from flask import url_for

from hotpotato import roles, teams
from hotpotato.models import Role
from hotpotato.tests import teams as test_teams, users
from hotpotato.tests.conftest import login


def test_adding_role(client, session, app):
    """
    Test that adding a role to your team works.
    """
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.add_role",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
            ),
            data={"name": "A New Role"},
            follow_redirects=True,
        )
        assert resp.status_code == 200

    role = Role.query.filter_by(description="A New Role").first()
    assert role


def test_adding_role_without_permission(client, session, app):
    """
    Test that adding a role without the right permission is denied.
    """
    user = users.UserFactory()
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.add_role",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
            ),
            data={"name": "A New Role"},
            follow_redirects=True,
        )
        assert resp.status_code == 200

    with pytest.raises(sqlalchemy.orm.exc.NoResultFound):
        Role.query.filter_by(description="A New Role").one()


def test_adding_role_to_another_team(client, session, app):
    """
    Test that adding a role to another team is denied
    """
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    team = test_teams.TeamFactory()
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.add_role", team_id=user.primary_team.id, _team_id=team.id
            ),
            data={"name": "A New Role"},
            follow_redirects=True,
        )
        assert resp.status_code == 200

    with pytest.raises(sqlalchemy.orm.exc.NoResultFound):
        Role.query.filter_by(description="A New Role").one()


def test_adding_role_to_another_team_with_hotpotato_admin_and_team_admin(
    client, session, app
):
    """
    Test that adding a role to another team is denied, if done from the wrong team context
    but the user has hotpotato admin and admin on the team their adding from.
    """
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    teams.hotpotato_team().add_users(user)
    team = test_teams.TeamFactory()
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.add_role", team_id=user.primary_team.id, _team_id=team.id
            ),
            data={"name": "A New Role"},
            follow_redirects=True,
        )
        assert resp.status_code == 200

    with pytest.raises(sqlalchemy.orm.exc.NoResultFound):
        Role.query.filter_by(description="A New Role").one()


def test_adding_role_to_another_team_with_hotpotato_admin(client, session, app):
    """
    Test that adding a role to another team is denied, if done from the wrong team context
    but the user has hotpotato admin but not admin on the team their adding from.
    """
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    teams.hotpotato_team().add_users(user)
    team = test_teams.TeamFactory()
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.add_role", team_id=user.primary_team.id, _team_id=team.id
            ),
            data={"name": "A New Role"},
            follow_redirects=True,
        )
        assert resp.status_code == 200

    with pytest.raises(sqlalchemy.orm.exc.NoResultFound):
        Role.query.filter_by(description="A New Role").one()


def test_adding_role_to_another_team_as_hotpotato_team(client, session, app):
    """
    Test that adding a role to another team works
    """
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    teams.hotpotato_team().add_users(user)
    team = test_teams.TeamFactory()
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.add_role",
                team_id=teams.hotpotato_team().id,
                _team_id=team.id,
            ),
            data={"name": "A New Role"},
            follow_redirects=True,
        )
        assert resp.status_code == 200

    role = Role.query.filter_by(description="A New Role").one()
    assert role


def test_adding_role_no_team(client, session, app):
    """
    Test adding a role to team that doesn't exists 404s.
    """
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    teams.hotpotato_team().add_users(user)
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.add_role",
                _team_id=12345,
                team_id=teams.hotpotato_team().id,
            ),
            data={"name": "A New Role"},
            follow_redirects=True,
        )
        assert resp.status_code == 404


def test_removing_role(client, session, app):
    """
    Test that removing a role works.
    """
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    session.commit()
    role_id = roles.create(team_id=user.primary_team.id, description="A New Role").id

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.remove_role",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
            ),
            data={"role_id": role_id},
            follow_redirects=True,
        )
        assert resp.status_code == 200

    with pytest.raises(roles.RoleIDError):
        roles.get(role_id)


def test_removing_role_no_team(client, session, app):
    """
    Test removing a role to team that doesn't exists 404s.
    """
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    teams.hotpotato_team().add_users(user)
    session.commit()
    role_id = roles.create(team_id=user.primary_team.id, description="A New Role").id

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.remove_role",
                _team_id=12345,
                team_id=teams.hotpotato_team().id,
            ),
            data={"role_id": role_id},
            follow_redirects=True,
        )
        assert resp.status_code == 404

    role = Role.query.filter_by(id=role_id).first()
    assert role
