from collections import OrderedDict
from datetime import date, datetime

from flask import url_for
from pytz import timezone

from hotpotato import roles, util
from hotpotato.models import Notification, Tag
from hotpotato.tests import notifications, servers, teams, users
from hotpotato.tests.conftest import login
from hotpotato.views import views


def test_servers_count(app, session):
    """
    test that the correct list of servers is returned
    """
    server = servers.ServerFactory()
    server2 = servers.ServerFactory()
    session.flush()
    _ = notifications.AlertFactory(
        json={
            "version": 1,
            "node_name": util.node_name,
            "event_id": None,
            "ticket_id": None,
            "method": None,
            "alert_type": "service",
            "server_id": server.id,
            "trouble_code": "NOTC",
            "hostname": "example.com",
            "display_name": "example.com",
            "service_name": "nginx",
            "state": "DOWN",
            "output": "Not responding to requests",
            "timestamp": "2019-03-01 10:33:31",
        }
    )
    _ = notifications.AlertFactory(
        json={
            "version": 1,
            "node_name": util.node_name,
            "event_id": None,
            "ticket_id": None,
            "method": None,
            "alert_type": "service",
            "server_id": server.id,
            "trouble_code": "NOTC",
            "hostname": "example.com",
            "display_name": "example.com",
            "service_name": "nginx",
            "state": "DOWN",
            "output": "Not responding to requests",
            "timestamp": "2019-03-01 10:33:31",
        }
    )
    _ = notifications.AlertFactory(
        json={
            "version": 1,
            "node_name": util.node_name,
            "event_id": None,
            "ticket_id": None,
            "method": None,
            "alert_type": "service",
            "server_id": server2.id,
            "trouble_code": "NOTC",
            "hostname": "example.com",
            "display_name": "example.com",
            "service_name": "nginx",
            "state": "DOWN",
            "output": "Not responding to requests",
            "timestamp": "2019-03-01 10:33:31",
        }
    )
    session.flush()
    counts = views.server_counts([])
    assert len(counts) == 2
    assert counts[0].count == 2
    assert counts[1].count == 1


def test_notif_count(app, session):
    notif_json = {
        "version": 1,
        "node_name": util.node_name,
        "event_id": None,
        "ticket_id": None,
        "method": None,
        "alert_type": "service",
        "server_id": 1,
        "trouble_code": "NOTC",
        "hostname": "example.com",
        "display_name": "example.com",
        "service_name": "nginx",
        "state": "DOWN",
        "output": "Not responding to requests",
        "timestamp": "2019-03-01 10:33:31",
    }
    _ = notifications.AlertFactory(json=notif_json)
    _ = notifications.AlertFactory(json=notif_json)
    _ = notifications.AlertFactory(json=notif_json)
    notif_json2 = notif_json.copy()
    notif_json2["state"] = "UP"
    _ = notifications.AlertFactory(json=notif_json2)
    session.flush()
    assert views.notif_count([]) == [
        (3, "NOTC", "example.com", "DOWN", "nginx"),
        (1, "NOTC", "example.com", "UP", "nginx"),
    ]


def test_notif_type_count(app, session):
    """
    Check notif_type_count works
    """
    _ = notifications.AlertFactory()
    _ = notifications.AlertFactory()
    _ = notifications.NotificationFactory(notif_type="handover")
    _ = notifications.NotificationFactory(notif_type="message")
    session.flush()
    assert views.notif_type_count([]) == {"alert": 2, "handover": 1, "message": 1}


def test_page_counts(app, session):
    """
    Check page counts works
    """
    _ = notifications.AlertFactory(
        received_dt=datetime(year=2019, month=9, day=9, hour=0)
    )
    session.flush()
    assert views.page_counts(
        [],
        start_time=timezone("Pacific/Auckland").localize(
            datetime(year=2019, month=9, day=5, hour=10)
        ),
        end_time=timezone("Pacific/Auckland").localize(
            datetime(year=2019, month=9, day=12, hour=10)
        ),
    ) == (
        0,
        1,
        OrderedDict(
            [
                (date(year=2019, month=9, day=5), 0),
                (date(year=2019, month=9, day=6), 0),
                (date(year=2019, month=9, day=7), 0),
                (date(year=2019, month=9, day=8), 0),
                (date(year=2019, month=9, day=9), 0),
                (date(year=2019, month=9, day=10), 0),
                (date(year=2019, month=9, day=11), 0),
                (date(year=2019, month=9, day=12), 0),
            ]
        ),
        OrderedDict(
            [
                (date(year=2019, month=9, day=5), 0),
                (date(year=2019, month=9, day=6), 0),
                (date(year=2019, month=9, day=7), 0),
                (date(year=2019, month=9, day=8), 0),
                (date(year=2019, month=9, day=9), 1),
                (date(year=2019, month=9, day=10), 0),
                (date(year=2019, month=9, day=11), 0),
                (date(year=2019, month=9, day=12), 0),
            ]
        ),
    )


def test_page_counts_edge_cases(app, session):
    """
    Test some edge cases in buisness hours for the page counts function
    to make sure it handles them correctly
    """
    _ = notifications.AlertFactory(
        received_dt=datetime(year=2019, month=9, day=11, hour=19)
    )
    _ = notifications.AlertFactory(
        received_dt=datetime(year=2019, month=9, day=11, hour=20)
    )
    _ = notifications.AlertFactory(
        received_dt=datetime(year=2019, month=9, day=11, hour=21)
    )
    _ = notifications.AlertFactory(
        received_dt=datetime(year=2019, month=9, day=4, hour=20)
    )
    _ = notifications.AlertFactory(
        received_dt=datetime(year=2019, month=9, day=4, hour=19)
    )
    _ = notifications.AlertFactory(
        received_dt=datetime(year=2019, month=9, day=10, hour=6)
    )
    _ = notifications.AlertFactory(
        received_dt=datetime(year=2019, month=9, day=10, hour=5, minute=59, second=59)
    )
    session.flush()

    start_time = timezone("Pacific/Auckland").localize(
        datetime(year=2019, month=9, day=5, hour=8)
    )
    end_time = timezone("Pacific/Auckland").localize(
        datetime(year=2019, month=9, day=12, hour=8)
    )
    filters = (
        Notification.received_dt >= start_time,
        Notification.received_dt < end_time,
    )
    assert views.page_counts(filters, start_time=start_time, end_time=end_time) == (
        2,
        2,
        OrderedDict(
            [
                (date(year=2019, month=9, day=5), 0),
                (date(year=2019, month=9, day=6), 0),
                (date(year=2019, month=9, day=7), 0),
                (date(year=2019, month=9, day=8), 0),
                (date(year=2019, month=9, day=9), 0),
                (date(year=2019, month=9, day=10), 1),
                (date(year=2019, month=9, day=11), 0),
                (date(year=2019, month=9, day=12), 1),
            ]
        ),
        OrderedDict(
            [
                (date(year=2019, month=9, day=5), 1),
                (date(year=2019, month=9, day=6), 0),
                (date(year=2019, month=9, day=7), 0),
                (date(year=2019, month=9, day=8), 0),
                (date(year=2019, month=9, day=9), 0),
                (date(year=2019, month=9, day=10), 1),
                (date(year=2019, month=9, day=11), 0),
                (date(year=2019, month=9, day=12), 0),
            ]
        ),
    )


def test_tag_counts(session):
    """
    Test tag counts produces correct counts.
    """
    team = teams.TeamFactory()
    session.flush()
    tag1 = Tag(name="Tag 1", team=team)
    tag2 = Tag(name="Tag 2", team=team)
    session.add(tag1)
    session.add(tag2)
    session.flush
    notif1 = notifications.AlertFactory(team=team)
    notif2 = notifications.AlertFactory(team=team)
    session.flush()
    notif1.tags.append(tag1)
    notif2.tags.append(tag2)
    session.flush()

    assert views.tag_counts([]) == [("Tag 1", 1), ("Tag 2", 1)]

    notif3 = notifications.AlertFactory(team=team)
    notif3.tags.append(tag2)
    assert views.tag_counts([]) == [("Tag 2", 2), ("Tag 1", 1)]

    notif1.tags.append(tag2)
    assert views.tag_counts([]) == [("Tag 2", 3), ("Tag 1", 1)]


def test_report_page(client, session, app):
    """
    Test report page loads without errors.
    """
    app.config["FEATURE_TAGS"] = True
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.get(
            url_for("views.report", start_date="2019-01-10", end_date="2019-01-17"),
            follow_redirects=True,
        )
        assert resp.status_code == 200


def test_report_page_without_tag(client, session, app):
    """
    Test report page loads without errors.
    """
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.get(
            url_for("views.report", start_date="2019-01-10", end_date="2019-01-17"),
            follow_redirects=True,
        )
        assert resp.status_code == 200
