def test_api_disabled(app, client, session):
    del app.config["STATS_API_KEY"]

    resp = client.get("/api/stats/v1/days_since_alert/1234")
    assert resp.status_code == 404


def test_no_api_key(app, client, session):
    app.config["STATS_API_KEY"] = "foo"

    resp = client.get("/api/stats/v1/days_since_alert/1234")
    assert resp.status_code == 401


def test_wrong_api_key(app, client, session):
    app.config["STATS_API_KEY"] = "foo"

    resp = client.get(
        "/api/stats/v1/days_since_alert/1234", headers={"Authorization": "Bearer bar"}
    )
    assert resp.status_code == 403


def test_good_api_key(app, client, session):
    app.config["STATS_API_KEY"] = "foo"

    resp = client.get(
        "/api/stats/v1/days_since_alert/1234", headers={"Authorization": "Bearer foo"}
    )
    assert resp.status_code not in [401, 403]
