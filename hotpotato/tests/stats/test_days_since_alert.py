from datetime import datetime, time

import pytest
import pytz
import workalendar
from freezegun import freeze_time

import hotpotato.api.stats.v1.stats as stats
from hotpotato.tests.notifications import NotificationFactory
from hotpotato.tests.teams import TeamFactory

# Around January, when the test data are, this is UTC+13
nz_tz = pytz.timezone("Pacific/Auckland")
nz_cal = workalendar.registry.registry.get("NZ")()


def test_lookup_none(session):
    team = TeamFactory()
    session.commit()

    with pytest.raises(StopIteration):
        stats.last_matching_alert_datetime(team.id, nz_tz)


def test_lookup_none_on_team(session):
    team = TeamFactory()
    NotificationFactory(notif_type="alert", received_dt=datetime(2019, 1, 1, 4, 20, 0))
    session.commit()

    with pytest.raises(StopIteration):
        stats.last_matching_alert_datetime(team.id, nz_tz)


def test_lookup_not_alert(session):
    team = TeamFactory()
    NotificationFactory(
        team=team, notif_type="handover", received_dt=datetime(2019, 1, 1, 4, 20, 0)
    )
    NotificationFactory(
        team=team, notif_type="message", received_dt=datetime(2019, 1, 1, 4, 21, 0)
    )
    session.commit()

    with pytest.raises(StopIteration):
        stats.last_matching_alert_datetime(team.id, nz_tz)


def test_lookup_simple(session):
    team = TeamFactory()
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 1, 4, 20, 0)
    )
    session.commit()

    assert datetime(2019, 1, 1, 17, 20, 0) == stats.last_matching_alert_datetime(
        team.id, nz_tz
    ).replace(tzinfo=None)


def test_lookup_out_of_hours(session):
    team = TeamFactory()
    # This alert is just before we start work on the 3rd of January NZ time
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 2, 20, 29, 4)
    )

    # We work from 9:30am to 9:35am (nz time), so we don't mind the alerts during that time
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 1, 20, 30, 1)
    )
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 4, 20, 31, 40)
    )
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 8, 20, 33, 0)
    )
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 9, 20, 34, 59)
    )
    session.commit()

    assert datetime(2019, 1, 3, 9, 29, 4) == stats.last_matching_alert_datetime(
        team.id, nz_tz, business_hours=(time(9, 30), time(9, 35))
    ).replace(tzinfo=None)


def test_lookup_ooh_and_calendar(session):
    team = TeamFactory()
    # This alert is after we finish work on the 3rd of January NZ time (which is a working day)
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 3, 5, 29, 4)
    )

    # We work from 9:30am to 9:35am (nz time), so we don't mind the alerts during that time (since they're on weekdays)
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 6, 20, 31, 40)
    )
    session.commit()

    assert datetime(2019, 1, 3, 18, 29, 4) == stats.last_matching_alert_datetime(
        team.id, nz_tz, business_hours=(time(9, 30), time(9, 35)), cal=nz_cal
    ).replace(tzinfo=None)

    # This alert is during working hours, but it's on Waitangi day!
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 2, 5, 20, 31, 46)
    )
    session.commit()

    assert datetime(2019, 2, 6, 9, 31, 46) == stats.last_matching_alert_datetime(
        team.id, nz_tz, business_hours=(time(9, 30), time(9, 35)), cal=nz_cal
    ).replace(tzinfo=None)

    # This alert is out of working hours, on Waitangi day!
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 2, 5, 21, 0, 0)
    )
    session.commit()

    assert datetime(2019, 2, 6, 10, 0, 0) == stats.last_matching_alert_datetime(
        team.id, nz_tz, business_hours=(time(9, 30), time(9, 35)), cal=nz_cal
    ).replace(tzinfo=None)


def test_lookup_ignore_server_ids(session):
    team = TeamFactory()
    notif = NotificationFactory(
        notif_type="alert", received_dt=datetime(2019, 1, 1, 4, 20, 0)
    )
    session.commit()

    sid = notif.json["server_id"]

    with pytest.raises(StopIteration):
        stats.last_matching_alert_datetime(team.id, nz_tz, ignore_server_ids=[sid])

    with pytest.raises(StopIteration):
        stats.last_matching_alert_datetime(
            team.id, nz_tz, ignore_server_ids=[sid, 1234]
        )


def do_api_req(app, client, session, notifs, args):
    app.config["STATS_API_KEY"] = "foo"

    team = TeamFactory()
    for n in notifs:
        NotificationFactory(team=team, notif_type=n[0], received_dt=n[1])
    session.commit()

    resp = client.get(
        "/api/stats/v1/days_since_alert/{}?{}".format(team.id, args),
        headers={"Authorization": "Bearer foo"},
    )
    return resp


def test_api_none(app, client, session):
    resp = do_api_req(app, client, session, [], "tz=Pacific/Auckland")

    assert resp.status_code == 204


def test_api_simple(app, client, session):
    notifs = [("alert", datetime(2019, 1, 13, 10, 58, 0))]
    resp = do_api_req(app, client, session, notifs, "tz=Pacific/Auckland")

    assert resp.status_code == 200
    # I wrote this test 60 days after the date above
    assert int(resp.data) > 60


def test_api_all_args(app, client, session):
    resp = do_api_req(
        app,
        client,
        session,
        [],
        "tz=Pacific/Auckland&region=NZ&business_hours=9-17:30&ignore_servers=123,345",
    )

    assert 200 <= resp.status_code < 300


def test_api_last_night(app, client, session):
    query = "tz=Pacific/Auckland&region=NZ&business_hours=9-17:30"

    # 5 minutes before Midnight
    notifs = [("alert", datetime(2019, 10, 23, 10, 55, 00))]

    # Just after alert
    with freeze_time("2019-10-23 23:59:00", tz_offset=-13):
        print("now", datetime.now())
        resp = do_api_req(app, client, session, notifs, query)
        assert resp.status_code == 200

        assert int(resp.data) == 0

    # Just after midnight
    with freeze_time("2019-10-24 00:00:00", tz_offset=-13):
        resp = do_api_req(app, client, session, notifs, query)
        assert resp.status_code == 200

        assert int(resp.data) == 0

    # After business starts
    with freeze_time("2019-10-24 10:00:00", tz_offset=-13):
        resp = do_api_req(app, client, session, notifs, query)
        assert resp.status_code == 200

        assert int(resp.data) == 0

    # After business ends
    with freeze_time("2019-10-24 10:00:00", tz_offset=-13):
        resp = do_api_req(app, client, session, notifs, query)
        assert resp.status_code == 200

        assert int(resp.data) == 0

    # Just before 24h
    with freeze_time("2019-10-24 23:50:00", tz_offset=-13):
        resp = do_api_req(app, client, session, notifs, query)
        assert resp.status_code == 200

        assert int(resp.data) == 0

    # Just after 24h
    with freeze_time("2019-10-24 23:58:00", tz_offset=-13):
        resp = do_api_req(app, client, session, notifs, query)
        assert resp.status_code == 200

        assert int(resp.data) == 1

    # Next day
    with freeze_time("2019-10-25 05:00:00", tz_offset=-13):
        resp = do_api_req(app, client, session, notifs, query)
        assert resp.status_code == 200

        assert int(resp.data) == 1
