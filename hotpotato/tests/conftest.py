"""
Provides fixtures for all tests.
"""


# pylint: disable=redefined-outer-name


import contextlib
import json
import logging
import os

import pytest

from hotpotato import app as hotpotato_app, models
from hotpotato.tests import (
    notifications,
    oncall_contacts,
    roles,
    servers,
    teams,
    users as test_users,
)


@pytest.fixture(scope="session")
def logger():
    """
    Returns pytest's logger. Only warning & above messages will be displayed.
    """

    yield logging.getLogger(__name__)


def setup_app(logger, init_cli=True):
    """
    Create the app with test-specific settings.
    """

    # pytest keeps this set for some reason. delete it so we know we're not
    # running in the CLI
    try:
        del os.environ["FLASK_RUN_FROM_CLI"]
    except KeyError:
        pass

    # Designed to be run in the Docker image.
    preset = {
        "flask": {"testing": "true"},
        "modica": {"enabled": "true"},
        "twilio": {"enabled": "true"},
        "app": {"enabled": "true"},
        "pushover": {"enabled": "true"},
        "pager": {"enabled": "true"},
        "sms": {"enabled": "true"},
    }

    _app = hotpotato_app.create(
        config_preset=preset,
        config_use_default_paths=False,
        unsafe_crypto=True,
        init_cli=init_cli,
    )
    _app.logger = logger

    _app.config["WTF_CSRF_ENABLED"] = False

    # Disable debug toolbar during unit tests
    _app.config["DEBUG_TB_HOSTS"] = "some-host-that-doesnt-exist"

    _app.config["SERVER_NAME"] = _app.config["HOTPOTATO_WEBUI_URL"]

    _app.config["SQLALCHEMY_DATABASE_URI"] = "cockroachdb://{}:{}/{}test".format(
        _app.config["COCKROACH_SERVER"],
        _app.config["COCKROACH_PORT"],
        _app.config["COCKROACH_DATABASE"],
    )
    return _app


@pytest.fixture(scope="function")
def app(logger):
    """
    Function scoped app for use in tests
    """
    yield setup_app(logger)


@pytest.fixture(scope="function")
def no_cli_app(logger):
    """
    Function scoped app for use in tests
    """
    yield setup_app(logger, init_cli=False)


@pytest.fixture(scope="session")
def db_app():
    """
    Session scoped app for use in initial db setup and teardown
    """

    yield setup_app(logger)


@pytest.fixture
def client(app):
    """
    Return a test client.
    """

    _client = app.test_client()
    yield _client


@pytest.fixture
def runner(app):
    """
    Return a cli runner
    """

    yield app.test_cli_runner()


@pytest.fixture(scope="session")
def db(db_app):
    """
    Return the test database. Handles setup & teardown.
    """

    with db_app.app_context():

        # Create the database if it doesn't exist.
        models.create(db_app)

        # Create the tables and initialise them with test data.
        models.reinitialise(db_app)

    yield models.db

    with db_app.app_context():
        # Drop all the tables in the database.
        models.db.drop_all()


@pytest.fixture(scope="function")
def session(db, app):
    """
    Return a session on the test database. Wraps everything in
    a transaction and rolls it back on teardown.
    """
    backup_session = db.session
    with app.app_context():
        connection = db.engine.connect()
        transaction = connection.begin()
        options = dict(bind=connection, binds={})
        session = db.create_scoped_session(options=options)
        db.session = session
        test_users.UserFactory._meta.sqlalchemy_session = session
        servers.ServerFactory._meta.sqlalchemy_session = session
        notifications.NotificationFactory._meta.sqlalchemy_session = session
        notifications.AlertFactory._meta.sqlalchemy_session = session
        oncall_contacts.OncallContactsFactory._meta.sqlalchemy_session = session
        teams.TeamFactory._meta.sqlalchemy_session = session
        roles.RoleFactory._meta.sqlalchemy_session = session

    with app.app_context():
        yield session

    with app.app_context():
        transaction.rollback()
        connection.close()
        session.remove()

    db.session = backup_session


@contextlib.contextmanager
def login(client, user):
    """
    Minimal context manager for performing requests with a logged in user.
    """

    r = client.post(
        "/login",
        data=json.dumps(dict(email=user.email, password="password", csrf_token="")),
        content_type="application/json",
    )
    assert r.status_code == 200

    yield user

    r = client.post("/logout", follow_redirects=True)
