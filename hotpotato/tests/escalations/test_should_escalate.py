"""
Tests for the function which checks whether notifications should be escalated.
"""

import datetime

import pytest
import pytz

from hotpotato.models.teams import EscalationPolicy
from hotpotato.teams import should_escalate

EMERGENCY = "DOWN"
NORMAL = "OK"

ALWAYS = EscalationPolicy.Always
OUTSIDEHOURS = EscalationPolicy.OutsideBusinessHours
NEVER = EscalationPolicy.Never

SHOULD_NOT = False
SHOULD = True


class BusinessHours(datetime.datetime):
    @classmethod
    def now(cls, timezone):
        return datetime.datetime(
            2019, 5, 6, 12, tzinfo=pytz.timezone("Pacific/Auckland")
        )


class OutsideBusinessHours(datetime.datetime):
    @classmethod
    def now(cls, timezone):
        return datetime.datetime(
            2019, 5, 7, 23, tzinfo=pytz.timezone("Pacific/Auckland")
        )


class NotWorkingDay(datetime.datetime):
    @classmethod
    def now(cls, timezone):
        return datetime.datetime(
            2019, 5, 5, 12, tzinfo=pytz.timezone("Pacific/Auckland")
        )


@pytest.mark.parametrize(
    "status,priority,should,patched_datetime",
    [
        ("READ_BY_CLIENT", EMERGENCY, SHOULD_NOT, BusinessHours),
        ("SENT", EMERGENCY, SHOULD, BusinessHours),
        ("SENT", NORMAL, SHOULD_NOT, BusinessHours),
        ("SENT", EMERGENCY, SHOULD, OutsideBusinessHours),
        ("SEND", EMERGENCY, SHOULD, NotWorkingDay),
    ],
)
def test_should_escalate(app, status, priority, should, patched_datetime, monkeypatch):
    """
    Tests whether a notification should be escalated.
    """

    notif = Struct(
        {
            "json": {"state": priority},
            "send_attempts": [Struct({"status": status})],
            "team": Struct({"pytz_timezone": pytz.timezone("Pacific/Auckland")}),
            "escalations": [],
        }
    )

    with app.app_context():
        monkeypatch.setattr(datetime, "datetime", patched_datetime)

        assert should_escalate(notif) == should


class Struct:
    """Anonymous object from a dict"""

    def __init__(self, entries):
        self.__dict__.update(entries)
