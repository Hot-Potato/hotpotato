import datetime
from importlib import reload

import dramatiq
import pytest
import pytz

from hotpotato import teams
from hotpotato.models.teams import EscalationPolicy
from hotpotato.teams import hotpotato_team
from hotpotato.tests.notifications import AlertFactory
from hotpotato.tests.oncall_contacts import OncallContactsFactory
from hotpotato.tests.teams import TeamFactory
from hotpotato.tests.users import UserFactory

EMERGENCY = "DOWN"
NORMAL = "OK"

ALWAYS = EscalationPolicy.Always
OUTSIDEHOURS = EscalationPolicy.OutsideBusinessHours
NEVER = EscalationPolicy.Never

SHOULD_NOT = False
SHOULD = True


class Escalated(Exception):
    """
    Exception used to check if the notification is escalated
    """

    pass


class EscalateCheck(Exception):
    """
    Exception used to gain control when an escalation check is scheduled. This means we can perform hte call ourselves once setting the desired notification status.
    """

    pass


class BusinessHours(datetime.datetime):
    @classmethod
    def now(cls, timezone, *args, **kwargs):
        return datetime.datetime(
            2019, 5, 6, 12, tzinfo=pytz.timezone("Pacific/Auckland")
        )


class OutsideBusinessHours(datetime.datetime):
    @classmethod
    def now(cls, timezone, *args, **kwargs):
        return datetime.datetime(
            2019, 5, 7, 23, tzinfo=pytz.timezone("Pacific/Auckland")
        )


class NotWorkingDay(datetime.datetime):
    @classmethod
    def now(cls, timezone, *args, **kwargs):
        return datetime.datetime(
            2019, 5, 5, 12, tzinfo=pytz.timezone("Pacific/Auckland")
        )


class PatchedActor(dramatiq.Actor):
    def send_with_options(self, *args, **kwargs):
        # If this call was for the escalate check actor raise an exception so we regain control
        from hotpotato.notifications.queue import actors

        if self.actor_name == actors.escalate_check.fn.__name__:
            raise EscalateCheck()

        super().send_with_options(*args, **kwargs)


def patched_escalate(notif_id, *args, **kwargs):
    raise Escalated()


@pytest.mark.parametrize(
    "deferred_status,escalation_policy,state,should_send,should_escalate,patched_datetime",
    [
        ("READ_BY_CLIENT", ALWAYS, EMERGENCY, SHOULD, SHOULD_NOT, BusinessHours),
        ("SENT_TO_CLIENT", ALWAYS, EMERGENCY, SHOULD, SHOULD, BusinessHours),
        ("SENT_TO_CLIENT", ALWAYS, NORMAL, SHOULD, SHOULD_NOT, BusinessHours),
        ("SENT_TO_CLIENT", NEVER, EMERGENCY, SHOULD, SHOULD_NOT, BusinessHours),
        (
            "SENT_TO_CLIENT",
            OUTSIDEHOURS,
            EMERGENCY,
            SHOULD_NOT,
            SHOULD,
            OutsideBusinessHours,
        ),
        ("SENT_TO_CLIENT", OUTSIDEHOURS, EMERGENCY, SHOULD, SHOULD_NOT, BusinessHours),
        ("SENT_TO_CLIENT", OUTSIDEHOURS, EMERGENCY, SHOULD_NOT, SHOULD, NotWorkingDay),
    ],
)
def test_escalate(
    app,
    session,
    monkeypatch,
    deferred_status,
    escalation_policy,
    state,
    should_send,
    should_escalate,
    patched_datetime,
):
    """
    Check that escalation policies are followed correctly in different situations. See parametized arguments for expected results.

    This test requires monkeypatching to avoid having to wait for the escalation delay to determine if a notification was escalated.
    """

    hp_team = hotpotato_team()
    team = TeamFactory(
        escalates_to=hp_team,
        escalation_policy=escalation_policy,
        # Working day checking requires a regional timezone
        timezone="Pacific/Auckland",
    )

    user1, user2 = UserFactory(), UserFactory()

    OncallContactsFactory(user=user1, contact="+646458888555", method="pager")
    OncallContactsFactory(user=user2, contact="+646458888555", method="pager")

    hp_team.on_call = user1
    team.on_call = user2

    notif = AlertFactory(notif_type="alert", team=team)
    notif.json["state"] = state

    session.commit()

    with app.app_context(), monkeypatch.context() as m:
        m.setattr(datetime, "datetime", patched_datetime)
        m.setattr(teams, "escalate", patched_escalate)
        m.setattr(dramatiq, "Actor", PatchedActor)

        # Imported here so the non-worker app context is used and patched actor class is available
        from hotpotato.notifications.queue import actors

        # Modules are only imported once, but the monkeypatch fixture is only loaded per-parametization .
        # In order for the patched actor to be reapplied the module needs to be reloaded after the patch has been applied.
        reload(actors)
        # Has the notiifcation been escalated?
        escalate_called = False
        # Has a scheduled escalate_check been called?
        escalation_check = False

        try:
            notif.send(user2.id, run_async=False)
        except Escalated:
            escalate_called = True
        except EscalateCheck:
            escalation_check = True

        session.commit()

        session.add(notif)

        # Check it sent if it should have
        assert (len(notif.send_attempts) == 1) == should_send

        if escalation_check:
            notif.send_attempts[0].status = deferred_status
            try:
                actors.escalate_check(notif.id)
            except Escalated:
                escalate_called = True

        session.commit()

        assert escalate_called == should_escalate
