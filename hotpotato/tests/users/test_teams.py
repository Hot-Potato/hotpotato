from hotpotato.models import TeamsUsers
from hotpotato.tests import teams, users
from hotpotato.users import set_primary_team


def test_set_primary_team(session):
    """
    Check set_primary_team sets the correct team and that there is only one
    primary team set.
    """

    user = users.UserFactory()
    assert user.primary_team is not None
    team = teams.TeamFactory()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    session.commit()

    set_primary_team(user, team.id)

    teams_users = TeamsUsers.query.filter_by(user_id=user.id, primary=True).all()
    assert len(teams_users) == 1
    assert teams_users[0].team_id == team.id


def test_get_all_accessible_teams(session):
    user = users.UserFactory()
    team = teams.TeamFactory(escalates_to=user.primary_team)
    session.commit()
    assert user.get_all_accessible_teams(exclude_joined=True)[0].id == team.id
    assert len(user.get_all_accessible_teams(exclude_joined=True)) == 1
    assert len(user.get_all_accessible_teams(exclude_joined=False)) == 2


def test_get_all_accessible_teams_recursive(session):
    user = users.UserFactory()
    team = teams.TeamFactory(escalates_to=user.primary_team)
    team2 = teams.TeamFactory(escalates_to=team)
    session.commit()
    assert len(user.get_all_accessible_teams(exclude_joined=True)) == 2
    assert team in user.get_all_accessible_teams(exclude_joined=True)
    assert team2 in user.get_all_accessible_teams(exclude_joined=True)
    assert len(user.get_all_accessible_teams(exclude_joined=False)) == 3
