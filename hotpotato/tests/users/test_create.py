import json
from http import HTTPStatus as Status

import flask_security
from flask import url_for

from hotpotato import roles, teams
from hotpotato.teams import hotpotato_team
from hotpotato.tests import users
from hotpotato.tests.conftest import login


def test_create(client, app, session):
    """
    Test a user can be created and that the returned password passes login.
    """
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(hotpotato_team().id, "admin"))
    teams.hotpotato_team().add_users(user)
    session.commit()

    with app.app_context():
        with login(client, user):
            r = client.post(
                url_for("views_admin.new_user", team_id=hotpotato_team().id),
                data=dict(
                    email="test@example.com",
                    name="TEST USER",
                    timezone="UTC",
                    primary_team=hotpotato_team().id,
                ),
            )
        j = json.loads(r.get_data())

        password = j["password"]

        r = client.post(
            flask_security.url_for_security("login"),
            data=dict(username="test@example.com", password=password),
            follow_redirects=True,
        )
        assert r.status_code == Status.OK
