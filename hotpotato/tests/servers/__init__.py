"""
Server unit test functions.
"""


import secrets
import string

import factory
import flask_security

from hotpotato import models, servers
from hotpotato.models import Server, db


class ServerFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Server
        sqlalchemy_session = db.session
        exclude = "api_key_secret_unhashed"

    hostname = factory.LazyFunction(
        lambda: "{}-prod-monitor{}".format(
            factory.Faker("word").generate({}),
            factory.Faker("random_element", elements=[1, 2, 3]).generate({}),
        )
    )
    disable_missed_heartbeat_notif = factory.Faker("boolean")
    missed_heartbeat_limit = factory.Faker("random_int", max=20)
    timezone = factory.Faker("timezone")
    link = factory.Faker("url")

    apikey_id = factory.LazyFunction(
        lambda: "".join(
            secrets.choice(
                string.ascii_uppercase + string.ascii_lowercase + string.digits
            )
            for _ in range(20)
        )
    )
    apikey_secret = factory.LazyAttribute(
        lambda obj: flask_security.utils.hash_password(obj.api_key_secret_unhashed)
    )

    api_key_secret_unhashed = factory.LazyFunction(
        lambda: "".join(
            secrets.choice(
                string.ascii_uppercase + string.ascii_lowercase + string.digits
            )
            for _ in range(80)
        )
    )


def create_fake(fake):
    """
    Generate a fake server, with randomly set data.
    """

    apikey_id, apikey_secret = servers.generate_api_key()

    return models.Server(
        hostname="{}-prod-monitor{}".format(
            fake.word(), fake.random_element([1, 2, 3])
        ),
        apikey_id=apikey_id,
        apikey_secret=apikey_secret,
        disable_missed_heartbeat_notif=fake.boolean(),
        missed_heartbeat_limit=fake.random.randrange(20),
        timezone=fake.timezone(),
        link=fake.url(),
    )
