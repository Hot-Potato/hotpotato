"""
Test contact related forms
"""


from unittest.mock import Mock

import flask_login
import requests
from requests.models import Response
from werkzeug.datastructures import MultiDict

from hotpotato import teams
from hotpotato.models import TeamsUsers
from hotpotato.tests import oncall_contacts, users
from hotpotato.views.account.forms import AddContactForm


def test_valid_contact(session, monkeypatch):
    """
    Test validating the contact from with valid data
    """
    user = users.UserFactory()
    session.commit()

    monkeypatch.setattr(flask_login.utils, "_get_user", lambda: user)

    form = AddContactForm(MultiDict({"method": "sms", "contact": "+645559986"}))
    assert form.validate()


def test_duplicate_contact(session, monkeypatch):
    """
    Test validating the contact from with valid data
    """
    user = users.UserFactory()
    oncall_contacts.OncallContactsFactory(user=user, method="sms", contact="+645559986")
    session.commit()

    monkeypatch.setattr(flask_login.utils, "_get_user", lambda: user)

    form = AddContactForm(MultiDict({"method": "sms", "contact": "+645559986"}))
    assert form.validate() is False


def test_duplicate_contact_different_method(session, monkeypatch):
    """
    Test validating the contact from with valid data
    """
    user = users.UserFactory()
    oncall_contacts.OncallContactsFactory(user=user, method="sms", contact="+645559986")
    session.commit()

    monkeypatch.setattr(flask_login.utils, "_get_user", lambda: user)

    form = AddContactForm(MultiDict({"method": "pager", "contact": "+645559986"}))
    assert form.validate()


def test_send_failures_not_hotpotato(session, monkeypatch):
    """
    Test validating the contact from with valid data
    """
    user = users.UserFactory()
    session.commit()

    monkeypatch.setattr(flask_login.utils, "_get_user", lambda: user)

    form = AddContactForm(
        MultiDict({"method": "pager", "contact": "+645559986", "send_failures": True})
    )
    assert form.validate() is False


def test_send_failures_in_hotpotato(session, monkeypatch):
    """
    Test validating the contact from with valid data
    """
    user = users.UserFactory()
    session.add(TeamsUsers(user=user, team=teams.hotpotato_team(), primary=False))
    session.commit()

    monkeypatch.setattr(flask_login.utils, "_get_user", lambda: user)

    form = AddContactForm(
        MultiDict({"method": "pager", "contact": "+645559986", "send_failures": True})
    )
    assert form.validate()


def test_made_up_method(session, monkeypatch):
    """
    Test validating the contact from with valid data
    """
    user = users.UserFactory()
    session.commit()

    monkeypatch.setattr(flask_login.utils, "_get_user", lambda: user)

    form = AddContactForm(
        MultiDict({"method": "made-up-method", "contact": "+645559986"})
    )
    assert form.validate() is False


def pushover_api_mock_success(url, data=None, json=None, **kwargs):
    """
    Return a successful modica response to outgoing requests
    """
    response = Mock(spec=Response)
    response.status_code = 200
    response.json = lambda: {"status": 1}
    return response


def pushover_api_mock_fail(url, data=None, json=None, **kwargs):
    """
    Return a successful modica response to outgoing requests
    """
    response = Mock(spec=Response)
    response.status_code = 200
    response.json = lambda: {"status": 0}
    return response


def test_pushover(session, monkeypatch):
    """
    Test validating the contact from with valid data
    """
    user = users.UserFactory()
    session.commit()

    monkeypatch.setattr(flask_login.utils, "_get_user", lambda: user)
    monkeypatch.setattr(requests, "post", pushover_api_mock_success)

    form = AddContactForm(MultiDict({"method": "pushover", "contact": "+645559986"}))
    assert form.validate()


def test_pushover_fail(session, monkeypatch):
    """
    Test validating the contact from with valid data
    """
    user = users.UserFactory()
    session.commit()

    monkeypatch.setattr(flask_login.utils, "_get_user", lambda: user)
    monkeypatch.setattr(requests, "post", pushover_api_mock_fail)

    form = AddContactForm(MultiDict({"method": "pushover", "contact": "+645559986"}))
    assert form.validate() is False
