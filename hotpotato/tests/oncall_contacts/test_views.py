"""
Test contact related views
"""


from flask import url_for

from hotpotato import teams
from hotpotato.models import OncallContact, TeamsUsers
from hotpotato.tests import oncall_contacts, users
from hotpotato.tests.conftest import login


def test_create(app, client, session):
    """
    Test that a contact can be created.
    """
    user = users.UserFactory()
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post(
            url_for("views_account.add_contact_method", team_id=user.primary_team.id),
            data=dict(method="sms", contact="+642739000"),
            follow_redirects=True,
        )

        assert r.status_code == 200

    contact = OncallContact.query.filter_by(contact="+642739000").first()
    assert contact.method == "sms"
    assert contact.user == user


def test_create_invalid(app, client, session):
    """
    Test that a contact can be created.
    """
    user = users.UserFactory()
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post(
            url_for("views_account.add_contact_method", team_id=user.primary_team.id),
            data=dict(method="made-up-method", contact="+642739000"),
            follow_redirects=True,
        )

        assert r.status_code == 200

    contact = OncallContact.query.filter_by(contact="+642739000").first()
    assert contact is None


def test_move_up(app, client, session):
    """
    Test that a contact method can be moved up.
    """
    user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(user=user, priority=1)
    contact2 = oncall_contacts.OncallContactsFactory(user=user, priority=2)
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post(
            url_for(
                "views_account.move_contact_up",
                contact_id=contact2.id,
                team_id=user.primary_team.id,
            ),
            follow_redirects=True,
        )

        assert r.status_code == 200

    contact = session.merge(contact)
    contact2 = session.merge(contact2)
    session.refresh(contact)
    session.refresh(contact2)
    assert contact.priority == 2
    assert contact2.priority == 1


def test_move_down(app, client, session):
    """
    Test that a contact method can be moved down.
    """
    user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(user=user, priority=1)
    contact2 = oncall_contacts.OncallContactsFactory(user=user, priority=2)
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post(
            url_for(
                "views_account.move_contact_down",
                contact_id=contact.id,
                team_id=user.primary_team.id,
            ),
            follow_redirects=True,
        )

        assert r.status_code == 200

    contact = session.merge(contact)
    contact2 = session.merge(contact2)
    session.refresh(contact)
    session.refresh(contact2)
    assert contact.priority == 2
    assert contact2.priority == 1


def test_toggle_send(app, client, session):
    """
    Test that toggling send pages works.
    """
    user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(user=user, send_pages=False)
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post(
            url_for(
                "views_account.toggle_send_pages",
                contact_id=contact.id,
                team_id=user.primary_team.id,
            ),
            follow_redirects=True,
        )

        assert r.status_code == 200

    contact = session.merge(contact)
    session.refresh(contact)
    assert contact.send_pages is True


def test_toggle_notify_fail(app, client, session):
    """
    Test that toggling notify fail works.
    """
    user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(
        user=user, method="sms", send_failures=False
    )
    session.add(TeamsUsers(primary=False, user=user, team=teams.hotpotato_team()))
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post(
            url_for(
                "views_account.toggle_notify_on_fail",
                contact_id=contact.id,
                team_id=user.primary_team.id,
            ),
            follow_redirects=True,
        )
        assert r.status_code == 200

        contact = session.merge(contact)
        session.refresh(contact)
        assert contact.send_failures is True

        r = client.post(
            url_for(
                "views_account.toggle_notify_on_fail",
                contact_id=contact.id,
                team_id=user.primary_team.id,
            ),
            follow_redirects=True,
        )
        assert r.status_code == 200

    contact = session.merge(contact)
    session.refresh(contact)
    assert contact.send_failures is False


def test_toggle_notify_fail_not_hotpotato(app, client, session):
    """
    Test that toggling notify fail works.
    """
    user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(
        user=user, send_failures=False, method="sms"
    )
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post(
            url_for(
                "views_account.toggle_notify_on_fail",
                contact_id=contact.id,
                team_id=user.primary_team.id,
            ),
            follow_redirects=True,
        )

        assert r.status_code == 200

    contact = session.merge(contact)
    session.refresh(contact)
    assert contact.send_failures is False


def test_send_test_message(app, client, session):
    """
    Test that sending a test message works.
    """
    user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(user=user)
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post(
            url_for(
                "views_account.send_test_message",
                contact_id=contact.id,
                team_id=user.primary_team.id,
            ),
            follow_redirects=True,
        )

        assert r.status_code == 200


def test_removing_contact_methods(app, client, session):
    """
    Test that removing contact methods works.
    """
    user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(user=user)
    session.commit()
    contact_id = contact.id
    with app.app_context(), login(client, user):
        r = client.post(
            url_for(
                "views_account.remove_contact_methods", team_id=user.primary_team.id
            ),
            data={str(contact.id): 1},
            follow_redirects=True,
        )

        assert r.status_code == 200

    assert OncallContact.query.get(contact_id) is None


def test_removing_contact_methods_no_contacts(app, client, session):
    """
    Test that removing contact methods works.
    """
    user = users.UserFactory()
    oncall_contacts.OncallContactsFactory(user=user)
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post(
            url_for(
                "views_account.remove_contact_methods", team_id=user.primary_team.id
            ),
            data={},
            follow_redirects=True,
        )

        assert r.status_code == 200


def test_removing_contact_methods_invalid_values(app, client, session):
    """
    Test that removing contact methods works.
    """
    user = users.UserFactory()
    oncall_contacts.OncallContactsFactory(user=user)
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post(
            url_for(
                "views_account.remove_contact_methods", team_id=user.primary_team.id
            ),
            data={"test-value": 1},
            follow_redirects=True,
        )

        assert r.status_code == 200


def test_removing_contact_methods_invalid_and_real(app, client, session):
    """
    Test that removing contact methods works.
    """
    user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(user=user)
    session.commit()
    contact_id = contact.id
    with app.app_context(), login(client, user):
        r = client.post(
            url_for(
                "views_account.remove_contact_methods", team_id=user.primary_team.id
            ),
            data={str(contact.id): 1, "test-value": 1},
            follow_redirects=True,
        )

        assert r.status_code == 200

    assert OncallContact.query.get(contact_id) is None


def test_removing_contacts_mutltiple(app, client, session):
    """
    Test that removing contact methods works.
    """
    user = users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(user=user)
    contact2 = oncall_contacts.OncallContactsFactory(user=user)
    session.commit()
    contact_id = contact.id
    contact2_id = contact2.id
    with app.app_context(), login(client, user):
        r = client.post(
            url_for(
                "views_account.remove_contact_methods", team_id=user.primary_team.id
            ),
            data={str(contact.id): 1, str(contact2.id): 1},
            follow_redirects=True,
        )

        assert r.status_code == 200

    assert OncallContact.query.get(contact_id) is None
    assert OncallContact.query.get(contact2_id) is None
