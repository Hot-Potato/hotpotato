from flask_security import current_user, login_user

from hotpotato import permissions, roles, rules, teams
from hotpotato.rules import contexts, login, permission
from hotpotato.rules.contexts.team import TeamContext
from hotpotato.rules.contexts.team_and_escalates_to import TeamAndEscalatesToContext
from hotpotato.tests import roles as roles_test, teams as teams_test, users


def test_allowed_access(session, app, monkeypatch):
    monkeypatch.setattr(teams, "current_team", teams.hotpotato_team())
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(teams.hotpotato_team().id, "admin"))
    teams.hotpotato_team().add_users(user)
    session.commit()
    with app.test_request_context():
        login_user(user)
        assert rules.has_access_to("views_admin.admin")


def test_login_rule_anonymous(session, app):
    with app.test_request_context():
        assert not login.LoginRule().test(contexts.team)


def test_login_rule_deactivated(session, app):
    user = users.UserFactory()
    user.active = False
    session.commit()
    with app.test_request_context():
        login_user(user)
        assert not login.LoginRule().test(contexts.team)


def test_login_rule_active(session, app):
    user = users.UserFactory()
    session.commit()
    with app.test_request_context():
        login_user(user)
        assert login.LoginRule().test(contexts.team)


class TeamTestContext(TeamContext):
    @property
    def team(self):
        return current_user.primary_team


def test_permission_rule_missing(session, app):
    user = users.UserFactory()
    session.commit()
    with app.test_request_context():
        login_user(user)
        assert permission.PermissionRule("handover.do").test(TeamTestContext()) is None


def test_permission_rule_allowed(session, app):
    user = users.UserFactory()
    role = roles_test.RoleFactory(team=user.primary_team)
    user.roles.append(role)
    session.commit()
    permissions.get_by_name("handover.do").set_permission(role.id, True)
    with app.test_request_context():
        login_user(user)
        assert permission.PermissionRule("handover.do").test(TeamTestContext()) is True


def test_permission_rule_denied(session, app):
    user = users.UserFactory()
    role = roles_test.RoleFactory(team=user.primary_team)
    user.roles.append(role)
    session.commit()
    permissions.get_by_name("handover.do").set_permission(role.id, False)
    with app.test_request_context():
        login_user(user)
        assert permission.PermissionRule("handover.do").test(TeamTestContext()) is False


def test_permission_rule_allowed_wrong_team(session, app):
    user = users.UserFactory()
    role = roles_test.RoleFactory(team=user.primary_team)
    user.roles.append(role)
    team = teams_test.TeamFactory()

    class TeamTestContextAgain(TeamContext):
        @property
        def team(self):
            return team

    session.commit()
    permissions.get_by_name("handover.do").set_permission(role.id, True)
    with app.test_request_context():
        login_user(user)
        assert (
            permission.PermissionRule("handover.do").test(TeamTestContextAgain())
            is None
        )


def test_permission_rule_escalates(session, app):
    user = users.UserFactory()
    role = roles_test.RoleFactory(team=user.primary_team)
    user.roles.append(role)
    teams_test.TeamFactory(escalates_to=user.primary_team)

    class TeamAndEscalatesToTestContext(TeamAndEscalatesToContext):
        @property
        def team(self):
            return (
                self._test(current_user, current_user.primary_team)
                if current_user.primary_team
                else None
            )

    session.commit()
    permissions.get_by_name("handover.do").set_permission(role.id, True)
    with app.test_request_context():
        login_user(user)
        assert (
            permission.PermissionRule("handover.do").test(
                TeamAndEscalatesToTestContext()
            )
            is True
        )
