"""
Test permissions related views
"""

from flask import url_for

from hotpotato import permissions, roles, teams
from hotpotato.tests import users
from hotpotato.tests.conftest import login


def test_adding_permission_to_role(client, session, app):
    """
    Test that adding a permission works.
    """
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    session.commit()
    role_id = roles.create(user.primary_team.id, "A New Role").id

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.manage_team",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
            ),
            data={
                "action": "add_permission",
                "role_id": role_id,
                "permission_name": "handover.do",
                "access": "ALLOW",
            },
            follow_redirects=True,
        )
        assert resp.status_code == 200
    permission = permissions.get_by_name("handover.do")
    assert permission.get_permission(role_id).access is True


def test_toggling_permission_on_role(client, session, app):
    """
    Test that adding a permission works.
    """
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    session.commit()
    role_id = roles.create(user.primary_team.id, "A New Role").id
    permission = permissions.get_by_name("handover.do")
    permission.set_permission(role_id, True)
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.toggle_permission",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
            ),
            data={"role_id": role_id, "permission_name": "handover.do"},
            follow_redirects=True,
        )
        assert resp.status_code == 200
    permission = permissions.get_by_name("handover.do")
    assert permission.get_permission(role_id).access is False


def test_removing_permission_from_role(client, session, app):
    """
    Test that adding a permission works.
    """
    user = users.UserFactory()
    user.roles.append(roles.get_by_name(user.primary_team.id, "admin"))
    teams.hotpotato_team().add_users(user)
    session.commit()
    role_id = roles.create(user.primary_team.id, "A New Role").id
    permission = permissions.get_by_name("handover.do")
    permission.set_permission(role_id, True)
    session.commit()

    with app.app_context(), login(client, user):
        resp = client.post(
            url_for(
                "views_admin.remove_permission",
                team_id=user.primary_team.id,
                _team_id=user.primary_team.id,
            ),
            data={"role_id": role_id, "permission_name": "handover.do"},
            follow_redirects=True,
        )
        assert resp.status_code == 200
    assert permissions.get_by_name("handover.do").get_permission(role_id) is None
