"""
Alert classes and functions.
"""

import sqlalchemy

from hotpotato import util
from hotpotato.models import Alert
from hotpotato.notifications import exceptions, notifications

NOTIF_TYPE = "alert"

# A frozen set of values representing all possible JSON API parameters for an alert.
JSON_API_PARAMS = frozenset(
    (
        "wr",
        "alert_type",
        "server_id",
        "trouble_code",
        "hostname",
        "display_name",
        "service_name",
        "state",
        "output",
        "timestamp",
    )
)


# A frozen set of values representing the type of an alert.
TYPE = frozenset(("service", "host"))

# Frozen sets of values representing the state of a service alert.
SERVICE_EMERGENCY_STATE = frozenset(("CRITICAL",))
SERVICE_STANDARD_STATE = frozenset(("OK", "WARNING"))
SERVICE_STATE = SERVICE_EMERGENCY_STATE | SERVICE_STANDARD_STATE

# Frozen sets of values representing the state of a host alert.
HOST_EMERGENCY_STATE = frozenset(("DOWN",))
HOST_STANDARD_STATE = frozenset(("UP",))
HOST_STATE = HOST_EMERGENCY_STATE | HOST_STANDARD_STATE

# Frozen sets of values representing all possible states of an alert.
STATE = SERVICE_STATE | HOST_STATE
EMERGENCY_STATE = SERVICE_EMERGENCY_STATE | HOST_EMERGENCY_STATE
STANDARD_STATE = SERVICE_STANDARD_STATE | HOST_STANDARD_STATE


def get(ale_id, team_id=None):
    """
    Get an alert.
    """
    try:
        if team_id is not None:
            return Alert.query.filter_by(id=ale_id, team_id=team_id).one()
        else:
            return Alert.query.filter_by(id=ale_id).one()
    except (sqlalchemy.orm.exc.NoResultFound, sqlalchemy.exc.DataError):
        raise exceptions.NotificationIDError(ale_id)


def get_by(**kwargs):
    """
    Return a tuple of all alerts matching the given search parameters.
    """

    kwargs["notif_type"] = "alert"
    return notifications.get_by_helper(**kwargs)


def get_query(ale_id=None):
    """
    Get a query object for selecting the row object for an alert.
    If ale_id is specified, applies a filter for it on the query object.
    """

    return notifications.get_query(ale_id).filter_by(notif_type="alert")


def get_failed_query():
    """
    Return a query object for getting failed alerts.
    """

    return get_query().filter(notifications.in_json("status", "SEND_FAILED"))


def get_failed():
    """
    Return a list of all Alert that failed to send.
    """

    return (obj for obj in get_failed_query().all())


def get_num_failed():
    """
    Get the number of failed alerts.
    """

    return get_failed_query().count()


# pylint: disable=too-many-arguments
def create(
    team_id,
    alert_type,
    server,
    trouble_code,
    hostname,
    display_name,
    service_name,
    state,
    output,
    timestamp,
):
    """
    Create a new alert.
    """

    # pylint: disable=too-many-locals

    if not trouble_code:
        trouble_code = "NOTC"

    ts_str = util.datetime_get_as_string(timestamp)

    json_obj = {
        "alert_type": alert_type,
        "server_id": server.id,
        "trouble_code": trouble_code,
        "hostname": hostname,
        "display_name": display_name,
        "service_name": service_name,
        "state": state,
        "output": output,
        "timestamp": ts_str,
    }

    # Body is computed later
    return notifications.create(team_id=team_id, notif_model=Alert, json_obj=json_obj)
