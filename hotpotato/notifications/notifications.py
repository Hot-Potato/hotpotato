"""
Notification classes and helper functions.
"""


import json
from datetime import datetime

import sqlalchemy
from sqlalchemy.orm import joinedload

from hotpotato import util
from hotpotato.models import Notification as Model, db

from hotpotato.notifications import (  # noqa: F401; Import package globals so they can be referenced from this module by external code.
    APP_PROVIDER,
    JSON_VERSION,
    METHOD,
    PAGER_PROVIDER,
    PROVIDER,
    PROVIDER_FOR_METHOD,
    SMS_PROVIDER,
    STATUS,
    TYPE,
    exceptions,
)


# Not actually a valid notification type, but is used by API notification handlers
# to help them define special behaviour for when representing things as generic notifications
# are required.
NOTIF_TYPE = "notification"


JSON_API_PARAMS = frozenset(("version", "node_name", "event_id", "ticket_id", "method"))


def get(notif_id, eager=False, team_id=None):
    """
    Get a notification.
    """

    try:
        if eager:
            return get_joined_query(notif_id=notif_id, team_id=team_id).one()
        else:
            if team_id is not None:
                return Model.query.filter_by(id=notif_id, team_id=team_id).one()
            else:
                return Model.query.filter_by(id=notif_id).one()
    except (sqlalchemy.orm.exc.NoResultFound, sqlalchemy.exc.DataError):
        raise exceptions.NotificationIDError(notif_id)


def get_by(**kwargs):
    """
    Return a generator of all notifications matching the given search parameters.
    """

    return (obj for obj in get_by_helper(**kwargs))


def get_query(notif_id=None):
    """
    Get a query object for selecting the row object for a notification.
    If notif_id is specified, applies a filter for it on the query object.
    """

    if notif_id:
        return Model.query.filter_by(id=notif_id)
    else:
        return Model.query


def get_joined_query(notif_id=None, team_id=None):
    """
    Get a query with joins to replies/users/servers.
    """

    query = Model.query.options(
        joinedload("send_attempts"), joinedload("team"), joinedload("escalations")
    )

    if notif_id:
        query = query.filter(Model.id == notif_id)
    if team_id:
        query = query.filter(Model.team_id == team_id)

    return query


def in_json(key, value):
    """
    Return an SQLAlchemy expression object for checking if the given value is
    available at the given key in the JSON field of a notification.
    Used in query filters.
    """

    return Model.json.op("@>")(json.dumps({key: value}))


def not_in_json(key, value):
    """
    Return an SQLAlchemy expression object for checking if the given value is
    *NOT* available at the given key in the JSON field of a notification.
    Used in query filters.
    """

    return sqlalchemy.not_(in_json(key, value))


# pylint: disable=too-many-arguments
def create(team_id, notif_model, user_id=None, status=None, json_obj=None):
    """
    Create a new notification.
    """

    received_dt = datetime.utcnow()
    if not status:
        status = "NEW" if user_id else "UNSENT"

    if status not in STATUS:
        raise exceptions.NotificationStatusError(status)

    # Add the API standard data to the JSON object.
    if not json_obj:
        json_obj = {}
    json_obj.update(
        {
            "version": JSON_VERSION,
            "node_name": util.node_name,
            "event_id": None,  # TODO: automatically fill event_id in?
            "ticket_id": None,  # TODO: automatically fill ticket_id in?
            "method": None,
        }
    )

    obj = notif_model(team_id=team_id, received_dt=received_dt, json=json_obj)

    db.session.add(obj)
    db.session.commit()

    return obj


#
# Internal helper methods.
#


def get_by_helper(**kwargs):
    """
    Return a query result object of all notifications matching the given search parameters.
    """

    model_keys = tuple(column.key for column in Model.__table__.columns)

    query = Model.query

    for key, value in kwargs.items():
        if key == "json":
            continue
        elif key in model_keys:
            query = query.filter(getattr(Model, key) == value)
        else:
            query = query.filter(in_json(key, value))

    return query.all()
