"""
Handover notification classes and functions.
"""


from flask_babelex import gettext as _

from hotpotato.models import Handover, db
from hotpotato.notifications import notifications

NOTIF_TYPE = "handover"

# A frozen set of values representing all possible JSON API parameters for a handover notification.
JSON_API_PARAMS = frozenset(("to_user_id", "from_user_id", "message"))


class NoContactMethodsError(ValueError):
    pass


def get(notif_id, team_id=None):
    """
    Get a handover.
    """

    return notifications.get(notif_id, team_id=team_id)


def get_by(**kwargs):
    """
    Return a tuple of all handover notifications matching the given search parameters.
    """

    kwargs["notif_type"] = "handover"
    return notifications.get_by_helper(**kwargs)


def handover(team, to_user, from_user=None, message=None):
    if len(to_user.contacts) == 0:
        raise NoContactMethodsError(
            _("You cannot take the pager if you have no contact methods.")
        )

    # Update on_call for the current team
    team.on_call = to_user
    db.session.commit()

    from_user_id = from_user.id if from_user else None

    # Send the handover notifications.
    handover_notif = create(
        team_id=team.id,
        to_user_id=to_user.id,
        from_user_id=from_user_id,
        message=message,
    )
    handover_notif.send(to_user.id)
    if from_user:
        handover_notif.send(from_user.id)


def create(team_id, to_user_id, from_user_id=None, message=None):
    """
    Create a new pair of handover notifications, and returns it as a tuple,
    with the first being the handover notifications for the new on-call person,
    and the second being the handover notifications for the current on-call person.

    If from_user_id is not specified the handover notifications for the current on-call
    person will not be created, and the second element of the tuple will be None.
    """

    json_obj = {
        "to_user_id": to_user_id,
        "from_user_id": from_user_id if from_user_id else None,
        "message": message,
    }

    return notifications.create(
        team_id=team_id, notif_model=Handover, json_obj=json_obj
    )
