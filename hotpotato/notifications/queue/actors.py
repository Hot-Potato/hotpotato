"""
Notification message queue actor functions.
"""


import dramatiq
import flask
from dramatiq import Actor

# When Hot Potato itself addresses the actors, there is a Flask app context available,
# but when this file is started as a Dramatiq worker, there is none.
# This creates a basic one that allows the Dramatiq workers to connect to the message queue,
# the database and everything else needed to send notifications.
if flask.has_app_context():
    from flask import current_app as app
else:
    from hotpotato.app.worker import app

PRIORITY_HANDOVER = 0
PRIORITY_ALERT = 10
PRIORITY_MESSAGE = 20


# An explicit actor is passed here so it can be patched during testing
@dramatiq.actor(actor_class=Actor)
def escalate_check(notif_id):
    """
    Checks whether a message was read by the client and hence if it should be
    escalated. This is scheduled `Interval` after a message with priority of
    at least `Priority` was sent.
    """
    with app.app_context():
        from hotpotato.notifications import alerts
        from hotpotato.teams import escalate, should_escalate

        ale = alerts.get(notif_id)
        if should_escalate(ale):
            escalate(ale)


@dramatiq.actor(priority=PRIORITY_ALERT, max_retries=3)
def alert(notif_id, user_id, contact_id=None, method=None, escalating=False):
    """
    Send the given alert, with the appropriate priority.
    """

    with app.app_context():
        from hotpotato.models.teams import EscalationPolicy
        from hotpotato.notifications import alerts
        from hotpotato.teams import escalate, in_business_hours, should_escalate

        alert = alerts.get(notif_id)

        # If it is outside business hours escalate immediately and *do not* send to the normal team
        if (
            alert.team.escalation_policy == EscalationPolicy.OutsideBusinessHours
            and not in_business_hours(alert.team)
            and should_escalate(alert)
            and not escalating
        ):
            escalate(alert)
            return

        # If it is outside business hours but not critical do not send at all
        if (
            alert.team.escalation_policy == EscalationPolicy.OutsideBusinessHours
            and not in_business_hours(alert.team)
            and alert.json["state"] not in alerts.EMERGENCY_STATE
        ):
            return

        # Otherwise try to send normally
        try:
            actor_send(alert, user_id, contact_id=contact_id, method=method)
        finally:
            # Schedule an escalation check
            # Delay is in milleseconds
            # Business hours policy is already delt with
            if (
                alert.team.escalation_policy == EscalationPolicy.Always
                and not escalating
            ):
                escalate_check.send_with_options(
                    args=(notif_id,), delay=alert.team.escalation_delay_mins * 60 * 1000
                )


@dramatiq.actor(priority=PRIORITY_HANDOVER, max_retries=3)
def handover(notif_id, user_id, contact_id=None, method=None, escalating=False):
    """
    Send the given handover notification, with the appropriate priority.
    """

    with app.app_context():
        from hotpotato.notifications import handovers

        actor_send(
            handovers.get(notif_id), user_id, contact_id=contact_id, method=method
        )


@dramatiq.actor(priority=PRIORITY_MESSAGE, max_retries=3)
def message(notif_id, user_id, contact_id=None, method=None, escalating=False):
    """
    Send the given message, with the appropriate priority.
    """

    with app.app_context():
        from hotpotato.notifications import messages

        actor_send(
            messages.get(notif_id), user_id, contact_id=contact_id, method=method
        )


def actor_send(notif, user_id, contact_id=None, method=None):
    """
    Helper method to perform the sending action.
    """

    from hotpotato.notifications import send

    if contact_id:
        send.using_contact_id(notif, user_id, contact_id)
    elif method:
        send.using_method(notif, user_id, method)
    else:
        send.using_all(notif, user_id)
