"""
Functions for managing NotificationAttempts.
"""

import sqlalchemy

from hotpotato.models import NotificationAttempt as Model
from hotpotato.notifications import PROVIDER, exceptions


def get_by(**kwargs):
    """
    Return all attempts matching given filters.
    """

    return Model.query.filter_by(**kwargs).all()


def get_by_provider_notif_id(provider, provider_notif_id):
    """
    Get a notification attempt by its handling provider and the corresponding notification ID.
    """

    if provider not in PROVIDER:
        raise exceptions.NotificationProviderError(provider)

    try:
        return Model.query.filter_by(
            provider=provider, provider_notif_id=provider_notif_id
        ).one()
    except (sqlalchemy.orm.exc.NoResultFound, sqlalchemy.exc.DataError):
        raise exceptions.NotificationProviderNotificationIDError(
            provider, provider_notif_id
        )
