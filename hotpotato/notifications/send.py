"""
Sender functions.
"""

import traceback

from flask import current_app
from sqlalchemy.dialects.postgresql import array

from hotpotato import notifications, oncall_contacts
from hotpotato.models import db
from hotpotato.models.notifications.notifications import NotificationAttempt
from hotpotato.notifications import exceptions, senders


def using_all(notif, user_id):
    """
    Try to send a notification using all available senders, in Hot Potato's preferred order.
    """

    _send(notif, oncall_contacts.get_for_user_id(user_id))


def using_contact_id(notif, user_id, contact_id):
    """
    Try to send a notification using a given contact ID.
    """

    try:
        contact = oncall_contacts.get(contact_id)
    except oncall_contacts.OncallContactIDError as err:
        raise exceptions.NotificationSendError(
            "While trying to send {} with ID {} "
            "to contact with ID {}: {} ({})".format(
                notif.description, notif.id, contact_id, str(err), type(err).__name__
            )
        )

    using_contact(notif, user_id, contact)


def using_contact(notif, user_id, contact):
    """
    Try to send a notification using a given contact.
    """

    _send(notif, tuple((contact,)))


def using_method(notif, user_id, method):
    """
    Try to send a notification using a given sending method.
    """

    if method not in notifications.METHOD:
        raise exceptions.NotificationSendError(
            "Invalid or unsupported sending method '{}'".format(method)
        )

    _send(notif, oncall_contacts.get_by(user_id=user_id, method=method))


def using_sender(notif, sender, contact):
    """
    Try to send a notification using a given sender and a given contact.
    Usually only used for testing purposes.
    """

    _send(notif, tuple((contact,)), senders_for_method=lambda m: tuple((sender,)))


def _send(notif, contacts, senders_for_method=None):
    """
    Sending helper method that does all the heavy lifting.
    """

    if not senders_for_method:
        senders_for_method = senders.for_method

    for i, contact in enumerate(contacts):
        if notif.notif_type == "alert" and not contact.send_pages:
            current_app.logger.debug(
                "Skipped sending because send_pages is disabled "
                "for contact with ID {} for user {}".format(contact.id, contact.user.id)
            )
            continue

        if not current_app.config["{}_ENABLED".format(contact.method.upper())]:
            current_app.logger.debug(
                "Skipped sending because the contact method '{}' is disabled "
                "for contact with ID {} for user {}".format(
                    contact.method, contact.id, contact.user_id
                )
            )
            continue

        attempt = NotificationAttempt(
            notification=notif,
            user=contact.user,
            warnings=array([], type_=db.Text()),
            errors=array([], type_=db.Text()),
            status="SEND_FAILED",
        )
        (warnings, errors) = ([], [])
        db.session.add(attempt)

        contact_senders = senders_for_method(contact.method)

        if not contact_senders:
            error = "There were no senders for method {}.".format(contact.method)
            current_app.logger.error(error)
            errors.append(error)
            attempt.errors = array(errors, type_=db.Text())
            attempt.warnings = array(warnings, type_=db.Text())
            db.session.commit()
            continue

        for j, sender in enumerate(contact_senders):
            try:
                current_app.logger.debug(
                    "Attempting to send {} using {} "
                    "to contact with number '{}'".format(
                        notif.description, sender.description, contact.contact
                    )
                )

                data = sender.send(notif, contact)

                errors.extend(data["errors"])
                warnings.extend(data["warnings"])

                if data["success"]:
                    attempt.status = "SENT_TO_PROVIDER"
                    attempt.method = sender.method
                    attempt.provider = sender.provider
                    attempt.provider_notif_id = data["provider_notif_id"]
                    # Operation succeeded, break out here to update the object.
                    break

            except Exception as err:
                error = "Unhandled exception while sending to {} using {}.\n{}".format(
                    contact.contact,
                    sender.description,
                    traceback.format_exception(type(err), err, err.__traceback__),
                )
                current_app.logger.error(error)
                errors.append(error)
                continue

        attempt.errors = array(errors, type_=db.Text())
        attempt.warnings = array(warnings, type_=db.Text())
        db.session.commit()

        # If send succeeded stop trying to send to more contacts.
        if attempt.status == "SENT_TO_PROVIDER":
            break

    # We didn't succeed sending to any contacts
    else:
        if contacts:
            error = "Attempt to send notifications to {} failed.\nAttempts:\n".format(
                contact.user.email
            )
            for attempt in NotificationAttempt.query.filter(
                NotificationAttempt.notification_id == notif.id
            ):
                error += "Errors: {} Warnings: {}\n".format(
                    attempt.errors, attempt.warnings
                )
        else:
            error = "No contacts were found to send to."

        current_app.logger.warning(error)
        raise exceptions.NotificationSendError(error)
