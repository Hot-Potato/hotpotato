"""
Notification Twilio SMS sender classes and functions.
"""


import flask
import twilio.rest

from hotpotato.notifications.senders.sms import SMSBaseSender


class TwilioSMSSender(SMSBaseSender):
    """
    Notification Twilio SMS sender.
    """

    name = "sms_twilio"
    description = "Twilio SMS"

    provider = "twilio"

    def send(self, notif, contact):
        """
        Send a notification to an individual contact via Twilio SMS.
        """

        success = False
        warnings = []
        errors = []
        provider_notif_id = None

        account_sid = flask.current_app.config["TWILIO_ACCOUNT_SID"]
        auth_token = flask.current_app.config["TWILIO_AUTH_TOKEN"]
        sms_number = flask.current_app.config["TWILIO_SMS_NUMBER"]

        client = twilio.rest.Client(account_sid, auth_token)

        provider_notif = client.messages.create(
            contact.contact, body=notif.body(user=contact.user), from_=sms_number
        )

        # If we got this far, the operation succeeded and we can return.
        success = True
        provider_notif_id = provider_notif.sid

        return {
            "success": success,
            "warnings": warnings,
            "errors": errors,
            "provider_notif_id": str(provider_notif_id),
        }

    def can_send(self, user_id):
        """
        Return True if the user with the given ID can use the SMS sender,
        and sending via SMS is enabled and Twilio is enabled.
        """

        return (
            super().can_send(user_id)
            if flask.current_app.config["TWILIO_ENABLED"]
            else False
        )
