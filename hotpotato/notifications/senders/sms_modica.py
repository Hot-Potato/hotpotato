"""
Notification Modica SMS sender classes and functions.
"""

import flask

from hotpotato.notifications.senders.modica import ModicaBaseSender
from hotpotato.notifications.senders.sms import SMSBaseSender


class ModicaSMSSender(ModicaBaseSender, SMSBaseSender):
    """
    Notification Modica SMS sender.
    """

    name = "sms_modica"
    description = "Modica SMS"

    def can_send(self, user_id):
        """
        Return True if the user with the given ID can use the SMS sender,
        and sending via SMS is enabled and Modica is enabled.
        """

        return (
            super().can_send(user_id)
            if flask.current_app.config["MODICA_ENABLED"]
            else False
        )
