"""
Stats API version 1 blueprint.
"""


from flask import Blueprint, current_app, request

blueprint = Blueprint("api_stats_v1", __name__)


@blueprint.before_request
def authenticate():
    key = current_app.config.get("STATS_API_KEY")
    if key is None:
        return "Stats API is disabled", 404
    else:
        header = request.headers.get("Authorization")

        if header is None or header.split(" ")[0].lower() != "bearer":
            return "", 401, {"WWW-Authenticate": 'Bearer realm="Stats API"'}

        if header.split(" ")[1] != key:
            return "", 403
