"""
API notification functions.
"""


from http import HTTPStatus

import dateutil
import flask
import sqlalchemy
import werkzeug.datastructures
from sqlalchemy import and_, or_

from hotpotato import util
from hotpotato.api.functions import exceptions as api_functions_exceptions
from hotpotato.models import Alert, Escalation, Tag, Team, db
from hotpotato.notifications import notifications


def get(handler, notif_id, team_id=None):
    """
    Return the notification object with the given ID, using the given handler.
    """

    return handler.get(notif_id, team_id=team_id)


def as_dict(handler, notif):
    """
    Return a notification object as an API-compatible dictionary.
    """

    return handler.schema.dump(notif)


def as_csv(handler, notifs):
    """
    Return a list of notifications in CSV format.
    """

    schema = handler.schema

    columns = tuple(schema.fields.keys())

    rows = tuple((tuple(schema.dump(notif).values()) for notif in notifs))

    return (columns, rows)


def search_filters_get(handler=None, ignore_notif_type=None, fail_if_empty=True):
    """
    Return a MultiDict of all notification search filters defined in the URL arguments.
    More than one value can be defined for a given key.
    """

    # If handler is specified, redefine ignore_notif_type according to
    # the type of notification the handler processes.
    if ignore_notif_type is None and handler:
        ignore_notif_type = handler.NOTIF_TYPE != "notification"

    sfs = werkzeug.datastructures.MultiDict(mapping=flask.request.args)

    # Used when type is determined separately from search filters.
    if ignore_notif_type:
        sfs.poplist("notif_type")
        sfs.poplist("no_notif_type")

    sfs.poplist("csrf_token")

    return sfs


def get_from_search_filters(handler, search_filters, eager=True, show_escalations=True):
    """
    Take in a MultiDict representing search filters, and return a
    list of notification database objects that match.
    """

    if eager:
        query = notifications.get_joined_query()
    else:
        query = notifications.get_query()

    if handler.NOTIF_TYPE != "notification":
        query = query.filter(notifications.Model.notif_type == handler.NOTIF_TYPE)

    # Filter to all notifications from the specified date.
    # NOTE: Time processing is required here, to shift the 24 hour period
    # specified by the user to its equivalent in UTC.
    start_date_dt = None
    try:
        start_date_dt = util.datetime_process(
            dateutil.parser.isoparse(search_filters["start_date"]),
            to_utc=True,
            naive=True,
        )
        query = query.filter(start_date_dt <= notifications.Model.received_dt)
    except ValueError as e:
        flask.current_app.logger.error(e)
        raise api_functions_exceptions.APISearchFilterError(
            "start_date format error", success=False, code=HTTPStatus.BAD_REQUEST
        )
    except KeyError:
        pass

    # Filter to all notifications up until the specified date.
    # NOTE: Time processing is required here, to shift the 24 hour period
    # specified by the user to its equivalent in UTC.
    end_date_dt = None
    try:
        end_date_dt = util.datetime_process(
            dateutil.parser.isoparse(search_filters["end_date"]),
            to_utc=True,
            naive=True,
        )
        query = query.filter(notifications.Model.received_dt < end_date_dt)
    except ValueError:
        raise api_functions_exceptions.APISearchFilterError(
            "end_date format error", success=False, code=HTTPStatus.BAD_REQUEST
        )
    except KeyError:
        pass

    # Sanity checks for the start_date and end_date parameters.
    if (start_date_dt or end_date_dt) and "received_dt" in search_filters:
        raise api_functions_exceptions.APISearchFilterError(
            "Search filter parameters 'received_dt' and 'start_date/end_date' "
            "are mutually exclusive",
            success=False,
            code=HTTPStatus.BAD_REQUEST,
        )
    if start_date_dt and end_date_dt and start_date_dt >= end_date_dt:
        raise api_functions_exceptions.APISearchFilterError(
            "start_date ({}) is set to a later date than end_date ({})".format(
                start_date_dt.date().isoformat(), end_date_dt.date().isoformat()
            ),
            success=False,
            code=HTTPStatus.BAD_REQUEST,
        )

    # Direct notification attribute filtering.
    # N.B. supports more than one value for each parameter, using the MultiDict structure.
    for param, values in search_filters.lists():
        if not values or all(not value for value in values):
            continue
        if param in ("start_date", "end_date"):
            continue
        if handler.NOTIF_TYPE != "notification" and param in (
            "notif_type",
            "no_notif_type",
        ):
            continue

        if param == "team_id" and show_escalations:
            continue

        if param.startswith("not_"):
            search_equals = False
            raw_param = param[4:]
            if raw_param in search_filters:
                raise api_functions_exceptions.APISearchFilterError(
                    "Search filter parameter '{}': "
                    "equal and not-equal search filters cannot be combined".format(
                        raw_param
                    ),
                    success=False,
                    code=HTTPStatus.BAD_REQUEST,
                )
        else:
            search_equals = True
            raw_param = param
            if "not_{}".format(raw_param) in search_filters:
                raise api_functions_exceptions.APISearchFilterError(
                    "Search filter parameter '{}': "
                    "equal and not-equal search filters cannot be combined".format(
                        raw_param
                    ),
                    success=False,
                    code=HTTPStatus.BAD_REQUEST,
                )

        if raw_param == "tag":
            filters = []
            for value in values:
                if search_equals:
                    filters.append(
                        Alert.tags.any(
                            sqlalchemy.sql.expression.and_(
                                Tag.name.in_([value]),
                                Tag.team_id.in_(search_filters.getlist("team_id")),
                            )
                        )
                    )
                else:
                    filters.append(
                        ~Alert.tags.any(
                            sqlalchemy.sql.expression.and_(
                                Tag.name.in_([value]),
                                Tag.team_id.in_(search_filters.getlist("team_id")),
                            )
                        )
                    )
            query = query.filter(sqlalchemy.or_(*filters))
            continue

        if raw_param not in handler.schema.fields:
            raise api_functions_exceptions.APISearchFilterError(
                "Unsupported search filter parameter '{}'".format(param),
                success=False,
                code=HTTPStatus.BAD_REQUEST,
            )

        field = handler.schema.fields[raw_param]

        # Iterate over all values, and store their respective filters.
        filters = []
        for value in values:

            if raw_param == "body":
                filters.append(
                    notifications.Model.__table__.columns[raw_param].ilike(
                        field.deserialize(value)
                    )
                )
                continue

            if field.metadata.get("json", None):
                if search_equals:
                    filters.append(
                        notifications.in_json(raw_param, field.deserialize(value))
                    )
                else:
                    filters.append(
                        notifications.not_in_json(raw_param, field.deserialize(value))
                    )
            else:
                if search_equals:
                    filters.append(
                        notifications.Model.__table__.columns[raw_param]
                        == field.deserialize(value)
                    )
                else:
                    filters.append(
                        notifications.Model.__table__.columns[raw_param]
                        != field.deserialize(value)
                    )

        # Generate a simple filter in the case of a single value.
        if len(filters) == 1:
            query = query.filter(filters[0])
        # Generate a complex filter in the case of multiple values,
        # which tests if it is any one of the given values (boolean OR).
        elif len(filters) > 1:
            query = query.filter(sqlalchemy.or_(*filters))
        # Of course, do nothing for len(filters) < 1.

    # Show notifications escalated from other teams
    if show_escalations:
        team_id = search_filters["team_id"]

        query = query.outerjoin(Escalation).filter(
            or_(
                notifications.Model.team_id == team_id,
                and_(
                    notifications.Model.team_id.in_(
                        db.session.query(Team.id).filter(
                            Team.escalates_to_id == team_id
                        )
                    ),
                    Escalation.escalated_to_id == team_id,
                    Escalation.notif_id == notifications.Model.id,
                ),
            )
        )

    return query.order_by(notifications.Model.received_dt.desc()).all()
