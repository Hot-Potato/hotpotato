"""
API function exception classes.
"""


from hotpotato.api import exceptions as api_exceptions


class APIFunctionError(api_exceptions.APIError):
    """
    API function exception base class.
    """

    pass


class APIDataValueError(APIFunctionError):
    """
    API data value exception.
    """

    pass


class APISearchFilterError(APIFunctionError):
    """
    API search filter exception.
    """

    pass


class APIDataUnprocessableError(APIDataValueError):
    """
    API invalid input data exception.
    """

    def __init__(self, message, success, code, error):
        self.error = error
        super().__init__(message, success, code)
