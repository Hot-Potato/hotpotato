"""
API helper functions.
"""


import csv
import io

import flask
import werkzeug.datastructures
from webargs import flaskparser

from hotpotato.api.functions import exceptions

_parser = flaskparser.FlaskParser()


def data_get(schema):
    """
    Return a MultiDict of API GET/POST data from a request.
    This tries to automatically detect the type of data.
    """

    return werkzeug.datastructures.MultiDict(
        mapping=_parser.parse(schema, flask.request, location="json_or_form").items()
    )


@_parser.error_handler
def handle_error(error, req, schema, error_status_code, error_headers):
    raise exceptions.APIDataUnprocessableError(
        "Request is not valid JSON for this endpoint", False, 422, error
    )


def json_response_get(json_obj, code=200, no_cache=False):
    """
    Return a JSON request response.
    """

    response = flask.make_response(flask.jsonify(json_obj), code)

    if no_cache:
        # For HTTP/1.1 clients
        response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
        # For HTTP/1.0 clients (curl)
        response.headers["Pragma"] = "no-cache"
        # For HTTP/1.0 proxies
        response.headers["Expires"] = "0"

    return response


def csv_response_get(columns, rows, code=200):
    """
    Return a CSV request response.
    """

    string_io = io.StringIO()
    csv_writer = csv.writer(string_io)

    csv_writer.writerow(columns)
    for row in rows:
        csv_writer.writerow(row)

    response = flask.make_response(string_io.getvalue(), code)
    response.headers["Content-Disposition"] = "attachment; filename=export.csv"
    response.headers["Content-Type"] = "text/csv"

    return response
