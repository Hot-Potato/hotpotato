"""
Server API version 1 endpoints for Modica callback functions.
"""


from hotpotato.api.server import modica
from hotpotato.api.server.v1._blueprint import blueprint


@blueprint.route("/modica/mo-message", methods=["POST"])
def modica_mo_message():
    """
    Handle incoming SMS messages from Modica.
    """

    return modica.mo_message()


@blueprint.route("/modica/dlr-status", methods=["POST"])
def modica_dlr_status():
    """
    Handle delivery status receipts from Modica.
    """

    return modica.notificationstatus()
