"""
Server API version 1 endpoint for creating heartbeats.
"""

from http import HTTPStatus

import flask
from flask import abort, current_app

from hotpotato import heartbeats, servers
from hotpotato.api.server.v1._blueprint import blueprint


@blueprint.route("/heartbeat", methods=["POST"])
def heartbeat():
    """
    Create a heartbeat for a server.
    """

    request = flask.request

    try:
        server = servers.get_by_api_key(request.form["apikey"])
        heartbeats.create(server.id)
        return flask.make_response("I'm not dead yet", HTTPStatus.OK)

    except servers.ServerAPIKeyError as err:
        current_app.logger.error(err)
        return abort(HTTPStatus.UNAUTHORIZED)
    except KeyError as err:
        current_app.logger.error(err)
        return abort(HTTPStatus.BAD_REQUEST)
