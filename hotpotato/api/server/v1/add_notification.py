"""
Server API version 1 endpoint for adding alerts.
"""


from http import HTTPStatus

import flask
from flask import abort, current_app
from marshmallow import EXCLUDE, Schema, fields

from hotpotato import servers, teams, util
from hotpotato.api import functions as api_functions
from hotpotato.api.functions import exceptions as api_functions_exceptions
from hotpotato.api.server.v1._blueprint import blueprint
from hotpotato.notifications import alerts


class APIDateTime(fields.Field):
    """
    Field that deserializes from old not fully iso8601 compliant hot potato timestamps
    """

    default_error_messages = {"invalid": "Not a valid timestamp."}

    def _deserialize(self, value, attr, data, **kwargs):
        try:
            return util.datetime_get_from_string(value, tz_space_sep=True)
        except ValueError as error:
            raise self.make_error("invalid", input=value) from error


class AlertDataSchema(Schema):
    apikey = fields.Str()
    id = fields.Int()
    team_id = fields.Int()
    hostname = fields.Str()
    service_name = fields.Str(data_key="servicename")
    trouble_code = fields.Str(data_key="troublecode")
    display_name = fields.Str(data_key="displayname")
    output = fields.Str()
    state = fields.Str()
    timestamp = APIDateTime(data_key="icingadatetime")

    class Meta:
        strict = True
        unknown = EXCLUDE


# pylint: disable=inconsistent-return-statements
@blueprint.route("/add/notification", methods=["POST"])
def add_notification():
    """
    Create an alert, and send it to the on-call person.
    """

    try:
        data = api_functions.data_get(schema=AlertDataSchema())
        server = servers.get_by_api_key(data["apikey"])
        if "team_id" in data:
            team = teams.get(data["team_id"])
        else:
            current_app.logger.warning(
                "Deprecated: Attempt to send alert with no team from {} ({})".format(
                    server.hostname, server.id
                )
            )
            team = teams.hotpotato_team()
        user = team.on_call

        current_app.logger.debug(
            "The person currently on-call is: {0} ({1})".format(
                user.name if user else "no one", user.id if user else None
            )
        )

        # Generate the alert and put it on the message queue.
        ale = alerts.create(
            team_id=team.id,
            alert_type="service"
            if "service_name" in data and data["service_name"]
            else "host",
            server=server,
            trouble_code=data["trouble_code"],
            hostname=data["hostname"],
            display_name=data["display_name"],
            service_name=data["service_name"] if "service_name" in data else None,
            state=data["state"],
            output=data["output"],
            timestamp=util.datetime_process(
                data["timestamp"],
                default_tz=server.timezone_get_as_timezone(),
                to_utc=True,
                naive=True,
            ),
        )
        if user:
            ale.send(user_id=user.id)

        return flask.make_response(str(ale.id), HTTPStatus.CREATED)

    except (KeyError, api_functions_exceptions.APIDataValueError) as err:
        current_app.logger.error("Error receiving notification (v1): {}".format(err))
        return abort(HTTPStatus.BAD_REQUEST)

    except servers.ServerAPIKeyError as err:
        current_app.logger.error("Error receiving notification (v1): {}".format(err))
        return abort(HTTPStatus.FORBIDDEN)
