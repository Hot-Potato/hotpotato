"""
Server API endpoints for Twilio callbacks.
"""


import flask
from flask import current_app
from marshmallow import EXCLUDE, Schema, fields
from webargs.flaskparser import parser

from hotpotato import models
from hotpotato.notifications import attempts, exceptions as notifications_exceptions

# TODO: Handle incoming messages in Twilio
#       https://www.twilio.com/docs/sms/tutorials/how-to-receive-and-reply-python


class MessageStatusError(Exception):
    """
    Message status exception class.
    """

    pass


class TwilioCallbackSchema(Schema):
    MessageSid = fields.Str(required=True)
    MessageStatus = fields.Str(required=True)
    ErrorCode = fields.Str()
    ErrorMessage = fields.Str()

    class Meta:
        unknown = EXCLUDE


def notificationstatus():
    """
    Handle delivery status receipts from Twilio.
    """

    json_obj = parser.parse(
        TwilioCallbackSchema, flask.request, location="json_or_form"
    )

    provider_notif_id = json_obj["MessageSid"]
    provider_notif_status = json_obj["MessageStatus"]

    current_app.logger.debug("Received delivery receipt from Twilio:")
    current_app.logger.debug("- provider_notif_id = {}".format(provider_notif_id))
    current_app.logger.debug(
        "- provider_notif_status = {}".format(provider_notif_status)
    )

    # Try to find the notification associated with the provider notification ID.
    try:
        attempt = attempts.get_by_provider_notif_id("twilio", provider_notif_id)
    except notifications_exceptions.NotificationProviderNotificationIDError as err:
        err_message = "While receiving message status from Twilio: {}".format(str(err))
        current_app.logger.error(err_message)
        return (err_message, 400)

    current_app.logger.debug("Found associated attempt with ID: {}".format(attempt.id))

    # Modify the status of the message based on the information in the delivery receipt.
    if (
        provider_notif_status == "accepted"
        or provider_notif_status == "queued"
        or provider_notif_status == "sending"
    ):
        attempt.status = "RECEIVED_BY_PROVIDER"
    elif provider_notif_status == "sent":
        attempt.status = "SENT_TO_CLIENT"
    elif provider_notif_status == "delivered":
        attempt.status = "RECEIVED_BY_CLIENT"
    elif provider_notif_status == "undelivered" or provider_notif_status == "failed":
        attempt.status = "SEND_FAILED"
        attempt.status.append(
            "Twilio: Error {}: {}".format(
                json_obj.get("ErrorCode", None), json_obj.get("ErrorMessage", None)
            )
        )
    else:
        body = "Unknown message status value received from Twilio: {}".format(
            provider_notif_status
        )
        attempt.errors.append(body)
        raise MessageStatusError(body)

    current_app.logger.debug(
        "Changed status of attempt with ID {} to {}".format(attempt.id, attempt.status)
    )

    if attempt.status == "SEND_FAILED":
        current_app.logger.error(
            "Failed to send notification with ID {}: {}".format(
                attempt.id, attempt.errors[-1]
            )
        )

    models.db.session.commit()

    return ("", 204)
