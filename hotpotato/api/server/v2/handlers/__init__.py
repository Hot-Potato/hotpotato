"""
Server API version 2 notification handler functions.
"""


from http import HTTPStatus

from hotpotato.api.server.v2 import exceptions as api_server_exceptions
from hotpotato.api.server.v2.handlers import alerts as handler_alerts

_notif_handlers = {"alerts": handler_alerts}


def get(handler_name):
    """
    Return the notification handler for the given handler name.
    """

    if handler_name not in _notif_handlers:
        raise api_server_exceptions.APIServerV2HandlerError(
            "Invalid notification handler type: "
            "expected one of {} but got '{}'".format(
                _notif_handlers.keys(), handler_name
            ),
            success=False,
            code=HTTPStatus.BAD_REQUEST,
        )

    return _notif_handlers[handler_name]
