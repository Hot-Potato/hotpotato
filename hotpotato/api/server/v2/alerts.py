"""
Server API version 2 endpoints for alerts.
"""


from http import HTTPStatus

from flask import current_app
from marshmallow import (
    EXCLUDE,
    Schema,
    ValidationError,
    fields,
    validate,
    validates_schema,
)

from hotpotato import teams, util
from hotpotato.api import functions as api_functions
from hotpotato.api.functions import notifications as api_functions_notifications
from hotpotato.api.server.v2 import (
    decorators,
    functions as api_server_functions,
    handlers,
)
from hotpotato.api.server.v2._blueprint import blueprint
from hotpotato.notifications import alerts, exceptions as notifications_exceptions


@blueprint.route("/alerts/get/<ale_id>", methods=["GET"])
@decorators.auth_required
def alerts_get(ale_id):
    """
    Get an alert, and return it as a JSON object.
    """

    try:
        handler = handlers.get("alerts")
        ale = alerts.get(ale_id)
        return api_server_functions.api_json_response_get(
            data=api_functions_notifications.as_dict(handler, ale)
        )

    except notifications_exceptions.NotificationIDError as err:
        return api_server_functions.api_json_response_get(
            success=False, code=HTTPStatus.NOT_FOUND, message=str(err)
        )


class AlertDataSchema(Schema):
    alert_type = fields.Str(required=True, validate=validate.OneOf(alerts.TYPE))
    trouble_code = fields.Str(required=True)
    hostname = fields.Str(required=True)
    display_name = fields.Str(required=True)
    service_name = fields.Str()
    state = fields.Str(required=True)
    output = fields.Str(required=True)
    timestamp = fields.DateTime(required=True, format="iso")
    team_id = fields.Int(required=True)

    @validates_schema
    def validate_service_name(self, data, **kwargs):
        if "alert_type" not in data:
            return
        if data["alert_type"] == "service" and data.get("service_name", "") == "":
            raise ValidationError(
                "service_name must be provided if alert_type is 'service'",
                "service_name",
            )

    @validates_schema
    def validate_team_id(self, data, **kwargs):
        if "team_id" not in data:
            return
        try:
            teams.get(data["team_id"])
        except teams.TeamIDError:
            raise ValidationError(
                "Team with id {} not found".format(data["team_id"]), "team_id"
            )

    class Meta:
        strict = True
        unknown = EXCLUDE


@blueprint.route("/alerts/send", methods=["POST"])
@decorators.auth_required
def alerts_send():
    """
    Create an alert, and send it to the on-call person.
    """
    handler = handlers.get("alerts")

    data = api_functions.data_get(schema=AlertDataSchema())

    server = api_server_functions.server_get_from_auth()

    team = teams.get(data["team_id"])
    user = team.on_call

    current_app.logger.debug(
        "The person currently on-call for {} is: {} ({})".format(
            team.name, user.name if user else "no one", user.id if user else None
        )
    )

    if data["alert_type"] == "service":
        service_name = data["service_name"]
    else:
        service_name = None

    # Generate the notification and put it on the message queue.
    # TODO: tickets
    # TODO: events
    ale = alerts.create(
        team_id=data["team_id"],
        alert_type=data["alert_type"],
        server=server,
        trouble_code=data["trouble_code"],
        hostname=data["hostname"],
        display_name=data["display_name"],
        service_name=service_name,
        state=data["state"],
        output=data["output"],
        timestamp=util.datetime_process(
            data["timestamp"],
            default_tz=server.timezone_get_as_timezone(),
            to_utc=True,
            naive=True,
        ),
    )
    if user:
        ale.send(user_id=user.id)

    return api_server_functions.api_json_response_get(
        code=HTTPStatus.CREATED, data=api_functions_notifications.as_dict(handler, ale)
    )
