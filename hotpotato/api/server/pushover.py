"""
Server API endpoints for Pushover callbacks.
"""


import flask
from flask import current_app
from marshmallow import EXCLUDE, Schema, fields
from webargs.flaskparser import parser

from hotpotato import models
from hotpotato.notifications import attempts, exceptions as notifications_exceptions


class ProviderNotificationStatusError(Exception):
    """
    Message status exception class.
    """

    pass


class PushoverCallbackSchema(Schema):
    receipt = fields.Str(required=True)
    acknowledged = fields.Int(required=True)

    class Meta:
        unknown = EXCLUDE


def notificationstatus():
    """
    Handle delivery status receipts from Pushover.
    """

    json_obj = parser.parse(PushoverCallbackSchema, flask.request, location="form")

    provider_notif_id = json_obj["receipt"]
    provider_notif_status = json_obj["acknowledged"]

    current_app.logger.debug("Received acknowledgement receipt from Pushover:")
    current_app.logger.debug("- provider_notif_id = {}".format(provider_notif_id))
    current_app.logger.debug(
        "- provider_notif_status = {}".format(provider_notif_status)
    )

    # Try to find the notification associated with the provider notification ID.
    try:
        attempt = attempts.get_by_provider_notif_id("pushover", provider_notif_id)
    except notifications_exceptions.NotificationProviderNotificationIDError as err:
        err_message = "While receiving message status from Pushover: {}".format(
            str(err)
        )
        current_app.logger.error(err_message)
        return (err_message, 400)

    current_app.logger.debug("Found associated attempt with ID: {}".format(attempt.id))

    # Modify the status of the message based on the information in the acknowledgement receipt.
    if provider_notif_status == 1:
        attempt.status = "READ_BY_CLIENT"
    else:
        body = "Unknown message status value received from Pushover: {}".format(
            provider_notif_status
        )
        attempt.errors.append(body)
        raise ProviderNotificationStatusError(body)

    current_app.logger.debug(
        "Changed status of attempt with ID {} to {}".format(attempt.id, attempt.status)
    )

    if attempt.status == "SEND_FAILED":
        current_app.logger.error(
            "Failed to send attempt with ID {}: {}".format(
                attempt.id, attempt.errors[-1]
            )
        )

    models.db.session.commit()

    return ("", 204)
