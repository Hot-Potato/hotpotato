from flask import jsonify, request

from hotpotato import rules
from hotpotato.api.web.v1._blueprint import blueprint
from hotpotato.users import search


@blueprint.route("/users/get", methods=["GET"])
@rules.access_control_list(
    blueprint, rules.PermissionRule("teams.user_add"), context=rules.contexts.team
)
def users_get():
    users = search(
        request.args.get("query"), not_in_team_id=request.args.get("__team_id")
    )
    return jsonify(
        [
            {"id": str(user.id), "name": user.name, "last_login_at": user.last_login_at}
            for user in users
        ]
    )
