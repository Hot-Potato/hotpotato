"""
Web API version 1 exception classes.
"""


from hotpotato.api import exceptions as api_exceptions


class APIWebV1Error(api_exceptions.APIError):
    """
    Web API version 1 exception base class.
    """

    pass


class APIWebV1ResponseError(APIWebV1Error):
    """
    Web API version 1 response exception.
    """

    pass
