"""
Web API version 1 handover notification handler.
"""


from marshmallow import fields

from hotpotato.api.web.v1.notifications.handlers import (
    notifications as handler_notifications,
)
from hotpotato.notifications.handovers import NOTIF_TYPE, get

__all__ = ["NOTIF_TYPE", "get", "schema"]


class HandoverSchema(handler_notifications.NotificationSchema):
    to_user_id = fields.Int(required=True, json=True)
    from_user_id = fields.Int(required=True, json=True)
    message = fields.Str(required=True, json=True)


schema = HandoverSchema()
