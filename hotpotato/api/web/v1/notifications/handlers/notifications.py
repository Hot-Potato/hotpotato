"""
Web API version 1 notification handler.
"""


from marshmallow import Schema, fields

from hotpotato import util
from hotpotato.notifications.notifications import NOTIF_TYPE, get

__all__ = ["NOTIF_TYPE", "get", "schema"]


class UserDateTime(fields.DateTime):
    def _serialize(self, value, attr, obj, **kwargs):
        if value is None:
            return None
        data_format = self.format or self.DEFAULT_FORMAT
        format_func = self.SERIALIZATION_FUNCS.get(data_format)
        if format_func:
            try:
                return format_func(
                    util.datetime_process(value, as_utc=True, to_user_tz=True)
                )
            except (AttributeError, ValueError) as error:
                raise self.make_error(
                    "format", input=value, obj_type=self.OBJ_TYPE
                ) from error
        else:
            return value.strftime(self.dateformat)


class NotificationSchema(Schema):
    id = fields.Int(required=True)
    team_id = fields.Int(required=True)
    received_dt = UserDateTime(required=True, format="iso")
    notif_type = fields.Str(required=True)
    body = fields.Function(required=True, serialize=lambda x: x.body())
    version = fields.Int(required=True, json=True)
    node_name = fields.Str(required=True, json=True)
    event_id = fields.Int(required=True, json=True)
    ticket_id = fields.Int(required=True, json=True)
    warnings = fields.List(fields.Str(), json=True)
    errors = fields.List(fields.Str(), required=True, json=True)
    status = fields.Str(required=True, json=True)
    method = fields.Str(required=True, json=True)
    provider = fields.Str(required=True, json=True)
    provider_notif_id = fields.Str(required=True, json=True)

    def get_attribute(self, obj, attr, default):
        if "json" in self.fields[attr].metadata and self.fields[attr].metadata["json"]:
            return super().get_attribute(obj, "json.{}".format(attr), default)
        return super().get_attribute(obj, attr, default)

    class Meta:
        strict = True
        ordered = True


schema = NotificationSchema()
