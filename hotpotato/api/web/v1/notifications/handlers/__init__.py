"""
Web API version 1 notification handler functions.
"""


from http import HTTPStatus

from flask import abort

from hotpotato.api.web.v1.notifications.handlers import (
    alerts as handler_alerts,
    handovers as handler_handovers,
    messages as handler_messages,
    notifications as handler_notifications,
)

_notif_handlers = {
    "notifications": handler_notifications,
    "alerts": handler_alerts,
    "handovers": handler_handovers,
    "messages": handler_messages,
}


def get(handler_name):
    """
    Return the notification handler for the given handler name.
    """

    if handler_name not in _notif_handlers:
        abort(HTTPStatus.NOT_FOUND)
    return _notif_handlers[handler_name]
