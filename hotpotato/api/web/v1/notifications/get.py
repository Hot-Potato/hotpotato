"""
Web API version 1 endpoints for getting notifications.
"""


from http import HTTPStatus

from marshmallow import Schema, ValidationError, fields, validates

from hotpotato import rules
from hotpotato.api import functions as api_functions
from hotpotato.api.functions import notifications as api_functions_notifications
from hotpotato.api.web.v1 import functions as api_web_functions
from hotpotato.api.web.v1._blueprint import blueprint
from hotpotato.api.web.v1.notifications import handlers as notifications_handlers
from hotpotato.models import Tag, db
from hotpotato.notifications import alerts, exceptions as notifications_exceptions
from hotpotato.teams import current_team


@blueprint.route("/notifications/get", methods=["GET"])
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("notifications.get"),
    context=rules.contexts.team_and_escalates_to,
)
def notifications_get():
    """
    Return a dictionary of notifications, found using the given search parameters.
    """

    return _notifications_get()


@blueprint.route("/notifications/<handler_name>/get", methods=["GET"])
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("notifications.get"),
    context=rules.contexts.team_and_escalates_to,
)
def notifications_get_for_type(handler_name):
    """
    Return a dictionary of notifications of a given type, found using
    the given search parameters.
    """

    return _notifications_get(handler_name=handler_name)


@blueprint.route("/notifications/get/as_csv", methods=["GET"])
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("notifications.get"),
    context=rules.contexts.team_and_escalates_to,
)
def notifications_get_as_csv():
    """
    Return a CSV document of notifications, found using the given search parameters.
    """

    return _notifications_get_as_csv()


@blueprint.route("/notifications/<handler_name>/get/as_csv", methods=["GET"])
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("notifications.get"),
    context=rules.contexts.team_and_escalates_to,
)
def notifications_get_for_type_as_csv(handler_name):
    """
    Return a CSV document of notifications of a given type, found using
    the given search parameters.
    """

    return _notifications_get_as_csv(handler_name=handler_name)


@blueprint.route("/notifications/get/<notif_id>", methods=["GET"])
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("notifications.get"),
    context=rules.contexts.team_and_escalates_to,
)
def notifications_get_by_id(notif_id):
    """
    Return the notification with the given ID in a JSON object.
    """

    return _notifications_get_by_id(notif_id)


@blueprint.route("/notifications/get/<notif_id>/as_csv", methods=["GET"])
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("notifications.get"),
    context=rules.contexts.team_and_escalates_to,
)
def notifications_get_by_id_as_csv(notif_id):
    """
    Return the notification with the given ID in a CSV document.
    """

    return _notifications_get_by_id_as_csv(notif_id)


@blueprint.route("/notifications/<handler_name>/get/<notif_id>", methods=["GET"])
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("notifications.get"),
    context=rules.contexts.team_and_escalates_to,
)
def notifications_get_for_type_by_id(handler_name, notif_id):
    """
    Return the notification of the given type, with the given ID in a JSON object.
    """

    return _notifications_get_by_id(notif_id, handler_name=handler_name)


@blueprint.route("/notifications/<handler_name>/get/<notif_id>/as_csv", methods=["GET"])
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("notifications.get"),
    context=rules.contexts.team_and_escalates_to,
)
def notifications_get_for_type_by_id_as_csv(handler_name, notif_id):
    """
    Return the notification of the given type, with the given ID in a CSV document.
    """

    return _notifications_get_by_id_as_csv(notif_id, handler_name=handler_name)


class TagIDSchema(Schema):
    id = fields.Int(required=True)

    @validates("id")
    def validate_id(self, tag_id):
        if not Tag.query.filter_by(id=tag_id, team_id=current_team.id).first():
            raise ValidationError("Tag does not exist")

    class Meta:
        strict = True


@blueprint.route("/notifications/alerts/<int:ale_id>/add_tag", methods=["POST"])
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("notifications.get"),
    context=rules.contexts.team_and_escalates_to,
)
def notifications_alerts_add_tag(ale_id):
    """
    Adds a tag to the provided alert. If the notification already has this tag it will
    return an error.
    """
    try:
        ale = alerts.get(ale_id, team_id=current_team.id)
        data = api_functions.data_get(schema=TagIDSchema())
        tag = Tag.query.get(data["id"])

        if tag in ale.tags:
            return api_web_functions.api_json_response_get(
                success=False,
                code=HTTPStatus.UNPROCESSABLE_ENTITY,
                message="Tag already on alert",
            )

        ale.tags.append(tag)

        db.session.add(ale)
        db.session.commit()

        return api_web_functions.api_json_response_get()

    except notifications_exceptions.NotificationIDError as err:
        return api_web_functions.api_json_response_get(
            success=False, code=HTTPStatus.NOT_FOUND, message=str(err)
        )


@blueprint.route("/notifications/alerts/<int:ale_id>/remove_tag", methods=["POST"])
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("notifications.get"),
    context=rules.contexts.team_and_escalates_to,
)
def notifications_alerts_remove_tag(ale_id):
    """
    Removes the tag from the provided alert.
    """
    try:
        ale = alerts.get(ale_id, team_id=current_team.id)
        data = api_functions.data_get(schema=TagIDSchema())
        tag = Tag.query.get(data["id"])

        if tag in ale.tags:
            ale.tags.remove(tag)
            db.session.add(ale)
            db.session.commit()
            return api_web_functions.api_json_response_get()

        return api_web_functions.api_json_response_get(
            success=False,
            code=HTTPStatus.UNPROCESSABLE_ENTITY,
            message="Tag not found on alert",
        )

    except notifications_exceptions.NotificationIDError as err:
        return api_web_functions.api_json_response_get(
            success=False, code=HTTPStatus.NOT_FOUND, message=str(err)
        )


#
# API endpoint subroutines.
#


def _notifications_get(handler_name="notifications"):
    """
    Return a dictionary of notifications of a given type, found using
    the given search parameters.
    """

    handler = notifications_handlers.get(handler_name)
    search_filters = api_functions_notifications.search_filters_get(handler=handler)
    notifs = api_functions_notifications.get_from_search_filters(
        handler, search_filters
    )

    return api_web_functions.api_json_response_get(
        data={
            notif.id: api_functions_notifications.as_dict(handler, notif)
            for notif in notifs
        }
    )


def _notifications_get_as_csv(handler_name="notifications"):
    """
    Return a list of notifications in CSV format.
    """

    handler = notifications_handlers.get(handler_name)
    search_filters = api_functions_notifications.search_filters_get(handler=handler)
    notifs = api_functions_notifications.get_from_search_filters(
        handler, search_filters
    )

    return api_functions.csv_response_get(
        *api_functions_notifications.as_csv(handler, notifs)
    )


def _notifications_get_by_id(notif_id, handler_name="notifications"):
    """
    Get a notification using the given ID.
    """

    try:
        handler = notifications_handlers.get(handler_name)
        notif = api_functions_notifications.get(
            handler, notif_id, team_id=current_team.id
        )

        return api_web_functions.api_json_response_get(
            data=api_functions_notifications.as_dict(handler, notif)
        )

    except notifications_exceptions.NotificationIDError as err:
        return api_web_functions.api_json_response_get(
            success=False, code=HTTPStatus.NOT_FOUND, message=str(err)
        )


def _notifications_get_by_id_as_csv(notif_id, handler_name="notifications"):
    """
    Get a notification using the given ID, and return it in CSV format.
    """

    try:
        handler = notifications_handlers.get(handler_name)
        notifs = (
            api_functions_notifications.get(handler, notif_id, team_id=current_team.id),
        )

        return api_functions.csv_response_get(
            *api_functions_notifications.as_csv(handler, notifs)
        )

    except notifications_exceptions.NotificationIDError as err:
        return api_web_functions.api_json_response_get(
            success=False, code=HTTPStatus.NOT_FOUND, message=str(err)
        )
