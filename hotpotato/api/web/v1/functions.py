"""
Web API version 1 helper functions.
"""


import copy
from http import HTTPStatus

from hotpotato.api import functions as api_functions
from hotpotato.api.web.v1 import exceptions as api_web_exceptions


def api_json_response_get(
    message=None, data=None, success=True, code=HTTPStatus.OK, no_cache=False
):
    """
    Return a Web API version 1 standard JSON response.
    """

    if not isinstance(success, bool):
        raise api_web_exceptions.APIWebV1ResponseError(
            "Invalid value for success: got {}, expected bool".format(
                type(success).__name__
            ),
            success=False,
            code=HTTPStatus.INTERNAL_SERVER_ERROR,
        )
    if not isinstance(code, int):
        raise api_web_exceptions.APIWebV1ResponseError(
            "Invalid value for code: got {}, expected int".format(type(code).__name__),
            success=False,
            code=HTTPStatus.INTERNAL_SERVER_ERROR,
        )
    if message and not isinstance(message, str):
        raise api_web_exceptions.APIWebV1ResponseError(
            "Invalid value for message: got {}, expected str".format(
                type(message).__name__
            ),
            success=False,
            code=HTTPStatus.INTERNAL_SERVER_ERROR,
        )
    if data and not isinstance(data, dict):
        raise api_web_exceptions.APIWebV1ResponseError(
            "Invalid value for data: got {}, expected dict".format(type(data).__name__),
            success=False,
            code=HTTPStatus.INTERNAL_SERVER_ERROR,
        )

    return api_functions.json_response_get(
        {
            "success": success,
            "code": code,
            "message": message,
            "data": copy.deepcopy(data) if data else {},
        },
        code=code,
        no_cache=no_cache,
    )
