from hotpotato.models.database import db
from hotpotato.models.notifications.notifications import Notification
from hotpotato.models.users import User


class Handover(Notification):
    """
    Handover notification object class.
    """

    name = "handover"
    description = "handover notification"

    from_user = db.relationship(
        User,
        primaryjoin="foreign(Handover.json.op('->>')('from_user_id')).cast(Integer)==User.id",
        lazy="joined",
    )
    to_user = db.relationship(
        User,
        primaryjoin="foreign(Handover.json.op('->>')('to_user_id')).cast(Integer)==User.id",
        lazy="joined",
    )

    def body(self, **kwargs):
        from_user_name = self.from_user.name if self.json["from_user_id"] else "No one"
        to_user_name = self.to_user.name if self.json["to_user_id"] else "No one"
        if from_user_name and self.json["message"]:
            return "{} is now on call, taking over from {} with the following message: {}".format(
                to_user_name, from_user_name, self.json["message"]
            )
        elif from_user_name:
            return "{} is now on call, taking over from {}.".format(
                to_user_name, from_user_name
            )
        elif self.json["message"]:
            return "{} is now on call with the following message: {}".format(
                to_user_name, self.json["message"]
            )
        else:
            return "{} is now on call.".format(to_user_name)

    # pylint: disable=arguments-differ
    def send(self, user_id, run_async=True, contact=None, method=None):
        """
        Send the handover.
        """

        # The reason why this import is here and not at the top of the file is
        # because when the actors get defined, they connect to the message queue broker
        # to register themselves as jobs to be run.
        # Therefore, the message queue broker needs to already be running BEFORE
        # the actors get imported/defined.
        from hotpotato.notifications.queue.actors import handover as actor

        super().send(
            actor, user_id, run_async=run_async, contact=contact, method=method
        )

    __mapper_args__ = {"polymorphic_identity": "handover"}
