import json

from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.ext.mutable import MutableDict

from hotpotato import notifications, util
from hotpotato.models.database import db
from hotpotato.models.teams import Team
from hotpotato.models.users import User


class Notification(db.Model):
    """
    Stores a log of notifications sent through and received by Hot Potato.
    """

    __tablename__ = "notification_log"

    __table_args__ = (db.Index("received_dt__team_id__idx", "team_id", "received_dt"),)

    id = db.Column(db.Integer, primary_key=True)
    received_dt = db.Column(db.DateTime, nullable=False)
    notif_type = db.Column(db.Enum(*notifications.TYPE), nullable=False)
    # The built-in JSON types do not detect in-place modifications, which are used
    # extensively. In order for this to work, the MutableDict type, part of
    # the sqlalchemy.ext.mutable module, is required.
    json = db.Column(MutableDict.as_mutable(db.JSON), nullable=False)

    team_id = db.Column(db.Integer, db.ForeignKey(Team.id, ondelete="SET NULL"))
    team = db.relationship("Team")

    def body(self, **kwargs):
        raise NotImplementedError(
            "{}.body has no concrete implementation".format(type(self).__name__)
        )

    __mapper_args__ = {
        "polymorphic_on": notif_type,
        "polymorphic_identity": "notification",
        "with_polymorphic": "*",
    }

    """
    Notification object class.
    """

    name = "notification"
    description = "notification"

    def received_dt_get_as_string(self):
        """
        Return this notification's received_dt field as a string in ISO 8601 format.
        """

        return util.datetime_get_as_string(self.received_dt)

    def received_dt_set_from_string(self, datetime_string):
        """
        Set this notification's received_dt field to the given string in ISO 8601 format.
        """

        self.received_dt = util.datetime_get_from_string(datetime_string)

    def as_json(self, user=None, **kwargs):
        """
        Express this notification as a JSON string.
        """

        return json.dumps(
            {
                "id": self.id,
                "received_dt": self.received_dt_get_as_string(),
                "notif_type": self.notif_type,
                "body": self.body(user=user),
                "json": self.json,
                "attempts": [attempt.as_dict() for attempt in self.send_attempts],
            },
            **kwargs,
        )

    def failed_to_send(self):
        """
        Returns True if this notification failed to send.
        """

        return any(attempt.failed_to_send() for attempt in self.send_attempts)

    def send(
        self,
        actor,
        user_id,
        run_async=True,
        contact=None,
        method=None,
        escalating=False,
    ):
        """
        Send the notification.
        """

        contact_id = contact.id if contact else None

        if run_async:
            actor.send(
                self.id,
                user_id,
                contact_id=contact_id,
                method=method,
                escalating=escalating,
            )
        else:
            actor(
                self.id,
                user_id,
                contact_id=contact_id,
                method=method,
                escalating=escalating,
            )


class NotificationAttempt(db.Model):
    """
    Each row in this table is an attempt to send a notification to a particular
    contact.
    """

    id = db.Column(db.Integer(), primary_key=True)

    notification_id = db.Column(
        db.Integer(), db.ForeignKey(Notification.id, ondelete="CASCADE"), nullable=False
    )
    notification = db.relationship(
        Notification, backref=db.backref("send_attempts", lazy="joined"), lazy="joined"
    )

    user_id = db.Column(
        db.Integer(), db.ForeignKey(User.id, ondelete="SET NULL"), nullable=True
    )
    user = db.relationship(User, lazy="joined")

    status = db.Column(db.Enum(*list(notifications.STATUS)), nullable=False)

    warnings = db.Column(ARRAY(db.Text), nullable=False)
    errors = db.Column(ARRAY(db.Text), nullable=False)

    method = db.Column(db.Enum(*list(notifications.METHOD)))
    provider = db.Column(db.Enum(*list(notifications.PROVIDER)))
    provider_notif_id = db.Column(db.Text())

    def is_unsent(self):
        """
        Returns True if this notification has not yet been sent.
        """

        return self.status in ("NEW", "UNSENT")

    def failed_to_send(self):
        """
        Returns True if this notification failed to send.
        """

        return self.status in ("SEND_FAILED", "SEND_FAILED_ACKNOWLEDGED")

    def send_failed_acknowledged(self):
        """
        Returns True if the notification failed to send and was acknowledged.
        """

        return self.status == "SEND_FAILED_ACKNOWLEDGED"

    def is_sending(self):
        """
        Returns True if this notification has been sent, or is currently sending.
        """

        return True if self.status == "SENDING" else self.has_been_sent()

    def has_been_sent(self):
        """
        Returns True if this notification has been sent to its contact method provider.
        """

        return self.status in (
            "SENT_TO_PROVIDER",
            "RECEIVED_BY_PROVIDER",
            "SENT_TO_CLIENT",
            "RECEIVED_BY_CLIENT",
            "READ_BY_CLIENT",
        )

    def has_been_received_by_client(self):
        """
        Returns True if this notification has been received by the client.
        """

        return self.status in ("RECEIVED_BY_CLIENT", "READ_BY_CLIENT")

    def as_dict(self):
        return {
            "id": self.id,
            "user_id": self.user_id,
            "status": self.status,
            "warnings": self.warnings,
            "errors": self.errors,
            "method": self.method,
            "provider": self.provider,
            "provider_notif_id": self.provider_notif_id,
        }


class Escalation(db.Model):
    """
    If a notification was escalated a row is created here so it can be indicated
    in UI, used for reporting, etc.

    In future this might store information about *why* a notification was escalated.
    """

    notif_id = db.Column(db.Integer, db.ForeignKey(Notification.id), primary_key=True)
    notification = db.relationship(Notification, backref=db.backref("escalations"))

    escalated_to_id = db.Column(db.Integer, db.ForeignKey(Team.id), primary_key=True)
    escalated_to = db.relationship(Team)
