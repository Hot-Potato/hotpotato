from datetime import datetime

from hotpotato import util
from hotpotato.models.database import db
from hotpotato.models.notifications.notifications import Notification
from hotpotato.models.servers import Server
from hotpotato.models.tags import Tag, tag_notification
from hotpotato.models.users import User


class Alert(Notification):
    """
    Alert object class.
    """

    name = "alert"
    description = "alert"
    tags = db.relationship(Tag, secondary=tag_notification, backref="notifications")
    server = db.relationship(
        Server,
        primaryjoin="foreign(Alert.json.op('->>')('server_id')).cast(Integer)==Server.id",
        lazy="joined",
    )
    from_user = db.relationship(
        User,
        primaryjoin="foreign(Alert.json.op('->>')('from_user_id')).cast(Integer)==User.id",
        lazy="joined",
    )
    to_user = db.relationship(
        User,
        primaryjoin="foreign(Alert.json.op('->>')('to_user_id')).cast(Integer)==User.id",
        lazy="joined",
    )

    def body(self, user=None, **kwargs):
        # Generate the alert body using the JSON object data and user data.
        #
        # There are two different types of alerts:
        # * service - used for reporting the state of services on hosts
        # * host    - used for reporting the status of the hosts themselves
        #
        # Any other alert type is unsupported and will raise a ValueError.
        timestamp = util.datetime_get_from_string(self.json["timestamp"])
        user_ts_str = datetime.strftime(
            util.datetime_process(timestamp, as_utc=True, to_tz=user.pytz_timezone)
            if user
            else timestamp,
            (
                "%Y-%m-%d %H:%M:%S"
                if user and user.use_24hr
                else "%Y-%m-%d %-I:%M:%S %p"
            ),
        )

        if self.json["alert_type"] == "service":
            return (
                "{server.hostname}: {trouble_code} {hostname} {service_name} state is "
                "{state} ({output}) "
                "{user_timestamp}"
            ).format(**self.json, server=self.server, user_timestamp=user_ts_str)
        elif self.json["alert_type"] == "host":
            return (
                "{server.hostname}: {trouble_code} {hostname} state is "
                "{state} ({output}) "
                "{user_timestamp}"
            ).format(**self.json, server=self.server, user_timestamp=user_ts_str)
        else:
            raise ValueError(
                'Unexpected value "{}" for alert_type, '
                'must be either "service" or "host"'.format(self.json["alert_type"])
            )

    # pylint: disable=arguments-differ
    def send(
        self,
        user_id,
        run_async=True,
        withhold_warnings=True,
        contact=None,
        method=None,
        escalating=False,
    ):
        """
        Send the Alert.
        """

        # The reason why this import is here and not at the top of the file is
        # because when the actors get defined, they connect to the message queue broker
        # to register themselves as jobs to be run.
        # Therefore, the message queue broker needs to already be running BEFORE
        # the actors get imported/defined.
        from hotpotato.notifications.queue.actors import alert as actor

        # Do not actually send if it is just a warning, and withhold_warnings
        # is set to True.
        if withhold_warnings and self.json["state"] == "WARNING":
            self.json["status"] = "UNSENT"
            db.session.commit()
            return

        super().send(
            actor,
            user_id,
            run_async=run_async,
            contact=contact,
            method=method,
            escalating=escalating,
        )

    def timestamp_get_as_datetime(self):
        """
        Return the timestamp value from this alert as a DateTime object.
        """

        return util.datetime_get_from_string(self.json["timestamp"])

    def timestamp_set_from_datetime(self, timestamp):
        """
        Set the timestamp value of this alert from the given DateTime object.
        """

        self.json["timestamp"] = util.datetime_get_as_string(timestamp)

    __mapper_args__ = {"polymorphic_identity": "alert"}
