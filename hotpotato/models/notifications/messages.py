from hotpotato.models.database import db
from hotpotato.models.notifications.notifications import Notification
from hotpotato.models.users import User


class Message(Notification):
    """
    Message object class.
    """

    name = "message"
    description = "message"

    from_user = db.relationship(
        User,
        primaryjoin="foreign(Message.json.op('->>')('from_user_id')).cast(Integer)==User.id",
        lazy="joined",
    )

    def body(self, **kwargs):
        return (
            "Message from {} via Hot Potato: {}".format(
                self.from_user.name, self.json["message"]
            )
            if self.json["from_user_id"]
            else "Message from Hot Potato: {}".format(self.json["message"])
        )

    # pylint: disable=arguments-differ
    def send(self, user_id, run_async=True, contact=None, method=None):
        """
        Send the message.
        """

        # The reason why this import is here and not at the top of the file is
        # because when the actors get defined, they connect to the message queue broker
        # to register themselves as jobs to be run.
        # Therefore, the message queue broker needs to already be running BEFORE
        # the actors get imported/defined.
        from hotpotato.notifications.queue.actors import message as actor

        super().send(
            actor, user_id, run_async=run_async, contact=contact, method=method
        )

    __mapper_args__ = {"polymorphic_identity": "message"}
