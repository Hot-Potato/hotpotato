"""
User model classes.
"""
import flask_security
import pytz
from sqlalchemy.orm import joinedload, lazyload, validates

from hotpotato.models.database import db

# Maps users to roles.
roles_users = db.Table(
    "roles_users",
    db.Column("id", db.Integer(), primary_key=True),
    db.Column("user_id", db.Integer(), db.ForeignKey("user.id", ondelete="CASCADE")),
    db.Column("role_id", db.Integer(), db.ForeignKey("role.id", ondelete="CASCADE")),
)


class Role(db.Model, flask_security.RoleMixin):
    """
    Stores the various user roles.
    """

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(255))
    description = db.Column(db.Text)
    priority = db.Column(db.Integer, nullable=False)

    team_id = db.Column(
        db.Integer, db.ForeignKey("team.id", ondelete="CASCADE"), nullable=False
    )
    team = db.relationship(
        "Team", backref=db.backref("roles", cascade="all, delete-orphan")
    )

    __table_args__ = (
        db.UniqueConstraint("name", "team_id"),
        db.UniqueConstraint("team_id", "priority"),
    )

    def get_permissions(self):
        """
        Override flask security permissions callback to return an empty set
        so it doesn't interfare with our permissions attribute
        """
        return set()

    def __str__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)


class User(db.Model, flask_security.UserMixin):
    """
    Stores user information.
    """

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.Text)
    name = db.Column(db.Text)
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    last_login_at = db.Column(db.DateTime())
    current_login_at = db.Column(db.DateTime())
    last_login_ip = db.Column(db.Text)
    current_login_ip = db.Column(db.Text)
    login_count = db.Column(db.Integer())
    timezone = db.Column(db.Text)
    use_24hr = db.Column(db.Boolean(), nullable=False, server_default="false")
    language = db.Column(db.Text, nullable=False, server_default="en")
    roles = db.relationship("Role", secondary=roles_users, backref=db.backref("users"))

    teams = db.relationship(
        "Team", secondary="teams_users", backref=db.backref("users")
    )
    teams_users = db.relationship("TeamsUsers", back_populates="user")

    primary_team = db.relationship(
        "Team",
        uselist=False,
        secondary="teams_users",
        secondaryjoin="and_(User.id==TeamsUsers.user_id, TeamsUsers.team_id==Team.id, TeamsUsers.primary==True)",
    )

    @property
    def pytz_timezone(self):
        """
        Return the timezone of the user as a timezone object.
        """

        try:
            return pytz.timezone(self.timezone or "UTC")
        except pytz.UnknownTimeZoneError:
            return pytz.timezone("UTC")

    @validates("timezone")
    def validate_timezone(self, key, timezone):
        """
        Check timezone is a valid pytz timezone.
        """

        if timezone not in pytz.all_timezones:
            from hotpotato.users import UserTimezoneError

            raise UserTimezoneError(timezone, self.email)

        return timezone

    def add_role(self, role):
        """
        Add a role to this user.
        """

        user_datastore.add_role_to_user(self, role)

    def get_roles_for_team(self, team, sort=False):
        """
        Get the roles this user has for the given team.
        If `sort` is `True`, sort the returned list by priority level,
        with highest priority level first.
        """

        data = [r for r in self.roles if r.team_id == team.id]
        if sort:
            data.sort(key=lambda r: r.priority)
        return data

    def get_all_accessible_teams(self, exclude_joined=False):
        """
        Get all the accessible teams for this user.
        This constitutes:
        * Teams the user is part of (can be optionally excluded)
        * Teams that escalate to any team the user is part of
        """

        teams = []

        for team in self.teams:
            self._get_all_accessible_teams(teams, team, append=not exclude_joined)
        teams.sort(key=lambda t: t.name)

        return teams

    def _get_all_accessible_teams(self, teams, team, append=True):
        """
        Recursive helper method for traversing the team tree.
        """

        if append:
            teams.append(team)
        for t in team.escalated_from:
            self._get_all_accessible_teams(teams, t)


class HotpotatoUserDatastore(flask_security.SQLAlchemyUserDatastore):
    def find_user(self, **kwargs):
        query = self.user_model.query.options(
            joinedload("roles"), joinedload("teams"), lazyload("primary_team")
        )
        return query.filter_by(**kwargs).first()


# Initialize the Flask-Security user datastore.
user_datastore = HotpotatoUserDatastore(db, User, Role)
