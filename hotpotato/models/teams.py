"""
Team models
"""

from enum import Enum, auto

import pytz

from hotpotato.models.database import db


class TeamsUsers(db.Model):
    user_id = db.Column(
        db.Integer(), db.ForeignKey("user.id", ondelete="CASCADE"), primary_key=True
    )
    user = db.relationship("User", back_populates="teams_users")
    team_id = db.Column(
        db.Integer(), db.ForeignKey("team.id", ondelete="CASCADE"), primary_key=True
    )
    team = db.relationship("Team", back_populates="teams_users")
    primary = db.Column(db.Boolean(), nullable=False)


class EscalationPolicy(Enum):
    """
    Determines if/when notifications get escalated.
    """

    Never = auto()
    OutsideBusinessHours = auto()
    Always = auto()


class Team(db.Model):
    """
    A team is a group within an organisation responsible for a particular
    service or group of services. One project might first be monitored by
    the dev team, then be monitored by an operations team when it reaches
    production.

    Notifications are initially sent to a single team. However, if they are
    not acknowledged or not in business hours for the team they can be
    escalated to another team.

    Teams that are escalated to have all permissions of the teams below them.
    """

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False, unique=True)
    # The team that notifications can be escalated to from this team,
    escalates_to_id = db.Column(
        db.Integer, db.ForeignKey("team.id", ondelete="RESTRICT")
    )
    escalates_to = db.relationship("Team", remote_side=[id], backref="escalated_from")

    escalation_policy = db.Column(
        db.Enum(EscalationPolicy), nullable=False, server_default="Never"
    )

    # Number of minutes before notifications are escalated if unread (As determined by the escalation policy)
    escalation_delay_mins = db.Column(db.Integer, nullable=False, server_default="30")

    on_call_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    on_call = db.relationship("User")

    timezone = db.Column(db.Text, nullable=False, server_default="UTC")

    teams_users = db.relationship("TeamsUsers", back_populates="team", cascade="delete")

    __table_args__ = (
        db.CheckConstraint("id != escalates_to_id", "not_escalates_to_self"),
    )

    @property
    def pytz_timezone(self):
        """
        Return the timezone of the user as a timezone object.
        """

        try:
            return pytz.timezone(self.timezone or "UTC")
        except pytz.UnknownTimeZoneError:
            return pytz.timezone("UTC")

    def add_users(self, *users):
        """
        Add the given list of users to this team.
        """

        teams_users = [
            TeamsUsers(primary=False, user=user, team=self)
            for user in users
            if user not in self.users
        ]
        db.session.add_all(teams_users)
