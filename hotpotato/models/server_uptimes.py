"""
Server Uptime model classes.
"""

from hotpotato.models.database import db
from hotpotato.models.servers import Server


class ServerUptime(db.Model):
    """
    Stores the availability statistics on a given day for each server,
    for the last day, 7 days and calendar month.
    """

    __tablename__ = "server_uptimes"

    id = db.Column(db.Integer, primary_key=True)
    server_id = db.Column(db.Integer, db.ForeignKey(Server.id, ondelete="CASCADE"))
    updated_dt = db.Column(db.DateTime)
    avail_last_1d = db.Column(db.Numeric, nullable=True)
    avail_last_7d = db.Column(db.Numeric, nullable=True)
    avail_last_month = db.Column(db.Numeric, nullable=True)
