"""
Config model classes.
"""
from hotpotato.models.database import db


class Config(db.Model):
    """
    Stores key value config
    """

    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.Text, nullable=False, unique=True)
    value = db.Column(db.Text, nullable=True)
