"""
Permission models.
"""


import hotpotato
from hotpotato.models.database import db


class Permission(db.Model):
    """
    Permission model.
    """

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False, unique=True)

    @property
    def description(self):
        """
        Get the description for this permission.
        """

        return hotpotato.permissions.DATA_BY_NAME[self.name]["description"]

    def test(self, user, team):
        """
        Run the permission test for this permission on the given user.
        Optionally runs additional checks based on a team with respect to the given user.

        * True - Match, access allowed
        * False - Match, access denied
        * None - No match
        """

        # No match if the user is not part of the given team.
        # TODO: See how this relates to teams the user is part of which
        # the given team escalates to.
        if team not in user.teams:
            return None

        # Check permissions for the user's roles within the given team.
        for role in user.get_roles_for_team(team, sort=True):
            perm = RolePermission.get(self, role)
            if perm:
                return perm.access
        return None

    def get_permission(self, obj_id):
        """
        Get the permission to this permission for the given object.
        """

        return RolePermission.get(self, hotpotato.roles.get(obj_id))

    def set_permission(self, obj_id, access):
        """
        Set the permission to this permission for the given object.
        """

        return RolePermission.set(self, hotpotato.roles.get(obj_id), access)


class RolePermission(db.Model):
    """
    Permission definiton for roles.
    """

    id = db.Column(db.Integer, primary_key=True)

    permission_id = db.Column(
        db.Integer, db.ForeignKey("permission.id", ondelete="CASCADE"), nullable=False
    )
    permission = db.relationship(
        "Permission", backref=db.backref("role_permissions", passive_deletes=True)
    )

    role_id = db.Column(
        db.Integer, db.ForeignKey("role.id", ondelete="CASCADE"), nullable=False
    )
    role = db.relationship(
        "Role",
        backref=db.backref(
            "permissions", cascade="all, delete-orphan", passive_deletes=True
        ),
    )

    access = db.Column(db.Boolean, nullable=False)

    @staticmethod
    def get(permission, role):
        """
        Get a role permission for the given permission.
        """

        return RolePermission.query.filter_by(
            permission_id=permission.id, role_id=role.id
        ).one_or_none()

    @staticmethod
    def set(permission, role, access):
        """
        Set a role permission for the given permission, and return the resulting object.
        """

        perm = RolePermission.get(permission, role)
        if perm:
            if access != perm.access:
                perm.access = access
        else:
            perm = RolePermission(
                permission_id=permission.id, role_id=role.id, access=access
            )
            db.session.add(perm)
        return perm
