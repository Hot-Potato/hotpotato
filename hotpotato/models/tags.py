"""
Tag model classes.
"""
from hotpotato.models.database import db
from hotpotato.models.teams import Team

tag_notification = db.Table(
    "tag_notification",
    db.Column(
        "tag_id",
        db.Integer(),
        db.ForeignKey("tag.id", ondelete="CASCADE"),
        nullable=False,
        primary_key=True,
    ),
    db.Column(
        "notification_id",
        db.Integer(),
        db.ForeignKey("notification_log.id", ondelete="CASCADE"),
        nullable=False,
        primary_key=True,
    ),
)


class Tag(db.Model):
    """
    String that can be attached to objects to allow for categorising and searching through them.
    """

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False)
    team_id = db.Column(
        db.Integer, db.ForeignKey(Team.id, ondelete="CASCADE"), nullable=False
    )
    team = db.relationship("Team", backref="tags")
