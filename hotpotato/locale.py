from flask import request
from flask_babelex import Babel
from flask_security import current_user

babel = Babel()


def init_app(app):
    babel.init_app(app)
    babel.list_translations()


@babel.localeselector
def get_locale():
    try:
        return current_user.language
    except AttributeError:
        return request.accept_languages.best_match(["en_NZ"])


@babel.timezoneselector
def get_timezone():
    return current_user.timezone
