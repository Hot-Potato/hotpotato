import re
from copy import deepcopy

from flask_static_digest import FlaskStaticDigest
from flask_static_digest.digester import DIGESTED_FILE_REGEX
from flask_talisman import Talisman
from whitenoise import WhiteNoise

flask_static_digest = FlaskStaticDigest()


talisman = Talisman()

csp = {
    "default-src": "'none'",
    "script-src": "'self'",
    "style-src": ["'self'"],
    "img-src": ["'self'", "data:"],
    "font-src": "'self'",
    "manifest-src": "'self'",
    "connect-src": "'self'",
}

csp_inline_style = deepcopy(csp)

csp_inline_style["style-src"].append("'unsafe-inline'")


def init_app(app):
    talisman.init_app(
        app,
        content_security_policy=csp_inline_style,
        force_https=False,
        strict_transport_security=False,
        session_cookie_secure=False,
    )

    flask_static_digest.init_app(app)
    app.wsgi_app = WhiteNoise(
        app.wsgi_app,
        root=app.static_folder,
        prefix="static",
        immutable_file_test=lambda path, url: re.search(DIGESTED_FILE_REGEX, url),
    )
