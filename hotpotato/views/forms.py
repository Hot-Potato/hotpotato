import wtforms
from flask_wtf import FlaskForm


class MessageForm(FlaskForm):
    message = wtforms.StringField()
