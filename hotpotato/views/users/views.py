from flask import flash, redirect, render_template, url_for
from flask_babelex import gettext as _
from flask_security import current_user
from flask_wtf import FlaskForm

from hotpotato import rules, users
from hotpotato.models import db
from hotpotato.views._csrf import csrf
from hotpotato.views.users._blueprint import blueprint


@blueprint.route("/<user_id>", methods=["GET"])
@rules.access_control_list(blueprint, rules.LoginRule())
def get(user_id):
    """
    Display information about a specific user.
    """

    return render_template(
        "users/get.html",
        user=users.get(user_id),
        user_toggle_active_form=FlaskForm(),
        admin=rules.test_rules(
            rules.PermissionRule("users.get"), context=rules.contexts.hotpotato_team
        ),
    )


@blueprint.route("/toggle_active/<user_id>", methods=["POST"])
@csrf.exempt
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("users.toggle_active"),
    context=rules.contexts.hotpotato_team,
)
def toggle_active(user_id):
    """
    Toggle whether a specific user is active
    """

    form = FlaskForm()
    if form.validate_on_submit():
        user = users.get(user_id)

        if user == current_user:
            flash(_("Unable to deactivate your own user account."), "error")
        else:
            user.active = not user.active
            db.session.commit()

    if form.csrf_token.errors:
        flash(_("CSRF error, please try again"), "error")
    return redirect(url_for("views_users.get", user_id=user_id))
