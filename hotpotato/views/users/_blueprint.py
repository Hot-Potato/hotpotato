"""
Users views blueprint.
"""


import flask

blueprint = flask.Blueprint("views_users", __name__)
