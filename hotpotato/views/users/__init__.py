from hotpotato.views.users import views
from hotpotato.views.users._blueprint import blueprint

__all__ = ["views", "blueprint"]
