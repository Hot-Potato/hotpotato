import collections
from datetime import datetime, timedelta

import werkzeug
from flask import flash, render_template
from flask_security import current_user

from hotpotato import util
from hotpotato.api.functions import (
    exceptions as api_functions_exceptions,
    notifications as api_functions_notifications,
)
from hotpotato.api.web.v1.notifications import handlers as api_notification_handlers
from hotpotato.notifications import notifications
from hotpotato.teams import current_team
from hotpotato.views.forms import MessageForm
from hotpotato.views.notifications.forms import AcknowledgeForm


def render_notifications_page(
    heading="Notifications",
    handler_name="notifications",
    json_attr_info=None,
    handover_form=None,
    message_form=None,
):
    """
    Display all notifications, with optional custom heading and JSON attributes.
    """

    handler = api_notification_handlers.get(handler_name)

    tag_names = [x.name for x in current_team.tags]

    # The list of possible search filter attributes, in the order they are meant
    # to show on the web page (using an OrderedDict).
    attr_info_list = [
        ("start_date", {"description": "Start Date", "type": "date"}),
        ("end_date", {"description": "End Date", "type": "date"}),
        ("id", {"description": "Notification ID", "type": "int"}),
        ("user_id", {"description": "User ID", "type": "int"}),
        ("received_dt", {"description": "Received Date/Time", "type": "datetime"}),
        ("tag", {"description": "Tag", "type": "enum", "values": tag_names}),
    ]

    if handler.NOTIF_TYPE == "notification":
        attr_info_list.append(
            (
                "notif_type",
                {
                    "description": "Notification Type",
                    "notif_type": "enum",
                    "values": notifications.TYPE,
                },
            )
        )

    attr_info_list.extend(
        (
            ("body", {"description": "Notification Body", "type": "str"}),
            ("version", {"description": "Notification Version", "type": "int"}),
            ("node_name", {"description": "Handling Node Name", "type": "str"}),
            ("event_id", {"description": "Associated Event ID", "type": "int"}),
            ("ticket_id", {"description": "Associated Ticket ID", "type": "str"}),
            (
                "status",
                {
                    "description": "Sending Status",
                    "type": "enum",
                    "values": notifications.STATUS,
                },
            ),
            (
                "method",
                {
                    "description": "Sending Method",
                    "type": "enum",
                    "values": notifications.METHOD,
                },
            ),
            (
                "provider",
                {
                    "description": "Sending Provider",
                    "type": "enum",
                    "values": notifications.PROVIDER,
                },
            ),
            (
                "provider_notif_id",
                {"description": "Provider Notification ID", "type": "str"},
            ),
        )
    )
    search_filter_attr_info = collections.OrderedDict(attr_info_list)

    if json_attr_info:
        search_filter_attr_info.update(json_attr_info)

    # Get the search filters.
    search_filters = api_functions_notifications.search_filters_get(
        handler=handler, fail_if_empty=False
    )

    if not [k for k in search_filters.keys() if k != "team_id"]:
        now_dt = util.datetime_process(datetime.utcnow(), as_utc=True, to_user_tz=True)
        end_date = now_dt  # Inclusive
        start_date = end_date - timedelta(days=1)  # Exclusive
        search_filters = werkzeug.datastructures.MultiDict(
            [("start_date", start_date.isoformat()), ("end_date", end_date.isoformat())]
        )

    search_filters["team_id"] = current_team.id

    # Query the database according to the search filters, flashing any error message
    # that results if the query fails.
    try:
        notifs = api_functions_notifications.get_from_search_filters(
            handler, search_filters, eager=True
        )
    except api_functions_exceptions.APISearchFilterError as err:
        flash("{}.".format(str(err)), "error")
        notifs = []

    if handover_form is None:
        handover_form = MessageForm()
    if message_form is None:
        message_form = MessageForm()
    acknowledge_form = AcknowledgeForm()

    return render_template(
        "notifications/get.html",
        pagerperson=current_team.on_call,
        heading=heading,
        current_search_filters=search_filters,
        search_filter_attr_info=search_filter_attr_info,
        id_notif=notifs,
        handover_form=handover_form,
        message_form=message_form,
        acknowledge_form=acknowledge_form,
        no_contact_methods=len(current_user.contacts) == 0,
    )
