from http import HTTPStatus as Status

from flask import abort, flash, redirect, render_template, request, url_for
from flask_babelex import gettext as _
from flask_wtf import FlaskForm

from hotpotato import rules
from hotpotato.models import db
from hotpotato.notifications import (
    exceptions as notifications_exceptions,
    notifications,
)
from hotpotato.views._csrf import csrf
from hotpotato.views.notifications import functions
from hotpotato.views.notifications._blueprint import blueprint
from hotpotato.views.notifications.forms import AcknowledgeForm


@blueprint.route("/", methods=["GET"])
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("notifications.get"),
    context=rules.contexts.team_and_escalates_to,
)
def notifications_get():
    """
    Display all notifications.
    """

    return functions.render_notifications_page()


@blueprint.route("/<notif_id>", methods=["GET"])
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("notifications.get"),
    context=rules.contexts.team_and_escalates_to,
)
def notifications_get_by_id(notif_id):
    """
    Display information about a specific notifications.
    """

    form = AcknowledgeForm()

    try:
        return render_template(
            "notifications/get_by_id.html",
            notif=notifications.get(notif_id, eager=True),
            form=form,
        )

    except notifications_exceptions.NotificationIDError:
        return abort(Status.NOT_FOUND)


@blueprint.route("/acknowledge/<notif_id>", methods=["POST"])
@csrf.exempt
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("notifications.acknowledge"),
    context=rules.contexts.team_and_escalates_to,
)
def acknowledge(notif_id):
    try:
        notif = notifications.get(notif_id)
    except notifications.exceptions.NotificationIDError:
        return abort(Status.NOT_FOUND)

    form = FlaskForm()
    if form.validate_on_submit():
        if notif.json["status"] != "SEND_FAILED":
            return abort(Status.BAD_REQUEST)

        notif.json["status"] = "SEND_FAILED_ACKNOWLEDGED"
        db.session.commit()

        flash("Notification acknowledged")

    if form.csrf_token.errors:
        flash(_("CSRF error, please try again"), "error")

    return redirect(
        request.referrer
        or url_for("views_notifications.notifications_get", **request.args)
    )
