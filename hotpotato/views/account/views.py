import flask_babelex
from flask import abort, current_app, flash, redirect, render_template, request, url_for
from flask_babelex import gettext as _
from flask_security import current_user
from flask_wtf import FlaskForm

from hotpotato import oncall_contacts, rules, teams, users
from hotpotato.models import OncallContact, db
from hotpotato.notifications import messages
from hotpotato.views._csrf import csrf
from hotpotato.views.account._blueprint import blueprint
from hotpotato.views.account.forms import AddContactForm, LanguageForm, TimezoneForm


@blueprint.route("/", methods=["GET"])
@rules.access_control_list(blueprint, rules.LoginRule())
def manage_account(add_contact_form=None):
    """
    Page to view account information
    """
    if add_contact_form is None:
        add_contact_form = AddContactForm()

    timezone_form = TimezoneForm()
    timezone_form.use_24hr.data = current_user.use_24hr
    language_form = LanguageForm()

    verifiable_contacts = tuple(
        oncall_contacts.get_for_user(current_user, verifiable=True)
    )
    unverifiable_contacts = tuple(
        oncall_contacts.get_for_user(current_user, verifiable=False)
    )

    return render_template(
        "account/settings.html",
        user=current_user,
        verifiable_contacts=verifiable_contacts,
        unverifiable_contacts=unverifiable_contacts,
        timezone_form=timezone_form,
        language_form=language_form,
        add_contact_form=add_contact_form,
        feature_translations=current_app.config["FEATURE_TRANSLATIONS"],
    )


@blueprint.route("/remove-contact-method", methods=["POST"])
@csrf.exempt
@rules.access_control_list(blueprint, rules.LoginRule())
def remove_contact_methods():
    form = FlaskForm()
    if form.validate_on_submit():
        contact_ids = []
        for key in request.form.keys():
            try:
                contact_ids.append(int(key))
            except ValueError:
                pass
        OncallContact.query.filter(
            OncallContact.user == current_user, OncallContact.id.in_(contact_ids)
        ).delete(synchronize_session=False)
        db.session.commit()

    if hasattr(form, "csrf_token") and form.csrf_token.errors:
        flash(_("CSRF error, please try again"), "error")

    return redirect(url_for("views_account.manage_account"))


@blueprint.route("/move-contact-up/<contact_id>", methods=["POST"])
@csrf.exempt
@rules.access_control_list(blueprint, rules.LoginRule())
def move_contact_up(contact_id):
    form = FlaskForm()
    if form.validate_on_submit():
        contact = OncallContact.query.filter(
            OncallContact.user == current_user, OncallContact.id == contact_id
        ).first_or_404()

        contact.move_up()

        db.session.commit()

    if hasattr(form, "csrf_token") and form.csrf_token.errors:
        flash(_("CSRF error, please try again"), "error")

    return redirect(url_for("views_account.manage_account"))


@blueprint.route("/move-contact-down/<contact_id>", methods=["POST"])
@csrf.exempt
@rules.access_control_list(blueprint, rules.LoginRule())
def move_contact_down(contact_id):
    form = FlaskForm()
    if form.validate_on_submit():
        contact = OncallContact.query.filter(
            OncallContact.user == current_user, OncallContact.id == contact_id
        ).first_or_404()

        contact.move_down()

        db.session.commit()

    if hasattr(form, "csrf_token") and form.csrf_token.errors:
        flash(_("CSRF error, please try again"), "error")

    return redirect(url_for("views_account.manage_account"))


@blueprint.route("/manage_team", methods=["POST"])
@csrf.exempt
@rules.access_control_list(blueprint, rules.LoginRule())
def account_team():
    form = FlaskForm()
    if form.validate_on_submit():
        f = request.form
        if "action" not in f or "team_id" not in f:
            flash("Unknown action", "error")
            return redirect(url_for("views_account.manage_account"))

        if f["action"] == "set_primary":
            try:
                team = teams.get(f["team_id"])
            except teams.TeamIDError:
                flash(
                    "The team you are trying to set as primary doesn't exist", "error"
                )
                return redirect(url_for("views_account.manage_account"))

            if team not in current_user.teams:
                flash("You can't set a team your not a member of as primary", "error")
                return redirect(url_for("views_account.manage_account"))

            users.set_primary_team(current_user, f["team_id"])
            return redirect(url_for("views_account.manage_account"))

        if f["action"] == "leave":
            try:
                team = teams.get(f["team_id"])
            except teams.TeamIDError:
                flash("The team you are trying to leave doesn't exist", "error")
                return redirect(url_for("views_account.manage_account"))

            if current_user.primary_team == teams.get(f["team_id"]):
                flash("You can't leave your primary team.", "error")
                return redirect(url_for("views_account.manage_account"))
            current_user.teams.remove(team)
            db.session.commit()
            return redirect(url_for("views_account.manage_account"))

        flash("Unknown action", "error")

    if hasattr(form, "csrf_token") and form.csrf_token.errors:
        flash(_("CSRF error, please try again"), "error")

    return redirect(url_for("views_account.manage_account"))


@blueprint.route("/tz", methods=["POST"])
@csrf.exempt
@rules.access_control_list(blueprint, rules.LoginRule())
def change_timezone():
    """
    Changes the preffered timezone of the user
    """

    form = TimezoneForm()

    if form.validate_on_submit():
        current_user.timezone = form.timezone.data
        current_user.use_24hr = form.use_24hr.data
        db.session.commit()
        flask_babelex.refresh()

    if hasattr(form, "csrf_token") and form.csrf_token.errors:
        flash(_("CSRF error, please try again"), "error")

    return redirect(url_for("views_account.manage_account"))


@blueprint.route("/language", methods=["POST"])
@csrf.exempt
@rules.access_control_list(blueprint, rules.LoginRule())
def change_language():
    """
    Changes the prefered language of the user
    """
    if not current_app.config["FEATURE_TRANSLATIONS"]:
        abort(404)

    form = LanguageForm()

    if form.validate_on_submit():
        current_user.language = form.language.data
        db.session.commit()
        flask_babelex.refresh()

    if hasattr(form, "csrf_token") and form.csrf_token.errors:
        flash(_("CSRF error, please try again"), "error")

    return redirect(url_for("views_account.manage_account"))


@blueprint.route("/add-contact-method", methods=["GET", "POST"])
@csrf.exempt
@rules.access_control_list(blueprint, rules.LoginRule())
def add_contact_method():
    """
    Page to allow adding contact methods for on-call people.
    """
    form = AddContactForm()
    if form.validate_on_submit():
        # If the contact method doesn't exist, create it.
        oncall_contact = oncall_contacts.create(
            user_id=current_user.id,
            contact=form.contact.data,
            method=form.method.data,
            send_pages=form.send_pages.data,
            send_failures=form.send_failures.data,
        )

        # Send a test message if the contact method is SMS.
        if form.method.data == "sms" or form.method.data == "pushover":
            messages.create(team_id=None, body="This is a test message").send(
                current_user.id, contact=oncall_contact
            )

        return redirect(url_for("views_account.manage_account"))

    return manage_account(add_contact_form=form)


@blueprint.route("/toggle-send-pages/<contact_id>", methods=["POST"])
@csrf.exempt
@rules.access_control_list(blueprint, rules.LoginRule())
def toggle_send_pages(contact_id):
    """
    Toggles the sending of pages to a contact method
    """
    oncall_contact = OncallContact.query.filter(
        OncallContact.user == current_user, OncallContact.id == contact_id
    ).first_or_404()

    form = FlaskForm()
    if form.validate_on_submit():
        oncall_contact.send_pages = not oncall_contact.send_pages
        db.session.commit()

    if hasattr(form, "csrf_token") and form.csrf_token.errors:
        flash(_("CSRF error, please try again"), "error")

    return redirect(url_for("views_account.manage_account"))


@blueprint.route("/toggle-send-failures/<contact_id>", methods=["POST"])
@csrf.exempt
@rules.access_control_list(blueprint, rules.LoginRule())
def toggle_notify_on_fail(contact_id):
    """
    Toggle sending failure alerts to a contact method.
    """
    oncall_contact = OncallContact.query.filter(
        OncallContact.user == current_user, OncallContact.id == contact_id
    ).first_or_404()

    form = FlaskForm()
    if form.validate_on_submit():

        if oncall_contact.send_failures:
            current_app.logger.info(
                "Removing failure alerts for {}".format(current_user.name)
            )

            if oncall_contact.method != "pager":
                messages.create(
                    team_id=None, body="You will no longer receive failure alerts"
                ).send(current_user.id, contact=oncall_contact)

            oncall_contact.send_failures = False
            db.session.commit()

        else:
            if teams.hotpotato_team() not in current_user.teams:
                current_app.logger.info(
                    "Refused to add failure alerts for {}".format(current_user.name)
                )
                flash(
                    "You must be in the {} team to recieve failure alerts".format(
                        teams.hotpotato_team().name
                    ),
                    "error",
                )
                return redirect(url_for("views_account.manage_account"))

            current_app.logger.info(
                "Adding failure alerts for {}".format(current_user.name)
            )

            oncall_contact.send_failures = True
            db.session.commit()

            # Send a notification notifying the user they will get failure notifications on this
            # on-call contact.
            if oncall_contact.method != "pager":
                messages.create(
                    team_id=None, body="You will now get failure alerts"
                ).send(current_user.id, contact=oncall_contact)

    if hasattr(form, "csrf_token") and form.csrf_token.errors:
        flash(_("CSRF error, please try again"), "error")

    return redirect(url_for("views_account.manage_account"))


@blueprint.route("/send-test-message/<contact_id>", methods=["POST"])
@csrf.exempt
@rules.access_control_list(blueprint, rules.LoginRule())
def send_test_message(contact_id):
    """
    Sends a test message to a contact method.
    """
    oncall_contact = OncallContact.query.filter(
        OncallContact.user == current_user, OncallContact.id == contact_id
    ).first_or_404()

    form = FlaskForm()
    if form.validate_on_submit():
        messages.create(team_id=None, body="This is a test message").send(
            current_user.id, contact=oncall_contact
        )

        flash(
            "A test message has been sent to {}".format(oncall_contact.contact), "info"
        )

    if hasattr(form, "csrf_token") and form.csrf_token.errors:
        flash(_("CSRF error, please try again"), "error")

    return redirect(url_for("views_account.manage_account"))
