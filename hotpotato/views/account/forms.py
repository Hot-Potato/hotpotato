import pytz
import requests
import wtforms
from flask import current_app
from flask_babelex import gettext as _
from flask_security import current_user
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, ValidationError

from hotpotato import locale, oncall_contacts, teams, util


class AddContactForm(FlaskForm):
    method = wtforms.SelectField(_("Contact Method"))
    contact = wtforms.StringField(_("Contact Identifier"), validators=[DataRequired()])
    send_pages = wtforms.BooleanField(_("Send Pages"), false_values=None, default=True)
    send_failures = wtforms.BooleanField(
        _("Send notification failure alerts"), false_values=None, default=True
    )

    def __init__(self, *args, **kwargs):
        super(AddContactForm, self).__init__(*args, **kwargs)
        self.method.choices = [
            (method, method) for method in util.enabled_sending_methods()
        ]

    def validate_contact(self, field):
        if self.method.data not in util.enabled_sending_methods():
            return
        if oncall_contacts.exists(
            user_id=current_user.id, contact=self.contact.data, method=self.method.data
        ):
            raise ValidationError(_("Contact method already exists"))
        # Verify that the pushover token is valid
        if self.method.data == "pushover":
            token = current_app.config["PUSHOVER_API_TOKEN"]
            verify = requests.post(
                "https://api.pushover.net/1/users/validate.json",
                json={"token": token, "user": self.contact.data},
            )
            if verify.status_code != 200 or verify.json()["status"] != 1:
                raise ValidationError(_("pushover token is invalid"))

    def validate_send_failures(self, field):
        if self.send_failures.data and teams.hotpotato_team() not in current_user.teams:
            raise ValidationError(
                _(
                    "You must be in the %(team)s team to receive failure alerts",
                    team=teams.hotpotato_team().name,
                )
            )


class TimezoneForm(FlaskForm):
    timezone = wtforms.SelectField(
        _("Timezone"),
        choices=[(timezone, timezone) for timezone in pytz.all_timezones],
        validators=[DataRequired()],
    )
    use_24hr = wtforms.BooleanField(_("Use 24-hour time"))

    def __init__(self, **kwargs):
        super(TimezoneForm, self).__init__(**kwargs)
        if self.timezone.data is None:
            self.timezone.data = current_user.timezone


class LanguageForm(FlaskForm):
    language = wtforms.SelectField(_("Language"), validators=[DataRequired()])

    def __init__(self, **kwargs):
        super(LanguageForm, self).__init__(**kwargs)
        self.language.choices = [
            (translation.language, translation.display_name)
            for translation in locale.babel.list_translations()
        ]
        if self.language.data is None:
            self.language.data = current_user.language
