from flask import render_template


def generic(e):
    return render_template("errors/generic.html.j2", error=e), e.code
