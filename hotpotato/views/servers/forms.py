import pytz
import wtforms
from flask_security import current_user
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, ValidationError

from hotpotato import servers


class AddServerForm(FlaskForm):
    hostname = wtforms.StringField("Hostname", validators=[DataRequired()])
    disable_missed_heartbeat_notif = wtforms.BooleanField(
        "Disable missed heartbeat notification", false_values=None
    )
    timezone = wtforms.SelectField(
        "Timezone",
        choices=[(timezone, timezone) for timezone in pytz.common_timezones],
        validators=[DataRequired()],
    )
    link = wtforms.StringField("Link", validators=[DataRequired()])

    def __init__(self, **kwargs):
        super(AddServerForm, self).__init__(**kwargs)
        if self.timezone.data is None:
            self.timezone.data = current_user.timezone

    def validate_hostname(self, field):
        if servers.get_by(hostname=self.hostname.data):
            raise ValidationError("Server with that hostname already exists.")
