from datetime import datetime

from flask import abort, flash, jsonify, redirect, render_template, request, url_for
from flask_babelex import gettext as _
from flask_wtf import FlaskForm

from hotpotato import heartbeats, rules, servers
from hotpotato.models import Server, db
from hotpotato.servers import (
    get_missed_hbeats as get_missed_hbeats_servs,
    get_num_missed_hbeats,
)
from hotpotato.views._csrf import csrf
from hotpotato.views.servers._blueprint import blueprint
from hotpotato.views.servers.forms import AddServerForm


@blueprint.route("/", methods=["GET"])
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("servers.get"),
    context=rules.contexts.hotpotato_team,
)
def get_servers():
    """
    Page that displays information about the servers that talk to Hot Potato.
    """

    form = AddServerForm()

    if request.args.get("filter", "") == "missed-hbeat":
        servs = get_missed_hbeats_servs()
    else:
        subquery_last_hbeats = heartbeats.get_last_query().subquery()
        servs = (
            db.session.query(
                Server.hostname,
                Server.timezone,
                Server.disable_missed_heartbeat_notif,
                subquery_last_hbeats.c.last_hbeat,
                Server.id,
                Server.link,
            )
            .outerjoin(
                subquery_last_hbeats, subquery_last_hbeats.c.server_id == Server.id
            )
            .all()
        )

    return render_template(
        "list_servers.html",
        now=datetime.utcnow(),
        servs=servs,
        num_missed_hbeats=get_num_missed_hbeats(),
        form=form,
        filter=request.args.get("filter"),
    )


@csrf.exempt
@blueprint.route("/add", methods=["POST"])
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("servers.add"),
    context=rules.contexts.hotpotato_team,
)
def add_server():
    """
    Add a new monitoring server, generating an API key and returning it as json.
    """
    form = AddServerForm()
    if form.validate_on_submit():
        _, apikey = servers.create(
            hostname=form.hostname.data,
            timezone=form.timezone.data,
            link=form.link.data,
            disable_missed_heartbeat_notif=form.disable_missed_heartbeat_notif.data,
        )

        return jsonify({"apikey": apikey})

    return (jsonify(form.errors), 400)


@blueprint.route("/toggle-missed-heartbeat-notif/<server_id>", methods=["POST"])
@csrf.exempt
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("servers.heartbeats_toggle"),
    context=rules.contexts.hotpotato_team,
)
def server_toggle_missed_heartbeat(server_id):
    """
    Toggle disabling missed heartbeat notifications for a server.
    """

    try:
        server = servers.get(server_id)
    except servers.ServerIDError:
        abort(404)

    form = FlaskForm()
    if form.validate_on_submit():
        server.disable_missed_heartbeat_notif = (
            not server.disable_missed_heartbeat_notif
        )
        db.session.commit()

    if form.csrf_token.errors:
        flash(_("CSRF error, please try again"), "error")

    return redirect(url_for("views_servers.get_servers"))
