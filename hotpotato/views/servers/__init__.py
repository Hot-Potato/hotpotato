from hotpotato.views.servers import views
from hotpotato.views.servers._blueprint import blueprint

__all__ = ["views", "blueprint"]
