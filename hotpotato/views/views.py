from collections import OrderedDict
from datetime import datetime, time, timedelta

from flask import (
    Response,
    current_app,
    flash,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_security import current_user
from marshmallow import EXCLUDE, Schema, fields
from pytz import timezone
from sqlalchemy.sql.expression import label
from webargs.flaskparser import parser

from hotpotato import rules
from hotpotato.headers import csp_inline_style, talisman
from hotpotato.models import Alert, Notification, Server, Tag, db
from hotpotato.notifications import handovers, messages
from hotpotato.teams import current_team
from hotpotato.views._blueprint import blueprint
from hotpotato.views._csrf import csrf
from hotpotato.views.forms import MessageForm
from hotpotato.views.notifications import functions


@blueprint.route("/", methods=["GET"])
@rules.access_control_list(
    blueprint, rules.LoginRule(), context=rules.contexts.team_and_escalates_to
)
def main_site(handover_form=None, message_form=None):
    """
    The main index of the API.
    """
    return functions.render_notifications_page(
        handover_form=handover_form, message_form=message_form
    )


@blueprint.route("/handover", methods=["POST"])
@csrf.exempt
@rules.access_control_list(blueprint, rules.PermissionRule("handover.do"))
def do_handover():
    """
    Endpoint to do handover.
    """
    form = MessageForm()
    if form.validate_on_submit():
        from_user = current_team.on_call
        to_user = current_user

        # Get the message from the new on-call person.
        message = form.message.data

        try:
            handovers.handover(current_team, to_user, from_user, message)
        except ValueError as e:
            flash(str(e), category="error")
            return redirect(url_for("views.main_site"))

        if from_user:
            flash("You are now on call, taking over from {}.".format(from_user.name))
        else:
            flash("You are now on call.")
        return redirect(url_for("views.main_site"))
    return main_site(handover_form=form)


@blueprint.route("/message", methods=["POST"])
@csrf.exempt
@rules.access_control_list(blueprint, rules.PermissionRule("messages.send"))
def send_message():
    """
    Endpoint to send a message to the pager person.
    """
    form = MessageForm()
    if form.validate_on_submit():
        body = form.message.data
        from_user = current_user
        to_user = current_team.on_call

        messages.create(
            team_id=current_team.id, body=body, from_user_id=from_user.id
        ).send(to_user.id)

        if to_user:
            flash("Message sent to {}".format(to_user.name), "success")
        else:
            flash("Message sent", "success")
        return redirect(url_for("views.main_site"))
    return main_site(message_form=form)


class ReportArgsSchema(Schema):
    start_date = fields.Date(required=True)
    end_date = fields.Date(required=True)

    class Meta:
        unknown = EXCLUDE


def server_counts(filters):
    return (
        db.session.query(
            label(
                "count",
                db.func.count(Alert.json.op("->>")("server_id").cast(db.Integer)),
            ),
            label("hostname", Server.hostname),
        )
        .outerjoin(
            Server, Alert.json.op("->>")("server_id").cast(db.Integer) == Server.id
        )
        .group_by(Server)
        .order_by(
            db.desc(db.func.count(Alert.json.op("->>")("server_id").cast(db.Integer)))
        )
        .filter(
            Alert.json.op("->>")("server_id") != "50",
            Notification.notif_type == "alert",
            *filters
        )
        .all()
    )


def notif_count(filters):
    return (
        db.session.query(
            label("count", db.func.count(Alert.id)),
            label("trouble_code", Alert.json.op("->>")("trouble_code")),
            label("display_name", Alert.json.op("->>")("display_name")),
            label("state", Alert.json.op("->>")("state")),
            label("service_name", Alert.json.op("->>")("service_name")),
        )
        .group_by(
            Alert.json.op("->>")("trouble_code"),
            Alert.json.op("->>")("display_name"),
            Alert.json.op("->>")("state"),
            Alert.json.op("->>")("service_name"),
        )
        .order_by(
            db.desc(db.func.count(Alert.id)),
            Alert.json.op("->>")("trouble_code"),
            Alert.json.op("->>")("display_name"),
            Alert.json.op("->>")("service_name"),
            Alert.json.op("->>")("state"),
        )
        .filter(
            Alert.json.op("->>")("server_id") != "50",
            Notification.notif_type == "alert",
            *filters
        )
        .all()
    )


def notif_type_count(filters):
    return dict(
        db.session.query(
            label("type", Notification.notif_type),
            label("count", db.func.count(Notification.id)),
        )
        .filter(*filters)
        .group_by(Notification.notif_type)
        .order_by(Notification.notif_type)
        .all()
    )


def page_counts(filters, start_time, end_time):
    ooh_pages = 0
    bh_pages = 0
    alerts = Alert.query.filter(*filters).options(db.lazyload("*")).all()
    ooh_pages_days = {}
    bh_pages_days = {}
    for day in [
        (start_time.date() + timedelta(days=x))
        for x in range(0, ((end_time.date() - start_time.date()).days) + 1)
    ]:
        ooh_pages_days[day] = 0
        bh_pages_days[day] = 0

    for alert in alerts:
        alert_time = alert.received_dt.astimezone(timezone("Pacific/Auckland"))
        if alert_time.hour >= 8 and alert_time.hour < 18 and alert_time.weekday() < 5:
            bh_pages += 1
            bh_pages_days[alert_time.date()] = (
                bh_pages_days.get(alert_time.date(), 0) + 1
            )
        else:
            ooh_pages += 1
            ooh_pages_days[alert_time.date()] = (
                ooh_pages_days.get(alert_time.date(), 0) + 1
            )

    ooh_pages_days = OrderedDict(sorted(ooh_pages_days.items(), key=lambda t: t[0]))
    bh_pages_days = OrderedDict(sorted(bh_pages_days.items(), key=lambda t: t[0]))
    return (ooh_pages, bh_pages, ooh_pages_days, bh_pages_days)


def tag_counts(filters):
    return (
        db.session.query(
            label("name", Tag.name), label("count", db.func.count(Notification.id))
        )
        .outerjoin(Tag.notifications)
        .group_by(Tag.name)
        .order_by(db.desc(db.func.count(Notification.id)), Tag.name)
        .filter(*filters)
        .all()
    )


@blueprint.route("/report", methods=["GET"])
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("notifications.get"),
    context=rules.contexts.team_and_escalates_to,
)
@talisman(content_security_policy=csp_inline_style)
def report():
    """
    Weekly Report
    """
    args = parser.parse(ReportArgsSchema, request, location="querystring")

    start_time = timezone(current_user.timezone).localize(
        datetime.combine(args["start_date"], time(hour=8))
    )
    end_time = timezone(current_user.timezone).localize(
        datetime.combine(args["end_date"], time(hour=8))
    )

    filters = (
        Notification.received_dt >= start_time,
        Notification.received_dt < end_time,
        Notification.team_id == current_team.id,
    )

    notif_type_counts = notif_type_count(filters)
    # TODO this is a hack to filter out some messages on Catalysts instance of hot potato
    # Hot Potato should never generate an id this small in real usage so it shouldn't affect
    # anyone else but we should try and remove this as soon as possible.
    test_pages = Alert.query.filter(
        Alert.json.op("->>")("server_id") == "50", *filters
    ).count()

    ooh_pages, bh_pages, ooh_pages_days, bh_pages_days = page_counts(
        filters, start_time, end_time
    )

    server_count = server_counts(filters)
    server_colours = [
        "#500926",
        "#611028",
        "#721829",
        "#84202a",
        "#96282b",
        "#a8302b",
        "#bb382c",
        "#ce402b",
        "#e1482b",
        "#f5512a",
    ]

    notif_counts = notif_count(filters)

    if current_app.config["FEATURE_TAGS"]:
        tag_count = tag_counts(filters)
    else:
        tag_count = None

    next_report_start = end_time.date()
    next_report_end = (end_time + timedelta(weeks=1)).date()
    prev_report_start = (start_time - timedelta(weeks=1)).date()
    prev_report_end = start_time.date()

    return Response(
        render_template(
            "report.html.j2",
            start_time=start_time,
            end_time=end_time,
            test_pages=test_pages,
            notif_type_counts=notif_type_counts,
            ooh_pages=ooh_pages,
            bh_pages=bh_pages,
            ooh_pages_days=ooh_pages_days,
            bh_pages_days=bh_pages_days,
            server_count=server_count,
            notif_counts=notif_counts,
            tag_count=tag_count,
            server_colours=server_colours,
            next_report_start=next_report_start,
            next_report_end=next_report_end,
            prev_report_start=prev_report_start,
            prev_report_end=prev_report_end,
        ),
        200,
    )
