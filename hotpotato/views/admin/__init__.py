from hotpotato.views.admin import views
from hotpotato.views.admin._blueprint import blueprint

__all__ = ["views", "blueprint"]
