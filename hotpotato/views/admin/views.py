import pytz
from flask import abort, flash, jsonify, redirect, render_template, request, url_for
from flask_babelex import gettext as _
from flask_wtf import FlaskForm
from passlib.pwd import genphrase
from sqlalchemy.orm import joinedload, lazyload, subqueryload

from hotpotato import permissions, roles, rules, teams, users, util
from hotpotato.headers import csp_inline_style, talisman
from hotpotato.models import Role, Tag, Team, TeamsUsers, User, db, roles_users
from hotpotato.models.teams import EscalationPolicy
from hotpotato.rules.contexts.team import TeamContext
from hotpotato.views._csrf import csrf
from hotpotato.views.admin._blueprint import blueprint
from hotpotato.views.admin.forms import (
    AddRoleForm,
    AddTeamForm,
    AddUserForm,
    PermissionAddForm,
    PermissionRemoveForm,
    PermissionToggleForm,
    TagForm,
    TeamAddForm,
)

#
# Admin routes
#
# These are only available to admins of *Hot Potato*.


@blueprint.route("/", methods=["GET"])
@rules.access_control_list(
    blueprint, rules.PermissionRule("teams.edit"), context=rules.contexts.hotpotato_team
)
def admin(add_team_form=None):
    """
    Base admin page.
    """

    if add_team_form is None:
        add_team_form = AddTeamForm()
    add_user_form = AddUserForm()

    return render_template(
        "admin/admin.html",
        teams=Team.query.options(joinedload("escalates_to"), joinedload("on_call"))
        .order_by("id")
        .all(),
        add_team_form=add_team_form,
        add_user_form=add_user_form,
    )


@blueprint.route("/add_team", methods=["GET", "POST"])
@csrf.exempt
@rules.access_control_list(
    blueprint, rules.PermissionRule("teams.edit"), context=rules.contexts.hotpotato_team
)
def add_team():
    form = AddTeamForm()

    if form.validate_on_submit():
        team = teams.create(
            form.name.data,
            escalates_to_id=(
                form.escalates_to.data if form.escalates_to.data != -1 else None
            ),
            escalation_policy=form.escalation_policy.data,
            timezone=form.timezone.data,
        )
        return redirect(url_for("views_admin.manage_team", _team_id=team.id))
    return admin(add_team_form=form)


# We can't use 'team_id' here because it conflicts with the global 'current_team' parameter.
@blueprint.route("/manage_team/<_team_id>", methods=["GET", "POST"])
@csrf.exempt
@rules.access_control_list(blueprint, rules.PermissionRule("teams.edit"))
@talisman(content_security_policy=csp_inline_style)
def manage_team(_team_id, force_get=False, team_add_form=None, add_role_form=None):
    """
    Manage a team
    """
    get_team_or_error(_team_id, "teams.edit")
    team = Team.query.options(
        joinedload("escalates_to"),
        subqueryload("users"),
        # joinedload("users.primary_team"),
        lazyload("users.primary_team"),
        joinedload("users.roles"),
        subqueryload("roles"),
        joinedload("roles.permissions"),
        joinedload("roles.permissions.permission", innerjoin=True),
    ).get_or_404(_team_id)

    if request.method == "GET" or force_get:
        tag_form = TagForm()
        tag_form.team_id = _team_id
        if team_add_form is None:
            team_add_form = TeamAddForm()

        if add_role_form is None:
            add_role_form = AddRoleForm()
        return render_template(
            "admin/manage_team.html",
            team=team,
            teams=teams.escalatable_teams(_team_id),
            add_role_form=add_role_form,
            permission_add_form=PermissionAddForm(),
            permission_toggle_form=PermissionToggleForm(),
            permission_remove_form=PermissionRemoveForm(),
            tag_form=tag_form,
            EscalationPolicy=EscalationPolicy,
            timezones=pytz.all_timezones,
            team_add_form=team_add_form,
            permissions_list=permissions.DATA_BY_NAME.items(),
        )

    if request.method == "POST":

        form = FlaskForm()

        if form.validate_on_submit():

            f = request.form

            if f["action"] == "team_settings":
                team.escalates_to = (
                    teams.get(f["escalates_to"]) if f["escalates_to"] else None
                )
                team.escalation_policy = EscalationPolicy[f["escalation_policy"]]
                team.timezone = f["timezone"]
                team.escalation_delay_mins = int(f["escalation_delay_mins"])

                db.session.commit()

            if f["action"] == "escalates_to":
                try:
                    team.escalates_to = (
                        teams.get(f["escalates_to"]) if f["escalates_to"] else None
                    )
                except teams.TeamIDError:
                    flash(
                        "The team you are trying to escalate to doesn't exist", "error"
                    )
                db.session.commit()

            if f["action"] == "delete":
                if (
                    TeamsUsers.query.filter_by(team_id=team.id, primary=True).count()
                    != 0
                ):
                    flash(
                        "You can't delete a team if it's the primary team of any of its users.",
                        "error",
                    )
                    return redirect(
                        url_for("views_admin.manage_team", _team_id=team.id)
                    )
                db.session.delete(team)
                db.session.commit()
                return redirect(url_for("views_admin.admin"))

            if f["action"] == "remove_user":
                try:
                    if users.get(f["user"]).primary_team == team:
                        flash(
                            "You can't remove a user from their primary team.", "error"
                        )
                        return redirect(
                            url_for("views_admin.manage_team", _team_id=team.id)
                        )
                except users.UserIDError:
                    flash("User does not exist.", "error")
                    return redirect(
                        url_for("views_admin.manage_team", _team_id=team.id)
                    )
                team.users.remove(users.get(f["user"]))
                db.session.commit()

            if f["action"] == "set_primary":
                try:
                    users.set_primary_team(users.get(f["user"]), team.id)
                except users.UserIDError:
                    flash("User does not exist.", "error")
                    return redirect(
                        url_for("views_admin.manage_team", _team_id=team.id)
                    )
                db.session.commit()

            if f["action"] == "manage_roles":
                # Slightly ugly manipulation here to get the id the roles and if
                # they should be a member.
                roles_ = {
                    k.split("-")[2]: v for k, v in f.items() if k.startswith("role-")
                }
                try:
                    user = users.get(f["user"])
                except users.UserIDError:
                    flash("User does not exist.", "error")
                    return redirect(
                        url_for("views_admin.manage_team", _team_id=team.id)
                    )

                # Remove all roles they were apart of in this team
                # Cockroach doesn't support `DELETE USING` which sqlalchemy
                # generates if we're using a non-subquery delete.
                db.session.query(roles_users).filter(
                    roles_users.c.id.in_(
                        db.session.query(roles_users.c.id)
                        .select_from(roles_users.join(User).join(Role))
                        .filter(User.id == user.id, Role.team_id == _team_id)
                    )
                ).delete(synchronize_session=False)

                # Set new roles
                users.get(f["user"]).roles.extend(
                    [roles.get(role_id) for role_id, member in roles_.items() if member]
                )
                db.session.commit()

        if hasattr(form, "csrf_token") and form.csrf_token.errors:
            flash(_("CSRF error, please try again"), "error")

        if f["action"] == "add_permission":
            form = PermissionAddForm()
            if form.validate_on_submit():
                access = True if form.access.data == "ALLOW" else False
                permission = permissions.get_by_name(form.permission_name.data)
                permission.set_permission(form.role_id.data, access)
                db.session.commit()
                flash(
                    _(
                        "Successfully added permission '%(permission)s' on role",
                        permission=permission.name,
                    ),
                    "success",
                )
            else:
                for f, es in form.errors.items():
                    for e in es:
                        flash(
                            "Error while creating permission: {} - {}".format(f, e),
                            "error",
                        )

        return redirect(url_for("views_admin.manage_team", _team_id=team.id))


@blueprint.route("/manage_team/<_team_id>/add_role", methods=["GET", "POST"])
@rules.access_control_list(blueprint, rules.PermissionRule("teams.edit"))
def add_role(_team_id):
    get_team_or_error(_team_id, "teams.edit")
    form = AddRoleForm()

    if form.validate_on_submit():
        roles.create(team_id=_team_id, description=form.name.data)
        db.session.commit()
        return redirect(url_for("views_admin.manage_team", _team_id=_team_id))
    return manage_team(_team_id, add_role_form=form, force_get=True)


@blueprint.route("/manage_team/<_team_id>/remove_role", methods=["POST"])
@rules.access_control_list(blueprint, rules.PermissionRule("teams.edit"))
def remove_role(_team_id):
    team = get_team_or_error(_team_id, "teams.edit")

    try:
        role = roles.get(request.form["role_id"])
    except (roles.RoleIDError, KeyError):
        flash(_("Unable to remove role"), "error")
        return redirect(url_for("views_admin.manage_team", _team_id=team.id))

    db.session.delete(role)
    db.session.commit()

    return redirect(url_for("views_admin.manage_team", _team_id=team.id))


@blueprint.route("/manage_team/<_team_id>/remove_permission", methods=["GET", "POST"])
@rules.access_control_list(blueprint, rules.PermissionRule("teams.edit"))
def remove_permission(_team_id):
    get_team_or_error(_team_id, "teams.edit")
    form = PermissionRemoveForm()
    if form.validate_on_submit():
        permission = permissions.get_by_name(form.permission_name.data).get_permission(
            form.role_id.data
        )
        if permission:
            db.session.delete(permission)
            db.session.commit()

    return manage_team(_team_id, force_get=True)


@blueprint.route("/manage_team/<_team_id>/toggle_permission", methods=["GET", "POST"])
@rules.access_control_list(blueprint, rules.PermissionRule("teams.edit"))
def toggle_permission(_team_id):
    get_team_or_error(_team_id, "teams.edit")
    form = PermissionToggleForm()
    if form.validate_on_submit():
        permission = permissions.get_by_name(form.permission_name.data).get_permission(
            form.role_id.data
        )
        if permission:
            permission.access = not permission.access
            db.session.commit()

    return manage_team(_team_id, force_get=True)


@blueprint.route("/manage_team/<_team_id>/add_users", methods=["POST"])
@csrf.exempt
@rules.access_control_list(blueprint, rules.PermissionRule("teams.user_add"))
def team_add_users(_team_id):
    """
    Add users to a team.
    """
    team = get_team_or_error(_team_id, "teams.user_add")

    form = TeamAddForm()
    if form.validate_on_submit():
        # There may be a nicer way to do this.
        # It's complicated by the association data.
        teams_users = [
            TeamsUsers(primary=False, user=user, team=team)
            for user in form.users.data
            if user not in team.users
        ]
        db.session.add_all(teams_users)
        db.session.commit()
        return redirect(url_for("views_admin.manage_team", _team_id=_team_id))

    return manage_team(_team_id, force_get=True, team_add_form=form)


@blueprint.route("/manage_user/add", methods=["POST"])
@csrf.exempt
@rules.access_control_list(
    blueprint,
    rules.PermissionRule("users.create"),
    context=rules.contexts.hotpotato_team,
)
def new_user():
    """
    Create a new user, generating their password and returning it as json.
    """
    form = AddUserForm()

    if form.validate_on_submit():
        # Generate a password for the user.
        password = genphrase(length=6)

        users.create(
            form.email.data,
            password,
            form.name.data,
            form.timezone.data,
            form.language.data,
            teams.get(form.primary_team.data),
        )

        return jsonify({"password": password})

    return (jsonify(form.errors), 400)


@blueprint.route(
    "/manage_team/<int:_team_id>/tags/<int:tag_id>", methods=["GET", "POST"]
)
@rules.access_control_list(blueprint, rules.PermissionRule("teams.edit"))
def edit_tag(_team_id, tag_id):
    """
    Display a form to edit a single tag.
    """
    get_team_or_error(_team_id, "teams.edit")
    tag = Tag.query.get(tag_id)
    if not tag or tag.team_id != _team_id:
        abort(404)
    form = TagForm(obj=tag)
    form.team_id = _team_id
    form.obj = tag
    if form.validate_on_submit():
        form.populate_obj(tag)
        db.session.add(tag)
        db.session.commit()
        return redirect(url_for("views_admin.manage_team", _team_id=_team_id))

    util.flash_errors(form)
    return render_template("tags/get_by_id.html", tag=tag, form=form, _team_id=_team_id)


@blueprint.route("/manage_team/<_team_id>/tags/add", methods=["POST"])
@rules.access_control_list(blueprint, rules.PermissionRule("teams.edit"))
def add_tag(_team_id):
    """
    Process the form to add a tag.
    """
    get_team_or_error(_team_id, "teams.edit")
    form = TagForm()
    form.team_id = _team_id
    if form.validate_on_submit():
        tag = Tag(name=form.name.data, team_id=_team_id)
        db.session.add(tag)
        db.session.commit()

    util.flash_errors(form)
    return redirect(url_for("views_admin.manage_team", _team_id=_team_id))


def get_team_or_error(team_id, permission):
    team = Team.query.get(team_id)
    if not team and rules.test_rules(
        rules.PermissionRule(permission), context=rules.contexts.hotpotato_team
    ):
        abort(404)
    if not rules.test_rules(
        rules.PermissionRule(permission), context=TeamContext(team=team)
    ) and not rules.test_rules(
        rules.PermissionRule(permission), context=rules.contexts.hotpotato_team
    ):
        abort(rules.access_denied())
    return team
