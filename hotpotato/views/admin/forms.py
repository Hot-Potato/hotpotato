import pytz
import wtforms
from flask_babelex import gettext as _
from flask_security import current_user
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, ValidationError
from wtforms.widgets import HiddenInput

from hotpotato import locale, permissions, teams, users
from hotpotato.models import Tag
from hotpotato.models.teams import EscalationPolicy


def coerce_escalation_policy(x):
    if isinstance(x, str):
        return EscalationPolicy[x.split(".")[1]] if x else EscalationPolicy.Never
    else:
        return x


class AddTeamForm(FlaskForm):
    name = wtforms.StringField("Team name", validators=[DataRequired()])
    escalates_to = wtforms.SelectField("Escalates to", coerce=int)
    timezone = wtforms.SelectField(
        "Timezone",
        choices=[(timezone, timezone) for timezone in pytz.all_timezones],
        validators=[DataRequired()],
    )
    escalation_policy = wtforms.SelectField(
        "Escalation policy",
        choices=[
            (EscalationPolicy.Never, "Never"),
            (EscalationPolicy.Always, "Always"),
            (EscalationPolicy.OutsideBusinessHours, "Outside business hours"),
        ],
        coerce=coerce_escalation_policy,
    )

    def __init__(self, **kwargs):
        super(AddTeamForm, self).__init__(**kwargs)
        team_list = [(-1, "None")]
        team_list.extend(((team.id, team.name) for team in teams.all()))
        self.escalates_to.choices = team_list

    def validate_name(self, field):
        if teams.get_by(name=self.name.data):
            raise ValidationError("A team with this name already exists.")


class AddRoleForm(FlaskForm):

    name = wtforms.StringField(_("Role name"), validators=[DataRequired()])


class AddUserForm(FlaskForm):
    name = wtforms.StringField("Name", validators=[DataRequired()])
    email = wtforms.StringField("Email", validators=[DataRequired()])
    timezone = wtforms.SelectField(
        "Timezone",
        choices=[(timezone, timezone) for timezone in pytz.all_timezones],
        validators=[DataRequired()],
    )
    language = wtforms.SelectField(_("Language"), validators=[DataRequired()])
    primary_team = wtforms.SelectField(
        "Primary Team", validators=[DataRequired()], coerce=int
    )

    def __init__(self, **kwargs):
        super(AddUserForm, self).__init__(**kwargs)
        self.primary_team.choices = [(team.id, team.name) for team in teams.all()]
        if self.timezone.data is None:
            self.timezone.data = current_user.timezone
        self.language.choices = [
            (translation.language, translation.display_name)
            for translation in locale.babel.list_translations()
        ]
        if self.language.data is None:
            self.language.data = current_user.language

    def validate_email(self, field):
        try:
            users.get_by_email(self.email.data)
            raise ValidationError("A user with this email address already exists.")
        except users.UserEmailError:
            pass  # If we get to here the email is not in use and we can use it


class TagForm(FlaskForm):
    name = wtforms.StringField("Name", validators=[DataRequired()])

    def validate_name(self, field):
        if hasattr(self, "obj") and self.obj.name == field.data:
            return
        if Tag.query.filter_by(name=field.data, team_id=self.team_id).first():
            raise ValidationError("Tag with that name already exists")


class TeamAddSelectMultipleField(wtforms.SelectMultipleField):
    def pre_validate(self, form):
        """per_validation is disabled"""

    def process_formdata(self, valuelist):
        try:
            _users = [users.get(id) for id in valuelist]
        except users.UserIDError:
            raise ValueError(_("User does not exist"))
        self.data = _users


class TeamAddForm(FlaskForm):
    users = TeamAddSelectMultipleField(_("Users"), choices=[])


class PermissionForm(FlaskForm):
    permission_name = wtforms.SelectField(
        _("Permission"),
        widget=HiddenInput(),
        choices=[
            (permission, permission) for permission in permissions.DATA_BY_NAME.keys()
        ],
        validators=[DataRequired()],
    )
    role_id = wtforms.IntegerField(
        _("Role ID"), widget=HiddenInput(), validators=[DataRequired()]
    )


class PermissionAddForm(PermissionForm):
    access = wtforms.SelectField(
        _("Access"),
        choices=[("ALLOW", _("ALLOW")), ("DENY", _("DENY"))],
        validators=[DataRequired()],
    )


class PermissionToggleForm(PermissionForm):
    pass


class PermissionRemoveForm(PermissionForm):
    pass
