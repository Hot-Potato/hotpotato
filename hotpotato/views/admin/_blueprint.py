"""
Admin views blueprint.
"""


import flask

blueprint = flask.Blueprint("views_admin", __name__)
