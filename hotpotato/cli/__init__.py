"""
Command line interface (CLI) command groups and functions.
"""


from hotpotato.cli import database  # noqa
from hotpotato.cli.config import config
from hotpotato.cli.contact import contact
from hotpotato.cli.cron import cron
from hotpotato.cli.deprecated import alert, heartbeat
from hotpotato.cli.notification import notification
from hotpotato.cli.server import server
from hotpotato.cli.test import test
from hotpotato.cli.user import user

# The end_to_end code is not installed in production environments
try:
    import hotpotato.cli.end_to_end  # noqa
except ImportError:
    pass


def init_app(app):
    """
    Initialise the Flask app CLI with Hot Potato-related commands.
    """

    app.cli.add_command(config)
    app.cli.add_command(notification)
    app.cli.add_command(cron)
    app.cli.add_command(server)
    if app.config["DEBUG"]:
        app.cli.add_command(test)
    app.cli.add_command(user)
    app.cli.add_command(alert)
    app.cli.add_command(heartbeat)
    app.cli.add_command(contact)

    # Disable the Flask-Security built-in user and role management commands,
    # because we have Hot Potato ones built into the CLI.
    app.config["SECURITY_CLI_USERS_NAME"] = False
    app.config["SECURITY_CLI_ROLES_NAME"] = False
