import click
import flask
import flask.cli

from hotpotato import oncall_contacts, users


@click.group("contact", cls=flask.cli.AppGroup)
def contact():
    """
    Contact management.
    """

    pass


@contact.command("create")
@click.argument("email", type=click.STRING)
@click.argument("method", type=click.Choice(oncall_contacts.METHOD))
@click.argument("contact", type=click.STRING)
@click.option("-p", "--priority", type=click.INT)
@click.option("--send-pages", is_flag=True)
@click.option("--send-failures", is_flag=True)
def create(email, method, contact, priority, send_pages, send_failures):
    """
    Add a contact method.
    """

    try:
        user = users.get_by_email(email)
    except users.UserEmailError:
        raise click.UsageError("Email address not found")

    oncall_contacts.create(
        user.id, method, contact, priority, send_pages, send_failures
    )
