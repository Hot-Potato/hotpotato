# OLD DEPRECATED COMMANDS SHOULD NOT BE USED WILL BE REMOVE IN 0.9.4
import click
import flask

from hotpotato.cli.cron import (
    check_failed_alerts_internal,
    check_missed_hbeats_internal,
)
from hotpotato.cli.server import server
from hotpotato.heartbeats import delete_old as _delete_old


@click.group("alert", cls=flask.cli.AppGroup, hidden=True)
def alert():
    """
    Alert commands.
    """

    pass


@alert.command("check-failed")
def check_failed():
    """
    Run the check for failed alerts.
    """

    check_failed_alerts_internal()


@click.group("heartbeat", cls=flask.cli.AppGroup, hidden=True)
def heartbeat():
    """
    Heartbeat commands.
    """

    pass


@heartbeat.command("delete-old")
def delete_old():
    """
    Purge the heartbeats table of heartbeats older than a day.
    """

    _delete_old()


@server.command("check-missed-hbeats", hidden=True)
def check_missed_hbeats():
    """
    Run the check for missed heartbeats.
    """
    check_missed_hbeats_internal()
