from end_to_end.harness import end_to_end
from hotpotato.cli.test import test


@test.command("end-to-end", hidden=True)
def end_to_end_cli():
    end_to_end()
