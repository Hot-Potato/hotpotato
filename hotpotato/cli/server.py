"""
Command line interface (CLI) server functions.
"""


import click
import flask.cli
import pytz

from hotpotato import servers


@click.group("server", cls=flask.cli.AppGroup)
def server():
    """
    Server commands.
    """

    pass


@server.command("get-monitored")
def get_monitored():
    """
    List all servers currently being monitored.
    """

    for serv in servers.get_monitored():
        click.echo(serv.hostname)


@server.command("create")
@click.argument("hostname", type=click.STRING)
@click.argument("link", type=click.STRING)
@click.argument("timezone", type=click.STRING)
@click.option("--missed-heartbeat-limit", default=5)
@click.option("--disable-missed-heartbeat-notif", is_flag=True)
@click.option(
    "--api-key", type=click.STRING, help="If not set an api key will be generated"
)
def create_server(
    hostname,
    link,
    timezone,
    missed_heartbeat_limit,
    disable_missed_heartbeat_notif,
    api_key,
):
    """
    Create a new monitoring server.

    The API key is written to STDOUT.
    """
    if timezone not in pytz.all_timezones:
        raise click.UsageError("Unknown timezone")

    if missed_heartbeat_limit < 0:
        raise click.UsageError("Missed heartbeat limit must be positive")

    try:
        _, apikey = servers.create(
            hostname,
            timezone,
            link,
            disable_missed_heartbeat_notif,
            missed_heartbeat_limit,
            apikey_tuple=api_key.strip().split(".") if api_key else None,
        )
    except servers.ServerCreateError as e:
        raise click.UsageError(e)

    click.echo(apikey)
