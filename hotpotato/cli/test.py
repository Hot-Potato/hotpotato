"""
Command line interface (CLI) test functions.
"""

import click
import flask

from hotpotato import tests


@click.group("test", cls=flask.cli.AppGroup)
def test():
    """
    Commands useful in a dev environment.
    """
    pass


@test.command("create-data")
@click.option("--number", default=100, help="Number of each object to generate")
@click.option("--seed", default=None, help="Seed to use for the RNG")
def create_data(number, seed):
    """
    Fill the database with test data.
    """

    click.echo("Generating test data, please wait...")
    tests.create_data(num=number, seed=seed)
