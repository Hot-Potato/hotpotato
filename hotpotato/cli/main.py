import click
from flask.cli import FlaskGroup

from hotpotato import __version__
from hotpotato.app import create


@click.version_option(__version__)
@click.group(
    cls=FlaskGroup,
    create_app=create,
    add_version_option=False,
    add_default_commands=False,
)
def cli():
    """Hot Potato CLI commands"""
