"""
Command line interface (CLI) Hot Potato commands.
"""


import json

import click
from flask import current_app
from flask.cli import with_appcontext


@click.command("config")
@with_appcontext
def config():
    """
    Show Hot Potato configuration.
    """

    click.echo(
        json.dumps(
            {k: str(v) for k, v in current_app.config.items()}, indent=2, sort_keys=True
        )
    )
