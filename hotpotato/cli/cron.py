"""
Command line interface (CLI) cron functions.
"""


from datetime import datetime

import click
import flask.cli
from flask import current_app

from hotpotato import servers, teams, util
from hotpotato.heartbeats import delete_old as _delete_old
from hotpotato.models import OncallContact, TeamsUsers, User, db
from hotpotato.notifications import alerts, exceptions, messages


@click.group("cron", cls=flask.cli.AppGroup)
def cron():
    """
    Cron commands.
    """

    pass


@cron.command("delete-old-hbeats")
def delete_old_hbeats():
    """
    Purge the heartbeats table of heartbeats older than a day.
    """

    _delete_old()


@cron.command("check-missed-hbeats")
def check_missed_hbeats():
    """
    Run the check for missed heartbeats.
    """
    check_missed_hbeats_internal()


def check_missed_hbeats_internal():

    current_app.config["SERVER_NAME"] = current_app.config["HOTPOTATO_WEBUI_URL"]

    servs = servers.get_missed_hbeats()

    click.echo("There are {} missed heartbeats".format(len(servs)))

    if not servs:
        return

    oncall_user = teams.hotpotato_team().on_call
    team_id = teams.hotpotato_team().id

    if oncall_user is not None:
        click.echo("Paging on-call person {}".format(oncall_user.name))
    else:
        click.echo("WARNING: No on-call person will not be able to send alert")

    for serv in servs:
        body = "{}: HOTPOTATO002 Intervention required, {} ".format(
            util.node_name, serv.hostname
        )

        now = datetime.now()

        if serv.last_hbeat:
            body += "has not sent heartbeats for the last {} seconds ".format(
                int((now - serv.last_hbeat).total_seconds())
            )
        else:
            body += "has never sent a heartbeat "

        body += "(threshold is {} heartbeats, or {} seconds)".format(
            serv.missed_heartbeat_limit,
            int(servers.HBEAT_FREQ * serv.missed_heartbeat_limit),
        )

        click.echo(body)

        # TODO: replace message with alert of some kind
        try:
            message = messages.create(team_id=team_id, body=body)
            if oncall_user is not None:
                message.send(oncall_user.id, run_async=False)
        except exceptions.NotificationSendError as err:
            click.echo(
                "Failed to send failed notification to on-call person: {}".format(err)
            )


@cron.command("check-failed-alerts")
def check_failed_alerts():
    """
    Run the check for failed alerts.
    """
    check_failed_alerts_internal()


def check_failed_alerts_internal():

    current_app.config["SERVER_NAME"] = current_app.config["HOTPOTATO_WEBUI_URL"]

    num_failed_ales = alerts.get_num_failed()

    click.echo("There are {} failed alerts".format(num_failed_ales))

    if num_failed_ales:
        body = (
            "{}: TATER001 Intervention required, "
            "there are {} alerts that failed to page"
        ).format(util.node_name, num_failed_ales)

        click.echo(body)

        team = teams.hotpotato_team()
        oncall_user = team.on_call

        click.echo("Paging on-call person {}".format(oncall_user.name))
        # WARNING: This assignment is required, otherwise the team object gets
        # expired even though it is not modified.
        team_id = team.id
        message = messages.create(team_id=team_id, body=body)
        try:
            message.send(run_async=False, user_id=oncall_user.id)
        except exceptions.NotificationSendError as err:
            click.echo(
                "Failed to send failed notification to on-call person: {}".format(err)
            )
        # This is required, otherwise the message gets removed from the session
        # after it is sent. Why this occurs here and not elsewhere I do not know.
        db.session.add(message)

        # Notify all the people that wanted to know.
        contacts = (
            OncallContact.query.filter_by(send_failures=True)
            .join(User, TeamsUsers)
            .filter(TeamsUsers.team_id == team_id)
        )
        for contact in contacts:
            # As above, the contact is removed from the session if this is not done.
            db.session.add(contact)
            click.echo(
                "Notifying {} of the failure via {}".format(
                    contact.contact, contact.method
                )
            )
            try:
                message.send(contact.user_id, contact=contact, run_async=False)
                db.session.add(message)
            except exceptions.NotificationSendError as e:
                click.echo(
                    "Failed to notify {} of the failure: {}".format(contact.contact, e)
                )
