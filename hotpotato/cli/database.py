"""
Command line interface to create database.
"""


import click
import flask
import flask.cli
from flask.cli import with_appcontext
from flask_migrate.cli import db

from hotpotato import models, roles, teams, users


@db.command("create")
@with_appcontext
def create():
    """
    Create the database
    """
    models.create(flask.current_app)
    models.initialise(flask.current_app)


@db.command("create-dev")
@click.option(
    "--recreate", is_flag=True, help="Recreate the database if it does not exist"
)
@with_appcontext
def create_dev(recreate):
    """
    Create the database, with test users set up
    """
    models.create(flask.current_app)
    if recreate:
        models.reinitialise(flask.current_app)
    else:
        models.initialise(flask.current_app)

    with flask.current_app.app_context():
        team = teams.hotpotato_team()
        user_test1 = users.create(
            "test1@example.com", "test_password1", "Test User 1", "UTC", "en", team
        )
        user_test2 = users.create(
            "test2@example.com", "test_password2", "Test User 2", "UTC", "en", team
        )

        role_admin = roles.get_by_name(team.id, "admin")
        user_test1.add_role(role_admin)
        user_test2.add_role(role_admin)
        models.db.session.commit()
