"""
Command line interface for adding and removing users.
"""


import click
import flask
import flask.cli
import pytz
from sqlalchemy.orm.exc import NoResultFound

from hotpotato import locale, roles, teams, users
from hotpotato.models import db, user_datastore
from hotpotato.notifications import handovers


@click.group("user", cls=flask.cli.AppGroup)
def user():
    """
    User management.
    """

    pass


@user.command("create")
@click.argument("team_id", type=click.INT)
@click.argument("name", type=click.STRING)
@click.argument("email", type=click.STRING)
@click.option("-t", "--timezone", default="UTC")
@click.option("-l", "--language", default="en")
@click.option("-na", "--not-active", is_flag=True)
@click.option("-A", "--admin", is_flag=True)
@click.option("-o", "--oncall", is_flag=True)
@click.password_option()
def create(
    team_id,
    name,
    email,
    password,
    timezone,
    language="en",
    not_active=True,
    admin=False,
    oncall=False,
):
    """
    Create a user.
    """

    if timezone not in pytz.all_timezones:
        raise click.UsageError("Unknown timezone")

    if language not in [
        translation.language for translation in locale.babel.list_translations()
    ]:
        raise click.UsageError("Unknown language")

    try:
        team = teams.get((int(team_id)))
    except teams.TeamIDError:
        raise click.BadParameter("Team not found.")

    try:
        new_user = users.create(
            email, password, name, timezone, language, team, not not_active
        )
    except users.DuplicateEmailError:
        raise click.BadParameter("A user with this email already exists.")

    if admin and oncall:
        raise click.BadParameter(
            "'--admin' and '--oncall' are mutually exclusive parameters."
        )
    if admin:
        role = roles.get_by_name(int(team_id), "admin")
        new_user.add_role(role)
    elif oncall:
        role = roles.get_by_name(int(team_id), "oncall")
        new_user.add_role(role)
    else:
        role = roles.get_by_name(int(team_id), "guest")
        new_user.add_role(role)

    db.session.commit()
    click.echo("Created new user with email {}.".format(new_user.email))


@user.command("deactivate")
@click.argument("email", type=click.STRING)
def deactivate(email):
    """
    Deactivate a user.
    """
    try:
        user = users.get_by_email(email)
    except users.UserEmailError:
        raise click.UsageError("User does not exist")

    if user_datastore.deactivate_user(user):
        user_datastore.commit()
        click.echo("Deactivated user with email {}.".format(user.email))
    else:
        click.echo("User with email {} already deactivated.".format(user.email))


@user.command("activate")
@click.argument("email", type=click.STRING)
def activate(email):
    """
    Activate a user.
    """
    try:
        user = users.get_by_email(email)
    except users.UserEmailError:
        raise click.UsageError("User does not exist")

    if user_datastore.activate_user(user):
        user_datastore.commit()
        click.echo("Activated user with email {}.".format(user.email))
    else:
        click.echo("User with email {} already activated.".format(user.email))


@user.command("take-pager")
@click.argument("email", type=click.STRING)
@click.argument("team-name", type=click.STRING)
@click.option(
    "--message",
    type=click.STRING,
    default=None,
    help="Optional message which will be sent in the handover notification",
)
def take_pager(email, team_name, message):
    """
    Take the pager on a given team.

    The user must have at least one contact method.
    """
    try:
        to_user = users.get_by_email(email)
    except users.UserEmailError:
        raise click.UsageError("User does not exist")

    try:
        team = teams.Model.query.filter_by(name=team_name).one()
    except NoResultFound:
        raise click.UsageError("Team does not exist")

    try:
        handovers.handover(team, to_user, from_user=team.on_call, message=message)
    except handovers.NoContactMethodsError:
        raise click.UsageError(
            "User cannot take the pager because they have no contact methods"
        )
