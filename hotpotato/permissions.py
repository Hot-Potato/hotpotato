"""
Action data and functions.
"""


from functools import wraps

from flask import current_app
from flask_babelex import gettext as _
from flask_security import current_user
from flask_security.decorators import _get_unauthorized_view
from werkzeug.datastructures import ImmutableDict

from hotpotato.models import Permission as Model
from hotpotato.rules import contexts
from hotpotato.rules.login import LoginRule
from hotpotato.teams import current_team, hotpotato_team


class PermissionsError(Exception):
    """
    Action exception base class.
    """

    pass


class PermissionsNameError(PermissionsError):
    """
    Exception for invalid permission name.
    """

    def __init__(self, name):
        self.name = name
        super().__init__(
            _(
                "Invalid permission name (or unable to find permission with name): %(name)s",
                name=name,
            )
        )


def login_required(fn):
    """
    Replacement for flask_security's login_required decorator. This checks that
    the user is in the current team.
    """

    @wraps(fn)
    def decorated_view(*args, **kwargs):
        if LoginRule().test(contexts.team):
            return fn(*args, **kwargs)
        else:
            return current_app.login_manager.unauthorized()

    return decorated_view


def roles_accepted(*roles, hotpotato=False):
    """
    Replacement for flask_security's roles_accepted decorator to take into account teams.
    """

    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if hotpotato:
                user_roles = [
                    r for r in current_user.roles if r.team_id == hotpotato_team().id
                ]
            else:
                user_roles = [
                    r for r in current_user.roles if r.team_id == current_team.id
                ]

            for role in roles:
                if role in user_roles:
                    return fn(*args, **kwargs)

            if hasattr(
                current_app.extensions["security"], "_get_unauthorized_callback"
            ):
                return current_app.extensions["security"]._get_unauthorized_callback()
            return _get_unauthorized_view()

        return decorated_view

    return wrapper


def get_by_name(name):
    """
    Get a permission object using the given name, and return it.
    """

    permission = Model.query.filter_by(name=name).first()
    if not permission:
        raise PermissionsNameError(name)
    return permission


def permission_name_get(module_name, permission_key):
    """
    Generate a permission name from its module name and permission key.
    """

    return "{}.{}".format(module_name, permission_key)


# Data structures which stores all the permission types and data associated with them.
# Put after the function definitions because it uses those very functions.
DATA = ImmutableDict(
    {
        "handover": ImmutableDict(
            {"do": ImmutableDict({"description": _("Perform a handover")})}
        ),
        "messages": ImmutableDict(
            {"send": {"description": _("Send a message to the on-call persion")}}
        ),
        "notifications": ImmutableDict(
            {
                "get": ImmutableDict(
                    {"description": _("Get and search for notifications")}
                ),
                "acknowledge": ImmutableDict(
                    {"description": _("Acknowledge a notification that failed to send")}
                ),
            }
        ),
        "servers": ImmutableDict(
            {
                "get": ImmutableDict({"description": _("Get the status of servers")}),
                "add": ImmutableDict({"description": _("Add a new server")}),
                "heartbeats_toggle": ImmutableDict(
                    {"description": _("Turn on or off heartbeats for a server")}
                ),
            }
        ),
        "teams": ImmutableDict(
            {
                "create": ImmutableDict({"description": _("Create a new team")}),
                "edit": ImmutableDict({"description": _("Edit an existing team")}),
                "user_add": ImmutableDict({"description": _("Add users to a team")}),
            }
        ),
        "users": ImmutableDict(
            {
                "create": ImmutableDict({"description": _("Create a new user")}),
                "get": ImmutableDict({"description": _("View the details of a user")}),
                "toggle_active": ImmutableDict(
                    {"description": _("Activate or deactivate a user account")}
                ),
            }
        ),
    }
)

DATA_BY_NAME = {}
for module_name, module_data in DATA.items():
    for permission_key, permission_data in module_data.items():
        DATA_BY_NAME[permission_name_get(module_name, permission_key)] = permission_data
DATA_BY_NAME = ImmutableDict(DATA_BY_NAME)
