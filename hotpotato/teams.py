"""
Team related functions.
Warning: contains magic.
"""


import datetime
from functools import wraps
from urllib.parse import urlencode

import pytz
import sqlalchemy
import workalendar
from flask import abort, current_app, g, redirect, request
from flask_security import current_user
from sqlalchemy.exc import IntegrityError
from sqlalchemy.sql.expression import cast
from werkzeug.local import LocalProxy

from hotpotato import roles
from hotpotato.models import (
    Config,
    Escalation,
    Permission,
    RolePermission,
    Team as Model,
    db,
)
from hotpotato.notifications import alerts

HOTPOTATO_TEAM_NAME = "Hot Potato"


class TeamError(Exception):
    pass


class DuplicateTeamName(TeamError):
    pass


class TeamIDError(TeamError):
    pass


def hotpotato_team():
    try:
        return g.hotpotato_team
    except AttributeError:
        g.hotpotato_team = Model.query.filter(
            Model.id == cast(Config.value, sqlalchemy.Integer),
            Config.key == "hotpotato_team_id",
        ).one()
        return g.hotpotato_team


def get(team_id):

    try:
        return Model.query.filter_by(id=team_id).one()
    except (sqlalchemy.orm.exc.NoResultFound, sqlalchemy.exc.DataError):
        raise TeamIDError()


def get_by(**kwargs):
    """
    Return a tuple containing all team objects matching the given search parameters.
    """

    return Model.query.filter_by(**kwargs).all()


def all():
    return Model.query.all()


def _get_team():
    """
    Try to get the current team from the request url, redirect if it's not
    found and there is a user logged in.
    """

    # TODO: Should we do permissions checks here?

    if "current_team" not in g:
        team_id = request.args.get("team_id")

        # Redirect if team_id not found
        if not team_id:
            if (
                current_user.is_anonymous
                or request.method != "GET"
                or request.path.startswith("/api")
            ):
                return None

            new = request.args.copy()
            new["team_id"] = current_user.primary_team.id
            abort(redirect("{}?{}".format(request.path, urlencode(new))))

        try:
            g.current_team = get(team_id)
        except TeamIDError:
            if current_user.is_anonymous:
                return None
            abort(404)

    return g.current_team


def team_injector(route, args):
    """
    This inserts the team_id into the route parameters if it is not already set.
    If the route takes team_id as a paramater it will be set to this value, otherwise
    it will be passed as a query parameter.
    """

    # flask-debugtoolbar doesn't use url_for correctly, so ignore it's routes
    if ("_debug_toolbar" in route) or ("static" in route):
        return

    try:
        args["team_id"] = current_team.id
    # In order: no team_id, no current_team, not in request context (in tests)
    except (KeyError, AttributeError, RuntimeError):
        pass


def team_decorator(fn):
    """
    Inject the team_id into the keyword arguments of the decorated function,
    if it is not already there.

    The resulting decorated view also has the `inner_fn` attribute,
    which gives access to the inner function definition, just in case the
    caller needs it. This is specifically used for testing at the moment.
    """

    @wraps(fn)
    def decorated_view(*args, **kwargs):
        if "team_id" not in kwargs:
            kwargs["team_id"] = current_team
        return fn(*args, **kwargs)

    decorated_view.inner_fn = fn

    return decorated_view


"""
Proxy for the current team.
This is determined from the `team_id` query parameter which is injected into
every route by `team_injector`. If `team_id` is not found we redirect to the
same url with with the current user's primary team id added.
"""
current_team = LocalProxy(_get_team)


def init_app(app):
    """
    Sets up url_for rewriting. See team_injector docstring.
    """

    app.url_defaults(team_injector)


def escalatable_teams(team_id):
    """
    Return teams that `team` is allowed to escalate to.

    TODO: Detect cycles here?
    """

    return Model.query.filter(Model.id != team_id).all()


def create(
    name,
    escalates_to_id=None,
    on_call_id=None,
    escalation_policy=None,
    timezone=None,
    __id=None,
):
    """
    Create a new team, optionally setting escalations and on call person.
    """
    if __id is not None and not current_app.config["DEBUG"]:
        raise RuntimeError("Explicit ID setting is only allowed in debug mode")

    try:
        team = Model(
            id=__id,
            name=name,
            escalates_to_id=escalates_to_id,
            # FIXME: This should check to see if the user has any contact methods
            on_call_id=on_call_id,
            escalation_policy=escalation_policy,
            timezone=timezone,
        )
        db.session.add(team)
        db.session.flush()
    except IntegrityError:
        raise DuplicateTeamName()

    # Add default roles

    roles_list = []
    for role_name, data in roles.DEFAULT.items():
        role = roles.create(
            team_id=team.id,
            name=role_name,
            description=data["description"],
            priority=data["priority"],
        )
        query = db.session.query(
            Permission.id, sqlalchemy.literal(role.id), sqlalchemy.literal("true")
        ).filter(Permission.name.in_(data["permissions"]))
        db.session.execute(
            sqlalchemy.sql.expression.insert(RolePermission).from_select(
                (
                    RolePermission.permission_id,
                    RolePermission.role_id,
                    RolePermission.access,
                ),
                query,
            )
        )
        roles_list.append(role)

    team.roles.extend(roles_list)
    db.session.commit()

    return team


def in_business_hours(team):
    """
    Determine whether a time is in business hours for a given timezone.
    """

    reigon = next(
        (
            country
            for country, timezones in pytz.country_timezones.items()
            if team.pytz_timezone.zone in timezones
        ),
        None,
    )
    if reigon:
        cal = workalendar.registry.registry.get(reigon)()
        date = datetime.datetime.now(team.pytz_timezone).date()
        is_working_day = cal.is_working_day(date)
    else:
        current_app.logger.warning(
            f"Was not able to find reigon for timezone {team.pytz_timezone}, working day calculation disabled."
        )
        is_working_day = True

    business_hours = (9, 17)  # 9-5
    is_business_hours = (
        business_hours[0]
        <= datetime.datetime.now(team.pytz_timezone).hour
        <= business_hours[1]
    )

    return is_working_day and is_business_hours


def should_escalate(notif):
    """
    Determine whether a notification should be escalated.
    This may depend on:
    * Priority
    * Whether it has already been escalated
    * Whether it has been read (by any recipient)

    How long it has been since the notification was read has been checked
    already by the actor, so we don't check it here.
    """
    read = any(attempt.status == "READ_BY_CLIENT" for attempt in notif.send_attempts)
    emergency = notif.json["state"] in alerts.EMERGENCY_STATE
    escalated = len(notif.escalations) > 0

    return (not read) and (not escalated) and emergency


def escalate(notif):
    """
    Escalate the notification.
    This involves writing a database record and then sending the alert to
    the on-call member of the escalated team.
    """
    escalated_to = notif.team.escalates_to
    escalated_to_id = escalated_to.id

    # Team does not escalate
    if not escalated_to:
        return

    if escalated_to.on_call:
        # Send to the on call member of the new team
        # We're already in an actor here so don't bother queueing it
        notif.send(escalated_to.on_call.id, run_async=False, escalating=True)
    else:
        current_app.logger.warning(
            "Could not escalate notification because there was no one on call."
        )

    # Create a record of the escalation
    escalation = Escalation(notification=notif, escalated_to_id=escalated_to_id)
    db.session.add(escalation)

    db.session.commit()
