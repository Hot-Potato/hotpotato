"""
Rule classes and decorators.
"""


from functools import wraps

from flask import current_app, request
from flask_security import current_user
from flask_security.decorators import _get_unauthorized_view

from hotpotato.rules import contexts
from hotpotato.rules.login import LoginRule
from hotpotato.rules.permission import PermissionRule

__all__ = [
    "contexts",
    "PermissionRule",
    "LoginRule",
    "access_control_list",
    "access_denied",
    "has_access_to",
    "login_required",
]

_by_endpoint = {}


def access_control_list(blueprint, *rules, context=contexts.team):
    """
    Decorator for implementing an access control list for routes.
    Go through the supplied list of resource rules, and if the rule can be
    applied to the current user, return the desired result.

    A context can also optionally defined, to change the environment in which
    the rule tests are run.
    The default is a context based on the currently set team..

    If none of the rules apply to the current user, defaults to denying access.
    """

    def wrapper(fn):
        # Cache the context and for the given endpoint, so they can be queried later.
        endpoint_name = "{}.{}".format(blueprint.name, fn.__name__)
        _by_endpoint[endpoint_name] = {"context": context, "rules": tuple(rules)}

        @wraps(fn)
        def decorated_view(*args, **kwargs):
            res = has_access_to(request.endpoint)
            if res is True:
                return fn(*args, **kwargs)
            elif res is False:
                # Access denied because no rules were matched.
                return access_denied()
            else:
                return current_app.login_manager.unauthorized()

        return decorated_view

    return wrapper


def access_denied():
    """
    Helper function for redirecting the user when they're not allowed to access
    a resource.
    """

    if hasattr(current_app.extensions["security"], "_get_unauthorized_callback"):
        return current_app.extensions["security"]._get_unauthorized_callback()
    return _get_unauthorized_view()


def get_context():
    """
    Get the rule context for the current endpoint.

    Returns None if there is no context available for the given endpoint.
    """

    return (
        _by_endpoint[request.endpoint]["context"]
        if request.endpoint in _by_endpoint
        else None
    )


def has_access_to(endpoint_name):
    """
    Get the access permission for the given endpoint name according to
    the relevant rules and context.

    Returns:
    * True - Logged in, access approved
    * False - Logged in, access denied
    * None - Not logged in
    """

    context = _by_endpoint[endpoint_name]["context"]
    rules = _by_endpoint[endpoint_name]["rules"]

    # Check if the user is logged in, first.
    # Dump them to a login page if they aren't.
    if not LoginRule().test(context):
        current_app.logger.debug(
            "Access denied for anonymous or deactivated user, please log in"
        )
        return None

    return test_rules(*rules, context=context)


def test_rules(*rules, context=contexts.team):
    """
    Tests if the user has access to the given rules and context

    Returns:
    * True - Logged in, access approved
    * False - Logged in, access denied
    * None - Not logged in
    """

    # Check if the session has access to the given context.
    if context.test():
        current_app.logger.debug(
            "Access approved for user '{}' to context '{}', "
            "running rule tests".format(current_user.name, type(context).__name__)
        )
    else:
        current_app.logger.debug(
            "Access denied for user '{}' to context '{}'".format(
                current_user.name, type(context).__name__
            )
        )
        return False

    # Run through the access control list.
    for rule in rules:
        allowed = rule.test(context)
        if allowed is not None:
            if allowed:
                return True
            else:
                return False
    return False


def login_required(fn):
    """
    Replacement for flask_security's login_required decorator. This checks that
    the user is in the current team.
    """

    @wraps(fn)
    def decorated_view(*args, **kwargs):
        if LoginRule().test(contexts.team):
            return fn(*args, **kwargs)
        else:
            return access_denied()

    return decorated_view
