"""
Permission-based rule.
"""


from hotpotato import permissions
from hotpotato.rules.base import Rule


class PermissionRule(Rule):
    """
    Permission-based rule.

    Allows a user, role (from a team) or a team as a whole to be allowed (or denied)
    access to a specific resource, storing the permission in the database.
    """

    def __init__(self, permission_name, *methods):
        """
        Permission-based rule constructor.
        """

        if permission_name not in permissions.DATA_BY_NAME:
            raise permissions.PermissionsNameError(permission_name)

        self.permission_name = permission_name
        super().__init__(*methods)

    def test(self, context):
        """
        Tests the given access context against this rule.
        It checks if there is a match for this rule against the context,
        and whether or not this context is allowed access to the resource.

        Returns:
        * True - Match, access approved
        * False - Match, access denied
        * None - No match
        """

        return (
            permissions.get_by_name(self.permission_name).test(
                user=context.user, team=context.team
            )
            if self.test_method()
            else None
        )
