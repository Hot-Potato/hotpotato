"""
Rule base classes.
"""


import flask


class Rule:
    """
    Rule base class.
    """

    def __init__(self, *methods):
        """
        Rule base constructor.
        """

        self.methods = methods

    def test_method(self):
        """
        Helper method for rules, which tests if the current Flask session
        matches the methods defined in this rule.

        Returns:
        * True - no allowed methods defined, or the current method is in the
                 list of allowed methods
        * None - the current method is not in the list of allowed method
        """

        if self.methods:
            if flask.request.method not in self.methods:
                return None
        return True

    def test(self, context):
        """
        Tests the given access context against this rule.
        It checks if there is a match for this rule against the context,
        and whether or not this context is allowed access to the resource.

        * True - Match, access approved
        * False - Match, access denied
        * None - No match
        """

        raise NotImplementedError()
