"""
Hot Potato team-based context.
"""


from flask_security import current_user

from hotpotato import teams
from hotpotato.rules.contexts.base import Context


class HotpotatoTeamContext(Context):
    """
    Hot Potato team-based context.

    Gives access if the user is in the Hot Potato team.
    """

    def test(self):
        """
        Tests whether the current session is allowed to access this context.

        * True - Access approved
        * False - Access denied
        """

        return (
            True
            if teams.hotpotato_team() in current_user.teams
            and teams.current_team == teams.hotpotato_team()
            else False
        )

    @property
    def user(self):
        """
        Return the user associated with this context.
        """

        return current_user

    @property
    def team(self):
        """
        Return the team associated with this context.
        """

        return teams.hotpotato_team()
