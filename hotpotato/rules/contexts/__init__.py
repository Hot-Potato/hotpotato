"""
Contexts for rules.
"""


from hotpotato.rules.contexts.hotpotato_team import HotpotatoTeamContext
from hotpotato.rules.contexts.team import TeamContext
from hotpotato.rules.contexts.team_and_escalates_to import TeamAndEscalatesToContext

__all__ = ["hotpotato_team", "team", "team_and_escalates_to"]

hotpotato_team = HotpotatoTeamContext()
team = TeamContext()
team_and_escalates_to = TeamAndEscalatesToContext()
