"""
Team-based context.
"""


from flask_security import current_user

from hotpotato.rules.contexts.base import Context
from hotpotato.teams import current_team


class TeamContext(Context):
    """
    Team-based context.

    Gives access when the user is in the current team.
    """

    def __init__(self, team=None):
        if team is not None:
            self._team = team
        else:
            self._team = current_team

    def test(self):
        """
        Tests whether the current session is allowed to access this context.

        * True - Access approved
        * False - Access denied
        """

        return True if self._team in current_user.teams else False

    @property
    def user(self):
        """
        Return the user associated with this context.
        """

        return current_user

    @property
    def team(self):
        """
        Return the team associated with this context.
        """

        return self._team
