"""
Team and escalates_to-based context.
"""


from flask_security import current_user

from hotpotato.rules.contexts.base import Context
from hotpotato.teams import current_team


class TeamAndEscalatesToContext(Context):
    """
    Team and escalates_to-based context.

    Gives access when the user is in the current team, or the current team is one
    that escalates to the user.
    """

    def test(self):
        """
        Tests whether the current session is allowed to access this context.

        * True - Access approved
        * False - Access denied
        """

        return True if self.team else False

    @property
    def user(self):
        """
        Return the user associated with this context.
        """

        return current_user

    @property
    def team(self):
        """
        Return the team associated with this context.
        """

        return self._test(current_user, current_team) if current_team else None

    def _test(self, user, team):
        """
        Recursive helper method for traversing the team tree, to find the
        team to use in this context.
        """

        # Positive base case.
        if team in user.teams:
            return team
        # Traverse the tree.
        if team.escalates_to:
            return self._test(user, team.escalates_to)
        # Negative base case.
        return None
