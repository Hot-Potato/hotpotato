"""
Context base class.
"""


class Context:
    """
    Context base class.
    """

    def test(self):
        """
        Tests whether the current session is allowed to access this context.

        * True - Access approved
        * False - Access denied
        """

        raise NotImplementedError()

    @property
    def user(self):
        """
        Return the user associated with this context.
        """

        raise NotImplementedError()

    @property
    def team(self):
        """
        Return the team associated with this context.
        """

        raise NotImplementedError()
