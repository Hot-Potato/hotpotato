"""
Login-based rule.
"""


from hotpotato.rules.base import Rule


class LoginRule(Rule):
    """
    Login-based rule.

    Allows a used access to the resource if they are logged in.
    This is intended ONLY for the user settings pages.
    """

    def __init__(self):
        """
        Login-based rule constructor.
        """

        super().__init__()

    def test(self, context):
        """
        Tests the given access context against this rule.
        It checks if there is a match for this rule against the context,
        and whether or not this context is allowed access to the resource.

        Returns:
        * True - Match, access approved
        * False - Match, access denied
        * None - No match
        """

        if context.user.is_authenticated:
            return True
        return False
