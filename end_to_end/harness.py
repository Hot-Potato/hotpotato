import functools
import multiprocessing
import os
import queue
import sys
import time
from datetime import datetime, timezone
from urllib.parse import urlparse

import click
import requests
import urllib3
from flask import Flask, jsonify, request
from marshmallow import Schema, ValidationError, fields, validate, validates


class Fail:
    def __init__(self, reason):
        self.reason = reason


class Pass:
    pass


class PushoverMessageSchema(Schema):
    """
    https://pushover.net/api#messages
    """

    token = fields.Str(required=True)
    user = fields.Str(required=True)
    message = fields.Str(required=True)
    device = fields.Str()
    title = fields.Str()
    url = fields.Str()
    url_title = fields.Str()
    timestamp = fields.Int()
    priority = fields.Int(validate=validate.Range(-2, 3))
    callback = fields.Str()
    retry = fields.Int(validate=validate.Range(30, min_inclusive=True))
    expire = fields.Int(validate=validate.Range(max=10800, max_inclusive=True))

    @validates("timestamp")
    def validate_timestamp(self, timestamp):
        try:
            datetime.fromtimestamp(timestamp, tz=timezone.utc)
        except (OverflowError, OSError) as e:
            raise ValidationError(f"timestamp error: {e}")

    @validates("url")
    @validates("callback")
    def validate_url(self, url):
        try:
            r = urlparse(url)
            if not all([r.scheme, r.netloc]):
                raise ValidationError("url missing required components")
        except ValueError as e:
            raise ValidationError(e)

    class Meta:
        strict = True


def catchexcept(q):
    """
    Decorator that notifies the main thread of any exceptions that occured, then
    reraises them.
    """

    def decorator(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except Exception as e:
                q.put(e)
                raise e

        return wrapper

    return decorator


def pushover_mock(q, test):
    """
    Start a flask server which mocks the small part of the pushover API used by
    hotpotato.
    """
    pushover = Flask("end_to_end_pushover")

    @pushover.route("/1/apps/limits.json", methods=["GET"])
    @catchexcept(q)
    def limits():
        return test.limits()

    @pushover.route("/1/messages.json", methods=["POST"])
    @catchexcept(q)
    def messages():
        return test.messages()

    # https://github.com/pallets/flask/pull/2781
    os.environ["FLASK_RUN_FROM_CLI"] = "false"
    thread = multiprocessing.Process(
        target=lambda: pushover.run(host="0.0.0.0", port=8020, debug=False), daemon=True
    )
    thread.start()
    return thread


def end_to_end():
    """
    This command coordinates the end to end test of Hot Potato.

    This should only be run from docker-compose.
    """
    # Disable insecure warnings, this is only a test
    urllib3.disable_warnings()

    click.echo("=> Waiting for icinga2")
    while True:
        try:
            r = requests.get(
                "https://icinga2:5665/v1", auth=("root", "icinga"), verify=False
            )
        except requests.exceptions.RequestException:
            time.sleep(1)
            continue
        else:
            break

    click.echo("=> Waiting for Hot Potato")
    while True:
        try:
            r = requests.get("http://e2e-hotpotato:8000", verify=False)
        except requests.exceptions.RequestException:
            time.sleep(1)
            continue
        else:
            break

    click.echo("=> Setup complete, begining test run")

    from end_to_end import basic_tests  # noqa

    for cls in EndToEndTest.__subclasses__():
        click.echo(f"=> Running {cls.__name__}")
        q = multiprocessing.Queue()
        thread = pushover_mock(q, cls(q))

        r = requests.post(
            "https://icinga2:5665/v1/actions/send-custom-notification",
            auth=("root", "icinga"),
            json={
                "type": "Host",
                "author": "icingaadmin",
                "comment": "Test notification",
            },
            headers={"Accept": "application/json"},
            verify=False,
        )
        r.raise_for_status()

        while True:
            try:
                m = q.get_nowait()
            except queue.Empty:
                time.sleep(1)
                continue

            if isinstance(m, Pass):
                click.echo(f"=> Test passed: {cls.__name__}")
                break
            elif isinstance(m, Fail):
                click.echo(f"=> Test failed: {cls.__name__} because {m.reason}")
                sys.exit(1)
            else:
                ValueError("unreachable")

            if not thread.is_alive():
                click.echo(f"=> Test failed: {cls.__name__} because test thread died")
                sys.exit(1)

        thread.terminate()

    click.echo("=> All tests passed")


class EndToEndTest:
    """
    Base test harneses for end to end tests
    """

    timeout_secs = 10

    def __init__(self, q):
        self.q = q

    def result(self, r):
        self.q.put(r)

    def limits(self):
        if not request.args.get("token"):
            self.result(Fail("request to limits API did not contain a token"))

        return jsonify({"limit": 1000, "remaining": 100, "reset": None})

    def messages(self):
        raise Exception("This must be implemented by the test!")
