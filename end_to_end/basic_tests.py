from flask import jsonify, request

from end_to_end.harness import EndToEndTest, Fail, Pass, PushoverMessageSchema


class TestOk(EndToEndTest):
    """
    Test that Hot Potato sends a generic message successfully
    """

    def messages(self):
        data = request.get_json() or request.form
        errors = PushoverMessageSchema().validate(data)
        if errors:
            self.result(Fail(errors))

            return jsonify(
                {"status": 0, "request": "647d2300-702c-4b38-8b2f-d56326ae460b"}
            )

        self.result(Pass())

        return jsonify(
            {
                "status": 1,
                "request": "647d2300-702c-4b38-8b2f-d56326ae460b",
                "receipt": "r" * 30,
            }
        )


class TestMessageRetry(EndToEndTest):
    """
    Test that Hot Potato retries a message if the endpoint fails
    """

    first_response = False

    def messages(self):
        if not self.first_response:
            self.first_response = True
            return jsonify(
                {
                    "status": 0,
                    "errors": ["pushover failed!"],
                    "request": "647d2300-702c-4b38-8b2f-d56326ae460b",
                }
            )

        data = request.get_json() or request.form
        errors = PushoverMessageSchema().validate(data)
        if errors:
            self.result(Fail(errors))

            return jsonify(
                {"status": 0, "request": "647d2300-702c-4b38-8b2f-d56326ae460b"}
            )

        self.result(Pass())

        return jsonify(
            {
                "status": 1,
                "request": "647d2300-702c-4b38-8b2f-d56326ae460b",
                "receipt": "r" * 30,
            }
        )
