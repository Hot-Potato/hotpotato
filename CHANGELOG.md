# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.9.4] - 2020-09-15

### Fixed
- Fix &mdash; showing up on the notifications page

### Changed
- Update dependencies

## [v0.9.3.2] - 2019-12-11

### Fixed

- Display timezones in the user's timezone when copying an alert summary

## [v0.9.3.1] - 2019-12-11

### Fixed

- Fixed an import error which caused Hot Potato to fail to start in production environments

## [v0.9.3] - 2019-12-10

### Added

- Add initial support for escalations [!128](https://gitlab.com/Hot-Potato/hotpotato/merge_requests/128)
- Added a Content Security Policy [!177](https://gitlab.com/Hot-Potato/hotpotato/merge_requests/177)
- Added a CLI command `hotpotato user take-pager` which changes the on call user for a given team.
- Added a CLI command `hotpotato server create` which adds a monitoring server.
- Added a CLI command `hotpotato contact create` which adds a on-call contact to a user.

### Changed

- Tweaked behaviour of the `/api/stats/v1/days_since_alert` endpoint, to increment 24h after the incident rather than at midnight from [Jamie](https://gitlab.com/JJJollyjim)
- Cache static files [!180](https://gitlab.com/Hot-Potato/hotpotato/merge_requests/180)
- Added a tag breakdown to the weekly report [#116](https://gitlab.com/Hot-Potato/hotpotato/issues/116)
- Moved cli commands to be exposed under a hotpotato entrypoint [#122](https://gitlab.com/Hot-Potato/hotpotato/issues/122)
- Moved commands that should be scheduled in cron under a cron subcommand see deprecated section for required changes [#122](https://gitlab.com/Hot-Potato/hotpotato/issues/122)

### Fixed

- Fixed team_id missing from report range selection form [#115](https://gitlab.com/Hot-Potato/hotpotato/issues/115)
- Fix translation files missing in wheel [#118](https://gitlab.com/Hot-Potato/hotpotato/issues/118)
- Fixed timezone handling on notification filters [!174](https://gitlab.com/Hot-Potato/hotpotato/merge_requests/174)

### Deprecated

- `flask heartbeat delete-old` has been moved to `hotpotato cron delete-old-hbeats` the old command will be removed in a later release
- `flask server check-missed-hbeats` has been moved to `hotpotato cron check-missed-hbeats` the old command will be removed in a later release
- `flask alert check-failed` has been moved to `hotpotato cron check-failed-alerts` the old command will be removed in a later release

### Removed

- `flask notification send handover` has been removed. This command sent handover notifications without performing a handover. The replacement command is `hotpotato user take-pager` which correctly updates the database.

## [v0.9.2] - 2019-09-13

### Added

- Added an option for users to display timestamps in 24 hour time from [@rhysmd](https://gitlab.com/rhysmd)
- /api/stats/v1/days_since_alert endpoint that shows the number of days since the last alert was received from [Jamie](https://gitlab.com/JJJollyjim)
- Added a user details page with info on users and the ability to disable users from [@rhysmd](https://gitlab.com/rhysmd)
- Add the ability to catagorise an alert with a pre-defined tag. WIP for more details and to enable see [!26](https://gitlab.com/Hot-Potato/hotpotato/merge_requests/26) from [@rhysmd](https://gitlab.com/rhysmd)
- Automatically build wheels of all dependencies in gitlab ci from [@rhysmd](https://gitlab.com/rhysmd)
- Added a weekly report showing how many notifications have been sent through Hot Potato from [@rhysmd](https://gitlab.com/rhysmd)
- Added the ability to see details when an alert is sent multiple times [@zacps](https://gitlab.com/zacps)
- Added an initial version of Role Based Access Control from [@callum027](https://gitlab.com/Callum027) and [@rhysmd](https://gitlab.com/rhysmd)

### Changed

- Alerts will now only be sent to a users highest priority contact method rather than all a users contact methods.
- You can no longer take the 'tater' if you have no contact methods from [@zacps](https://gitlab.com/zacps)
- CSRF errors now reload the form pre filled out with an error rather than a full page error [@rhysmd](https://gitlab.com/rhysmd)
- Export data now provides timestamps in localtime from [@rhysmd](https://gitlab.com/rhysmd)
- API Keys are now stored in two parts with an identifier and a hashed value. All current keys automatically upgraded [@rhysmd](https://gitlab.com/rhysmd)
- Removed heartbeats enabled server option and left downtime heartbeat alerts [@rhysmd](https://gitlab.com/rhysmd)
- Changed date filters to datetime [@rhysmd](https://gitlab.com/rhysmd)
- Shorten new user password length [@rhysmd](https://gitlab.com/rhysmd)

### Fixed

- heartbeat delete-old command now only keep a days worth of heartbeats from [@rhysmd](https://gitlab.com/rhysmd)
- Export buttons not being filtered to the provided search filters from [@rhysmd](https://gitlab.com/rhysmd)
- Notifications details page was blank from [@rhysmd](https://gitlab.com/rhysmd)

## [v0.9.1] - 2019-03-01

### Added

- Added a pipenv command to automatically generate migrations from [@callum027](https://gitlab.com/Callum027)
- Added priority up and down buttons to contacts list [@rhysmd](https://gitlab.com/rhysmd)

### Changed

- The padding on the notification page has been reduced and colours changed to make it easier to read from [@rhysmd](https://gitlab.com/rhysmd)
- Changed the wording describing contact details on the accounts page to better reflect what is happeneing [@rhysmd](https://gitlab.com/rhysmd)

### Fixed

- Fixed the copy notification link/details buttons from [@zacps](https://gitlab.com/zacps)
- Improved the queries and rendering for the notification page to make it faster from [@zacps](https://gitlab.com/zacps)
- Fixed all forms on the accounts page to only allow editing of your own contact details from [@rhysmd](https://gitlab.com/rhysmd)

### Security

- Fixed insecure use of make_response allowing reflected XSS on 404 pages fix from [@zacps](https://gitlab.com/zacps) reported by Sam Banks

## [v0.9.0] - 2019-02-19

### Added

- Automated migration testing from [@rhysmd](https://gitlab.com/rhysmd)
- Debug sender in testing environment from [@zacps](https://gitlab.com/zacps)
- New CI coverage report from [@zacps](https://gitlab.com/zacps)
- New API tests from [@rhysmd](https://gitlab.com/rhysmd)
- Add a database creation cli command and stamp database with alembic revision on creation from [@rhysmd](https://gitlab.com/rhysmd)
- Add more context to custom message notifications from [@zacps](https://gitlab.com/zacps)
- Add support for teams from [@zacps](https://gitlab.com/zacps)
- Add standard & emergency priority pushover alerts from [@callum027](https://gitlab.com/Callum027)

### Changed

- Replace ModelObject with SQLAlchemy single table inheritance from [@rhysmd](https://gitlab.com/rhysmd)
- Re-factor Docker set-up to simplify maintenance from [@rhysmd](https://gitlab.com/rhysmd)
- Re-factor API notification handling from [@callum027](https://gitlab.com/Callum027)
- Remove rabbitmq and cockroach from the docker container from [@rhysmd](https://gitlab.com/rhysmd)
- Redesign forms and servers page from [@rhysmd](https://gitlab.com/rhysmd)
- Change all POST endpoints to require a CSRF token from [@rhysmd](https://gitlab.com/rhysmd)
- Reduced length of new user passwords

### Fixed

- Fix up issues in test data generation from [@callum027](https://gitlab.com/Callum027)
- Fix output of Modica notification status function from [@rhysmd](https://gitlab.com/rhysmd)
- Fix errors display when no user is currently on pager from [@rhysmd](https://gitlab.com/rhysmd)
- Fix creating a user with the active flag from [@callum027](https://gitlab.com/Callum027)
- Fix loading configuration from a file from [@callum027](https://gitlab.com/Callum027)
