variables:
  HOTPOTATO_FLASK_TESTING: "true"
  HOTPOTATO_FLASK_SECRET_KEY: "my-secure-secrety-secret"
  HOTPOTATO_FLASK_SERVER_NAME: hotpotato:8000
  HOTPOTATO_FLASK_URL_SCHEME: http
  HOTPOTATO_HOTPOTATO_TROUBLECODE_URL: https://wiki.example.com/w/index.php?search={trouble_code}
  HOTPOTATO_COCKROACH_DATABASE: hotpotato
  HOTPOTATO_COCKROACH_SERVER: cockroach
  HOTPOTATO_COCKROACH_PORT: "26257"
  HOTPOTATO_RABBITMQ_SERVER: rabbitmq
  HOTPOTATO_RABBITMQ_PORT: "5672"
  HOTPOTATO_RABBITMQ_USE_SSL: "false"
  HOTPOTATO_RABBITMQ_USERNAME: hotpotato
  HOTPOTATO_RABBITMQ_PASSWORD: hotpotato
  RABBITMQ_DEFAULT_USER: $HOTPOTATO_RABBITMQ_USERNAME
  RABBITMQ_DEFAULT_PASS: $HOTPOTATO_RABBITMQ_PASSWORD
  HOTPOTATO_RABBITMQ_VHOST: "/"
  HOTPOTATO_SECURITY_PASSWORD_SALT: "very-secret-salt"
  HOTPOTATO_MAIL_SUPPRESS_SEND: "true"
  HOTPOTATO_MAIL_DEFAULT_SENDER: "hotpotato-noreply@example.com"
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  PIPENV_CACHE_DIR: $CI_PROJECT_DIR/.cache/pipenv
  PIPENV_PACKAGE_VERSION: 2018.11.26

stages:
  - test
  - build

cache:
  paths:
    - $PIP_CACHE_DIR
    - $PIPENV_CACHE_DIR

#
# Lint jobs.
#

shell-lint:
  image: ubuntu:latest
  tags:
    - docker
  cache: {}
  only:
    - branches
    - tags
    - merge_requests
  before_script:
    - apt-get update
    - apt-get install --no-install-recommends -y shellcheck
  script:
    - cd "${CI_PROJECT_DIR}"
    - find -name '*.sh' -print -exec shellcheck '{}' '+'

eslint:
  image: node:11
  cache: {}
  tags:
    - docker
  only:
    - branches
    - tags
    - merge_requests
  before_script:
    - cd theme
    - npm ci --dev
  script:
    - npm run lint

flake8:
  image: python:3.6
  tags:
    - docker
  only:
    - branches
    - tags
    - merge_requests
  before_script:
    - pip install flake8==3.7.8 flake8-tuple==0.4.0
  script:
    - flake8

black:
  image: python:3.6
  only:
    - branches
    - tags
    - merge_requests
  before_script:
    - pip install black --pre
  script:
    - black --check --diff .

isort:
  image: python:3.6
  only:
    - branches
    - tags
    - merge_requests
  before_script:
    - pip install isort==4.3.21
  script:
    - isort --check-only .

check-requirements:
  image: python:3.6
  tags:
    - docker
  only:
    refs:
      - master
      - requirements-*
      - merge_requests
    variables:
      - $CI_COMMIT_MESSAGE =~ /requirements/
  before_script:
    - pip install pipenv==$PIPENV_PACKAGE_VERSION
    - pipenv sync --dev
  script:
    - pipenv check
    - cp Pipfile.lock Pipfile.old.lock
    - pipenv update --sequential --dev
    - diff --from-file=Pipfile.old.lock Pipfile.lock

# dependency_scan_build:
#   image: python:3.6
#   before_script:
#     - pip install pipenv==$PIPENV_PACKAGE_VERSION
#   script:
#     - pipenv --python python3 lock --requirements > requirements.txt
#     - sed -i '2d' requirements.txt
#     - pip wheel --wheel-dir ./dist -r requirements.txt
#   artifacts:
#     paths:
#       - ./dist

include:
  template: Dependency-Scanning.gitlab-ci.yml

dependency_scanning:
  # stage: dep_scan
  # dependencies:
  #   - dependency_scan_build
  # variables:
  #   PIP_DEPENDENCY_PATH: ./dist
  before_script:
    - apk add python3
    - python3 -m ensurepip
    - pip3 install pipenv==$PIPENV_PACKAGE_VERSION
    - pipenv --python python3 lock --requirements > requirements.txt
    - sed -i '2d' requirements.txt
    - sed -i '/psycopg2/d' requirements.txt
    - sed -i '/cockroachdb/d' requirements.txt
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'

editorconfig-lint:
  image: node:11
  tags:
    - docker
  cache: {}
  only:
    - branches
    - tags
    - merge_requests
  script:
    - npm ci
    - npm run lint

#
# Unit test jobs.
#

unit-test:
  image: python:3.6
  services:
    - name: cockroachdb/cockroach:v19.1.3
      alias: cockroach
      command: ["start", "--insecure"]
    - name: rabbitmq:3.7
      alias: rabbitmq
  tags:
    - docker
  only:
    - branches
    - tags
    - merge_requests
  before_script:
    - pip install pipenv==$PIPENV_PACKAGE_VERSION
    - pip install codecov
    - pipenv sync --dev
    - mkdir -p /etc/hotpotato/conf.d
    - rm -r hotpotato/static/*
  script:
    - pipenv run pytest --junitxml=report.xml --cov=hotpotato --cov-report=term --cov-branch
  after_script:
    - codecov
  coverage: '/TOTAL.*\s+(\d+%)$/'
  artifacts:
    reports:
      junit: report.xml

migration-test:
  image: python:3.6
  services:
    - name: cockroachdb/cockroach:v19.1.3
      alias: cockroach
      command: ["start", "--insecure"]
    - name: rabbitmq:3.7
      alias: rabbitmq
  variables:
    FLASK_APP: "hotpotato.app.main:app"
    FLASK_DEBUG: "true"
  tags:
    - docker
  only:
    refs:
      - merge_requests
    changes:
      - hotpotato/migrations/**/*
  before_script:
    # Skip the job if nothing in the migration folder has changed
    - pip install pipenv==$PIPENV_PACKAGE_VERSION
    - pipenv sync --dev
  script:
    - git checkout $CI_MERGE_REQUEST_TARGET_BRANCH_SHA
    - pipenv sync --dev
    - pipenv run hotpotato db create
    - pipenv run hotpotato test create-data
    - git checkout $CI_COMMIT_SHA
    - pipenv sync --dev
    - pipenv run hotpotato db upgrade --directory "hotpotato/migrations"
    - pipenv run hotpotato db downgrade --directory "hotpotato/migrations" efb0c42bf4f6

migration-down-test:
  image: python:3.6
  services:
    - name: cockroachdb/cockroach:v19.1.3
      alias: cockroach
      command: ["start", "--insecure"]
    - name: rabbitmq:3.7
      alias: rabbitmq
  variables:
    FLASK_APP: "hotpotato.app.main:app"
    FLASK_DEBUG: "true"
  tags:
    - docker
  only:
    refs:
      - merge_requests
    changes:
      - hotpotato/migrations/**/*
  before_script:
    # Skip the job if nothing in the migration folder has changed
    - pip install pipenv==$PIPENV_PACKAGE_VERSION
    - pipenv sync --dev
  script:
    - pipenv run hotpotato db create
    - pipenv run hotpotato test create-data
    - pipenv run hotpotato db downgrade --directory "hotpotato/migrations" efb0c42bf4f6

# Database reset test. This tests whether Hot Potato can handle the situation where
# the connection to the database is reset, while Hot Potato is serving requests.
db-reset-test:
  image: docker:latest
  services:
    - docker:dind
  cache:
    policy: pull
  tags:
    - docker
  only:
    - branches
    - tags
    - merge_requests
  artifacts:
    paths:
      - log
    expire_in: 1 week
    when: always
  variables:
    COCKROACH_WEBUI_URL: "http://docker:38080"
    HOTPOTATO_URL: "http://docker:38000"
    EMAIL: "test1@example.com"
    PASSWORD: "test_password1"
  script:
    - apk add python3 py-pip curl build-base python3-dev libffi-dev openssl-dev
    - pip install docker-compose
    - pip3 install beautifulsoup4 requests

    - docker-compose -f docker-compose.yml -f docker-compose.test.yml up -d cockroach-test
    - for i in `seq 1 60`; do if curl --output /dev/null --silent --head --fail "${COCKROACH_WEBUI_URL}"; then break; fi; sleep 1; done; if ! curl --output /dev/null --silent --head --fail "${COCKROACH_WEBUI_URL}"; then echo "CockroachDB did not start within an acceptable time frame (1 minute)" > /dev/stderr; false; fi

    - docker-compose -f docker-compose.yml -f docker-compose.test.yml up -d hotpotato-test
    - for i in `seq 1 60`; do if curl --output /dev/null --silent --head --fail "${HOTPOTATO_URL}"; then break; fi; sleep 1; done; if ! curl --output /dev/null --silent --head --fail "${HOTPOTATO_URL}"; then echo "Hot Potato did not start within an acceptable time frame (1 minute)" > /dev/stderr; false; fi

    - docker-compose -f docker-compose.yml -f docker-compose.test.yml run --entrypoint /venv/bin/hotpotato hotpotato-test db create-dev
    - docker/db-reset-test/hotpotato-login "${HOTPOTATO_URL}" "${EMAIL}" "${PASSWORD}" /tmp/hotpotato-cookies.txt

    - docker-compose -f docker-compose.yml -f docker-compose.test.yml restart cockroach-test
    - for i in `seq 1 60`; do if curl --output /dev/null --silent --head --fail "${COCKROACH_WEBUI_URL}"; then break; fi; sleep 1; done; if ! curl --output /dev/null --silent --head --fail "${COCKROACH_WEBUI_URL}"; then echo "CockroachDB did not start within an acceptable time frame (1 minute)" > /dev/stderr; false; fi

    - docker/db-reset-test/hotpotato-get "${HOTPOTATO_URL}" /tmp/hotpotato-cookies.txt
  after_script:
    - docker-compose -f docker-compose.yml -f docker-compose.test.yml logs hotpotato-test > log/hotpotato-test/hotpotato/app.log
    
end-to-end:
  image: docker/compose:latest
  services:
    - docker:dind
  only:
    - branches
    - tags
    - merge_requests
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  script:
    - docker-compose -f docker-compose.e2e.yml up --abort-on-container-exit --exit-code-from=e2e

#
# Build jobs.
#

build-docker:
  image: docker:latest
  services:
    - docker:dind
  cache: {}
  only:
    - branches
    - tags
    - merge_requests
  script:
    - docker build -t hotpotato:latest -f Dockerfile .

#
# Integration test jobs.
#

robot-test:
  image: docker:latest
  services:
    - docker:dind
  cache:
    policy: pull
  tags:
    - docker
  script:
    - apk add py-pip curl build-base python3-dev libffi-dev openssl-dev
    - pip install docker-compose
    - docker-compose -f docker-compose.yml -f docker-compose.test.yml  run --entrypoint /venv/bin/hotpotato hotpotato-test db create-dev
    - docker-compose -f docker-compose.yml -f docker-compose.test.yml  run robot-test
  only:
    - branches
    - tags
    - merge_requests
  artifacts:
    paths:
      - log
    expire_in: 1 week
    when: always

build-frontend:
  image: node:11.0.0
  cache: {}
  only:
    - branches
    - tags
    - merge_requests
  script:
    - cd theme
    - npm ci
    - npm run build
    - rm -r node_modules
  artifacts:
    paths:
      - theme/dist

build-wheels:
  image: python:3.6
  stage: build
  tags:
    - docker
  only:
    - branches
    - tags
    - merge_requests
  dependencies:
    - build-frontend
  needs:
    - build-frontend
  before_script:
    - pip install pipenv==$PIPENV_PACKAGE_VERSION
    - pipenv sync
  script:
    - pipenv lock -r > /tmp/requirements.txt
    - FLASK_APP="hotpotato.app.main:app" pipenv run flask digest compile
    - pip wheel --wheel-dir=wheels -r /tmp/requirements.txt
  artifacts:
    paths:
      - wheels
