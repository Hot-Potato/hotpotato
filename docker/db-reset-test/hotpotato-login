#!/usr/bin/env python3


"""
Log into Hot Potato and write the session key to a cookies.txt file,
so it can be used in curl commands.
"""


import http
import sys

import bs4
import requests

url = sys.argv[1]
email = sys.argv[2]
password = sys.argv[3]
cookies_filepath = "cookies.txt" if len(sys.argv) < 5 else sys.argv[4]


session = requests.Session()
session.cookies = http.cookiejar.MozillaCookieJar()

get_response = session.get("{}/login".format(url))

csrf_token = bs4.BeautifulSoup(get_response.text, "html.parser").find(
    "input", id="csrf_token"
)["value"]

post_response = session.post(
    "{}/login".format(url),
    data={"next": "/", "csrf_token": csrf_token, "email": email, "password": password},
)


if post_response.status_code == requests.codes.ok:
    soup = bs4.BeautifulSoup(post_response.text, "html.parser")
    if soup.find("div", id="notifications"):
        print(
            "Successfully logged in to Hot Potato, saved session cookie to {}".format(
                cookies_filepath
            )
        )
        session.cookies.save(cookies_filepath, ignore_discard=True)
        sys.exit(0)
    else:
        print(
            "Unable to login to Hot Potato (status code {}):\n{}".format(
                post_response.status_code, post_response.text
            ),
            file=sys.stderr,
        )
        sys.exit(1)
else:
    print(
        "Received unexpected response from Hot Potato (status code {}):\n{}".format(
            post_response.status_code, post_response.text
        ),
        file=sys.stderr,
    )
    sys.exit(1)
