#!/usr/bin/python3

# Copyright (c) 2019 Catalyst.Net Ltd
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import os
from datetime import datetime
from http import HTTPStatus

import requests


def alert(api_url, api_key, args):
    alert_type = os.getenv("TYPE")
    trouble_code = os.getenv("TROUBLECODE", "")
    hostname = os.getenv("HOSTALIAS", "")
    display_name = os.getenv("HOSTDISPLAYNAME", "")
    service_name = os.getenv("SERVICEDISPLAYNAME", "")
    team_id = os.getenv("TEAMID")

    if alert_type == "CUSTOM":
        if os.getenv("SERVICESTATE", None) is not None:
            alert_type = "service"
        else:
            alert_type = "host"

    if alert_type == "service":
        state = os.getenv("SERVICESTATE", "")
        output = os.getenv("SERVICEOUTPUT", "")
    elif alert_type == "host":
        state = os.getenv("HOSTSTATE", "")
        output = os.getenv("HOSTOUTPUT", "")
    else:
        raise Exception("Unkown alert type {}".format(alert_type))

    timestamp = os.getenv("TIMESTAMP")
    timestamp = datetime.fromtimestamp(int(timestamp)).isoformat()

    data = {
        "alert_type": alert_type,
        "trouble_code": trouble_code,
        "hostname": hostname,
        "display_name": display_name,
        "service_name": service_name,
        "state": state,
        "output": output,
        "timestamp": timestamp,
        "team_id": team_id,
    }

    response = requests.post(
        "{}/api/server/v2/alerts/send".format(api_url),
        json=data,
        headers={"Authorization": "apikey {}".format(api_key)},
        # verify=False
    )

    if response.status_code != HTTPStatus.CREATED:
        raise Exception(response.text)


def heartbeat(api_url, api_key, args):
    response = requests.post(
        "{}/api/server/v2/heartbeats/create".format(api_url),
        headers={"Authorization": "apikey {}".format(api_key)},
    )

    if response.status_code != HTTPStatus.CREATED:
        print(response.text)
        return False

    return True


def main():
    api_url = os.getenv("HP_API_URL")
    api_key = os.getenv("HP_API_KEY")

    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest="command")

    parser_a = subparsers.add_parser("alert")
    parser_a.set_defaults(func=alert)

    parser_b = subparsers.add_parser("heartbeat")
    parser_b.set_defaults(func=heartbeat)

    args = parser.parse_args()
    args.func(api_url, api_key, args)


if __name__ == "__main__":
    main()
