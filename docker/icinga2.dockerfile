FROM jordan/icinga2

# Run icinga2 initial setup
# -------------------------------------------------------------------
COPY docker/icinga2/run /tmp/icinga2_run
RUN chmod +x /tmp/icinga2_run
RUN /tmp/icinga2_run

# Setup Hot Potato notification agent
# -------------------------------------------------------------------

RUN apt-get update
RUN apt-get install python3-requests -y

# FIXME: Clone from git once the notification-agent is public
RUN mkdir -p /opt/notification-agent/hp-notif-agent
COPY docker/notification-agent/agent.py /opt/notification-agent/hp-notif-agent/agent.py
RUN chmod +x /opt/notification-agent/hp-notif-agent/agent.py
# WORKDIR /opt
# RUN apt-get update
# RUN apt-get install git -y

# RUN git clone https://gitlab.com/Hot-Potato/notification-agent.git
# RUN git checkout 5eaeb1d0d5aded8ad041219f692e8bf307297894

# Configure icinga2
# -------------------------------------------------------------------
COPY docker/icinga2/conf.d/commands.conf /etc/icinga2/conf.d/commands.conf
COPY docker/icinga2/conf.d/api-users.conf /etc/icinga2/conf.d/api-users.conf
COPY docker/icinga2/conf.d/notifications.conf /etc/icinga2/conf.d/notifications.conf

RUN icinga2 daemon --validate
