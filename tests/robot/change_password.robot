*** Settings ***
Documentation     Validate the Change Password page.
Resource          resources/common.robot
Suite Setup       Open Browser To Login Page
Suite Teardown    Close Browser
Test Teardown     Logout

*** Test Cases ***
Change Password
    Login to test1
    Go To Change Password Page
    Input Password for test1
    Input New Password for test1
    Input Confirm Password for test1
    Submit Password Change
    Logout
    Should be able to login with new password
    Reset Password

Don't Match
    Login to test1
    Go To Change Password Page
    Input Password for test1
    Input New Password for test1
    Input Confirm Password    not the same
    Submit Password Change
    Should show passwords do not match

Invalid Password
    Login to test1
    Go To Change Password Page
    Input Password    invalid
    Input New Password for test1
    Input Confirm Password for test1
    Submit Password Change
    Should show incorrect password

Same Password
    Login to test1
    Go To Change Password Page
    Input Password for test1
    Input New Password    ${PASSWORD test1}
    Input Confirm Password    ${PASSWORD test1}
    Submit Password Change
    Should Show Your new Password Must Be Different

*** Keywords ***
Input Password for test1
    Input Password    ${PASSWORD test1}

Input New Password
    [Arguments]    ${new_password}
    Input Text    new_password    ${new_password}

Input Confirm Password
    [Arguments]    ${confirm_password}
    Input Text    new_password_confirm    ${confirm_password}

Input New Password for test1
    Input New Password    new_password

Input Confirm Password for test1
    Input Confirm Password    new_password

Submit Password Change
    Click Button    Change Password

Should be able to login with new password
    Input Username    ${USERNAME test1}
    Input Text    password    new_password
    Submit Credentials
    Home Page Should Be Open

Reset Password
    Go To Change Password Page
    Input Password    new_password
    Input Text    new_password    ${PASSWORD test1}
    Input Text    new_password_confirm    ${PASSWORD test1}
    Submit Password Change

Should show passwords do not match
    Page Should Contain    Passwords do not match.

Should show incorrect password
    Page Should Contain    Incorrect username or password

Should Show Your new Password Must Be Different
    Page Should Contain    Your new password must be different
