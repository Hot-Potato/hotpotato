*** Settings ***
Documentation     Validate the Account Settings page.
Resource          resources/common.robot
Suite Setup       Open Browser To Login Page
Suite Teardown    Close Browser


*** Test Cases ***
Open Account Settings Page
    Login
    Go To Account Settings Page
    Account Settings Page Should Be Open

Add Contact Method
    Click Add Contact Method
    Select SMS Contact Method
    Add A SMS Number
    Submit Add A SMS Number
    Account Settings Page Should Be Open
    Account Settings Page Should Contain SMS Number

Add Duplicate Contact Method
    Click Add Contact Method
    Select SMS Contact Method
    Add A SMS Number
    Submit Add A SMS Number
    Contact Should Already Exist
    Close Add Account Settings Form

Delete Contact Method
    Go To Account Settings Page
    Account Settings Page Should Be Open
    Select Delete Checkbox
    Submit Delete SMS Number
    Account Settings Page Should Be Open
    Number Should Be Deleted
    Logout

*** Keywords ***

Contact Should Already Exist
    Page Should Contain    Contact method already exists

Close Add Account Settings Form
    Click Button    Close
