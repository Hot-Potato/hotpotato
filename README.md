![hot potato logo](theme/brand/logo/hot-potato-logo.svg)
[![pipeline status](https://gitlab.com/Hot-Potato/hotpotato/badges/master/pipeline.svg)](https://gitlab.com/Hot-Potato/hotpotato/commits/master)

Hot Potato is a message broker for monitoring systems. It sits in between monitoring systems and messaging providers to ensure consistent relaying of messages to on-call staff.

For more info check out our docs at https://docs.hotpotato.nz/

# Dev Setup

## Installing

### Prerequisites

- Python 3.6 or greater
- [pipenv](https://github.com/pypa/pipenv)
- [docker-compose](https://docs.docker.com/compose/)

### Running

To set up a development environment you first need to install the development dependencies

```bash
$ pipenv install --dev
```

Then setup the database

```bash
$ pipenv run cli db create-dev
```

Finally you can start Hot Potato with

```bash
$ docker-compose up
```

You can now access Hot Potato at http://localhost:8000

For more development docs check out our dev guide at https://docs.hotpotato.nz/developing.html

# Contributing

Check out out contribution guide at https://docs.hotpotato.nz/contributing.html

# License

This project is licenced under the GPLv3 check out [LICENSE](LICENSE) for more details
