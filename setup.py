#!/usr/bin/env python3
"""The setup script."""

import setuptools

import hotpotato

requirements = [
    # Base Application
    "click~=7.1.2",
    "click-datetime~=0.2",
    "sqlalchemy-cockroachdb~=1.3.1",
    "psycopg2~=2.8.5",
    "dramatiq[rabbitmq, watch]~=1.9.0",
    "flask-migrate~=2.5.3",
    "flask-security-too~=3.2.0",
    "flask-sqlalchemy~=2.4.4",
    "flask~=1.1.2",
    "Flask-BabelEx==0.9.4",
    "Flask-WTF~=0.14.2",
    "python-dateutil~=2.8.1",
    "pytz",
    "requests~=2.24.0",
    "sqlalchemy~=1.3.18",
    "sqlalchemy-utils~=0.36.8",
    "webargs~=6.1.0",
    "marshmallow~=3.7.1",
    "factory_boy~=3.0.1",
    "werkzeug~=1.0.1",
    "workalendar~=10.3.0",
    "Flask-Static-Digest==0.1.3",
    "whitenoise==5.2.0",
    "email-validator~=1.1.1",
    # Twilio
    "twilio~=6.45.0",
    # Security
    "bcrypt~=3.2.0",
    "flask-talisman~=0.7.0",
    # Testing
    "faker~=4.1.1",
]


packages = setuptools.find_packages(where="./", include=["hotpotato", "hotpotato.*"])
if not packages:
    raise ValueError("No packages detected.")

setuptools.setup(
    name="hotpotato",
    version=hotpotato.__version__,
    description="",
    long_description="",
    entry_points={"console_scripts": ["hotpotato=hotpotato.cli.main:cli"]},
    author="Hot Potato Team",
    author_email="help@hotpotato.nz",
    url="https://hotpotato.nz",
    packages=packages,
    include_package_data=True,
    install_requires=requirements,
    zip_safe=False,
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
)
