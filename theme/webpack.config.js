const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  plugins: [],
  output: {
    filename: 'build.js'
  },
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()]
  },
  externals: {
    moment: 'moment'
  },
  module: {
    rules: [
      {
        test: '/.m?js$/',
        exclude: '/(node_modules)/',
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  }
};
