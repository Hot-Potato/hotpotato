import Selectize from 'selectize';
import $ from 'jquery';

Selectize.define('no_results', function (optionsValue) {
  const self = this;

  const options = {
    message: 'No results found.',
    html(data) {
      return (
        `<div class="selectize-dropdown ${data.classNames} dropdown-empty-message">`
        + `<div class="selectize-dropdown-content" style="padding: 3px 12px">${data.message}</div>`
        + `</div>`
      );
    },
    ...optionsValue
  };

  self.displayEmptyResultsMessage = function () {
    this.$empty_results_container.css('top', this.$control.outerHeight());
    this.$empty_results_container.show();
  };

  self.onKeyDown = (() => {
    const original = self.onKeyDown;

    return function (...args) {
      original.apply(self, args);
      this.$empty_results_container.hide();
    };
  })();

  self.onBlur = (() => {
    const original = self.onBlur;

    return function (...args) {
      original.apply(self, args);
      this.$empty_results_container.hide();
    };
  })();

  self.setup = (() => {
    const original = self.setup;
    return (...args) => {
      original.apply(self, args);
      self.$empty_results_container = $(
        options.html(
          {
            classNames: self.$input.attr('class'),
            ...options
          }
        )
      );
      self.$empty_results_container.insertBefore(self.$dropdown);
      self.$empty_results_container.hide();
    };
  })();
});
