import { addDays, subDays } from 'date-fns';

function weekRangePlugin() {
  const startInput = document.getElementById('hp-report-start-input');
  const endInput = document.getElementById('hp-report-end-input');

  return (fp) => {
    function onDayHover(event) {
      const day = event.target;
      if (!day.classList.contains('flatpickr-day')) return;

      const days = fp.days.childNodes;
      const { dateObj } = day;

      const backDays = (((dateObj.getDay() - 4) % 7) + 7) % 7;
      const forwardDays = ((((4 - dateObj.getDay()) % 7) + 7) % 7) + 7 * (dateObj.getDay() === 4);

      const weekStartDay = subDays(dateObj, backDays);
      const weekEndDay = addDays(dateObj, forwardDays);

      for (let i = days.length - 1; i >= 0; i -= 1) {
        const dayElm = days[i];
        const date = dayElm.dateObj;
        if (date > weekEndDay || date < weekStartDay) dayElm.classList.remove('inRange');
        else dayElm.classList.add('inRange');
      }
    }

    function highlightWeek() {
      const selDate = fp.latestSelectedDateObj;
      if (
        selDate !== undefined
        && selDate.getMonth() === fp.currentMonth
        && selDate.getFullYear() === fp.currentYear
      ) {
        const { dateObj } = fp.selectedDateElem;
        const backDays = (((dateObj.getDay() - 4) % 7) + 7) % 7;
        const forwardDays = ((((4 - dateObj.getDay()) % 7) + 7) % 7) + 7 * (dateObj.getDay() === 4);

        fp.weekStartDay = subDays(dateObj, backDays);
        fp.weekEndDay = addDays(dateObj, forwardDays);
      }
      const days = fp.days.childNodes;
      for (let i = days.length - 1; i >= 0; i -= 1) {
        const date = days[i].dateObj;
        if (date >= fp.weekStartDay && date <= fp.weekEndDay) {
          days[i].classList.add('week', 'selected');
        }
      }
    }

    function clearHover() {
      const days = fp.days.childNodes;
      for (let i = days.length - 1; i >= 0; i -= 1) days[i].classList.remove('inRange');
    }

    function onReady() {
      if (fp.daysContainer !== undefined) {
        fp.daysContainer.addEventListener('mouseover', onDayHover);
      }
    }

    function onDestroy() {
      if (fp.daysContainer !== undefined) {
        fp.daysContainer.removeEventListener('mouseover', onDayHover);
      }
    }

    function onUpdate() {
      // eslint-disable-next-line no-underscore-dangle
      fp._input.value = `${fp.formatDate(fp.weekStartDay, fp.config.dateFormat)} to ${fp.formatDate(
        fp.weekEndDay,
        fp.config.dateFormat
      )}`;
      startInput.value = fp.formatDate(fp.weekStartDay, fp.config.dateFormat);
      endInput.value = fp.formatDate(fp.weekEndDay, fp.config.dateFormat);
    }

    return {
      onValueUpdate: [highlightWeek, onUpdate],
      onMonthChange: highlightWeek,
      onYearChange: highlightWeek,
      onOpen: highlightWeek,
      onClose: clearHover,
      onParseConfig() {
        fp.config.mode = 'single';
        fp.config.enableTime = false;
        fp.config.dateFormat = fp.config.dateFormat ? fp.config.dateFormat : '\\W\\e\\e\\k #W, Y';
        fp.config.altFormat = fp.config.altFormat ? fp.config.altFormat : '\\W\\e\\e\\k #W, Y';
      },
      onReady: [
        onReady,
        highlightWeek,
        () => {
          fp.loadedPlugins.push('weekRange');
        },
        onUpdate
      ],
      onDestroy
    };
  };
}

export default weekRangePlugin;
