import $ from 'jquery';

function notifHasTag(notif, tagId) {
  const tags = notif.querySelector('.label-group a');
  return tags.some((node) => {
    const badge = node.querySelector('.badge');
    if (badge.dataset.id === tagId) {
      return true;
    }
    return false;
  });
}

function addTagToTagGroup(tags, tagName, tagId) {
  const link = document.createElement('a');
  const url = new URL(document.location);
  url.searchParams.append('tag', tagName);
  link.setAttribute('href', url);
  link.dataset.id = tagId;
  link.classList.add('badge');
  link.classList.add('badge-secondary');
  link.classList.add('mr-1');

  const text = document.createTextNode(tagName);
  link.appendChild(text);
  tags.appendChild(link);
}

function addTickToDropdown(dropdown, tagId) {
  const link = dropdown.querySelector(`a[data-id='${tagId}']`);

  const tick = document.createElement('img');
  tick.classList.add('icon');
  tick.setAttribute('src', '/static/img/icons/icon-thumb-up.svg');

  link.appendChild(tick);
  link.dataset.added = true;
}

function addTagToNotif(notif, tagName, tagId) {
  const tags = notif.querySelector('.label-group');
  addTagToTagGroup(tags, tagName, tagId);

  const dropdown = notif.querySelector('.tag-dropdown');
  addTickToDropdown(dropdown, tagId);
}

function postTag(url, tagId) {
  const { csrftoken } = document.body.dataset;
  return fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json', 'X-CSRFToken': csrftoken },
    body: JSON.stringify({ id: tagId })
  }).then((response) => {
    if (!response.ok) {
      throw new Error(response.statusText);
    }
  });
}

function postAddTag(notifId, tagId, teamId) {
  return postTag(`/api/web/v1/notifications/alerts/${notifId}/add_tag?team_id=${teamId}`, tagId);
}

function removeTickFromDropdown(dropdown, tagId) {
  const link = dropdown.querySelector(`a[data-id='${tagId}']`);
  const tick = link.querySelector('.icon');
  tick.remove();

  link.dataset.added = false;
}

function removeTagFromTagGroup(tags, tagId) {
  tags.forEach((badge) => {
    if (badge.dataset.id === tagId) {
      badge.remove();
    }
  });
}

function removeTagFromNotif(notif, tagId) {
  const tags = notif.querySelectorAll('.label-group a');
  removeTagFromTagGroup(tags, tagId);

  const dropdown = notif.querySelector('.tag-dropdown');
  removeTickFromDropdown(dropdown, tagId);
}

function addTag(notif, tagName, tagId) {
  const notifId = notif.dataset.id;
  const { teamId } = notif.dataset;
  addTagToNotif(notif, tagName, tagId);
  postAddTag(notifId, tagId, teamId).catch(() => {
    removeTagFromNotif(notif, tagId);
  });
}

function postRemoveTag(notifId, tagId, teamId) {
  return postTag(`/api/web/v1/notifications/alerts/${notifId}/remove_tag?team_id=${teamId}`, tagId);
}

function removeTag(notif, tagName, tagId) {
  const notifId = notif.dataset.id;
  const { teamId } = notif.dataset;
  removeTagFromNotif(notif, tagId);
  postRemoveTag(notifId, tagId, teamId).catch(() => {
    addTagToNotif(notif, tagName, tagId);
  });
}

document.addEventListener('DOMContentLoaded', () => {
  $('.add-tag').click((e) => {
    e.preventDefault();
    const tagId = e.target.dataset.id;
    const tagName = e.target.innerText;
    const notif = $(e.target)
      .parents('.card-body')
      .get(0);

    if (e.target.dataset.added === 'true') {
      removeTag(notif, tagName, tagId);
    } else {
      addTag(notif, tagName, tagId);
    }
  });

  $('.add-bulk-tags').click((e) => {
    e.preventDefault();
    const tagId = e.target.dataset.id;
    const tagName = e.target.innerText;

    const checkboxes = $('.checkbox-notification:checkbox:checked');
    checkboxes.each((id, checkbox) => {
      const row = $(checkbox)
        .parents('tr')
        .get(0);
      const notif = row.querySelector('.notif-content');
      if (!notifHasTag(notif, tagId)) {
        addTag(notif, tagName, tagId);
      }
    });
  });
});
