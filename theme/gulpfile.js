const gulp = require('gulp');
const gulpcache = require('gulp-cache');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('autoprefixer');
const flexfixes = require('postcss-flexbugs-fixes');
const cssnano = require('cssnano');
const webpack = require('webpack-stream');
const webpackConfig = require('./webpack.config');

const PATHS = {
  src: {
    root: './src/**',
    js: './src/js/**/*.js',
    jsEntry: './src/js/hotpotato.js',
    scss: './src/scss/**/*.scss'
  },
  dist: {
    root: './dist/',
    js: './dist/js/',
    css: './dist/css/'
  }
};

gulp.task('copy-files', () => gulp
  .src([PATHS.src.root, `!${PATHS.src.scss}`, `!${PATHS.src.js}`])
  .pipe(gulp.dest(PATHS.dist.root)));

gulp.task('scss', () => gulp
  .src(PATHS.src.scss)
  .pipe(sourcemaps.init())
  .pipe(
    sass({
      includePaths: ['./node_modules/']
    }).on('error', sass.logError)
  )
  .pipe(
    postcss([
      // building, run minification
      autoprefixer({
        cascade: false,
        remove: false
      }),
      flexfixes(),
      cssnano()
    ])
  )
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest(PATHS.dist.css)));

gulp.task('js', () => gulp
  .src(PATHS.src.jsEntry)
  .pipe(webpack(webpackConfig))
  .pipe(gulp.dest(PATHS.dist.js)));

gulp.task('cache-clear', (done) => {
  gulpcache.clearAll();
  done();
});

gulp.task('build', gulp.parallel('copy-files', 'scss', 'js'));

gulp.task('watch', () => {
  gulp.watch(PATHS.src.root, gulp.series('cache-clear', 'build'));
});

gulp.task('default', gulp.series('copy-files', 'scss', 'js', 'watch'));
